'use strict';

angular.module('TmassAdmin', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ui.select',
  'ngTouch',
  'toastr',
  'smart-table',
  'btorfs.multiselect',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',
  'TmassAdmin.theme',
  'TmassAdmin.pages',
  'jqwidgets',
  'isteven-multi-select',
  '720kb.datepicker',
  //'angular-loading-bar',
  'angucomplete-alt',
  'moment-picker'
])

.config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    autoDismiss: true,
    containerId: 'toast-container',
    maxOpened: 0,    
    newestOnTop: true,
    positionClass: 'top-right-class',
    //positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: true,
    target: 'body',
    closeDuration : 1000
  });
})

//development and ip and ports
.constant('config', {
    //apiUrl:   'http://localhost:3005'         //local server
    //apiUrl: 'http://139.59.91.150:3000'     //test server
    apiUrl: 'http://testing.tmaas.in:3000'   //test server with dns
    //apiUrl: 'http://staging.tmaas.in:3000'   //staging server
})

//directive for sorting the data in dropdown
.filter("toArray", function(){
    return function(obj) {
    var result = [];
    angular.forEach(obj, function(val, key) {
      result.push(val);
    });
    return result;};
})
//directive and service
.directive('fileModel', ['$parse', function ($parse) {
  return {
     restrict: 'A',
     link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;
        element.bind('change', function(){
           scope.$apply(function(){
              modelSetter(scope, element[0].files[0]);
           });
        });
     }
  };
}])

.service('fileUpload', ['$http', function ($http) {
  this.uploadFileToUrl = function(file, uploadUrl){
     var fd = new FormData();
     fd.append('file', file);
     $http.post(uploadUrl, fd, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
     })
     .success(function(){
     })
     .error(function(){
     });
  }
}])

.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
})

.directive('multiSelect', function() {
 
  function link(scope, element) {
    var options = {
      enableClickableOptGroups: true,
      enableFiltering: true,
      buttonWidth: '100%',
      onChange: function() {
        //element.change();
      }
    };
    element.multiselect(options);
  }
 
  return {
    restrict: 'A',
    link: link
  };
})

.service('animation', [ function () {
  this.myFunc = function (element) {
    console.log(element);
    return $('#'+element).waitMe({
        effect: 'bounce',
        text: '',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        maxSize: '',
        waitTime: -1,
        source: 'img.svg',
        textPos: 'vertical',
        fontSize: '',
        onClose: function (el) {
        }
    });
  }
}])

.config(['$provide', function($provide) {
  $provide.decorator('selectDirective', ['$delegate', function($delegate) {
    var directive = $delegate[0];
 
    directive.compile = function() {
 
      function post(scope, element, attrs, ctrls) {
        directive.link.post.apply(this, arguments);
 
        var ngModelController = ctrls[1];
        if (ngModelController && attrs.multiSelect !== null && angular.isDefined(attrs.multiSelect)) {
          originalRender = ngModelController.$render;
          ngModelController.$render = function() {
            originalRender();
            element.multiselect('refresh');
          };
        }
      }
 
      return {
        pre: directive.link.pre,
        post: post
      };
    };
 
    return $delegate;
  }]);
}])
//directive and services
//http interceptor for checking the status of the state http
.service('authInterceptor', function ($q, $window,localStorage) {
  var service = this;
  console.log("inside inteceptor");
  service.responseError = function (response) {
    console.log('response in interceptor');
    console.log(response);
    console.log("localStorage");
    console.log(localStorage);
    if ( response.status==-1 && navigator.onLine==false) {
      console.log("inside response sccuess null if status error occurs")
      alertify.alert("Unable to connect to Server, Please check your Internet Connection and reload the page");
      return;
      //$window.location.reload();
    }
    else
    if ((response.data.errorCode!=undefined || response.data.errorCode!=null) && response.data.errorCode=="405" && response.statusText=="Forbidden" ) {
        console.log("inside inteceptor if status error occurs")
        console.log('response');
        console.log(response);        
        localStorage.clear();
        //$window.location.reload();
        $window.location.href = "/authSignIn";
        alertify.alert("Please Login again");
    }
    //else 
    //if (response.status == 401 || response.status == 403 || response.success==null) {
    if (response.status == 401 || response.status == 403 || response.success==null) {
        console.log("inside inteceptor if status error occurs")
        console.log('response');
        console.log(response);
        // alertify.alert("Unable to connect to Server, Please check your Internet Connection and reload the page");
        // localStorage.clear();
        // $window.location.href = "/authSignIn";
    }
    return $q.reject(response);
  };
});