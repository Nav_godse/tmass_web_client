/**
 * @author Nav
 * created on 17.12.2015
 */
(function () {
  'use strict';

  angular.module('TmassAdmin.theme')
      .filter('profilePicture', profilePicture);

  /** @ngInject */
  function profilePicture(layoutPaths) {
    return function(input, ext) {
      ext = ext || 'svg';
      return layoutPaths.images.profile + input + '.' + ext;
    };
  }

})();
