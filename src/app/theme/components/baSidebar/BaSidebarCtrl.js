(function () {
  'use strict';

  angular.module('TmassAdmin.theme.components')
    .controller('BaSidebarCtrl', BaSidebarCtrl);

  /** @ngInject */
  function BaSidebarCtrl($scope, baSidebarService, localStorage) {

    var userData = localStorage.getObject('dataUser');
    console.log("userData", userData);
    // ----------------to  reset role and permission uncomment this-----
    //$scope.menuItems = baSidebarService.getMenuItems();
    //--------------------------------------------------------------
    // -----  to set role and permission -------------------------------
    $scope.menuItems = baSidebarService.getAuthorizedMenuItems(userData.otherData);
    //-------------------------------------------------------------------------
    
    console.log($scope.menuItems);
    $scope.defaultSidebarState = $scope.menuItems.stateRef;

    $scope.hoverItem = function ($event) {
      $scope.showHoverElem = true;
      $scope.hoverElemHeight =  $event.currentTarget.clientHeight;
      var menuTopValue = 66;
      $scope.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - menuTopValue;
    };

    $scope.$on('$stateChangeSuccess', function () {
      if (baSidebarService.canSidebarBeHidden()) {
        baSidebarService.setMenuCollapsed(true);
      }
    });
  }
})();