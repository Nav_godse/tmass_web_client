/**
 * @author Nav_god
 * created on 16.3.2018
 */
(function () {
  'use strict';

  angular.module('TmassAdmin.theme.components')
      .directive('pageTop', pageTop);

  /** @ngInject */
  function pageTop() {
    return {
      restrict: 'E',
      templateUrl: 'app/theme/components/pageTop/pageTop.html',
      controller: 'PageTopCtrl'
    };
  }

})();