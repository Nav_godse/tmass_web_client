(function () {
    'use strict';
  
    angular.module('TmassAdmin.theme.components')
        .controller('PageTopCtrl', PageTopCtrl);
  
    /** @ngInject */
    function PageTopCtrl($scope, $sce, localStorage) {
        $scope.logout = function(){
            localStorage.clear();
            window.location.reload();
            $window.location.href = "/authSignIn";
        }
      $scope.users = {
        0: {
          name: 'Vlad',
        }
      };
  
      $scope.notifications = [
        {
          userId: 0,
          template: '&name posted a new article.',
          time: '1 min ago'
        }
      ];
  
      $scope.messages = [
        {
          userId: 3,
          text: 'After you get up and running, you can place Font Awesome icons just about...',
          time: '1 min ago'
        }
      ];
  
      $scope.getMessage = function(msg) {
        var text = msg.template;
        if (msg.userId || msg.userId === 0) {
          text = text.replace('&name', '<strong>' + $scope.users[msg.userId].name + '</strong>');
        }
        return $sce.trustAsHtml(text);
      };
    }
  })();