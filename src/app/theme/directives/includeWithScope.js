/**
 * @author Nav
 * created on 16.3.2018
 */
(function () {
  'use strict';

  angular.module('TmassAdmin.theme')
      .directive('includeWithScope', includeWithScope);

  /** @ngInject */
  function includeWithScope() {
    return {
      restrict: 'AE',
      templateUrl: function(ele, attrs) {
        return attrs.includeWithScope;
      }
    };
  }

})();
