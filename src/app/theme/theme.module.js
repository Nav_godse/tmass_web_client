/**
 * @author Nav
 * created on 15.12.2015
 */
(function () {
  'use strict';

  angular.module('TmassAdmin.theme', [
      'toastr',
      'chart.js',
      'angular-chartist',
      'angular.morris-chart',
      'textAngular',
      'TmassAdmin.theme.components',
      'TmassAdmin.theme.inputs'
  ]);

})();
