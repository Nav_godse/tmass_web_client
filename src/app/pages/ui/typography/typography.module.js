/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.typography', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.typography', {
        url: '/typography',
        templateUrl: 'app/pages/ui/typography/typography.html',
        title: 'Typography',
        sidebarMeta: {
          order: 0,
        },
        authenticate: true
      });
  }

})();