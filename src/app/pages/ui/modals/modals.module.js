/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.modals', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.modals', {
        url: '/modals',
        templateUrl: 'app/pages/ui/modals/modals.html',
        controller: 'ModalsPageCtrl',
        title: 'Modals',
        sidebarMeta: {
          order: 300,
        },
        authenticate: true
      });
  }

})();