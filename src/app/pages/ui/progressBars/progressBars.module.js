/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.progressBars', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.progressBars', {
        url: '/progressBars',
        templateUrl: 'app/pages/ui/progressBars/progressBars.html',
        title: 'Progress Bars',
        sidebarMeta: {
          order: 600,
        },
        authenticate: true
      });
  }

})();