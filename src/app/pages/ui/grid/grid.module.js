/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.grid', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.grid', {
        url: '/grid',
        templateUrl: 'app/pages/ui/grid/grid.html',
        title: 'Grid',
        sidebarMeta: {
          order: 400,
        },
        authenticate: true
      });
  }

})();