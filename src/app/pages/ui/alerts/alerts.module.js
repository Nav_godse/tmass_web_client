/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.alerts', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.alerts', {
        url: '/alerts',
        templateUrl: 'app/pages/ui/alerts/alerts.html',
        title: 'Alerts',
        sidebarMeta: {
          order: 500,
        },
        authenticate: true
      });
  }

})();