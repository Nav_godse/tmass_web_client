/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.notifications', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.notifications', {
        url: '/notifications',
        templateUrl: 'app/pages/ui/notifications/notifications.html',
        controller: 'NotificationsPageCtrl',
        title: 'Notifications',
        sidebarMeta: {
          order: 700,
        },
        authenticate: true
      });
  }

})();