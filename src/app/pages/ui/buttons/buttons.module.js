/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.buttons', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.buttons', {
        url: '/buttons',
        templateUrl: 'app/pages/ui/buttons/buttons.html',
        controller: 'ButtonPageCtrl',
        title: 'Buttons',
        sidebarMeta: {
          order: 100,
        },
        authenticate: true
      });
  }

})();