/**
 * @author k.danovsky
 * created on 12.01.2016
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui', [
      'TmassAdmin.pages.ui.typography',
      'TmassAdmin.pages.ui.buttons',
      'TmassAdmin.pages.ui.icons',
      'TmassAdmin.pages.ui.modals',
      'TmassAdmin.pages.ui.grid',
      'TmassAdmin.pages.ui.alerts',
      'TmassAdmin.pages.ui.progressBars',
      'TmassAdmin.pages.ui.notifications',
      'TmassAdmin.pages.ui.tabs',
      'TmassAdmin.pages.ui.slider',
      'TmassAdmin.pages.ui.panels',
    ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui', {
        url: '/ui',
        template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'UI Features',
        sidebarMeta: {
          icon: 'ion-android-laptop',
          order: 200,
        },
        authenticate: true
      });
  }

})();