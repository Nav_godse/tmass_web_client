/**
 * @author Nav
 * created on 23.12.2015
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.ui.panels', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui.panels', {
        url: '/panels',
        templateUrl: 'app/pages/ui/panels/panels.html',
        controller: 'NotificationsPageCtrl',
        title: 'Panels',
        sidebarMeta: {
          order: 1100,
        },
        authenticate: true
      });
  }

})();