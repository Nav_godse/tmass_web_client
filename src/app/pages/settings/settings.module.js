(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.settings', ['ui.select', 'ngSanitize'])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider,$httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
      $stateProvider
        .state('main.settings', {
          url: '/settings',
          templateUrl: 'app/pages/settings/settingDetails/settingDetails.html',
          controller: "SettingsCtrl",
          abstract: false,
          title: 'Settings',
          sidebarMeta: {
            icon: 'icoSettings',
            order: 5,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager']   // <-- roles allowed for this module
          }
        });
    }
  })();