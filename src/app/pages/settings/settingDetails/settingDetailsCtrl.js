(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('SettingsCtrl', SettingsCtrl);
  
    /** @ngInject */
    function SettingsCtrl($scope,customerService,$uibModal, allDashboardService, localStorage, toastr) {
        var vm = this; 
        vm.currloggedinUser = localStorage.getObject('dataUser');
        vm.currUserEmail = vm.currloggedinUser.otherData.email;
        //alert(vm.currUserEmail);
        //vm.gethomePageSettings = gethomePageSettings;
        //vm.addhomePageSettings = addhomePageSettings;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {};
        initController();
        $scope.tubes = [];
        var data = [];

        function initController() {
            gethomePageSettings();
            gettextvalues();
            getAllMyUsersAndCustomers();
            //addhomePageSettings();
            //updatehomePageSettings();
            //deletehomePageSettings();
        }
        //allmyusersandcustomer
        function getAllMyUsersAndCustomers () {
            allDashboardService.allmyusersandcustomer(function(result){
                $scope.userEmails=[];
                var userEmailsObj={};
                //alert("inside here" , JSON.stringify(result));
                console.log('result hererherher');
                console.log(result);
                $scope.myUsers=[];
                if (result != null && result.success === true ) { 
                    console.log('result.datamyusers');
                    console.log(result.data);
                    $scope.myUsers=result.data;
                    $scope.myUsers.push({email: vm.currloggedinUser.otherData.email, _id: vm.currloggedinUser._id})
                    console.log('result.datamyusers',$scope.myUsers);
                    //$scope.myUsers=[{"_id":"0","email":"No Emails Found"}];
                    //toastr.success(result.message);
                } else {
                    $scope.myUsers=[];
                    //$scope.myUsers=[{"_id":"0","email":"No Emails Found"}];
                    $scope.vehiclechartdata= 0;
                    if(result == null){
                        toastr.error("No Data Found");
                        //$scope.myUsers=[{"_id":"0","email":"No Emails Found"}];
                    }else{
                        toastr.error(result.message);
                    }
                    //vm.error = result.message;                    
                }
            });
        }
        function gettextvalues(){
            $scope.alerts = [
                "Tire Missing", 
                "Tire Removal", 
                "Tag Tampering", 
                "Tire Exchange", 
                "Tire Rotation",
                "High Pressure Count",
                "Low Pressure Count",
                "High Temperature Count",
                "Improper Tag Fitment",
                "Tread Depth Based Maintenance",
                "Invalid Tag"
            ];
            $scope.freqarr = [
                {
                    freqVal:1,
                    freqText:"Daily"
                },{
                    freqVal:6,
                    freqText:"Weekly"
                },{
                    freqVal:30,
                    freqText:"Monthly"
                }
            ];

            $scope.selectDashboard = [
                { id: "1", name: "Tire Dashboard"	},
                { id: "2", name: "Vehicle Dashboard"	},
                { id: "3", name: "Alert Dashboard"	},
                // { id: "4", name: "Avg Cost" },
                // { id: "5", name: "Avg Distance Travelled" },
                // { id: "6", name: "Avg Mantaianance Cost" }
            ];

            $scope.selectReports = [
                { id: "1", name: "Tire"	},
                { id: "2", name: "Vehicle"	},
                { id: "3", name: "Alert"	}
            ];
            $scope.minDate = moment();
        }
        function addhomePageSettings(myObj1) {            
            customerService.addhomePageSettings(myObj1, function (result) {
                if (result.success === true) { 
                    gethomePageSettings();
                    vm.ModalInstance2.close();
                    $scope.vccUser={};
                    toastr.success(result.message);
                    //$("#jqxwindowTubeType").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
 

        function gethomePageSettings() {       
            data=[];     
            customerService.gethomePageSettings(function (result) {
                if (result.success === true) { 
                    console.log(result , "result data");

                    for(var i=0;i<result.data.length;i++){
                        var showhomepageStrg='';
                        var newHomepageArr=[];
                        var newFreqArr=[];
                        var str_array='';
                        console.log(result.data[i]);
                        $scope.tubes.push(result.data[i]);    
                        showhomepageStrg = result.data[i].showOnHomePage;
                        str_array = showhomepageStrg.split(',');
                        for(var j in str_array){
                            if(str_array[j]==1)
                                newHomepageArr.push("Tire") 
                            if(str_array[j]==2)
                                newHomepageArr.push("Vehicle") 
                            if(str_array[j]==3)
                                newHomepageArr.push("Alert") 
                        }
                        if(result.data[i].freq==1)
                            newFreqArr.push("Daily") 
                        if(result.data[i].freq==6)
                            newFreqArr.push("Weekly") 
                        if(result.data[i].freq==30)
                            newFreqArr.push("Monthly") 
                        var settingObj = {
                            _id:result.data[i]._id,
                            dashboardName: newHomepageArr.toString(),
                            emails: result.data[i],
                            freq: newFreqArr.toString(),
                            //showOnHomePage: "1"
                        }

                        data.push(settingObj);

                    } 
                    
                } else {
                    data=[];
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
                myGrid();
            });
        };

        function deletehomePageSettings(_id) {            
            customerService.deletehomePageSettings(_id,function (result) {
                if (result.success === true) {  
                    $scope.tubes=[];
                    data=[];
                    gethomePageSettings();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function updatehomePageSettings() {            
            customerService.updatehomePageSettings(vm._id,vm.tubeTypeName,vm.tubeTypeDesc,function (result) {
                if (result.success === true) {
                    vm.tubeTypeName="";
                    vm.tubeTypeDesc=""; 
                    $scope.tubes=[];
                    data=[];
                    gethomePageSettings();
                    toastr.success(result.message);
                    //$("#jqxwindowTubeType").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            // Validation 
            $scope.isValidate= false;
            console.log(rowform, "Inside rowformdata");
            console.log($('#timeofDelivery').val())
            var Obj={};
            var vehicleData=[];
            if(rowform==undefined){
                alertify.alert("Please fill all feilds");
                return
            }else if(rowform.dashboardName==undefined){
                alertify.alert("Please select proper Report Type");
                return
            }else if(rowform.freq==undefined){
                alertify.alert("Please select proper Frequency of email");
                return
            }else if(rowform.showOnHomePage==undefined){
                alertify.alert("Please select proper showOnHomePage");
                return
            }else if(rowform.emails==undefined){
                alertify.alert("Please select proper email");
                return
            }
            if(rowform!= undefined){
                var rowFormlength = Object.keys(rowform).length;
                if(rowFormlength>=5){
                    var allDashbaordName=[];
                    for(var j in rowform.dashboardName){
                        allDashbaordName.push(rowform.dashboardName[j].id);
                    }
                    var showondashboardData=[];
                    for(var j in rowform.showOnHomePage){
                        showondashboardData.push(rowform.showOnHomePage[j].id);
                    }
                    var emails =[];
                    for(var x in rowform.emails){
                        emails.push(rowform.emails[x].email);
                    }
                    emails.toString();
                    emails+','+rowform.otherEmails
                    console.log(vehicleData);
                    var freq=0;
                    for(var q in rowform.freq){
                        freq=rowform.freq[q].freqVal
                    }
                    var myEmails='';
                    if(rowform.otherEmails!=undefined){
                        myEmails= emails+','+rowform.otherEmails
                    }else{
                        myEmails= emails
                    }
                    var myObj1 = {
                        "showOnHomePage":showondashboardData.toString(),
                        "dashboardName":allDashbaordName.toString(),
                        //"timeofDelivery":$('#timeofDelivery').val(),
                        "timeofDelivery":moment().format("YYYY-MM-DD HH:mm:ss"),
                        "emails":myEmails.toString(),
                        "freq":freq
                    }
                    console.log("f");
                    console.log(myObj1);
                    addhomePageSettings(myObj1);
                }else{
                    toastr.error("Please Fill valid data in required fields");
                    $scope.isValidate= false;
                    return;
                }
            }else{
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }
            // if(rowform== undefined){
            //     toastr.error("Please Fill valid data in required fields");
            //     $scope.isValidate= false;
            //     return;
            // }else{
            //     $scope.isValidate= true;
            // }

            // if(rowform._id == "" || rowform._id == undefined){
            //     addhomePageSettings();                
            //     $scope.clearData();
            //     rowform ={};
            // }else{
            //     vm._id = rowform._id; 
            //     updatehomePageSettings()
            //     $scope.clearData();
            //     rowform ={};
            // }             
        }
         // For Loading the Grid Widgets
         function myGrid(){ 
         var source =
         {
             localdata: data,
             datatype: "json",
             datafields:
             [
                 { name: '_id', type: 'string' },
                 { name: 'dashboardName', type: 'string' },
                 { name: 'emails', type: 'string' },
                 { name: 'freq', type: 'string' },
                 { name: 'sentAckDate', type: 'string' },
                 { name: 'showOnHomePage', type: 'string' },
                 { name: 'timeofDelivery', type: 'string' },
                 { name: 'userId', type: 'string' }

             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#grid").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Report Name', datafield: 'dashboardName',align: 'center',cellsalign: 'center', width: '33%' },
                 { text: 'Frequency of Mails', datafield: 'freq',align: 'center',cellsalign: 'center', width: '33%' },
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     //return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     return '<div class="text-center" style="padding:5px;"> <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
         // Initlize Button Event
         buttonsEvents();
         }
         function buttonsEvents() 
         {
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Edit Details
         function editDetails() { 
             var id = this.getAttribute("data-row"); 
             var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             $scope.vccUser = {
                 _id : selectedRowData._id,
                 tubeTypeName:selectedRowData.tubeTypeName,
                 tubeTypeDesc:selectedRowData.tubeTypeDesc
             }
             $scope.openModal();
             //$("#jqxwindowTubeType").jqxWindow('open'); 
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deletehomePageSettings(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
            
         }
     
         $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
             buttonsEvents();
         });
         $scope.clearData = function(){ //window.location.reload();
             $("#grid").jqxGrid('clearselection');
             $scope.vccUser = {}
         }

         $scope.openModal = function () {
            vm.ModalInstance2=$uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowSettings.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    //getSeletedDashboards()
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        }
        
     }
})();
  
  