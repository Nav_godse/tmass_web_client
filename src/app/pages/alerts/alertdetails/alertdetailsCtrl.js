(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('AlertsCtrl', AlertsCtrl);
  
    /** @ngInject */
    function AlertsCtrl($scope,tubeService,$uibModal,allDashboardService, localStorage, editableThemes,$timeout, toastr) {
        var vm = this; 
        //vm.getalertCrons = getalertCrons;
        //vm.addAlerts = addAlerts;
        var getselectedrowindexes =null;
        vm.currloggedinUser = localStorage.getObject('dataUser');
        vm.currUserEmail = vm.currloggedinUser.otherData.email;
        var selectedRowData =null;
        // $scope.vccUser = {
        //     _id:"",
        //     tubeTypeName:"",
        //     tubeTypeDesc:""
        // }
        initController();
        $scope.tubes = [];
        var data = [];
        $scope.alertCron=[];
        $scope.myUsers=[];

        function initController() {
            gettextvalues();
            getVehicleData();
            getalertCrons();

            // For Open Popup for Add and Edit
            //$("#jqxwindowTubeType").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }
        function gettextvalues(){
            $scope.alerts = [
                "Tire Missing", 
                "Tire Removal", 
                "Tag Tampering", 
                "Tire Exchange", 
                "Tire Rotation",
                "High Pressure Count",
                "Low Pressure Count",
                "High Temperature Count",
                "Improper Tag Fitment",
                "Tread Depth Based Maintenance",
                "Invalid Tag"
            ];
            $scope.freqarr = [
                {
                    freqVal:1,
                    freqText:"Daily"
                },{
                    freqVal:6,
                    freqText:"Weekly"
                },{
                    freqVal:30,
                    freqText:"Monthly"
                }
            ]
        }

        //for getting all the vehicle data for pie chart
        function getVehicleData () {
            allDashboardService.showVehicleDashboard(function(result){
                $scope.vehicleData=[];
                var vehicleObj={};
                getAllMyUsersAndCustomers();
                console.log('$scope.vehicleData');
                console.log($scope.vehicleData);
                if (result !=null && result.success === true ) { 
                    console.log(result);
                    var response =result.data;
                    for(var x=0;x<response.length;x++){
                        vehicleObj={
                            vehicleRegNo:response[x].vehicleRegistrationNumber,
                            _id:response[x]._id,
                        }
                        $scope.vehicleData.push(vehicleObj);
                    }
                    //toastr.success(result.message);
                } else {
                    $scope.vehiclechartdata= 0;
                    if(result == null){
                        toastr.error("No Data Found");
                    }else{
                        toastr.error(result.message);
                    }
                    //vm.error = result.message;                    
                }
            });
        }
        //allmyusersandcustomer
        function getAllMyUsersAndCustomers () {
            allDashboardService.allmyusersandcustomer(function(result){
                $scope.userEmails=[];
                var userEmailsObj={};
                //alert("inside here" , JSON.stringify(result));
                console.log('result hererherher');
                console.log(result);
                if (result != null && result.success === true ) { 
                    console.log('result.data');
                    console.log(result.data); 
                    $scope.myUsers=result.data;
                    console.log("$scope.myUsers",$scope.myUsers);
                    $scope.myUsers.push({"email": vm.currloggedinUser.otherData.email, "_id": vm.currloggedinUser._id})
                    //toastr.success(result.message);
                } else {
                    $scope.vehiclechartdata= 0;
                    if(result == null){
                        toastr.error("No Data Found");
                    }else{
                        toastr.error(result.message);
                    }
                    //vm.error = result.message;                    
                }
            });
        }

        function addAlerts(data) {       
            console.log(data);     
            allDashboardService.saveMyalertsSettings(data, function (result) {
                console.log(result);
                if (result.success === true ) { 
                    getalertCrons();
                    toastr.success(result.message);
                    $scope.isValidate= true;
                    vm.ModalInsrtance0.close();
                    //$("#jqxwindowTubeType").jqxWindow('close');
                } else {
                    $scope.isValidate= false;
                    toastr.error(result.message);
                    if(result == null){
                        toastr.error("No Data Found");
                    }else{
                        toastr.error(result.message);
                    }           
                }
            });
        };

        function getalertCrons() {   
           // var alert_ids=[];         
            allDashboardService.getalertCrons(function (result) {
                $scope.alertCron=[];
                if (result !=null && result.success === true ) { 
                    for(var i=0;i<result.data.length;i++){
                        var currVehcileNo = 'None';
                        for(var j in $scope.vehicleData){
                            console.log($scope.vehicleData[j] ,"inside the vechile details");
                            if(result.data[i].Data.vehicleId==$scope.vehicleData[j]._id)
                            {
                                currVehcileNo=$scope.vehicleData[j].vehicleRegNo;
                                break;
                            }
                            else    
                            {
                                currVehcileNo="None"
                                break;
                            }
                        }
                        console.log(result.data[i].Data, "llA result");
                        var alertType= "";
                        if(result.data[i].Data.alertType=="tireMissing"){
                            alertType= "Tire Missing";
                        }else if(result.data[i].Data.alertType=="tireRemoval"){
                            alertType= "Tire Removal";
                        }else if(result.data[i].Data.alertType=="tagTampering"){
                            alertType= "Tag Tampering";
                        }else if(result.data[i].Data.alertType=="tireExchange"){
                            alertType= "Tire Exchange";
                        }else if(result.data[i].Data.alertType=="tireRotation"){
                            alertType= "Tire Rotation";
                        }else if(result.data[i].Data.alertType=="tireRemold"){
                            alertType= "Tire Remould";
                        }else if(result.data[i].Data.alertType=="highPressureCount"){
                            alertType= "Tire Exchange";
                        }else if(result.data[i].Data.alertType=="lowPressureCount"){
                            alertType= "Low Pressure Count";
                        }else if(result.data[i].Data.alertType=="highTemperatureCount"){
                            alertType= "High Temperature";
                        }else if(result.data[i].Data.alertType=="improperTagfitment"){
                            alertType= "Improper Tag Fitment";
                        }else if(result.data[i].Data.alertType=="treadDepthBasedMaintenance"){
                            alertType= "TreadDepth Based Maintenance";
                        }else if(result.data[i].Data.alertType=="invalidTag"){
                            alertType= "Invalid Tag";
                        }else if(result.data[i].Data.alertType=="invalidTire"){
                            alertType= "Invalid Tire";
                        }else if(result.data[i].Data.alertType=="invalidVehicle"){
                            alertType= "Invalid Vehicle";
                        }
                        var obj={
                            addedOn: result.data[i].Data.addedOn,
                            alertTypelabel: alertType,
                            alertType: result.data[i].Data.alertType,
                            alertonvehicle_id: result.data[i].Data.alertonvehicle_id,
                            emails: result.data[i].Data.emails,
                            freq: result.data[i].Data.freq,
                            timeofDelivery: result.data[i].Data.timeofDelivery,
                            userId: result.data[i].Data.userId,
                            vehicleId: result.data[i].Data.vehicleId,
                            vehicleNo: result.data[i].Data.vehicleNo,
                            _id: result.data[i].Data._id,
                        }
                        console.log(result.data[i].Data.vehicleId, "testsetetset");
                        $scope.alertCron.push(obj);
                    }        
                    console.log($scope.alertCron);
                    
                } else {
                    if(result == null){
                        $scope.alertCron=[];
                        toastr.error("No Data Found");
                    }else{
                        toastr.error(result.message);
                    }                
                }
                myGrid();
            });
        };

        function deleteAlertDetails(_id,vehicleId,alertType) {            
            allDashboardService.deletealertCron(_id,vehicleId,alertType,function (result) {
                if (result !=null && result.success === true)  {  
                    $scope.tubes=[];
                    data=[];
                    getalertCrons();
                    toastr.success(result.message);
                } else {
                    if(result == null){
                        toastr.error("No Data Found");
                    }else{
                        toastr.error(result.message);}

                    //vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            // Validation 
            console.log(rowform);
            var Obj={};
            if(rowform==undefined){
                alertify.alert("Please fill all feilds");
                return
            }else if(rowform.alertType==undefined){
                alertify.alert("Please select proper alertType");
                return
            }else if(rowform.freq==undefined){
                alertify.alert("Please select proper Frequency of email");
                return
            }else if(rowform.vehicleNo==undefined){
                alertify.alert("Please select proper vehicles");
                return
            }else if(rowform.emails==undefined){
                alertify.alert("Please select proper email");
                return
            }
            var vehicleData=[];
            if(rowform!= undefined){
                var rowFormlength = Object.keys(rowform).length;
                if(rowFormlength>=4){
                    for(var i in rowform.alertType){
                        for(var j in rowform.vehicleNo){
                            var alertType= "";
                            if(rowform.alertType[i]=="Tire Missing"){
                                alertType= "tireMissing";
                            }else if(rowform.alertType[i]=="Tire Removal"){
                                alertType= "tireRemoval";
                            }else if(rowform.alertType[i]=="Tag Tampering"){
                                alertType= "tagTampering";
                            }else if(rowform.alertType[i]=="Tire Exchange"){
                                alertType= "tireExchange";
                            }else if(rowform.alertType[i]=="Tire Rotation"){
                                alertType= "tireRotation";
                            }else if(rowform.alertType[i]=="Tire Remould"){
                                alertType= "tireRemold";
                            }else if(rowform.alertType[i]=="High Pressure Count"){
                                alertType= "highPressureCount";
                            }else if(rowform.alertType[i]=="Low Pressure Count"){
                                alertType= "lowPressureCount";
                            }else if(rowform.alertType[i]=="High Temperature"){
                                alertType= "highTemperatureCount";
                            }else if(rowform.alertType[i]=="Improper Tag Fitment"){
                                alertType= "improperTagfitment";
                            }else if(rowform.alertType[i]=="TreadDepth Based Maintenance"){
                                alertType= "treadDepthBasedMaintenance";
                            }else if(rowform.alertType[i]=="Invalid Tag"){
                                alertType= "invalidTag";
                            }else if(rowform.alertType[i]=="Invalid Tire"){
                                alertType= "invalidTire";
                            }else if(rowform.alertType[i]== "Invalid Vehicle"){
                                alertType="invalidVehicle"
                            }
                            Obj={
                                "vehicleId":rowform.vehicleNo[j]._id,
                                "alertType":alertType
                            }
                            vehicleData.push(Obj);
                        }
                    }
                    var emails =[];
                    for(var x in rowform.emails){
                        emails.push(rowform.emails[x].email);
                    }
                    emails.toString();
                    emails+','+rowform.otherEmails
                    console.log(vehicleData);
                    var freq=0;
                    for(var q in rowform.freq){
                        freq=rowform.freq[q].freqVal
                    }
                    var myEmails='';
                    if(rowform.otherEmails!=undefined){
                        myEmails= emails+','+rowform.otherEmails
                    }else{
                        myEmails= emails
                    }
                    var myObj1 = {
                        "vehicleData":vehicleData,
                        "timeofDelivery":"",
                        "emails":myEmails,
                        "freq":freq
                    }
                    addAlerts(myObj1);
                }else{
                    toastr.error("Please Fill valid data in required fields");
                    $scope.isValidate= false;
                    return;
                }
            }else{
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }        
        }

         // For Loading the Grid Widgets
         function myGrid(){ 
         var source =
         {
             localdata: $scope.alertCron,
             datatype: "array",
             datafields:
             [
                 { name: '_id', type: 'string' },
                 { name: 'alertTypelabel', type: 'string'},
                 { name: 'alertType', type: 'string' },
                 { name: 'vehicleNo', type: 'string' },
                 { name: 'emails', type: 'string' },
                 { name: 'vehicleId', type: 'string' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#alertgrid").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             autorowheight: true,
             autoheight: true,
             altrows: true,
             pagesize:5,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '15%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Alert Type', datafield: 'alertTypelabel',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Vehicle Number', datafield: 'vehicleNo',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Vehicle Id',hidden:true, datafield: 'vehicleId',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Recipient', datafield: 'emails',cellsalign: 'center',align: 'center', width: '20%' },
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '25%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     return '<button data-row="' + row + '" class="deleteButtons btn btn-danger" style="width: 100%;margin: 5px;" > Disable / Delete </button> </div>';
                     //return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
         // Initlize Button Event
         buttonsEvents();
         }

         function buttonsEvents() 
         {
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#alertgrid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#alertgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             console.log("selectedRowData");
             console.log(selectedRowData);
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteAlertDetails(selectedRowData._id,selectedRowData.vehicleId,selectedRowData.alertType);
            }, function() {
                // user clicked "cancel"
            });
            
         }
     
         $("#alertgrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
             buttonsEvents();
         });
         $scope.clearData = function(){ //window.location.reload();
             $("#alertgrid").jqxGrid('clearselection');
         }

         $scope.openModal = function () {
            vm.ModalInsrtance0=$uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowSettings.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    //getSeletedDashboards()
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        }

        $scope.selectDashboard = [
            {	icon: "", name: "Avg Cost",	maker: "Avg Cost / kM",	ticked: false	},
            {	icon: "", name: "Avg Distance Travelled",	maker: "Avg Distance Travelled",	ticked: false	},
            {	icon: "", name: "Avg Mantaianance Cost",	maker: "Avg Mantaianance Cost",	ticked: false	},
            {	icon: "", name: "Tire Dashboard",	maker: "Tire Dashboard",	ticked: false	},
            {	icon: "", name: "Vehicle Dashboard",	maker: "Vehicle Dashboard",	ticked: false	},
            {	icon: "", name: "Performance Dashboard",	maker: "Performance Dashboard",	ticked: false	}
        ];

        $scope.getSeletedDashboards= function(){
            angular.forEach( $scope.getSelectDashboard, function( value, key ) {    
                /* do your stuff here */
                console.log(key, value);    
            });
        }
        
     }
})();
  
  