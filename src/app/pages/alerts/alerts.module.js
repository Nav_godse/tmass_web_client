(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.alerts', ['ui.select', 'ngSanitize'])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('main.alerts', {
          url: '/alerts',
          templateUrl: 'app/pages/alerts/alertdetails/alertdetails.html',
          controller: "AlertsCtrl",
          abstract: false,
          title: 'Alerts',
          sidebarMeta: {
            icon: 'icoAlerts',
            order: 4,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin','OEM Manager',  'Client Admin', 'Client Manager']   // <-- roles allowed for this module
          }
        });
    }
  })();