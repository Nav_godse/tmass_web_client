(function() {
  'use strict';

  angular.module('TmassAdmin.pages.authSignIn')
    .controller('authSignInCtrl', authSignInCtrl);

  /** @ngInject */
  function authSignInCtrl($scope, localStorage, $state,AuthenticationService, toastr, $uibModal) {
    var vm = this;

    vm.logar = logar;

    init();

    function init() {
    //   localStorage.clear();
    //   logMeOut();
      if (localStorage.chkbx && localStorage.chkbx != '') {
          $('#remember_me').attr('checked', 'checked');
          $('#username').val(localStorage.usrname);
          $('#pass').val(localStorage.pass);
      } else {
          $('#remember_me').removeAttr('checked');
          $('#username').val('');
          $('#pass').val('');
      }

      $('#remember_me').click(function() {
          if ($('#remember_me').is(':checked')) {
              // save username and password
              localStorage.usrname = $('#username').val();
              localStorage.pass = $('#pass').val();
              localStorage.chkbx = $('#remember_me').val();
          } else {
              localStorage.usrname = '';
              localStorage.pass = '';
              localStorage.chkbx = '';
          }
      });
    }

    function logar() {
        var currUser = (vm.user).toLowerCase();
        AuthenticationService.Login(currUser, vm.password, function (result) {
            if (result.success === true) {
                toastr.success(result.message);
                console.log("localStorage.otherData");
                // vm.currUserData = localStorage.getObject('dataUser');
                // console.log(vm.currUserData.otherData.isloginNew);
                // if(vm.currUserData.otherData.isloginNew==true){
                //     $window.location.href = "/profile";
                // }else{
                    $state.go('main.home');
                //}
                
            } else {
                //console.log(result);
                if(result.message){
                    toastr.error(result.message);
                }else{
                    // if (result.errorCode == 403 ) {
                    //     alertify.alert("Invalid Details, Login again");
                    //     localStorage.clear();
                    //     //$window.location.reload();
                    //     $window.location.href = "/authSignIn";
                    // }
                    toastr.error(result);
                }
                return
            }
        });
    };

    function getUserDetails(email) {
        AuthenticationService.GetCurrentUserDetails(function () {
            // if (result.success === true) {
            //     toastr.success(result.message);
            //     $state.go('main.home');
            // } else {
            //     //console.log(result);
            //     toastr.error(result.message);
            //     return
            // }
        });
    };
    
    function logMeOut() {
        AuthenticationService.Logout(function () {
            // if (result.success === true) {
            //     toastr.success(result.message);
            //     $state.go('main.home');
            // } else {
            //     //console.log(result);
            //     toastr.error(result.message);
            //     return
            // }
        });
    };


  }

})();