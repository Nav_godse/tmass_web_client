/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages', [
      'ui.router',
      'TmassAdmin.pages.authSignIn',
      'TmassAdmin.pages.services',
      'TmassAdmin.pages.config',
      'TmassAdmin.pages.main',
      'TmassAdmin.pages.dashboard',
      'TmassAdmin.pages.profile',
      'TmassAdmin.pages.authSignUp',
      'TmassAdmin.pages.forgotPassword',
      'TmassAdmin.pages.resetPassword',
      'TmassAdmin.pages.masters',
      'TmassAdmin.pages.reports',
      'TmassAdmin.pages.alerts',
      'TmassAdmin.pages.settings',
      'TmassAdmin.pages.vehicleTpms',
      //'TmassAdmin.pages.test'
      //'TmassAdmin.pages.tables'
    ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    $urlRouterProvider.otherwise('/authSignIn');
  }

})();