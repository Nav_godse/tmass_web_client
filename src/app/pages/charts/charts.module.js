/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.charts', [
      'TmassAdmin.pages.charts.amCharts',
      'TmassAdmin.pages.charts.chartJs',
      'TmassAdmin.pages.charts.chartist',
      'TmassAdmin.pages.charts.morris'
    ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.charts', {
        url: '/charts',
        abstract: true,
        template: '<div ui-view  autoscroll="true" autoscroll-body-top></div>',
        title: 'Charts',
        sidebarMeta: {
          icon: 'ion-stats-bars',
          order: 150,
        },
        authenticate: true
      });
  }

})();