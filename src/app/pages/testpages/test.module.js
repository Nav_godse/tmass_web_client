(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.test', ['ui.select', 'ngSanitize'])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider,$httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
      $stateProvider
        .state('main.test', {
          url: '/test',
          //app/pages/settings/settingDetails/settingDetails.html
          templateUrl: 'app/pages/testpages/test.html',
          //controller: "VehicleTpmsCtrl",
          abstract: false,
          title: 'test View',
          sidebarMeta: {
            icon: 'icoTPMSView',
            order: 5,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager']   // <-- roles allowed for this module
          }
        });
    }
  })();