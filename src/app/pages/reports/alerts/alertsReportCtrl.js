(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('AlertReportCtrl', AlertReportCtrl);
  
    /** @ngInject */
    function AlertReportCtrl($scope,allDashboardService,allReportService,localStorage,vehicleService,toastr, $uibModal,$http,config) {
        var vm = this;
        //vm.getAllTagMasterDetails = getAllTagMasterDetails;
        //vm.addCustomerAssignedTag = addCustomerAssignedTag;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        $scope.vccUser = {}   
        $scope.getLoggedInUserdata = localStorage.getObject('dataUser');
        initController();
        $scope.customerAlertDetails = [];
        $scope.customers= [];
        $scope.tagTypes= [];
        $scope.tagTypes= [];
        $scope.customerDepot= [];
        $scope.orginalData= [];
        $scope.gridArr1 =[];
        $scope.gridArr2 =[];
        $scope.myalertArr = [];

        function initController() {
            getAllDropdowns();
            //$("#jqxwindowDepot").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function getAllDropdowns() {
            getAllCustomerDepotDetails();
            // getAllTagTypeDetails();
            // getAllTagQtyAssignmentDetails();
            GetAllVehicleGroups();
            setTimeout(function(){
                getAllCustomerTagMasterDetails();
            }, 1000);          
        }
        function GetAllVehicleGroups(){
            vehicleService.GetAllVehicleGroup(function (result) {
                $scope.VehicleGroup=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.VehicleGroup.push({value: result.data[i]._id, text: result.data[i].vehicleGroupName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }

        //to get all Depot details.
        function getAllCustomerDepotDetails() {            
            vehicleService.GetAllVehicleMaster(function (result) {
                $scope.vehicles=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.vehicles.push({value: result.data[i]._id, text: result.data[i].vehicleRegistrationNumber});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }

        //end all loops
        function getAllCustomerTagMasterDetails() {            
            allReportService.ShowAlertsReport(function (result) {
                $scope.customerAlertDetails=[];
                $scope.orginalData= [];
                $scope.gridArr1 =[];
                $scope.myObj ={};
                console.log(result.data);
                if (result.success === true) {
                    $scope.orginalData= result.data;
                    for(var i=0;i<result.data.length;i++){
                        // "_id" : {
                        //     "vehicleId" : ObjectId("5b6c20f69f53c1718b1ba23d"),
                        //     "customerId" : ObjectId("5b68143b2d3809625a3d0ac6"),
                        //     "vehicleNo" : "MH105",
                        //     "occuredOn" : "2018-08-12"
                        // },
                        $scope.myObj={
                            vehicleId:result.data[i]._id.vehicleId,
                            vehicleNo:result.data[i]._id.vehicleNo,
                            occuredOn:result.data[i]._id.occuredOn,
                            count:result.data[i].count,
                        }
                        $scope.customerAlertDetails.push($scope.myObj);    
                    }
                    
                } else {
                    $scope.customerAlertDetails=[];
                    toastr.error(result.message);       
                }
                mygridAlertReport();
            });
        };
        
         // For Loading the gridAlertReport Widgets
         function mygridAlertReport(){ 
         var source =
         {
             localdata: $scope.customerAlertDetails,
             datatype: "array",
             datafields:
             [
                { name: '_id', type: 'string' },
                { name: 'vehicleNo', type: 'string' },
                { name: 'occuredOn', type: 'string' },
                { name: 'count', type: 'number' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#gridAlertReport").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             showfilterrow: true,
             filterable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                {
                text: 'Sr No',sortable: false, filterable: false,exportable:false, editable: false,
                groupable: false, draggable: false, resizable: false,
                datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '15%' ,
                cellsrenderer: function (row, column, value) {
                    return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                }
                },
                { text: 'Vehicle No', datafield: 'vehicleNo',align: 'center', width: '25%', cellsalign: 'center'},
                { text: 'Alerts Occured On', datafield: 'occuredOn',align: 'center',cellsalign: 'center', width: '25%' },
                { text: 'Alerts Occured Count', datafield: 'count',align: 'center',cellsalign: 'center', width: '15%' },
                {
                text: 'Action', cellsAlign: 'center', align: "center",width: '20%',exportable:false, filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                // render custom column.
                return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >View Details </button> </div>';
                }
                }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#gridAlertReport").on("pagechanged sort filter scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
            }
         }
         
         // For Edit Details
         function editDetails() { 
             var getselectedrowindexes = $('#gridAlertReport').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#gridAlertReport').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             console.log(selectedRowData);            
            $scope.myalertArr=[];
            for(var q=0;q<$scope.orginalData.length;q++){
                if(selectedRowData.vehicleNo==$scope.orginalData[q]._id.vehicleNo){
                    for(var w=0 ;w<$scope.orginalData[q].alertData.length;w++){
                        console.log($scope.orginalData[q].alertData);
                        $scope.myalertArr.push($scope.orginalData[q].alertData[w]);
                        console.log($scope.myalertArr);
                    }                        
                }                    
            }
            $scope.currvehicleData=selectedRowData.vehicleNo;
            console.log($scope.myalertArr);
            $scope.openModalReportDetails();
         }  
         // For Delete Details 
         $scope.printData=function (){
            var rows = $("#gridAlertReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            $("#gridAlertReport").jqxGrid('exportdata', 'xls', 'Alerts Report'+new Date());  
        }
         $scope.clearData = function(){ 
            $("#gridAlertReport").jqxGrid('clearselection');
            $("#gridAlertReport").jqxGrid('clearfilters');
            $scope.vccUser = {}            
         }
         $scope.clearData1 = function(){ 
            //$("#gridAlertReport").jqxGrid('clearselection');
            $("#gridAlertReport").jqxGrid('clearfilters');
            //$scope.vccUser = {}            
         }
         $scope.openModalReportDetails = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxVehicleReportDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };              
                $scope.viewDetails = function() {  
                    var currData = {};
                    //GetallTagbyTagType();
                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
         };
                 //for filter
        $('#generateAlertReport').bind('click', function (event) {
            var searchText = $("#vehicleRegNo").val();
            //var searchVehicleGroup = $("#vehicleGroup").val();
            if (searchText.length > 0 ) {
                var searchTextfiltergroup = new $.jqx.filter();
                var filtervalue = searchText;
                var filtercondition = 'contains';
                var filter = searchTextfiltergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // used when there are multiple filters on a grid column:
                var filter_or_operator = 1;
                // used when there are multiple filters on the grid:
                //searchTextfiltergroup.operator = 'or';
                searchTextfiltergroup.addfilter(filter_or_operator, filter);
                //vehiclegroup
                //var searchVehicleGroupfiltergroup = new $.jqx.filter();
                //var filtervalue = searchVehicleGroup;
                var filtercondition = 'contains';
                //var filter = searchVehicleGroupfiltergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // used when there are multiple filters on a grid column:
                var filter_or_operator = 1;
                // used when there are multiple filters on the grid:
                //searchVehicleGroupfiltergroup.operator = 'or';
                //searchVehicleGroupfiltergroup.addfilter(filter_or_operator, filter);
                $("#gridAlertReport").jqxGrid('addfilter', 'vehicleNo', searchTextfiltergroup);
                //$("#gridAlertReport").jqxGrid('addfilter', 'vehicleGroupName', searchVehicleGroupfiltergroup);
                // apply the filters.
                $("#gridAlertReport").jqxGrid('applyfilters');
            }
            else {
                $("#gridAlertReport").jqxGrid('clearfilters');
            }
            $scope.filteredData =$("#gridAlertReport").jqxGrid("getrows");
        });
        //create table 
        function buildTableBody(data, columns) {
            var body = [];

            body.push(columns);

            data.forEach(function(row) {
                var dataRow = [];

                columns.forEach(function(column) {
                    dataRow.push(row[column].toString());
                })

                body.push(dataRow);
            });

            return body;
        }
        //get table body
        function table(data, columns) {
            console.log(data, columns);
            return {
                table: {
                    headerRows: 1,
                    widths: ['25%', '25%', '25%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        $scope.sendMail = function()
        {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            
            var rows = $("#gridAlertReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            // get the date as a string
            var n = date.toDateString();
                dashheader='Vehicle Report \n';
                headerArr=['Vehicle No', 'Alerts Occured On','Alerts Occured Count'];
                for(var i=0;i<rows.length;i++){
                    var a={ 
                        'Vehicle No': rows[i].vehicleNo, 
                        'Alerts Occured On':rows[i].occuredOn ,
                        'Alerts Occured Count':rows[i].count,                 
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                JSONToCSVConvertor2(externalDataRetrievedFromServer,dashheader, true,"Report : Vehicle Report",dashheader);
        }
        
        $scope.uploadFile = function(){
            var file = $scope.myFile;
            var uploadUrl = "/savedata";
            fileUpload.uploadFileToUrl(file, uploadUrl);
        };
        ///===========================================
        function dataURItoBlob(dataURI, callback) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var bb = new Blob([ab]);
            return bb;
        }
        function JSONToCSVConvertor2(JSONData,ReportTitle, ShowLabel,subject,dashboardName) {
            //alert("in");
            // var JSONData = [
            //     { "Vehicle": "BMW", "Date": "30, Jul 2013 09:24 AM", "Location": "Hauz Khas, Enclave, New Delhi, Delhi, India", "Speed": 42 }, 
            //     { "Vehicle": "Honda CBR", "Date": "30, Jul 2013 12:00 AM", "Location": "Military Road,  West Bengal 734013,  India", "Speed": 0 }, 
            //     { "Vehicle": "Supra", "Date": "30, Jul 2013 07:53 AM", "Location": "Sec-45, St. Angel's School, Gurgaon, Haryana, India", "Speed": 58 }, 
            // ];
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            // var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            var uri = 'data:text/csv;charset=utf-8,' + btoa(CSV);
            //  btoa(string)
            var block = uri.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1]
            console.log(realData);
            console.log(realData);
            var link = document.createElement("a");
            link.href = uri;
            console.log(link);

            console.log(dataURItoBlob(link.href));

            var fd = new FormData();
            fd.append('fname', 'test.csv');
            fd.append('data', dataURItoBlob(uri));

            ///console.log(fd);

            $scope.uploadInfo = {
                scaleId: 29719,
                fileDate: "23/09/1992"
            };

            var data = $scope.uploadInfo;
            $scope.uploadInfo.fileDate = "23/09/1992";
            $scope.uploadInfo.fileDate = new Date($scope.uploadInfo.fileDate);
            $scope.uploadInfo.fileDate = new Date();
            data.fileDate = $scope.uploadInfo.fileDate.toISOString();


            $scope.uploadAttachDetails(uri,subject,dashboardName);
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
        }

        $scope.uploadAttachDetails = function(file,subject,dashboardName) { 
            var file_obj=dataURItoBlob(file) 
            console.log(file_obj);
            var currData = {};
        
            var fd = new FormData();
            fd.append('file', file_obj);
            fd.append('data', 'string');
            $http.post(config.apiUrl+'/api/upload_mail_attach', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(response){
                    console.log(response);
                    allDashboardService.SendMail($scope.getLoggedInUserdata.otherData.email,subject,dashboardName,'',response.data,function(result){
                        if (result.success === true) {
                            console.log(result);   
                            toastr.success(result.message);      
                        }else{
                            toastr.error(result.message);
                            vm.error = result.message;                    
                        }
                    })
            })
            .error(function(response){
                console.log(response);
            });
        }

        //=============================================
    }
  
})();
  
  