(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('PerformanceReportCtrl', PerformanceReportCtrl);
  
    /** @ngInject */
    function PerformanceReportCtrl($scope,allDashboardService,allReportService,localStorage,vehicleService,toastr, $uibModal,$http,config) {
        var vm = this;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        $scope.vccUser = {}   
        $scope.getLoggedInUserdata = localStorage.getObject('dataUser');
        initController();
        $scope.configuredVehicles = [];
        $scope.customers= [];
        $scope.tagTypes= [];
        $scope.tagTypes= [];
        $scope.customerDepot= [];
        $scope.myalertArr = [];

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllCustomerDepotDetails();
            GetAllVehicleGroups();
            setTimeout(function(){
                getAllCustomerTagMasterDetails();
            }, 1000);          
        }
        function GetAllVehicleGroups(){
            vehicleService.GetAllVehicleGroup(function (result) {
                $scope.VehicleGroup=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.VehicleGroup.push({value: result.data[i]._id, text: result.data[i].vehicleGroupName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }

        //to get all Depot details.
        function getAllCustomerDepotDetails() {            
            vehicleService.GetAllVehicleMaster(function (result) {
                $scope.vehicles=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.vehicles.push({value: result.data[i]._id, text: result.data[i].vehicleRegistrationNumber});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }
        $scope.configuredVehicles = []
        $scope.nonConfiguredVehicles = [];
        $scope.configuredVehiclesCount = 0
        $scope.nonConfiguredVehiclesCount = 0;

        var totalAvg=0;
        var oldvalueavg =0 ;
        var newvalueavg = 0
        var newval =0;
        var vehicleThreadDepth = 0;
        var vehicleAvgThreadDepth = 0;
        var totalgt80TiresData =[];
        var totalgt50TiresData  =[];
        var totalgt20TiresData =[];
        var totallt20TiresData =[];
        $scope.realData = [];
        //end all loops
        function getAllCustomerTagMasterDetails() {            
            allDashboardService.showPerformanceDashboard(function(result){
                if (result.success === true) { 
                    console.log('result.data');
                    console.log(result.data);
                     var vehicleCount =0;
                     var currResult = result.data;
                     $scope.realData= result.data                     
                    if(result.data==null || result.data==undefined){                        
                        $scope.tireDashboard={
                            totalCount: 0,
                            totalgt80:0,
                            totalgt50: 0,
                            totalgt20: 0,
                            totallt20: 0,
                            totalData:0,
                            totalgt80Data:0,
                            totalgt50Data: 0,
                            totalgt20Data: 0,
                            totallt20Data: 0,
                        };
                    }else{
                        vehicleCount =currResult.length;
                        for(var q=0 ;q<currResult.length ;q++){
                            console.log(currResult[q].TireConfigdetails);
                           if(currResult[q].TireConfigdetails==undefined || currResult[q].TireConfigdetails.length==0 ){
                               $scope.nonConfiguredVehicles.push(currResult[q]);                            
                            }else{
                               $scope.configuredVehicles.push(currResult[q]);
                            }
                        }
                        //console.log(TotalAlertsOccured)
                        for(var w=0;w<$scope.configuredVehicles.length;w++){ 
                           vehicleThreadDepth=0;
                           var currtireDetail =$scope.configuredVehicles[w].TireConfigdetails;
                           var curralertDetails = $scope.configuredVehicles[w].TotalAlertsOccured;
                           if(curralertDetails!=null){
                                console.log("Alerts occured",curralertDetails);    
                           }                           
                           for(var e=0 ;e<currtireDetail.length ;e++){ 
                               var currtirethreaddepth =currtireDetail[e].tireThreadDepthDetails;
                               console.log(currtireDetail[e].tirePositionName); 
                                   totalAvg=0;
                                   if(currtirethreaddepth.length>1){
                                       newval = currtirethreaddepth[currtirethreaddepth.length - 1];
                                      
                                       oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                       newvalueavg=(newval.tireThreadDepth_t1+newval.tireThreadDepth_t2+newval.tireThreadDepth_t3)/3;
                                       totalAvg =newvalueavg*100/oldvalueavg ; 
                                       currtireDetail[e].TD= totalAvg;
                                   }else{ 
                                       oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                       newvalueavg=oldvalueavg; 
                                       totalAvg =newvalueavg*100/oldvalueavg;  
                                       currtireDetail[e].TD= totalAvg;
                                   } 
                               vehicleThreadDepth = totalAvg +vehicleThreadDepth; 
                               vehicleAvgThreadDepth=vehicleThreadDepth/currtireDetail.length;
                               $scope.configuredVehicles[w].vehicleTD = Math.round((((vehicleAvgThreadDepth))*50/100)*100)/100 ;
                           }
                        }
                        console.log("$scope.configuredVehicles",$scope.configuredVehicles);
                        for(var t=0;t<$scope.configuredVehicles.length;t++){
                           if($scope.configuredVehicles[t].vehicleTD>=80){
                               totalgt80TiresData.push($scope.configuredVehicles[t])
                           }else if($scope.configuredVehicles[t].vehicleTD<80 && $scope.configuredVehicles[t].vehicleTD>=50){
                               totalgt50TiresData.push($scope.configuredVehicles[t])
                           }else if($scope.configuredVehicles[t].vehicleTD<50 && $scope.configuredVehicles[t].vehicleTD>=20){
                               totalgt20TiresData.push($scope.configuredVehicles[t])
                           }else if($scope.configuredVehicles[t].vehicleTD<20 ){
                               totallt20TiresData.push($scope.configuredVehicles[t])
                           }
                        }
                       $scope.tireDashboard={
                           totalCount: vehicleCount,
                           totalgt80: totalgt80TiresData.length,
                           totalgt50: totalgt50TiresData.length,
                           totalgt20: totalgt20TiresData.length,
                           totallt20: totallt20TiresData.length,
                           totalData: currResult,
                           totalgt80Data: totalgt80TiresData,
                           totalgt50Data: totalgt50TiresData,
                           totalgt20Data: totalgt20TiresData,
                           totallt20Data: totallt20TiresData,
                       };
                    }  
                    mygridPerformanceReport()

                } else {
                    $scope.tireDashboard={
                        totalCount: 0,
                        totalgt80: 0,
                        totalgt50: 0,
                        totalgt20: 0,
                        totallt20: 0
                    };
                    //toastr.error(result.message);
                    vm.error = result.message;  
                    mygridPerformanceReport();                  
                }
            });
        };
        
         // For Loading the gridPerformanceReport Widgets
         function mygridPerformanceReport(){ 
         var source =
         {
             localdata: $scope.configuredVehicles,
             datatype: "json",
             datafields:
             [
                // TotalAlertsOccured: null
                // TotalTire: 5
                // addedOn: "2018-09-27T13:11:19.187Z"
                // customerId: "5bab66183b54d709931c922b"
                // distanceTravelled: 0
                // loadCapacity: 999
                // rotationThreshold: 20
                // subscriptionEndDate: "2019-08-26T00:00:00.000Z"
                // subscriptionStartDate: "2018-09-27T00:00:00.000Z"
                // vehicleAxel: "2x2"
                // vehicleAxelConfigurationId: "5b489e9b73f6337a49886b01"
                // vehicleGroupId: "5b08f68808013a4cb6f6412a"
                // vehicleGroupName: "Trailer"
                // vehicleManufacturerId: "5ba1ea8f1bd621532d92e051"
                // vehicleModelId: "5ba1eadc1bd621532d92e052"
                // vehicleRegistrationNumber: "MH45AG5622"
                // vehicleTD: 71.51
                // _id: "5bacd6f753ed833e091b374e"
                { name: '_id', type: 'string' },
                { name: 'vehicleRegistrationNumber', type: 'string' },
                { name: 'vehicleTD', type: 'number' },
                { name: 'distanceTravelled', type: 'number' },
                //{ name: 'TotalAlertsOccured', type: 'number' },
                //{ name: 'lastscandoneDepot', type: 'string' },
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#gridPerformanceReport").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             showfilterrow: true,
             filterable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                {
                text: 'Sr No',sortable: false, filterable: false,exportable:false, editable: false,
                groupable: false, draggable: false, resizable: false,
                datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                cellsrenderer: function (row, column, value) {
                    return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                }
                },
                { text: 'Vehicle No', datafield: 'vehicleRegistrationNumber',align: 'center', width: '30%', cellsalign: 'center'},
                { text: 'Performance %', datafield: 'vehicleTD',align: 'center',cellsalign: 'center', width: '25%' },
                { text: 'Distance Travelled', datafield: 'distanceTravelled',align: 'center',cellsalign: 'center', width: '25%' },
                //{ text: 'Alert Count',datafield: 'TotalAlertsOccured',align: 'center',cellsalign: 'center', width: '10%' },
                //{ text: 'Last Scan Done At', datafield: 'lastscandoneDepot',align: 'center',cellsalign: 'center', width: '10%' },
                {
                text: 'Action', cellsAlign: 'center', align: "center",width: '15%',exportable:false, filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                // render custom column.
                return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >View Details </button> </div>';
                }
                }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#gridPerformanceReport").on("pagechanged sort filter scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
            }
         }
         
         // For Edit Details
         function editDetails() { 
             var getselectedrowindexes = $('#gridPerformanceReport').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#gridPerformanceReport').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             console.log(selectedRowData);            
            if(selectedRowData.totalvehicleAlert!=0){
                $scope.myalertArr=[];
                for(var q=0;q<$scope.configuredVehicles.length;q++){
                    if(selectedRowData.vehicleRegistrationNumber==$scope.configuredVehicles[q].vehicleRegistrationNumber){
                        for(var w=0 ;w<$scope.configuredVehicles[q].VehicleAlerts.length;w++){
                            $scope.myalertArr.push($scope.configuredVehicles[q].VehicleAlerts[w]);
                        }                        
                    }                    
                }
                $scope.currvehicleData=selectedRowData.vehicleRegistrationNumber;
                console.log($scope.myalertArr);
                if(selectedRowData.TotalAlertsOccured!=null){
                    $scope.openModalReportDetails();
                }else{
                    alertify.alert("There are no alerts on this vehicle to view");
                }
                
            }else{
                alertify.alert("There are no alerts on this vehicle to view");
            }
         }  
         // For Delete Details 
         $scope.printData=function (){
            var rows = $("#gridPerformanceReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            $("#gridPerformanceReport").jqxGrid('exportdata', 'xls', 'Vehicles Report'+new Date());  
        }
         $scope.clearData = function(){ 
            $("#gridPerformanceReport").jqxGrid('clearselection');
            $("#gridPerformanceReport").jqxGrid('clearfilters');
            $scope.vccUser = {}            
         }
         $scope.clearData1 = function(){ 
            //$("#gridPerformanceReport").jqxGrid('clearselection');
            $("#gridPerformanceReport").jqxGrid('clearfilters');
            //$scope.vccUser = {}            
         }

         $("#button_no").click(function () {
            //$("#jqxwindowDepot").jqxWindow('close');
         });

         $scope.openModalReportDetails = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxgridPerformanceReportDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };              
                $scope.viewDetails = function() {  
                    var currData = {};
                    //GetallTagbyTagType();
                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                    // $("#gridTireReport").jqxGrid('clearfilters');
                    // $("#gridTireReport").jqxGrid('clearselection');
                    // $scope.grid2arr=[];
                    //location.reload();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
         };
                 //for filter
        $('#generateAlertReport').bind('click', function (event) {
            var searchText = $("#vehicleRegNo").val();
            var searchVehicleGroup = $("#vehicleGroup").val();
            if (searchText.length > 0 || searchVehicleGroup.length > 0 ) {
                var searchTextfiltergroup = new $.jqx.filter();
                var filtervalue = searchText;
                var filtercondition = 'contains';
                var filter = searchTextfiltergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // used when there are multiple filters on a grid column:
                var filter_or_operator = 1;
                // used when there are multiple filters on the grid:
                //searchTextfiltergroup.operator = 'or';
                searchTextfiltergroup.addfilter(filter_or_operator, filter);
                //vehiclegroup
                var searchVehicleGroupfiltergroup = new $.jqx.filter();
                var filtervalue = searchVehicleGroup;
                var filtercondition = 'contains';
                var filter = searchVehicleGroupfiltergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // used when there are multiple filters on a grid column:
                var filter_or_operator = 1;
                // used when there are multiple filters on the grid:
                //searchVehicleGroupfiltergroup.operator = 'or';
                searchVehicleGroupfiltergroup.addfilter(filter_or_operator, filter);
                $("#gridPerformanceReport").jqxGrid('addfilter', 'vehicleRegistrationNumber', searchTextfiltergroup);
                $("#gridPerformanceReport").jqxGrid('addfilter', 'vehicleGroupName', searchVehicleGroupfiltergroup);
                // apply the filters.
                $("#gridPerformanceReport").jqxGrid('applyfilters');
            }
            else {
                $("#gridPerformanceReport").jqxGrid('clearfilters');
            }
            $scope.filteredData =$("#gridPerformanceReport").jqxGrid("getrows");
        });
        //create table 
        function buildTableBody(data, columns) {
            var body = [];

            body.push(columns);

            data.forEach(function(row) {
                var dataRow = [];

                columns.forEach(function(column) {
                    dataRow.push(row[column].toString());
                })

                body.push(dataRow);
            });

            return body;
        }
        //get table body
        function table(data, columns) {
            console.log(data, columns);
            return {
                table: {
                    headerRows: 1,
                    widths: ['25%', '25%', '25%','25%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        $scope.sendMail = function()
        {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var rows = $("#gridPerformanceReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            
            var n = date.toDateString();
                dashheader='Vehicle Report \n';
                headerArr=['Vehicle No', 'Vehicle Manufacturer','Vehicle Model', 'Vehicle Group', 'Alert Occured Count'];
                for(var i=0;i<rows.length;i++){
                    var a={ 
                        'Vehicle No': rows[i].vehicleRegistrationNumber, 
                        'Vehicle Manufacturer':rows[i].vehicleManufacturer ,
                        'Vehicle Model':rows[i].vehicleModel,
                        'Vehicle Group':rows[i].vehicleGroupName,        
                        'Alerts Occured Count':rows[i].totalvehicleAlert,                   
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                // var dd = {
                //     content: [
                //         { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                //         { text: 'Date: '+n,bold: true, alignment:'left' },
                //         { text: '\n'},
                //         table(externalDataRetrievedFromServer, headerArr)                        
                //     ]
                // } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,dashheader, true,"Report : Vehicle Report",dashheader);
        }
        
        $scope.uploadFile = function(){
            var file = $scope.myFile;
            var uploadUrl = "/savedata";
            fileUpload.uploadFileToUrl(file, uploadUrl);
        };
        ///===========================================
        function dataURItoBlob(dataURI, callback) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var bb = new Blob([ab]);
            return bb;
        }
        function JSONToCSVConvertor2(JSONData,ReportTitle, ShowLabel,subject,dashboardName) {
            //alert("in");
            // var JSONData = [
            //     { "Vehicle": "BMW", "Date": "30, Jul 2013 09:24 AM", "Location": "Hauz Khas, Enclave, New Delhi, Delhi, India", "Speed": 42 }, 
            //     { "Vehicle": "Honda CBR", "Date": "30, Jul 2013 12:00 AM", "Location": "Military Road,  West Bengal 734013,  India", "Speed": 0 }, 
            //     { "Vehicle": "Supra", "Date": "30, Jul 2013 07:53 AM", "Location": "Sec-45, St. Angel's School, Gurgaon, Haryana, India", "Speed": 58 }, 
            // ];
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            // var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            var uri = 'data:text/csv;charset=utf-8,' + btoa(CSV);
            //  btoa(string)
            var block = uri.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1]
            console.log(realData);
            console.log(realData);
            var link = document.createElement("a");
            link.href = uri;
            console.log(link);

            console.log(dataURItoBlob(link.href));

            var fd = new FormData();
            fd.append('fname', 'test.csv');
            fd.append('data', dataURItoBlob(uri));

            ///console.log(fd);

            $scope.uploadInfo = {
                scaleId: 29719,
                fileDate: "23/09/1992"
            };

            var data = $scope.uploadInfo;
            $scope.uploadInfo.fileDate = "23/09/1992";
            $scope.uploadInfo.fileDate = new Date($scope.uploadInfo.fileDate);
            $scope.uploadInfo.fileDate = new Date();
            data.fileDate = $scope.uploadInfo.fileDate.toISOString();


            $scope.uploadAttachDetails(uri,subject,dashboardName);
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
        }

        $scope.uploadAttachDetails = function(file,subject,dashboardName) { 
            var file_obj=dataURItoBlob(file) 
            console.log(file_obj);
            var currData = {};
        
            var fd = new FormData();
            fd.append('file', file_obj);
            fd.append('data', 'string');
            $http.post(config.apiUrl+'/api/upload_mail_attach', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(response){
                    console.log(response);
                    allDashboardService.SendMail($scope.getLoggedInUserdata.otherData.email,subject,dashboardName,'',response.data,function(result){
                        if (result.success === true) {
                            console.log(result);   
                            toastr.success(result.message);      
                        }else{
                            toastr.error(result.message);
                            vm.error = result.message;                    
                        }
                    })
            })
            .error(function(response){
                console.log(response);
            });
        }

        //=============================================
    }
  
})();
  
  