(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.reports', ['ui.select', 'ngSanitize'])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider,$httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
      $stateProvider
        .state('main.reports', {
          url: '/reports',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'Reports',
          sidebarMeta: {
            icon: 'icoReports',
            order: 3,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'OEM Manager', 'Client Admin', 'Client Manager']
          }
        })
        .state('main.reports.vehicle', {
          url: '/vehicle_reports',
          templateUrl: 'app/pages/reports/vehicle/vehicleReport.html',
          controller: "VehicleReportCtrl",
          title: 'Vehicle Reports',
          sidebarMeta: {
            order: 0,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'OEM Admin',  'Client Admin']
          }
        })
        .state('main.reports.tire', {
          url: '/tyre_reports',
          templateUrl: 'app/pages/reports/tire/tireReport.html',
          controller: "TireReportCtrl",
          title: 'Tyre Reports',
          sidebarMeta: {
            order: 1,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'OEM Manager', 'Client Admin', 'Client Manager']
          }
        })
        .state('main.reports.alert', {
          url: '/alerts_reports',
          templateUrl: 'app/pages/reports/alerts/alertsReport.html',
          controller: "AlertReportCtrl",
          title: 'Alerts Reports',
          sidebarMeta: {
            order: 2,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'OEM Admin', 'OEM Manager', 'Client Admin', 'Client Manager']
          }
        })
        .state('main.reports.performance', {
          url: '/performance_reports',
          templateUrl: 'app/pages/reports/performance/performanceReport.html',
          controller: "PerformanceReportCtrl",
          title: 'Performance Reports',
          sidebarMeta: {
            order: 3,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'OEM Manager', 'Client Admin', 'Client Manager']
          }
        })
        .state('main.reports.issue', {
          url: '/issue_reports',
          templateUrl: 'app/pages/reports/issue/issueReport.html',
          controller: "IssueReportCtrl",
          title: 'Issue Reports',
          controllerAs:"issueCtrl",
          sidebarMeta: {
            order: 4,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'OEM Admin', 'OEM Manager', 'Client Admin']
          }
        })
    }
  })();