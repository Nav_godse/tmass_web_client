(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('IssueReportCtrl', IssueReportCtrl);
  
    /** @ngInject */
    function IssueReportCtrl($scope,allDashboardService,$http,vehicleService,issueReportService,toastr, $uibModal,config,localStorage) {
        var vm = this;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        $scope.getLoggedInUserdata = localStorage.getObject('dataUser');
        $scope.vccUser = {}   
        initController();
        $scope.customerIssueDetails = [];
        $scope.customers= [];
        $scope.tagTypes= [];
        $scope.tagTypes= [];
        $scope.customerDepot= [];

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllCustomerDepotDetails();
            setTimeout(function(){
                getAllCustomerTagMasterDetails();
            }, 1000);          
        }

        //to get all Depot details.
        function getAllCustomerDepotDetails() {            
            vehicleService.GetAllVehicleMaster(function (result) {
                $scope.vehicles=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.vehicles.push({value: result.data[i]._id, text: result.data[i].vehicleRegistrationNumber});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }

        //end all loops
        function getAllCustomerTagMasterDetails() {            
            issueReportService.GetAllIssueReport(function (result) {
                $scope.customerIssueDetails=[];
                if (result.success === true) {
                    console.log(result.data);
                    $scope.customerIssueDetails= result.data;    
                   
                } else {
                    $scope.customerIssueDetails=[];
                    toastr.error(result.message);       
                }
                mygridIssueReport();
            });
        };
        
         // For Open Popup for Add and Edit
         $scope.printData=function (){
            var rows = $("#gridIssueReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            $("#gridIssueReport").jqxGrid('exportdata', 'xls', 'Issue Report'+new Date());  
            //alert("test");
         }

         // For Loading the gridIssueReport Widgets
         function mygridIssueReport(){ 
            var source =
            {
                localdata: $scope.customerIssueDetails,
                datatype: "array",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'VehicleNo', type: 'string' },
                    { name: 'vehicleId', type: 'string' },
                    { name: 'TireSerialNo', type: 'string' },
                    { name: 'Photo', type: 'string' },
                    { name: 'IssueTypeId', type: 'string' },
                    { name: 'DepotName', type: 'string' },
                    { name: 'UserId', type: 'string' },
                    { name: 'ReporterName', type: 'string' },
                    { name: 'Description', type: 'string' },
                    { name: 'Status', type: 'string' },
                    { name: 'expenseOccured', type: 'number' },
                    //comments
                    { name: 'comments', type: 'number' },
                    { name: 'ReportingDateTime', type: 'Date' }
                ]
            };
 
            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#gridIssueReport").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                showfilterrow: true,
                filterable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false,exportable:false, draggable: false, resizable: false,export:false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Reporter Name', datafield: 'ReporterName',align: 'center', width: '15%', cellsalign: 'center'},
                    { text: 'Vehicle No', datafield: 'VehicleNo',align: 'center',cellsalign: 'center', width: '10%' },
                    { text: 'Issue Type', datafield: 'IssueTypeId',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'Depot Name', datafield: 'DepotName',align: 'center',cellsalign: 'center', width: '10%' },
                    { text: 'Status', datafield: 'Status',align: 'center', width: '15%', cellsalign: 'center' },
                    { text: 'Description', datafield: 'Description',align: 'center', width: '15%', cellsalign: 'center' },
                    { text: 'Comments',hidden:true,datafield: 'comments',align: 'center', width: '15%', cellsalign: 'center' },
                    { text: 'Expense Occured',hidden:true, datafield: 'expenseOccured',align: 'center', width: '15%', cellsalign: 'center' },
                    { text: 'Reported Date',hidden:true, datafield: 'ReportingDateTime',align: 'center',cellsalign: 'center', width: '20%' },
                    {
                        text: 'Action', cellsAlign: 'center', exportable:false,align: "center",width: '15%',export:false, filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >View All Details </button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
            $("#gridIssueReport").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
            }
         }
         
         // For Edit Details
         function editDetails() { 
             var getselectedrowindexes = $('#gridIssueReport').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#gridIssueReport').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             console.log(selectedRowData);
             $scope.curreIssueDetails=selectedRowData;
             $scope.openModalReportDetails()
             //$("#jqxwindowDepot").jqxWindow('open'); 
         }  
         // For Delete Details 

        var addfilter = function () {
            var filtergroup = new $.jqx.filter();
            var filter_or_operator = 1;
            var filtervalue = 'Beate';
            var filtercondition = 'contains';
            var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
            filtervalue = 'Andrew';
            filtercondition = 'starts_with';
            var filter2 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
            filtergroup.addfilter(filter_or_operator, filter1);
            filtergroup.addfilter(filter_or_operator, filter2);
            // add the filters.
            $("#jqxgrid").jqxGrid('addfilter', 'firstname', filtergroup);
            // apply the filters.
            $("#jqxgrid").jqxGrid('applyfilters');
        }

        $scope.openModalReportDetails = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxIssueReportDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };              
                $scope.viewDetails = function() {  
                    var currData = {};
                    //GetallTagbyTagType();
                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    //$scope.clearData();
                    $("#gridIssueReport").jqxGrid('clearselection');
                    $scope.vccUser = {};
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
         };
         $scope.clearData = function(){ //window.location.reload();
            $("#gridIssueReport").jqxGrid('clearselection');
            $("#gridIssueReport").jqxGrid('clearfilters');
            $scope.vccUser = {}            
         }
         $scope.clearData1 = function(){ //window.location.reload();
            //$("#gridIssueReport").jqxGrid('clearselection');
            $("#gridIssueReport").jqxGrid('clearfilters');
            //$scope.vccUser = {}            
         }
        //for filter
        $('#generateReport').bind('click', function (event) {
            var searchText = $("#IssueType").val();
            var searchVehicleGroup = $("#reporterName").val();
            if (searchText.length > 0 || searchVehicleGroup.length > 0 ) {
                var searchTextfiltergroup = new $.jqx.filter();
                var filtervalue = searchText;
                var filtercondition = 'contains';
                var filter = searchTextfiltergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // used when there are multiple filters on a grid column:
                var filter_or_operator = 1;
                // used when there are multiple filters on the grid:
                //searchTextfiltergroup.operator = 'or';
                searchTextfiltergroup.addfilter(filter_or_operator, filter);
                //vehiclegroup
                var searchVehicleGroupfiltergroup = new $.jqx.filter();
                var filtervalue = searchVehicleGroup;
                var filtercondition = 'contains';
                var filter = searchVehicleGroupfiltergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // used when there are multiple filters on a grid column:
                var filter_or_operator = 1;
                // used when there are multiple filters on the grid:
                //searchVehicleGroupfiltergroup.operator = 'or';
                searchVehicleGroupfiltergroup.addfilter(filter_or_operator, filter);
                $("#gridIssueReport").jqxGrid('addfilter', 'IssueTypeId', searchTextfiltergroup);
                $("#gridIssueReport").jqxGrid('addfilter', 'ReporterName', searchVehicleGroupfiltergroup);
                // apply the filters.
                $("#gridIssueReport").jqxGrid('applyfilters');
            }
            else {
                $("#gridIssueReport").jqxGrid('clearfilters');
            }
            $scope.filteredData =$("#gridIssueReport").jqxGrid("getrows");
        });

        //create table 
        function buildTableBody(data, columns) {
            var body = [];
            body.push(columns);
            data.forEach(function(row) {
                var dataRow = [];

                columns.forEach(function(column) {
                    dataRow.push(row[column].toString());
                })

                body.push(dataRow);
            });

            return body;
        }
        //get table body
        function table(data, columns) {
            console.log(data, columns);
            return {
                table: {
                    headerRows: 1,
                    widths: ['25%', '25%', '25%','25%', '25%','25%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        $scope.sendMail = function()
        {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            var rows = $("#gridIssueReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            // get the date as a string
            var n = date.toDateString();
                dashheader='Issue Report \n';
                headerArr=['Reporter Name', 'Vehicle No','Issue Type', 'Depot Name','Status','Description'];
                for(var i=0;i<rows.length;i++){
                    var a={ 
                        'Reporter Name': rows[i].ReporterName, 
                        'Vehicle No':rows[i].VehicleNo ,
                        'Issue Type':rows[i].IssueTypeId,
                        'Depot Name':rows[i].DepotName,    
                        'Status':rows[i].Status,    
                        'Description':rows[i].Description     
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Tire Report", true,"Report : Tire Report",dashheader);
        }
        
        $scope.uploadFile = function(){
            var file = $scope.myFile;
            var uploadUrl = "/savedata";
            fileUpload.uploadFileToUrl(file, uploadUrl);
        };
        ///===========================================
        function dataURItoBlob(dataURI, callback) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var bb = new Blob([ab]);
            return bb;
        }
        function JSONToCSVConvertor2(JSONData,ReportTitle, ShowLabel,subject,dashboardName) {
            //alert("in");
            // var JSONData = [
            //     { "Vehicle": "BMW", "Date": "30, Jul 2013 09:24 AM", "Location": "Hauz Khas, Enclave, New Delhi, Delhi, India", "Speed": 42 }, 
            //     { "Vehicle": "Honda CBR", "Date": "30, Jul 2013 12:00 AM", "Location": "Military Road,  West Bengal 734013,  India", "Speed": 0 }, 
            //     { "Vehicle": "Supra", "Date": "30, Jul 2013 07:53 AM", "Location": "Sec-45, St. Angel's School, Gurgaon, Haryana, India", "Speed": 58 }, 
            // ];
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            // var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            var uri = 'data:text/csv;charset=utf-8,' + btoa(CSV);
            //  btoa(string)
            var block = uri.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1]
            console.log(realData);
            console.log(realData);
            var link = document.createElement("a");
            link.href = uri;
            console.log(link);

            console.log(dataURItoBlob(link.href));

            var fd = new FormData();
            fd.append('fname', 'test.csv');
            fd.append('data', dataURItoBlob(uri));

            ///console.log(fd);

            $scope.uploadInfo = {
                scaleId: 29719,
                fileDate: "23/09/1992"
            };

            var data = $scope.uploadInfo;
            $scope.uploadInfo.fileDate = "23/09/1992";
            $scope.uploadInfo.fileDate = new Date($scope.uploadInfo.fileDate);
            $scope.uploadInfo.fileDate = new Date();
            data.fileDate = $scope.uploadInfo.fileDate.toISOString();


            $scope.uploadAttachDetails(uri,subject,dashboardName);
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
        }
        
        $scope.uploadAttachDetails = function(file,subject,dashboardName) { 
            var file_obj=dataURItoBlob(file) 
            console.log(file_obj);
            var currData = {};
        
            var fd = new FormData();
            fd.append('file', file_obj);
            fd.append('data', 'string');
            $http.post(config.apiUrl+'/api/upload_mail_attach', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(response){
                    console.log(response);
                    allDashboardService.SendMail($scope.getLoggedInUserdata.otherData.email,subject,dashboardName,'',response.data,function(result){
                        if (result.success === true) {
                            console.log(result);   
                            toastr.success(result.message);      
                        }else{
                            toastr.error(result.message);
                            vm.error = result.message;                    
                        }
                    })
            })
            .error(function(response){
                console.log(response);
            });
        }
    }
  
})();
  
  