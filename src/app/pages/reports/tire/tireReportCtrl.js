(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireReportCtrl', TireReportCtrl);
  
    /** @ngInject */
    function TireReportCtrl($scope,allDashboardService,tireService,localStorage,customerService,tagService,vehicleService,allReportService,toastr, $uibModal,$http,config) {
        
        var vm = this;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        
        vm._id='';
        $scope.vccUser = {}   
        $scope.getLoggedInUserdata = localStorage.getObject('dataUser');
        initController();
        
        $scope.customerAlertDetails = [];
        $scope.customers= [];
        $scope.tagTypes= [];
        $scope.tagTypes= [];
        $scope.customerDepot= [];
        $scope.currTire = [];
        $scope.currUsers=[];
        $scope.myTireData = [];
        $scope.allTiresData=[];
        $scope.grid1Obj = {};
        $scope.grid2Obj = {};
        $scope.grid1arr = [];
        $scope.grid2arr = [];
        
        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllTireReports();      
            getAllTireData();
            getmanufactureandmodelandtype();
        }

        //to get all Depot details.
        function getAllCustomerDepotDetails() {            
            vehicleService.GetAllVehicleMaster(function (result) {
                $scope.vehicles=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.vehicles.push({value: result.data[i]._id, text: result.data[i].vehicleRegistrationNumber});    
                    } 
                } else {
                    toastr.error(result.message);   
                }
            })
        }

        function getAllTireData() {
            allReportService.ShowAllTiresReport(function(result){
                $scope.allTiresData=[];
                if (result.success === true) {  
                    $scope.allTiresData = result.data;
                    //console.log('$scope.allTiresData');
                    //console.log($scope.allTiresData);
                    // for(var i=0;i<result.data.length;i++){
                    // } 
                } else {
                    toastr.error(result.message);   
                }
            })
        }

        // For Open Popup for Add and Edit
        $scope.printData=function (){
            var rows = $("#gridTireReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            $("#gridTireReport").jqxGrid('exportdata', 'xls', 'Tire Report'+new Date());  
        }

        function getmanufactureandmodelandtype(){            
            tireService.GetallTireModel(function (result) {
                $scope.arrManufacturerandmodel = [];
                if (result.success === true) {
                    //myGrid();
                    //console.log(result);
                    for(var i=0;i<result.data.length;i++){
                        $scope.arrManufacturerandmodel.push(result.data[i])
                    }
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
            tireService.GetAllTireType(function (result) {
                $scope.arrtireType = []
                if (result.success === true) {
                    //myGrid();
                    for(var i=0;i<result.data.length;i++){
                        $scope.arrtireType.push(result.data[i])
                    }
                    //console.log(result);
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        }
        
        function getAllTireReports() {
            allReportService.ShowAllTiresReport(function(result){
                if (result.success === true) {  
                        console.log(result);
                        $scope.currTire = result.data; //all tire data
                        console.log($scope.currTire);
                        $scope.currUsers = result.myusers;
                        var custId ='';
                        for(var i=0;i<result.data.length;i++){
                            
                            if($scope.getLoggedInUserdata.otherData.roleName=="Client Admin" || $scope.getLoggedInUserdata.otherData.roleName=="OEM Admin"){
                                $scope.grid1Obj={
                                    "customerId":result.data[i]._id.customerId,
                                    "tireTypeName" : result.data[i]._id.tireTypeName,
                                    "tireManufacturerName" : result.data[i]._id.tireManufacturerName[0],
                                    "tireModelNo" : result.data[i]._id.tireModelNo[0],
                                    "count" : result.data[i].count,
                                }
                            }else{
                                $scope.grid1Obj={
                                    "tireTypeName" : result.data[i]._id.tireTypeName,
                                    "tireManufacturerName" : result.data[i]._id.tireManufacturerName[0],
                                    "tireModelNo" : result.data[i]._id.tireModelNo[0],
                                    "count" : result.data[i].count,
                                }
                            }
                            $scope.grid1arr.push($scope.grid1Obj)
                        }
                        console.log($scope.grid1arr);
                    
                } else {
                    $scope.grid1arr=[];
                    toastr.error(result.message);       
                }
                mygridAlertReport();
            })
        }
        //end all loops
         // For Loading the gridAlertReport Widgets
         function mygridAlertReport(){ 
            var source =
            {
                localdata: $scope.grid1arr,
                datatype: "array",
                datafields:
                [
                    { name: 'customerId', type: 'string' },
                    { name: 'tireTypeName', type: 'string' },
                    { name: 'tireManufacturerName', type: 'string' },
                    { name: 'tireModelNo', type: 'string' },
                    { name: 'count', type: 'number' }
                ]
            };
 
            var dataAdapter = new $.jqx.dataAdapter(source);    
            var editrow = -1;
     
            $("#gridTireReport").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                showfilterrow: true,
                filterable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,exportable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'customerId',hidden:true, datafield: 'customerId',align: 'center', width: '20%', cellsalign: 'center' },
                    { text: 'Tire Type', datafield: 'tireTypeName',align: 'center', width: '20%', cellsalign: 'center' },
                    { text: 'Tire Manufacturer', datafield: 'tireManufacturerName',align: 'center',cellsalign: 'center', width: '30%' },
                    { text: 'Tire Model', datafield: 'tireModelNo',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Tire Count', datafield: 'count',align: 'center',cellsalign: 'center', width: '10%' },
                    // { text: 'Action',cellsalign: 'center',align: 'center', datafield: 'Edit',sortable: false, filterable: false, editable: false,
                    // groupable: false, draggable: false, resizable: false,exportable: false, columntype: 'button',width: '15%', cellsrenderer: function () {
                    //     return "View Details";
                    //  }, buttonclick: function (row) {
                    //     editDetails();
                    // }
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '15%',exportable:false, filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >View Details </button> </div>';
                    }
                    }   
                ]
            });
            // Initlize Button Event
            buttonsEvents();
            $("#gridTireReport").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }
         buttonsEvents();
        
         function buttonsEvents() 
         {            
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
         }
         // For Edit Details
         function editDetails() { 
             //alert("hit");
             $scope.myTireData=[];
             $scope.grid2arr=[];
             var getselectedrowindexes = $('#gridTireReport').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#gridTireReport').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             console.log(selectedRowData);
             console.log($scope.currTire);
             if($scope.getLoggedInUserdata.otherData.roleName=="Client Admin" || $scope.getLoggedInUserdata.otherData.roleName=="OEM Admin"){
                for(var q=0;q<$scope.currTire.length;q++){                
                    console.log($scope.currTire[q]._id);
                    //for(var r=0;r<$scope.currUsers.length;r++){         
                        if($scope.currTire[q]._id.customerId==selectedRowData.customerId && $scope.currTire[q]._id.tireTypeName==selectedRowData.tireTypeName && $scope.currTire[q]._id.tireManufacturerName==selectedRowData.tireManufacturerName && $scope.currTire[q]._id.tireModelNo==selectedRowData.tireModelNo)
                        { 
                            for(var w=0;w<$scope.currTire[q].tireData.length;w++){
                                $scope.grid2arr.push($scope.currTire[q].tireData[w]);
                            } 
                        }     
                    //}          
                }
             }else{
                for(var q=0;q<$scope.currTire.length;q++){                
                    console.log($scope.currTire[q]._id);
                   if($scope.currTire[q]._id.tireTypeName==selectedRowData.tireTypeName && $scope.currTire[q]._id.tireManufacturerName==selectedRowData.tireManufacturerName && $scope.currTire[q]._id.tireModelNo==selectedRowData.tireModelNo)
                   { 
                       for(var w=0;w<$scope.currTire[q].tireData.length;w++){
                           $scope.grid2arr.push($scope.currTire[q].tireData[w]);
                       } 
                   }               
                }
             }

             console.log($scope.grid2arr);
             $scope.currTireData = selectedRowData;             
            $scope.openModalReportDetails();
         }  
         // For Delete Details 

         $scope.clearData = function(){ 
            //$("#gridTireReport").jqxGrid('clearfilters');
            $("#gridTireReport").jqxGrid('clearselection');
            $scope.grid2arr=[];
         }
        $scope.clearData1 = function(){ 
            $("#gridTireReport").jqxGrid('clearfilters');
            // $("#gridTireReport").jqxGrid('clearselection');
            //$scope.grid2arr=[];
            $scope.vccUser.tireType = null;
         }

         $scope.openModalReportDetails = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxtireReportDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };              
                $scope.viewDetails = function() {  
                    var currData = {};
                    //GetallTagbyTagType();
                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                    // $("#gridTireReport").jqxGrid('clearfilters');
                    //$("#gridTireReport").jqxGrid('clearselection');
                    // $scope.grid2arr=[];
                    //location.reload();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        //for filter
        $('#generateReport').bind('click', function (event) {
            var searchText = $("#tireType").val();
            if (searchText.length > 0) {
                var filtergroup = new $.jqx.filter();
                var filtervalue = searchText;
                var filtercondition = 'contains';
                var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                // used when there are multiple filters on a grid column:
                var filter_or_operator = 1;
                // used when there are multiple filters on the grid:
                //filtergroup.operator = 'or';
                filtergroup.addfilter(filter_or_operator, filter);
                $("#gridTireReport").jqxGrid('addfilter', 'tireTypeName', filtergroup);
                //$("#gridTireReport").jqxGrid('addfilter', 'lastname', filtergroup);
                // apply the filters.
                $("#gridTireReport").jqxGrid('applyfilters');
            }
            else {
                $("#gridTireReport").jqxGrid('clearfilters');
            }
            $scope.filteredData =$("#gridTireReport").jqxGrid("getrows");
        });

        //create table 
        function buildTableBody(data, columns) {
            var body = [];

            body.push(columns);

            data.forEach(function(row) {
                var dataRow = [];

                columns.forEach(function(column) {
                    dataRow.push(row[column].toString());
                })

                body.push(dataRow);
            });

            return body;
        }
        //get table body
        function table(data, columns) {
            console.log(data, columns);
            return {
                table: {
                    headerRows: 1,
                    widths: ['25%', '25%', '25%','25%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        $scope.sendMail = function()
        {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var rows = $("#gridTireReport").jqxGrid("getrows");
            if(Object.keys(rows).length==0){
                alertify.alert("No data present");
                return
            }
            var n = date.toDateString();
                dashheader='Tyres Report \n';
                headerArr=['Tyre Type', 'Tyre Manufacturer','Tyre Model', 'Tyre Count'];
                for(var i=0;i<rows.length;i++){
                    var a={ 
                        'Tyre Type': rows[i].tireTypeName, 
                        'Tyre Manufacturer':rows[i].tireManufacturerName ,
                        'Tyres Model':rows[i].tireModelNo,
                        'Tyre Count':rows[i].count,                        
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                // var dd = {
                //     content: [
                //         { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                //         { text: 'Date: '+n,bold: true, alignment:'left' },
                //         { text: '\n'},
                //         table(externalDataRetrievedFromServer, headerArr)                        
                //     ]
                // } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Tire Report", true,"Report : Tire Report",dashheader);
        }
        
        $scope.uploadFile = function(){
            var file = $scope.myFile;
            var uploadUrl = "/savedata";
            fileUpload.uploadFileToUrl(file, uploadUrl);
        };
        ///===========================================
        function dataURItoBlob(dataURI, callback) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var bb = new Blob([ab]);
            return bb;
        }
        function JSONToCSVConvertor2(JSONData,ReportTitle, ShowLabel,subject,dashboardName) {
            //alert("in");
            // var JSONData = [
            //     { "Vehicle": "BMW", "Date": "30, Jul 2013 09:24 AM", "Location": "Hauz Khas, Enclave, New Delhi, Delhi, India", "Speed": 42 }, 
            //     { "Vehicle": "Honda CBR", "Date": "30, Jul 2013 12:00 AM", "Location": "Military Road,  West Bengal 734013,  India", "Speed": 0 }, 
            //     { "Vehicle": "Supra", "Date": "30, Jul 2013 07:53 AM", "Location": "Sec-45, St. Angel's School, Gurgaon, Haryana, India", "Speed": 58 }, 
            // ];
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            // var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            var uri = 'data:text/csv;charset=utf-8,' + btoa(CSV);
            //  btoa(string)
            var block = uri.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1]
            console.log(realData);
            console.log(realData);
            var link = document.createElement("a");
            link.href = uri;
            console.log(link);

            console.log(dataURItoBlob(link.href));

            var fd = new FormData();
            fd.append('fname', 'test.csv');
            fd.append('data', dataURItoBlob(uri));

            ///console.log(fd);

            $scope.uploadInfo = {
                scaleId: 29719,
                fileDate: "23/09/1992"
            };

            var data = $scope.uploadInfo;
            $scope.uploadInfo.fileDate = "23/09/1992";
            $scope.uploadInfo.fileDate = new Date($scope.uploadInfo.fileDate);
            $scope.uploadInfo.fileDate = new Date();
            data.fileDate = $scope.uploadInfo.fileDate.toISOString();


            $scope.uploadAttachDetails(uri,subject,dashboardName);
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
        }

        $scope.uploadAttachDetails = function(file,subject,dashboardName) { 
            var file_obj=dataURItoBlob(file) 
            console.log(file_obj);
            var currData = {};
        
            var fd = new FormData();
            fd.append('file', file_obj);
            fd.append('data', 'string');
            $http.post(config.apiUrl+'/api/upload_mail_attach', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(response){
                    console.log(response);
                    allDashboardService.SendMail($scope.getLoggedInUserdata.otherData.email,subject,dashboardName,'',response.data,function(result){
                        if (result.success === true) {
                            console.log(result);   
                            toastr.success(result.message);      
                        }else{
                            toastr.error(result.message);
                            vm.error = result.message;                    
                        }
                    })
            })
            .error(function(response){
                console.log(response);
            });
        }

        //=============================================
    }
  
})();
  
  