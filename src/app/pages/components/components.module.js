/**
 * @author k.danovsky
 * created on 15.01.2016
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.components', [
      'TmassAdmin.pages.components.mail',
      'TmassAdmin.pages.components.timeline',
      'TmassAdmin.pages.components.tree',
    ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.components', {
        url: '/components',
        template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Components',
        sidebarMeta: {
          icon: 'ion-gear-a',
          order: 100,
        },
        authenticate: true
      });
  }

})();