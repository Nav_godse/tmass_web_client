(function() {
  'use strict';

  angular.module('TmassAdmin.pages.config')
    .run(stateChangeStart);

  /** @ngInject */
  function stateChangeStart($rootScope, $state, localStorage) {
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        var login = localStorage.getObject('dataUser');
        console.log(toState.name);
        console.log("is empty: ", _.isEmpty(login));
        
        if (toState.authenticate && _.isEmpty(login)) {
          // User isn’t authenticated
          console.log("in if");
          $state.transitionTo("authSignIn");
          event.preventDefault();
        }
        else if(toState.name=="authSignIn" && ! _.isEmpty(login))
        {
          console.log("login indide transitionsdoicnsdiofnv");
          console.log(login);
          $state.transitionTo("main.home");
          event.preventDefault();
        }
    });
  }

})();