/**
 * @author Nav
 * created on 16.3.2018
 */
(function () {
  'use strict';

  angular.module('TmassAdmin.pages.profile')
    .controller('ProfilePageCtrl', ProfilePageCtrl);

  /** @ngInject */
  function ProfilePageCtrl($scope, fileReader, $filter, $uibModal,userService,customerService,toastr,$window,animation,localStorage) {
    
    var vm = this;

    vm.getUserProfileData = getUserProfileData;    
    init();
    vm.curr_user = {};
    $scope.custData={};

    function init() {
      $scope.currloggedinUser = localStorage.getObject('dataUser');
      vm.curr_user = localStorage.getObject('dataUser');
      $scope.isCustomer= $scope.currloggedinUser.otherData.isCustomer;
      vm.my_userData = vm.curr_user;
      vm.usernameorEmail = vm.my_userData.usernameoremail;
      getUserProfileData();
    }

    function updateCustomerById(data) { 
      console.log("data",data)       
      animation.myFunc('profileform');
      customerService.updateCustomerProfile($scope.custData,function (result) {
          if (result.success === true) {
              toastr.success(result.message);
              $("#profileform").waitMe('hide');
          } else {
              $("#profileform").waitMe('hide');
              if(result.message=='Customer Added, Verification link sending failed!'){
                  //$scope.isValidate = true;
                  //vm.modalInstance.close()
              }else{
                  $scope.isValidate = false;
              }
              toastr.error(result.message); 
              vm.error = result.message;                    
          }
      });
    };

    $scope.updateProfile = function () {
      //vm.check();
      console.log(vm);
      console.log("$scope.profileVmtest")
      console.log($scope.profileVm)
      $scope.custData = {
        _id: $scope.currloggedinUser.otherData._id,
        customerName: $scope.profileVm.name,
        addressLine1: $scope.profileVm.addressLine1,
        addressLine2: $scope.profileVm.addressLine2,
        pincode: $scope.profileVm.pincode,
        cityId: $scope.profileVm.cityId,
        stateId: $scope.profileVm.stateId,
        country: $scope.profileVm.country,
        createdByCustomerId:$scope.currloggedinUser.otherData.createdByCustomerId,
        customerTypeId: $scope.currloggedinUser.otherData.customerTypeId,
        username: $scope.profileVm.email,
        name: $scope.profileVm.name,
        email:$scope.profileVm.email,
        password: $scope.profileVm.newPass,
        isloginNew:"false",
        isEnable:"true"
      }      
      console.log($scope.custData, $scope.currloggedinUser.otherData._id)
      //return
      // updateuserProfile
      if($scope.currloggedinUser.otherData.roleName=="System Admin" || $scope.currloggedinUser.otherData.roleName=="System Super Admin" || $scope.currloggedinUser.otherData.roleName=="Client Admin" || $scope.currloggedinUser.otherData.roleName=="OEM Admin" ){
        
        if(($scope.profileVm.confirmnewPass!=undefined || $scope.profileVm.confirmnewPass!="") && ($scope.profileVm.newPass!=undefined || $scope.profileVm.newPass!="") && $scope.profileVm.confirmnewPass!=$scope.profileVm.newPass){
            alertify.alert("Invalid Password");
            return
        }else if($scope.profileVm.cityId==''){
          alertify.alert("Invalid City Name");
          return
        }else if($scope.profileVm.stateId==''){
          alertify.alert("Invalid State");
          return
        }else if($scope.profileVm.country==''){
          alertify.alert("Invalid country");
          return
        }else if($scope.profileVm.pincode!='' && ($("#inputpincode").val().length<6 || $("#inputpincode").val().length>6)){
          alertify.alert("Invalid Pincode");
          return
        }
        updateCustomerById($scope.custData);
      }else{
        if((parseInt($("#mobile").val()))<=0 || ($("#mobile").val().length)!=10){
          alertify.alert("Invalid Mobile No");
          return
        }else if(($scope.profileVm.confirmnewPass!=undefined || $scope.profileVm.confirmnewPass!="") && ($scope.profileVm.newPass!=undefined || $scope.profileVm.newPass!="") && $scope.profileVm.confirmnewPass!=$scope.profileVm.newPass){
          alertify.alert("Invalid Password");
          return
        }
        updateUserDetails($scope.currloggedinUser.otherData._id, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.roleId, $scope.profileVm.newPass, $scope.profileVm.email, $scope.currloggedinUser.otherData.alternativeEmail, $scope.profileVm.mobile, $scope.currloggedinUser.otherData.depotId, $scope.currloggedinUser.otherData.userPushToken,false,false)
        
      }
    };
    function updateUserDetails() {
      vm.userPushToken = null;       
      vm.newEmail=false;
      if($scope.orgEmail!=vm.email){
          vm.newEmail== true;
      }

      animation.myFunc('profileform');
      userService.updateUserProfile($scope.currloggedinUser.otherData._id, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.roleId, $scope.profileVm.newPass, $scope.profileVm.email, $scope.currloggedinUser.otherData.alternativeEmail, $scope.profileVm.mobile, $scope.currloggedinUser.otherData.depotId, $scope.currloggedinUser.otherData.userPushToken,false, function (result) {
          if (result.success === true) {
            $("#profileform").waitMe('hide');
            toastr.success(result.message);
          } else {
            $("#profileform").waitMe('hide');
            toastr.error(result.message);       
          }
      });
    };
    function getUserProfileData() {
      if(localStorage.datauser)
        console.log(localStorage.getObject('dataUser'));
        console.log($scope.currloggedinUser.otherData.email, $scope.currloggedinUser.otherData.roleName);

        if($scope.isCustomer){
          $scope.profileVm={
            // email:$scope.currloggedinUser.otherData.email,
            // mobile:$scope.currloggedinUser.otherData.mobile,
            name:$scope.currloggedinUser.otherData.email,
            username:$scope.currloggedinUser.otherData.email,
            // roleName: $scope.currloggedinUser.otherData.roleName,
            addedOn: $scope.currloggedinUser.otherData.addedOn,
            addressLine1: $scope.currloggedinUser.otherData.addressLine1,
            addressLine2: $scope.currloggedinUser.otherData.addressLine2,
            cityId: $scope.currloggedinUser.otherData.cityId,
            country: $scope.currloggedinUser.otherData.country,
            createdByCustomerId: $scope.currloggedinUser.otherData.createdByCustomerId,
            customerName: $scope.currloggedinUser.otherData.customerName,
            customerTypeId: $scope.currloggedinUser.otherData.customerTypeId,
            customerTypeName: $scope.currloggedinUser.otherData.customerTypeName,
            email: $scope.currloggedinUser.otherData.email,
            isCustomer: $scope.currloggedinUser.otherData.isCustomer,
            isEnable: $scope.currloggedinUser.otherData.isEnable,
            pincode: $scope.currloggedinUser.otherData.pincode,
            roleId: $scope.currloggedinUser.otherData.roleId,
            roleLevel: $scope.currloggedinUser.otherData.roleLevel,
            roleName: $scope.currloggedinUser.otherData.roleName,
            stateId: $scope.currloggedinUser.otherData.stateId,
            id: $scope.currloggedinUser.otherData.id
          }
        }else{
          $scope.profileVm={
            email:$scope.currloggedinUser.otherData.email,
            mobile:$scope.currloggedinUser.otherData.mobile,
            name:$scope.currloggedinUser.otherData.email,
            username:$scope.currloggedinUser.otherData.email,
            roleName: $scope.currloggedinUser.otherData.roleName
          }
        }
        
        //$scope.isCustomer= false;
    };

    $scope.picture = $filter('profilePicture')('user');

    $scope.removePicture = function () {
      $scope.picture = $filter('appImage')('theme/no-photo.png');
      $scope.noPicture = true;
    };

    $scope.uploadPicture = function () {
      var fileInput = document.getElementById('uploadFile');
      fileInput.click();
    };

    $scope.unconnect = function (item) {
      item.href = undefined;
    };

    $scope.showModal = function (item) {
      $uibModal.open({
        animation: false,
        controller: 'ProfileModalCtrl',
        templateUrl: 'app/pages/profile/profileModal.html'
      }).result.then(function (link) {
          item.href = link;
        });
    };

    $scope.getFile = function () {
        fileReader.readAsDataUrl($scope.file, $scope)
        .then(function (result) {
            $scope.picture = result;
        });
    };

    vm.check=function(){
      // var pass1 = document.getElementById('mobile');
      // var message = document.getElementById('message');
      //   if(mobile.value.length!=10){
      //       message.innerHTML = "required 10 digits, match requested format!";
      //       return
      //   }else{
      //     if($scope.currloggedinUser.otherData.roleName=="System Admin" || $scope.currloggedinUser.otherData.roleName=="System Super Admin" || $scope.currloggedinUser.otherData.roleName=="Client Admin" || $scope.currloggedinUser.otherData.roleName=="System Manager" ){
      //       updateCustomerById($scope.custData);
      //     }else{
      //       updateUserDetails($scope.currloggedinUser.otherData._id, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.roleId, $scope.profileVm.newPass, $scope.currloggedinUser.otherData.email, $scope.currloggedinUser.otherData.alternativeEmail, $scope.currloggedinUser.otherData.mobile, $scope.currloggedinUser.otherData.depotId, $scope.currloggedinUser.otherData.userPushToken,false)
      //     }
      //   }
      // if($scope.currloggedinUser.otherData.roleName=="System Admin" || $scope.currloggedinUser.otherData.roleName=="System Super Admin" || $scope.currloggedinUser.otherData.roleName=="Client Admin" || $scope.currloggedinUser.otherData.roleName=="System Manager" ){
      //   updateCustomerById($scope.custData);
      // }else{
      //   updateUserDetails($scope.currloggedinUser.otherData._id, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.username, $scope.currloggedinUser.otherData.roleId, $scope.profileVm.newPass, $scope.currloggedinUser.otherData.email, $scope.currloggedinUser.otherData.alternativeEmail, $scope.currloggedinUser.otherData.mobile, $scope.currloggedinUser.otherData.depotId, $scope.currloggedinUser.otherData.userPushToken,false)
      // }
    }

  }

})();
