/**
 * @author Nav
 * created on 16.3.2018
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.profile', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.profile', {
        url: '/profile',
        title: 'Profile',
        templateUrl: 'app/pages/profile/profile.html',
        controller: 'ProfilePageCtrl',
        authenticate: true
      });
  }

})();