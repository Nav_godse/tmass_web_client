(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.forgotPassword')
      .controller('forgotPasswordCtrl', forgotPasswordCtrl);
  
    /** @ngInject */
    function forgotPasswordCtrl($scope,AuthenticationService,localStorage,toastr,$state) {
      var vm = this;

      vm.forgotpass = forgotpass;
  
      init();
  
      function init() {
        //localStorage.clear();
      }
  
      function forgotpass() {
          console.log(vm);
         // $state.go('resetPassword', {'user': vm.user});
          if(vm.user==undefined && vm.mobile==undefined){
              toastr.error("Please enter valid Username or Mobile");
              return ;  
          }
        AuthenticationService.Forgot(vm.user, vm.mobile, function (result) {
          console.log(vm.mobile, vm.user);
            if (result.success == true) {
              var forgetData = { usernameoremail: vm.user, mobile: vm.mobile };
              localStorage.setObject('forgetData', forgetData);
              toastr.success(result.message);
              $state.go('resetPassword');
            } else {
                //console.log(result);
                toastr.error(result.message);
                return
            }
        });
      };
  
    }
  
  })();