(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.forgotPassword', [])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('forgotPassword', {
          url: '/forgotPassword',
          templateUrl: 'app/pages/forgotPassword/forgotPassword.html',
          title: 'Forgot Password',
          controller: 'forgotPasswordCtrl',
          sidebarMeta: {
            order: 800,
          },
          authenticate: false
        });
    }
  
  })();