(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('tubeService', tubeService);
  
    /** @ngInject */
    function tubeService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {

        // Customer Tube Tag Allocates To Depot
        getallCustomerTubeTagAllocatesToDepot:'/api/getallCustomerTubeTagAllocatesToDepot',
        addCustTubeTagAllocatesToDepot: '/api/addCustTubeTagAllocatesToDepot',
        deleteCustTubeTagAllocatesToDepotById: '/api/deleteCustTubeTagAllocatesToDepotById/',
        updateCustTubeTagAllocatesToDepotById: '/api/updateCustTubeTagAllocatesToDepotById/',
        
        // customer Assigned Tube TagId Details
        getCustAssignedTubeTagIdDetails:'/api/getCustAssignedTubeTagIdDetails',
        addCustAssignedTubeTagIdDetails: '/api/addCustAssignedTubeTagIdDetails',
        deleteCustAssignedTubeTagIdDetailsById: '/api/deleteCustAssignedTubeTagIdDetailsById/',
        updateCustAssignedTubeTagIdDetailsById: '/api/updateCustAssignedTubeTagIdDetailsById/',           
        //Tube Qty Assignment Details
        getAllTubeQtyAssignDetail:'/api/getAllTubeQtyAssignDetail',
        addTubeQtyAssignDetail: '/api/addTubeQtyAssignDetail',
        deleteTubeQtyAssignDetailById: '/api/deleteTubeQtyAssignDetailById/',
        updateTubeQtyAssignDetailById: '/api/updateTubeQtyAssignDetailById/',
        //Tube Qty Assignment
        getAllTubeQtyAssignment:'/api/getAllTubeQtyAssignment',
        addTubeQtyAssignment: '/api/addTubeQtyAssignment',
        deleteTubeQtyAssignmentById: '/api/deleteTubeQtyAssignmentById/',
        updateTubeQtyAssignmentById: '/api/updateTubeQtyAssignmentById/',
        //Tube type
        getalltubetype:'/api/getallTubeType',
        addtubetype: '/api/addTubeType',
        deletetubetypeById: '/api/deleteTubeType/',
        updatetubeTypeById: '/api/updateTubeType/',
        //Tube Size
        getalltubesize:'/api/getallTubeSize',
        addtubesize: '/api/addTubeSize',
        deletetubesizeById: '/api/deleteTubeSizeById/',
        updatetubesizeById: '/api/updateTubeSizeById/',
        //Tube Manufacturer
        getalltubemanufacturer:'/api/getallTubeManufacturer',
        addtubemanufacturer: '/api/addTubeManufacturer',
        deletetubemanufacturerById: '/api/deleteTubeManufacturerById/',
        updatetubemanufacturerById: '/api/updateTubeManufacturerById/',
        //Tube Details
        getallTubeMaster:'/api/getallTubeMaster',
        addTubeMaster: '/api/addTubeMaster',
        deleteTubeMasterById: '/api/deleteTubeMasterById/',
        updateTubeMasterById: '/api/updateTubeMasterById/'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }
      
      var service = {

            // customer Customer Tube Tag Allocates To Depot
            GetallCustTubeTagAllocatesToDepot: GetallCustTubeTagAllocatesToDepot,
            DeleteCustTubeTagAllocatesToDepot: DeleteCustTubeTagAllocatesToDepot,
            AddCustTubeTagAllocatesToDepot: AddCustTubeTagAllocatesToDepot,
            UpdateCustTubeTagAllocatesToDepot: UpdateCustTubeTagAllocatesToDepot,
            // customer Assigned Tube TagId Details
            GetCustAssignedTubeTagIdDetail: GetCustAssignedTubeTagIdDetail,
            DeleteCustAssignedTubeTagIdDetail: DeleteCustAssignedTubeTagIdDetail,
            AddCustAssignedTubeTagIdDetail: AddCustAssignedTubeTagIdDetail,
            UpdateCustAssignedTubeTagIdDetail: UpdateCustAssignedTubeTagIdDetail,
            // Tube Qty Assignment Details
            GetAllTubeQtyAssignDetail: GetAllTubeQtyAssignDetail,
            DeleteTubeQtyAssignDetail: DeleteTubeQtyAssignDetail,
            AddTubeQtyAssignDetail: AddTubeQtyAssignDetail,
            UpdateTubeQtyAssignDetail: UpdateTubeQtyAssignDetail,
            // Tube Qty Assignment
            GetAllTubeQtyAssignment: GetAllTubeQtyAssignment,
            DeleteTubeQtyAssignment: DeleteTubeQtyAssignment,
            AddTubeQtyAssignment: AddTubeQtyAssignment,
            UpdateTubeQtyAssignment: UpdateTubeQtyAssignment,
            //Tube type
            GetAllTubeType: GetAllTubeType,
            DeleteTubeType: DeleteTubeType,
            AddTubeType: AddTubeType,
            UpdateTubeType: UpdateTubeType,
            // Tube Size
            GetAllTubeSize: GetAllTubeSize,
            DeleteTubeSize: DeleteTubeSize,
            AddTubeSize: AddTubeSize,
            UpdateTubeSize: UpdateTubeSize,
            // Tube Manufacturer
            GetAllTubeManufacturer: GetAllTubeManufacturer,
            DeleteTubeManufacturer: DeleteTubeManufacturer,
            AddTubeManufacturer: AddTubeManufacturer,
            UpdateTubeManufacturer: UpdateTubeManufacturer,
            // Tube Details
            GetAllTubeDetails: GetAllTubeDetails,
            DeleteTubeDetails: DeleteTubeDetails,
            AddTubeDetails: AddTubeDetails,
            UpdateTubeDetails: UpdateTubeDetails
      };

      var my_header_config = {
        headers : {
            'x-access-token':my_token.token,
            'Content-Type':'application/x-www-form-urlencoded'
        }
    };
      return service;


        // ----- Customer Tube Tag Allocates To Depot
        function GetallCustTubeTagAllocatesToDepot(callback) {
            $http.get(serverIp+apiUrls.getallCustomerTubeTagAllocatesToDepot, my_header_config, { })
                .success(function (response, my_header_config) {
                    if(response.success==true){

                    }
                    callback(response);
                }).error(function (response, my_header_config) {
                    callback(response);
                });
        }

        function AddCustTubeTagAllocatesToDepot(tubeTagQR, tubeQtyAssignmentDetailsId,depotId,depotAdditionDate,callback) {
                var data = $.param({
                    tubeTagQR: tubeTagQR, 
                    tubeQtyAssignmentDetailsId: tubeQtyAssignmentDetailsId, 
                    depotId: depotId, 
                    depotAdditionDate: depotAdditionDate
                });
            $http.post(serverIp+apiUrls.addCustTubeTagAllocatesToDepot, data, my_header_config)
                .success(function (response, my_header_config) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    callback(response);
                }).error(function (response, my_header_config) {
                    callback(response);
                });
        }
        function DeleteCustTubeTagAllocatesToDepot(TubeTagAssignedId,callback){
            $http.delete(serverIp+apiUrls.deleteCustTubeTagAllocatesToDepotById+TubeTagAssignedId, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        function UpdateCustTubeTagAllocatesToDepot(TubeTagAssignedId, tubeTagQR, tubeQtyAssignmentDetailsId, depotId, depotAdditionDate, callback){
            var data = $.param({
                tubeTagQR: tubeTagQR, 
                tubeQtyAssignmentDetailsId: tubeQtyAssignmentDetailsId, 
                depotId: depotId, 
                depotAdditionDate: depotAdditionDate, 
            });
            $http.put(serverIp+apiUrls.updateCustTubeTagAllocatesToDepotById+TubeTagAssignedId, data, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                callback(response);
            }).error(function (response) {

                callback(response);
            });
        }


        // ---- customer Assigned Tube TagId Details --

        function GetCustAssignedTubeTagIdDetail(callback) {
            $http.get(serverIp+apiUrls.getCustAssignedTubeTagIdDetails, my_header_config, { })
                .success(function (response, my_header_config) {
                    if(response.success==true){

                    }
                    callback(response);
                }).error(function (response, my_header_config) {
                    callback(response);
                });
        }
          
        function AddCustAssignedTubeTagIdDetail(tubeTagQR, tubeQtyAssignmentDetailsId,depotId,depotAdditionDate,callback) {
                var data = $.param({
                    tubeTagQR: tubeTagQR, 
                    tubeQtyAssignmentDetailsId: tubeQtyAssignmentDetailsId, 
                    depotId: depotId, 
                    depotAdditionDate: depotAdditionDate
                });
            $http.post(serverIp+apiUrls.addCustAssignedTubeTagIdDetails, data, my_header_config)
                .success(function (response, my_header_config) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    callback(response);
                }).error(function (response, my_header_config) {
                    callback(response);
                });
        }
        function DeleteCustAssignedTubeTagIdDetail(TubeTagAssignedId,callback){
            $http.delete(serverIp+apiUrls.deleteCustAssignedTubeTagIdDetailsById+TubeTagAssignedId, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        function UpdateCustAssignedTubeTagIdDetail(TubeTagAssignedId, tubeTagQR, tubeQtyAssignmentDetailsId, depotId, depotAdditionDate, callback){
            var data = $.param({
                tubeTagQR: tubeTagQR, 
                tubeQtyAssignmentDetailsId: tubeQtyAssignmentDetailsId, 
                depotId: depotId, 
                depotAdditionDate: depotAdditionDate, 
            });
            $http.put(serverIp+apiUrls.updateCustAssignedTubeTagIdDetailsById+TubeTagAssignedId, data, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
        
        // ---------------------- Tube Quantity Assignment Detail ------------------------
        function GetAllTubeQtyAssignDetail(callback) {
            $http.get(serverIp+apiUrls.getAllTubeQtyAssignDetail, my_header_config, { })
                .success(function (response, my_header_config) {
                    if(response.success==true){

                    }
                    callback(response);
                }).error(function (response, my_header_config) {
                    callback(response);
                });
        }

        function AddTubeQtyAssignDetail(tubeQtyAssignmentId, tagQty, tubeSizeId,
            tubeManufacturer, tubeModel, callback) {
                var data = $.param({
                    tubeQtyAssignmentId: tubeQtyAssignmentId, 
                    tagQty: tagQty, 
                    tubeSizeId: tubeSizeId, 
                    tubeManufacturer: tubeManufacturer, 
                    tubeModel: tubeModel 
                });
            $http.post(serverIp+apiUrls.addTubeQtyAssignDetail, data, my_header_config)
                .success(function (response, my_header_config) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response, my_header_config) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
        function DeleteTubeQtyAssignDetail(tubeQtyAssignmentDetailId,callback){
            $http.delete(serverIp+apiUrls.deleteTubeQtyAssignDetailById+tubeQtyAssignmentDetailId, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        function UpdateTubeQtyAssignDetail(tubeQtyAssignmentDetailId, tubeQtyAssignmentId, tagQty, tubeSizeId,
            tubeManufacturer, tubeModel, callback){
                var data = $.param({
                    tubeQtyAssignmentId: tubeQtyAssignmentId, 
                    tagQty: tagQty, 
                    tubeSizeId: tubeSizeId, 
                    tubeManufacturer: tubeManufacturer, 
                    tubeModel: tubeModel 
                });
            $http.put(serverIp+apiUrls.updateTubeQtyAssignDetailById+tubeQtyAssignmentDetailId, data, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
        //----------------Tube Qty Assignment
        function GetAllTubeQtyAssignment(callback) {
            $http.get(serverIp+apiUrls.getAllTubeQtyAssignment, my_header_config, { })
                .success(function (response) {
                    if(response.success==true){

                    }
                    callback(response);
                }).error(function (response) {
                    callback(response);
                });
        }
  
        function AddTubeQtyAssignment(assignmentDate, customerId, tubeQty, callback) {
            var data = $.param({
                assignmentDate: assignmentDate, 
                customerId: customerId, 
                tubeQty: tubeQty
            });
            console.log(data);
            $http.post(serverIp+apiUrls.addTubeQtyAssignment, data, my_header_config)
                .success(function (response, my_header_config) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response, my_header_config) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
        function DeleteTubeQtyAssignment(tubeQtyAssignmentId,callback){
            $http.delete(serverIp+apiUrls.deleteTubeQtyAssignmentById+tubeQtyAssignmentId, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
  
        function UpdateTubeQtyAssignment(tubeQtyAssignmentId, assignmentDate, customerId, tubeQty, callback){
            var data = $.param({
                assignmentDate: assignmentDate, 
                customerId: customerId, 
                tubeQty: tubeQty
            });
            $http.put(serverIp+apiUrls.updateTubeQtyAssignmentById+tubeQtyAssignmentId, data, my_header_config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

      //for Tube details
      function GetAllTubeType(callback) {
          $http.get(serverIp+apiUrls.getalltubetype, { })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function AddTubeType(tubeTypeName, tubeTypeDesc, callback) {
          $http.post(serverIp+apiUrls.addtubetype, { tubeTypeName: tubeTypeName, tubeTypeDesc: tubeTypeDesc })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      function DeleteTubeType(tubeTypeId,callback){
          $http.delete(serverIp+apiUrls.deletetubetypeById+tubeTypeId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateTubeType(tubeTypeId, tubeTypeName, tubeTypeDesc,callback){
          $http.put(serverIp+apiUrls.updatetubeTypeById+tubeTypeId, { tubeTypeName: tubeTypeName, tubeTypeDesc: tubeTypeDesc })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }
      //for Tube Size details
      function GetAllTubeSize(callback) {
        $http.get(serverIp+apiUrls.getalltubesize, { })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
   //for Add Tube Size details
    function AddTubeSize(tubeSizeName, tubeSizeDesc, callback) {
        $http.post(serverIp+apiUrls.addtubesize, { tubeSizeName: tubeSizeName, tubeSizeDesc: tubeSizeDesc })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
     //for Delete Tube Size details
    function DeleteTubeSize(tubeSizeId,callback){
        $http.delete(serverIp+apiUrls.deletetubesizeById+tubeSizeId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
     //for Update Tube Size details
    function UpdateTubeSize(tubeSizeId, tubeSizeName, tubeSizeDesc,callback){
        $http.put(serverIp+apiUrls.updatetubesizeById+tubeSizeId, { tubeSizeName: tubeSizeName, tubeSizeDesc: tubeSizeDesc })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }  
      //for Tube Manufacturer details
      function GetAllTubeManufacturer(callback) {
        $http.get(serverIp+apiUrls.getalltubemanufacturer, { })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
   //for Add Tube Size details
    function AddTubeManufacturer(tubeManufacturerName, callback) {
        $http.post(serverIp+apiUrls.addtubemanufacturer, { tubeManufacturerName: tubeManufacturerName })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
     //for Delete Tube Size details
    function DeleteTubeManufacturer(tubeManufacturerId,callback){
        $http.delete(serverIp+apiUrls.deletetubemanufacturerById+tubeManufacturerId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
     //for Update Tube Size details
    function UpdateTubeManufacturer(tubeManufacturerId, tubeManufacturerName,callback){
        $http.put(serverIp+apiUrls.updatetubemanufacturerById+tubeManufacturerId, { tubeManufacturerName: tubeManufacturerName})
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    } 
     //for Tube Master details
     function GetAllTubeDetails(callback) {
        $http.get(serverIp+apiUrls.getallTubeMaster, { })
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
   //for Add Tube Master details
    function AddTubeDetails(tubeQr,tubeTypeId,tubeSizeId,tubeManufacturerId,manufacturingDate,tubeModel, callback) {
        $http.post(serverIp+apiUrls.addTubeMaster, { tubeQr: tubeQr,tubeTypeId: tubeTypeId,tubeSizeId:tubeSizeId, tubeManufacturerId:tubeManufacturerId,manufacturingDate:manufacturingDate,
            tubeModel:tubeModel })
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
     //for Delete Tube Master details
    function DeleteTubeDetails(tubeDetailsId,callback){
        $http.delete(serverIp+apiUrls.deleteTubeMasterById+tubeDetailsId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
     //for Update Tube Master details
    function UpdateTubeDetails(tubeDetailsId, tubeQr,tubeTypeId,tubeSizeId,tubeManufacturerId,
        manufacturingDate,tubeModel,callback){
        $http.put(serverIp+apiUrls.updateTubeMasterById+tubeDetailsId, { tubeQr: tubeQr,tubeTypeId:tubeTypeId,tubeSizeId:tubeSizeId,
             tubeManufacturerId:tubeManufacturerId,manufacturingDate:manufacturingDate,tubeModel:tubeModel})
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
  }
  
  })();