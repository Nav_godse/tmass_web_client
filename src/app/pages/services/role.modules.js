(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('roleService', roleService);
  
    /** @ngInject */
    function roleService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
          //tag type
          getallroles:'/api/getallRoles',
          addrole: '/api/addRole',
          deleteroleById: '/api/deleteRoleById/',
          updateroleById: '/api/updateRoleById/',
          disableroleById: '/api/disableRoleById/',
          roleLevelforAddUser: '/api/roleLevelforAddUser',
          roleLevelforAddCustomer: '/api/roleLevelforAddCustomer'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }
      
      var service = {
        GetAllRoles: GetAllRoles,
        DeleteRole: DeleteRole,
        AddRole: AddRole,
        UpdateRole: UpdateRole,
        GetroleLevelforAddCustomer:GetroleLevelforAddCustomer,
        GetroleLevelforAddUser: GetroleLevelforAddUser
      };

      return service;
 
      //for Role details
      function GetAllRoles(callback) {
          $http.get(serverIp+apiUrls.getallroles, { })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function AddRole(roleName, roleLevel, callback) {
          $http.post(serverIp+apiUrls.addrole, { roleName: roleName, roleLevel: roleLevel })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      function DeleteRole(roleId,callback){
          $http.delete(serverIp+apiUrls.deleteroleById+roleId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateRole(roleId, roleName, roleLevel,callback){
          $http.put(serverIp+apiUrls.updateroleById+roleId, {  roleName: roleName, roleLevel: roleLevel })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      //add roles according to user or customer      
      function GetroleLevelforAddUser(callback){
        var data = $.param({

        });
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.roleLevelforAddUser,config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function GetroleLevelforAddCustomer(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.roleLevelforAddCustomer,config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

  }
  
  })();