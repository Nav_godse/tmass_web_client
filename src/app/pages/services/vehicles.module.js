(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('vehicleService', vehicleService);
  
    /** @ngInject */
    function vehicleService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
            //Vehicles type
            getallvehiclegroup:'/api/getallVehicleGroup',
            addvehiclegroup: '/api/addVehicleGroup',
            deletevehiclegroupById: '/api/deleteVehicleGroup/',
            updatevehiclegroupById: '/api/updateVehicleGroup/',
            //Vehicles Manufacturer
            getallvehiclemanufacturer:'/api/getallVehicleManufacturer',
            addvehiclemanufacturer: '/api/addVehicleManufacturer',
            deletevehiclemanufacturerById: '/api/deleteVehicleManufacturerById/',
            updatevehiclemanufacturerById: '/api/updateVehicleManufacturerById/',
            //Vehicles AxelConfiguration
            getallvehicleaxelconfiguration:'/api/getallVehicleAxelConfiguration',
            addVehicleaxelconfiguration: '/api/addVehicleAxelConfiguration',
            deletevehicleaxelconfigurationById: '/api/deleteVehicleAxelConfigurationById/',
            updatevehicleaxelconfigurationById: '/api/updateVehicleAxelConfigurationById/',
            //Vehicles Model
            getallvehiclemodel:'/api/getallVehicleModel',
            addvehiclemodel: '/api/addVehicleModel',
            deletevehiclemodelById: '/api/deleteVehicleModelById/',
            updatevehiclemodelById: '/api/updateVehicleModelById/',
            //Vehicles Specification
            getvehiclespecifications:'/api/getvehicleSpecifications',
            addvehiclespecification: '/api/addvehicleSpecification',
            deletevehiclespecificationById: '/api/deletevehicleSpecificationById/',
            updatevehiclespecificationById: '/api/updatevehicleSpecificationById/',
            //Vehicles Master
            getallvehicleMaster:'/api/vehicleMaster',
            addvehicleMaster: '/api/vehicleMaster',
            deletevehicleMasterById: '/api/deletevehicleMasterById/',
            updatevehicleMasterById: '/api/updatevehicleMasterById/',
            //Get Customer Details
            getallCustomers: '/api/getallCustomers/',
            AddBulkVehicleSpecMaster:'/api/AddBulkVehicleSpecMaster',
            addVehicaleSpecBulk:'/api/addVehicaleSpecBulk',
            getvehicleSpecification:'/api/getvehicleSpecification/',
            //get alerts on vehicle
            getVehicleAlertswithin24hrs: '/api/getVehicleAlertswithin24hrs/'
            
      }
      

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }
      var service = {
        GetAllVehicleGroup: GetAllVehicleGroup,
        DeleteVehicleGroup: DeleteVehicleGroup,
        AddVehicleGroup: AddVehicleGroup,
        UpdateVehicleGroup: UpdateVehicleGroup,
        // Vehicle Manufaturer
        GetAllVehicleManufacturer: GetAllVehicleManufacturer,
        DeleteVehicleManufacturer: DeleteVehicleManufacturer,
        AddVehicleManufacturer: AddVehicleManufacturer,
        UpdateVehicleManufacturer: UpdateVehicleManufacturer,
        // Vehicle AxelConfiguration
        GetAllVehicleAxelConfiguration: GetAllVehicleAxelConfiguration,
        DeleteVehicleAxelConfiguration: DeleteVehicleAxelConfiguration,
        AddVehicleAxelConfiguration: AddVehicleAxelConfiguration,
        UpdateVehicleAxelConfiguration: UpdateVehicleAxelConfiguration,
        // Vehicle Model
        GetAllVehicleModel: GetAllVehicleModel,
        DeleteVehicleModel: DeleteVehicleModel,
        AddVehicleModel: AddVehicleModel,
        UpdateVehicleModel: UpdateVehicleModel,
        // Vehicle Specification
        GetAllVehicleSpec: GetAllVehicleSpec,
        DeleteVehicleSpec: DeleteVehicleSpec,
        AddVehicleSpec: AddVehicleSpec,
        UpdateVehicleSpec: UpdateVehicleSpec,
        // Vehicle Master
        GetAllVehicleMaster: GetAllVehicleMaster,
        DeleteVehicleMaster: DeleteVehicleMaster,
        AddVehicleMaster: AddVehicleMaster,
        UpdateVehicleMaster: UpdateVehicleMaster,
         // get Cutomer Details
         GetAllCustomerDetails: GetAllCustomerDetails,
         AddVehicaleSpecBulk:AddVehicaleSpecBulk,
         GetVehicleSepcs:GetVehicleSepcs,
         GetAllTPMSViewDetails:GetAllTPMSViewDetails,
         getVehicleAlertswithin24hrs:getVehicleAlertswithin24hrs
      };

      return service;

        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }

      function AddVehicaleSpecBulk(depotdata, callback) {
        // var data1 = [];
        // var data1 = $.param({depotdata});
        $http.post(serverIp+apiUrls.AddBulkVehicleSpecMaster, depotdata)
            .success(function (data1, status, config) { 
                callback(data1);
            })
            .error(function (data1, status, config) {
            callback(data1);
        });
    }

      //for Manufaturer details
      function GetAllVehicleGroup(callback) {
          $http.get(serverIp+apiUrls.getallvehiclegroup, { })
              .success(function (response) {
                  if(response.success==true){
                   //   //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function AddVehicleGroup(vehicleGroupName, vehicleGroupDesc, callback) {
          $http.post(serverIp+apiUrls.addvehiclegroup, { vehicleGroupName: vehicleGroupName, vehicleGroupDesc: vehicleGroupDesc })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      function DeleteVehicleGroup(vehicleGroupId,callback){
          $http.delete(serverIp+apiUrls.deletevehiclegroupById+vehicleGroupId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateVehicleGroup(vehicleGroupId, vehicleGroupName, vehicleGroupDesc,callback){
          $http.put(serverIp+apiUrls.updatevehiclegroupById+vehicleGroupId, { vehicleGroupName: vehicleGroupName, vehicleGroupDesc: vehicleGroupDesc })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      //for  Get Vehicle Manufacture details
      function GetAllVehicleManufacturer(callback) {
        $http.get(serverIp+apiUrls.getallvehiclemanufacturer, { })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //for  Add Vehicle Manufacture details
    function AddVehicleManufacturer(vehicleManufacturerName, vehicleManufacturerPlace, callback) {
        $http.post(serverIp+apiUrls.addvehiclemanufacturer, { vehicleManufacturerName: vehicleManufacturerName, vehicleManufacturerPlace: vehicleManufacturerPlace })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
     //for  Delete Vehicle Manufacture details
    function DeleteVehicleManufacturer(vehicleManufId,callback){
        $http.delete(serverIp+apiUrls.deletevehiclemanufacturerById+vehicleManufId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
     //for  Update Vehicle Manufacture details
    function UpdateVehicleManufacturer(vehicleManufId, vehicleManufacturerName, vehicleManufacturerPlace,callback){
        $http.put(serverIp+apiUrls.updatevehiclemanufacturerById+vehicleManufId, { vehicleManufacturerName: vehicleManufacturerName, vehicleManufacturerPlace: vehicleManufacturerPlace })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
  
     //for  Get Vehicle AxelConfiguration details
     function GetAllVehicleAxelConfiguration(callback) {
        $http.get(serverIp+apiUrls.getallvehicleaxelconfiguration, { })
            .success(function (response) {
                if(response.success==true){
                    ////console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //for  Add Vehicle AxelConfiguration details
    function AddVehicleAxelConfiguration(vehicleAxelConfiguration, callback) {
        $http.post(serverIp+apiUrls.addVehicleaxelconfiguration, { vehicleAxelConfiguration: vehicleAxelConfiguration})
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
     //for  Delete Vehicle AxelConfiguration details
    function DeleteVehicleAxelConfiguration(vehicleconfigId,callback){
        $http.delete(serverIp+apiUrls.deletevehicleaxelconfigurationById+vehicleconfigId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
     //for  Update Vehicle Configuration details
    function UpdateVehicleAxelConfiguration(vehicleManufId, vehicleAxelConfiguration,callback){
        $http.put(serverIp+apiUrls.updatevehicleaxelconfigurationById+vehicleManufId, { vehicleAxelConfiguration: vehicleAxelConfiguration })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
   
    //for Get Vehicle Model details
    function GetAllVehicleModel(callback) {
        $http.get(serverIp+apiUrls.getallvehiclemodel, { })
            .success(function (response) {
                if(response.success==true){
                    ////console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //for Add Vehicle Model details
    function AddVehicleModel(vehicleManufacturerId, vehicleModelNo,vehicleModelDescription, callback) {
        $http.post(serverIp+apiUrls.addvehiclemodel, { vehicleManufacturerId: vehicleManufacturerId, vehicleModelNo: vehicleModelNo,vehicleModelDescription:vehicleModelDescription })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //for Delete Vehicle Model details
    function DeleteVehicleModel(vehicleModelId,callback){
        $http.delete(serverIp+apiUrls.deletevehiclemodelById+vehicleModelId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    
    //for Update Vehicle Model details
    function UpdateVehicleModel(vehicleModelId, vehicleManufacturerId, vehicleModelNo,vehicleModelDescription,callback){
        $http.put(serverIp+apiUrls.updatevehiclemodelById+vehicleModelId, { vehicleManufacturerId: vehicleManufacturerId, vehicleModelNo: vehicleModelNo,vehicleModelDescription:vehicleModelDescription })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
 
    //for Get Vehicle Specification details
    function GetAllVehicleSpec(callback) {
        $http.get(serverIp+apiUrls.getvehiclespecifications, { 
            headers: {  
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            },
        })
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }


    //for Add Vehicle Specification details
    function AddVehicleSpec(vehicleManufacturerId, vehicleModelNo,vehicleAxelConfigurationId,rotationThreshold,loadCapacity, callback) {
        var data = $.param({
            vehicleManufacturerId: vehicleManufacturerId, vehicleModelNo: vehicleModelNo,
            vehicleAxelConfigurationId:vehicleAxelConfigurationId,rotationThreshold:rotationThreshold,
            loadCapacity:loadCapacity
        });

        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        };
        $http.post(serverIp+apiUrls.addvehiclespecification, data, config)
            .success(function (data, status, headers, config) {
                //console.log(data);
                callback(data);
            })
            .error(function (data, status, header, config) {
             callback(data);
        });
    }
    //for Delete Vehicle Specification details
    function DeleteVehicleSpec(vehicleSpeclId,callback){

        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        };

        $http.delete(serverIp+apiUrls.deletevehiclespecificationById+vehicleSpeclId, config)

        .then(function (response) {
            if(response.data.success==true){
                //console.log(response.data);
            }
            // login successful if there's a token in the response
            callback(response.data);
    
        }, function (error) {
            //console.log(response.data);
            callback(response.data);
        }); 
    }
    
    //for Update Vehicle Specification details
    function UpdateVehicleSpec(vehicleSpeclId, vehicleManufacturerId,  vehicleModelNo,vehicleAxelConfigurationId,
            rotationThreshold,loadCapacity,callback){
        var data = $.param({
            vehicleManufacturerId: vehicleManufacturerId, vehicleModelNo: vehicleModelNo,
            vehicleAxelConfigurationId:vehicleAxelConfigurationId,rotationThreshold:rotationThreshold,
            loadCapacity:loadCapacity
        });
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.put(serverIp+apiUrls.updatevehiclespecificationById+vehicleSpeclId, data, config)
            .success(function (data, status, headers, config) { 
                callback(data);
            })
            .error(function (data, status, header, config) {
             callback(data);
        });
    } 
    //for Get Vehicle Model details
    function GetAllVehicleMaster(callback) {
        $http.get(serverIp+apiUrls.getallvehicleMaster, { 
            headers: {  
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            },
        })
        .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }


    //for Add Vehicle Master details
    function AddVehicleMaster(paramsObj, callback) {
        var data = $.param({
            vehicleGroupId: paramsObj.vehicleGroupId,
            vehicleRegistrationNumber: paramsObj.vehicleRegistrationNumber,
            vehicleAxelConfigurationId: paramsObj.vehicleAxelConfigurationId,
            rotationThreshold : paramsObj.rotationThreshold,
            loadCapacity : paramsObj.loadCapacity,
            vehicleManufacturerId : paramsObj.vehicleManufacturerId,
            vehicleModelNo : paramsObj.vehicleModelNo,
            odometerReading : paramsObj.odometerReading,
            distanceTravelled: paramsObj.distanceTravelled,
            subscriptionStartDate :paramsObj.subscriptionStartDate,
            subscriptionEndDate : paramsObj.subscriptionEndDate,
            isvehicleModelNew:paramsObj.isvehicleModelNew
        });

        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        };
        $http.post(serverIp+apiUrls.addvehicleMaster, data, config)
            .success(function (data, status, headers, config) {
                //console.log(data);
                callback(data);
            })
            .error(function (data, status, header, config) {
             callback(data);
        });
    }
    //for Delete Vehicle Master details
    function DeleteVehicleMaster(vehiclelId,callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        };
        $http.delete(serverIp+apiUrls.deletevehicleMasterById+vehiclelId, config)
        .then(function (response) {
            if(response.data.success==true){
                //console.log(response.data);
            }
            callback(response.data);
        }, function (error) {
            //console.log(response.data);
            callback(response.data);
        }); 
    }

    //for Get Vehicle Sepc details

    function GetVehicleSepcs(vehicleManu,vehiclemodel,callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        };
        $http.get(serverIp+apiUrls.getvehicleSpecification+vehicleManu+'/'+vehiclemodel, config)
        .then(function (response) {
            if(response.data.success==true){
                //console.log(response.data);
            }
            callback(response.data);
        }, function (error) {
            //console.log(response.data);
            callback(response.data);
        }); 
    }
    //for Update Vehicle Master details
    function UpdateVehicleMaster(vehiclelId,vehicleGroupId, vehicleRegistrationNumber,vehicleAxelConfigurationId,rotationThreshold,
        loadCapacity,vehicleManufacturerId,vehicleModelNo,odometerReading,distanceTravelled,subscriptionStartDate,subscriptionEndDate,isvehicleModelNew,callback){
        var data = $.param({
            vehicleGroupId: vehicleGroupId,
            vehicleRegistrationNumber: vehicleRegistrationNumber,
            vehicleAxelConfigurationId: vehicleAxelConfigurationId,
            rotationThreshold : rotationThreshold,
            loadCapacity : loadCapacity,
            vehicleManufacturerId : vehicleManufacturerId,
            vehicleModelNo : vehicleModelNo,
            odometerReading : odometerReading,
            distanceTravelled: distanceTravelled,
            subscriptionStartDate :subscriptionStartDate,
            subscriptionEndDate : subscriptionEndDate,
            isvehicleModelNew:isvehicleModelNew
            
        });
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.put(serverIp+apiUrls.updatevehicleMasterById+vehiclelId, data, config)
            .success(function (data, status, headers, config) { 
                callback(data);
            })
            .error(function (data, status, header, config) {
             callback(data);
        });
    }
     // Get All CustomerDetails
     function GetAllCustomerDetails(callback) {
        $http.get(serverIp+apiUrls.getallCustomers, { })
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
//for TPMS View Details details
    function GetAllTPMSViewDetails(DeviceID, DeviceType,callback) {
        $http({
            method: "GET",
            url: "http://demo.tmaas.in/treel_data_lg/device_details/get_device_info_deviceid.php?DeviceID="+DeviceID+"&deviceType="+DeviceType+"&MgrValue=0",
            
        }).then(function successCallback(response) {  
            callback(response);
            // $('#mydiv').hide();
        }, function errorCallback(response) {
            callback(response);
        });
    }
    //for getting all the vehcile alerts within 24 hours
    function getVehicleAlertswithin24hrs(vehicleId, callback){
        $http.get(serverIp+apiUrls.getVehicleAlertswithin24hrs+vehicleId, { })
        .success(function (response) { 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
  }
})();