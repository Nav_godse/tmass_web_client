(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('tireReportService', tireReportService);
  
    /** @ngInject */
    function tireReportService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
        //tag type
        //getAllTireReport:'/api/getAllTireReport',
        getAllTireReport:'/api/showTireDashboard',
        getallVehicleAlertsReport:'/api/getallVehicleAlertsReport'
        //   addrole: '/api/addRole',
        //   deleteroleById: '/api/deleteRoleById/',
        //   updateroleById: '/api/updateRoleById/',
        //   disableroleById: '/api/disableRoleById/'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }
      
      var service = {
        getAllTireReport: getAllTireReport,
        GetAllVehicleAlertsReport: GetAllVehicleAlertsReport
        // DeleteRole: DeleteRole,
        // AddRole: AddRole,
        // UpdateRole: UpdateRole
      };

      return service;
 
      //for get tire report details
    //   function getAllTireReport(callback) {
    //       $http.get(serverIp+apiUrls.getAllTireReport, { })
    //           .success(function (response) {
    //               if(response.success==true){
    //                   //console.log(response);
    //               }
    //               // login successful if there's a token in the response
    //               callback(response);
    //           }).error(function (response) {
    //               // handle error things
    //               //console.log(response);
    //               callback(response);
    //           });
    //   }

        //with headers
      //for get tire report details
    function getAllTireReport(callback) {
        $http.get(serverIp+apiUrls.getAllTireReport, { 
            headers: {  
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            },
        })
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //for get tire report details
    function GetAllVehicleAlertsReport(callback) {
        $http.get(serverIp+apiUrls.getallVehicleAlertsReport, {})
        .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

      function AddRole(roleName, roleLevel, callback) {
          $http.post(serverIp+apiUrls.addrole, { roleName: roleName, roleLevel: roleLevel })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      function DeleteRole(roleId,callback){
          $http.delete(serverIp+apiUrls.deleteroleById+roleId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateRole(roleId, roleName, roleLevel,callback){
          $http.put(serverIp+apiUrls.updateroleById+roleId, {  roleName: roleName, roleLevel: roleLevel })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

  }
  
  })();