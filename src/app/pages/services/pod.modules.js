(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('podService', podService);
  
    /** @ngInject */
    function podService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
          //pod details
          getallPods:'/api/getallpodinventory',
          addpodinventoryinbulk: '/api/addpodinventoryinbulk',
          showVehicleandPodDetails: '/api/showVehicleandPodDetails',
          getpodTPMSdetails: '/api/getpodTPMSdetails/'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }
      
      var service = {
        getallPods: getallPods,
        addpodinventoryinbulk: addpodinventoryinbulk,
        showVehicleandPodDetails: showVehicleandPodDetails,
        getpodTPMSdetails: getpodTPMSdetails
      };

      return service;
 
      //for Role details
      function getallPods(callback) {
          $http.get(serverIp+apiUrls.getallPods, { })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      
      function getpodTPMSdetails(podDeviceId,callback) {
        $http.get(serverIp+apiUrls.getpodTPMSdetails+podDeviceId)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
      }

      function addpodinventoryinbulk(roleName, callback) {
          $http.post(serverIp+apiUrls.addpodinventoryinbulk, roleName)
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function DeleteRole(roleId,callback){
          $http.delete(serverIp+apiUrls.deleteroleById+roleId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateRole(roleId, roleName, roleLevel,callback){
          $http.put(serverIp+apiUrls.updateroleById+roleId, {  roleName: roleName, roleLevel: roleLevel })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      //add roles according to user or customer      
      function GetroleLevelforAddUser(callback){
        var data = $.param({

        });
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.roleLevelforAddUser,config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function showVehicleandPodDetails(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showVehicleandPodDetails, config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

  }
  
  })();