(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('allReportService', allReportService);
  
    /** @ngInject */
    function allReportService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

        var my_user = {};

        var apiUrls = {
            getAllTireReport:'/api/getAllTireReport',
            getallVehicleAlertsReport:'/api/getallVehicleAlertsReport',
            showTireDashboard:'/api/showTireDashboard',
            showVehicleDashboard:'/api/showVehicleDashboard',
            showAlertDashboard:'/api/showAlertDashboard',
            showAllTiresReport:'/api/showAllTiresReport',
            showVehicleandAlertsDashboard: '/api/showVehicleandAlertsDashboard',
            showAlertsReport:'/api/showAlertsReport'
        }

        my_user = $window.localStorage.dataUser;
        var my_token = JSON.parse(my_user);
        //console.log( my_token.token );

        var my_headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-access-token':my_token.token
        }
      
        var service = {
            getAllTireReport: getAllTireReport,
            GetAllVehicleAlertsReport: GetAllVehicleAlertsReport,
            showTireDashboard: showTireDashboard,
            showVehicleDashboard: showVehicleDashboard,
            showAlertDashboard: showAlertDashboard,
            ShowAllTiresReport: ShowAllTiresReport,
            ShowVehicleandAlertsDashboard: ShowVehicleandAlertsDashboard,
            ShowAlertsReport:ShowAlertsReport
        };

        return service;
 
    //with headers
    //for get tire report details
    function getAllTireReport(callback) {
        $http.get(serverIp+apiUrls.getAllTireReport, { 
            headers: {  
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            },
        })
        .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //for get tire report details
    function GetAllVehicleAlertsReport(callback) {
        $http.get(serverIp+apiUrls.getallVehicleAlertsReport, {})
        .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function showTireDashboard(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showTireDashboard, config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    function ShowVehicleandAlertsDashboard(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showVehicleandAlertsDashboard, config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //showAlertsReport
    function ShowAlertsReport(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showAlertsReport, config)
        .success(function (response) {
            if(response.success==true){
                console.log('response in report controller');
                console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    function showAlertDashboard(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showAlertDashboard, config)
        .success(function (response) {
            if(response.success==true){
                console.log('response in report controller');
                console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function showVehicleDashboard(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showVehicleDashboard, config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function ShowAllTiresReport(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showAllTiresReport,config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
  }
  
  })();