(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('userService', userService);
  
    /** @ngInject */
    function userService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
        //Vehicles Model
        getallOperationMasterDetails:'/api/getallOperationMasterDetails',
        addOperationMasterDetails: '/api/AddOperationMasterDetails',
        deleteOperationMasterDetailsbyId: '/api/deleteOperationMasterDetailsbyId/',
        updateOperationMasterDetailsbyId: '/api/updateOperationMasterDetailsbyId/',

        // Role Mapping urls
        getallOperationRoleMapping:'/api/getallOperationRoleMapping',
        addOperationRoleMapping: '/api/AddOperationRoleMapping',
        deleteOperationRoleMappingbyId: '/api/deleteOperationRoleMappingbyId/',
        updateOperationRoleMappingbyId: '/api/updateOperationRoleMappingbyId/',

        //get all users
        getAllUser: '/api/getAllUser',
        addUser: '/api/addUser',
        updateUserById: '/api/updateUserById/',
        deleteUserById: '/api/deleteUserById/',

        //get my user details
        getCurrentUserProfile: "/api/getCurrentUserProfile/",
        updateUserProfile:'/api/updateUserProfileById/',
      }
      

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user); 
      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }

    var header_config= {
        headers: my_headers
    }

      var service = {   
        // Vehicle Specification
        GetAllOperationMaster: GetAllOperationMaster,
        DeleteOperationMaster: DeleteOperationMaster,
        AddOperationMaster: AddOperationMaster,
        UpdateOperationMaster: UpdateOperationMaster,

        //Role Mapping function
        GetallOperationRole: GetallOperationRole,
        DeleteOperationRole: DeleteOperationRole,
        AddOperationRole: AddOperationRole,
        UpdateOperationRole: UpdateOperationRole,

        GetAllUser: GetAllUser,
        AddUser: AddUser,
        UpdateUser: UpdateUser,
        DeleteUser: DeleteUser,
        updateUserProfile: updateUserProfile
      };

      return service;

        function AddUser(name, username, roleId, password, email, alternativeEmail, mobile, depotName, userPushToken, callback) {
            //console.log(header_config);
            var data = $.param({
                name:name, 
                username:username, 
                roleId:roleId, 
                password:password, 
                email:email, 
                alternativeEmail:alternativeEmail,
                mobile:mobile, 
                depotName:depotName, 
                userPushToken:userPushToken
            });
    
            var my_header_config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            };
            $http.post(serverIp+apiUrls.addUser, data, my_header_config)
                .success(function (data, status, headers, my_header_config) {
                    //console.log(data);
                    callback(data);
                })
                .error(function (data, status, header, my_header_config) {
                 callback(data);
            });



            // $http.post(serverIp+apiUrls.addUser,{
            //     headers:{
            //     'Content-Type': 'application/x-www-form-urlencoded',
            //     'x-access-token':my_token.token
            //   }
            // }, {
            //     name:name, 
            //     username:username, 
            //     roleId:roleId, 
            //     password:password, 
            //     email:email, 
            //     alternativeEmail:alternativeEmail,
            //     mobile:mobile, 
            //     depotName:depotName, 
            //     userPushToken:userPushToken
            // })
            // .success(function (response) { 
            //     callback(response);
            // }).error(function (response) {
            //     // handle error things
            //     //console.log(response);
            //     callback(response);
            // });
        }


        function GetAllUser(callback) {
            $http.get(serverIp+apiUrls.getAllUser,header_config, { })
                .success(function (response) { 
                    callback(response);
            }).error(function (response) {
            // handle error things
                    //console.log(response);
                    callback(response);
            });
        }

        function DeleteUser(id,callback){
    
            var my_header_config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            };
            $http.delete(serverIp+apiUrls.deleteUserById+id, my_header_config)
                .success(function (data, status, headers, my_header_config) {
                    //console.log(data);
                    callback(data);
                })
                .error(function (data, status, header, my_header_config) {
                 callback(data);
            });
            // $http.delete(serverIp+apiUrls.deleteUserById+_id, header_config)
            //     .success(function (response) {
            //     callback(response);
            // }).error(function (response) { 
            //      callback(response);
            // });
        }

        function UpdateUser(_id, name, username, roleId, password, email, alternativeEmail, mobile, depotName, userPushToken,newEmail, callback) {

                var data = $.param({
                    name:name, 
                    username:username, 
                    roleId:roleId, 
                    password:password,
                    email:email, 
                    alternativeEmail:alternativeEmail,
                    mobile:mobile, 
                    depotName:depotName, 
                    userPushToken:userPushToken,
                    newEmail:newEmail
                });
                console.log("parmas", data);
                var my_header_config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                };

            $http.put(serverIp+apiUrls.updateUserById+_id, data, my_header_config )
                .success(function (response) { 
                callback(response);
            }).error(function (response) {
                 callback(response);
            });
        }

      function GetallOperationRole(callback) {
        $http.get(serverIp+apiUrls.getallOperationRoleMapping, { })
            .success(function (response) { 
                callback(response);
        }).error(function (response) {
        // handle error things
                //console.log(response);
                callback(response);
        });
    }

    function AddOperationRole(userId, roleId, operationId, viewFlag, editFlag, addFlag, 
        deleteFlag,callback) {
        $http.post(serverIp+apiUrls.addOperationRoleMapping, {
            userId: userId, roleId: roleId, operationId: operationId, viewFlag: viewFlag, editFlag: editFlag, addFlag: addFlag, 
            deleteFlag: deleteFlag
         })
            .success(function (response) { 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function DeleteOperationRole(_id,callback){
        $http.delete(serverIp+apiUrls.deleteOperationRoleMappingbyId+_id)
            .success(function (response) {
            callback(response);
        }).error(function (response) { 
             callback(response);
        });
    }

    function UpdateOperationRole(_id, userId, roleId, operationId, viewFlag, editFlag, addFlag, 
        deleteFlag, callback){
        $http.put(serverIp+apiUrls.updateOperationRoleMappingbyId+_id, { 
            userId: userId, roleId: roleId, operationId: operationId, viewFlag: viewFlag, editFlag: editFlag,
            addFlag: addFlag, deleteFlag: deleteFlag
         })
            .success(function (response) { 
            callback(response);
        }).error(function (response) {
             callback(response);
        });
    }


        //for GetAllOperationMaster details
        function GetAllOperationMaster(callback) {
            $http.get(serverIp+apiUrls.getallOperationMasterDetails, { })
                .success(function (response) { 
                 callback(response);
            }).error(function (response) {
            // handle error things
                 //console.log(response);
                 callback(response);
            });
        }

        function AddOperationMaster(operationName, operationNameToDisplay,isMenu,isScreen, callback) {
            $http.post(serverIp+apiUrls.addOperationMasterDetails, { operationName: operationName, operationNameToDisplay: operationNameToDisplay,
                isMenu:isMenu,isScreen:isScreen })
                .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
        function DeleteOperationMaster(operationId,callback){
            $http.delete(serverIp+apiUrls.deleteOperationMasterDetailsbyId+operationId)
                .success(function (response) {
                callback(response);
            }).error(function (response) { 
                 callback(response);
            });
        }

        function UpdateOperationMaster(operationId, operationName, operationNameToDisplay,isMenu,isScreen,callback){
            $http.put(serverIp+apiUrls.updateOperationMasterDetailsbyId+operationId, { operationName: operationName, operationNameToDisplay: operationNameToDisplay,
                isMenu:isMenu,isScreen:isScreen })
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                 callback(response);
            });
        }

        function GetCurrentUserDetails(data,callback){
            var data = $.param({});
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            //getCurrentUserProfile
            $http.put(serverIp+apiUrls.getCurrentUserProfile+data, {},config)
            .success(function (response,headers,config) { 
                callback(response);
            }).error(function (response,headers,config) {
                 callback(response);
            });
        }

        function updateUserProfile(_id, name, username, roleId, password, email, alternativeEmail, mobile, depotName, userPushToken,newEmail, callback) {
            //updateUserProfile
            var data = $.param({
                    name:name, 
                    username:username, 
                    roleId:roleId, 
                    password:password,
                    email:email, 
                    alternativeEmail:alternativeEmail,
                    mobile:mobile, 
                    depotName:depotName, 
                    userPushToken:userPushToken,
                    newEmail:newEmail
                });
                console.log("parmas", data);
                var my_header_config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                };

            $http.put(serverIp+apiUrls.updateUserProfile+_id, data, my_header_config )
                .success(function (response) { 
                callback(response);
            }).error(function (response) {
                callback(response);
            });
        }
    
  }
  
  })();