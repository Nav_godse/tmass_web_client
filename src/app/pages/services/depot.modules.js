(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('depotService', depotService);
  
    /** @ngInject */
    function depotService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

        var my_user = {};

        var apiUrls = {
            //depots type
            getalldepot:'/api/getallDepot',
            addDepot: '/api/addDepot',
            deletedepotById: '/api/deletedepotById/',
            updatedepotById: '/api/updatedepotById/',
            getdepotBycustomer: '/api/getdepotBycustomer',
            AddBulkDepot : '/api/AddBulkDepot',
            addDepotinBulk: '/api/addDepotinBulk'
        }
      

        my_user = $window.localStorage.dataUser;
        var my_token = JSON.parse(my_user);
        ////console.log( my_token.token );

        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        var service = {
            GetAlldepot: GetAlldepot,
            Deletedepot: Deletedepot,
            Adddepot: Adddepot,
            Updatedepot: Updatedepot,
            GetAlldepotByCustomer: GetAlldepotByCustomer,
            AddBulkDepot: AddBulkDepot,
            AdddepotinBulk: AdddepotinBulk
        };

        return service;
 
      //for Manufaturer details
        function GetAlldepot(callback) {
            $http.get(serverIp+apiUrls.getalldepot, { })
                .success(function (response) {
                    if(response.success==true){
                    //   //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }

        //getdepotBycustomer
        function GetAlldepotByCustomer(callback) {
            // $http.get(serverIp+apiUrls.getdepotBycustomer, { })
            var data = $.param({
            
            })
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.getdepotBycustomer, config)
            .success(function (response) {
                if(response.success==true){
                }
                callback(response);
            }).error(function (response) {
                // handle error things
                callback(response);
            });
        }

        function Adddepot(depotdata, callback) {
            var data = $.param({
                customerId: depotdata.customerId,
                depotName:depotdata.depotName,
                addressLine1:depotdata.addressLine1,
                addressLine2:depotdata.addressLine2,
                pincode:depotdata.pincode,
                cityId:depotdata.cityId,
                stateId:depotdata.stateId,
                countryName:depotdata.countryName,
                latitude:depotdata.latitude,
                logitude:depotdata.logitude
            });

            $http.post(serverIp+apiUrls.addDepot, data, config)
                .success(function (data, status, config) { 
                    callback(data);
                })
                .error(function (data, status, config) {
                callback(data);
            });
        }

        //addDepotinBulk
        
        function AdddepotinBulk(depotdata, callback) {
            var data1 = [];
            var data1 = $.param({data:depotdata});
            $http.post(serverIp+apiUrls.addDepotinBulk, data1, config)
                .success(function (data1, status, config) { 
                    callback(data1);
                })
                .error(function (data1, status, config) {
                callback(data1);
            });
        }
        
        function Deletedepot(depotId,callback){
            $http.delete(serverIp+apiUrls.deletedepotById+depotId,config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        function Updatedepot(depotdata,callback){
            console.log(depotdata);
            
            var data = $.param({
                customerId: depotdata.customerId,
                depotName:depotdata.depotName,
                addressLine1:depotdata.addressLine1,
                addressLine2:depotdata.addressLine2,
                pincode:depotdata.pincode,
                cityId:depotdata.cityId,
                stateId:depotdata.stateId,
                countryName:depotdata.countryName,
                latitude:depotdata.latitude,
                logitude:depotdata.logitude
            });
            $http.put(serverIp+apiUrls.updatedepotById+depotdata._id, data, config)
                .success(function (data, status, config) { 
                    callback(data);
                })
                .error(function (data, status, config) {
                callback(data);
            });
        }
        //add bulk tags to db AddBulkDepot
        function AddBulkDepot(data,callback){
            $http.post(serverIp+apiUrls.AddBulkDepot,data )
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
}
})();