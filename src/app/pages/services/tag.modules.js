(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('tagService', tagService);
  
    /** @ngInject */
    function tagService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
        //tag type
        getalltagtype:'/api/getallTagTypes',
        addtagtype: '/api/addTagType',
        deletetagtypeById: '/api/deleteTagType/',
        updatetagTypeById: '/api/updateTagType/',
        //tag Assignment
        getallTagQtyAssignment:'/api/getallTagQtyAssignmentMaster',
        addTagQtyAssignment: '/api/addTagQtyAssignmentMaster',
        deleteTagQtyAssignmentById: '/api/deleteTagQtyAssignmentMasterById/',
        updateTagQtyAssignmentById: '/api/updateTagQtyAssignmentMasterById/',
        //tag Details
        getallTagMaster:'/api/getallTagMaster',
        addTagMaster: '/api/addTagMaster',
        deleteTagbyId: '/api/deleteTagbyId/',
        updateTagbyId: '/api/updateTagbyId/',
        //Get Customer Details
        getallCustomers: '/api/getallCustomers/',
        //upload
        fileUploadandConvert: '/api/upload',
        //bulk add AddBulkTagMaster
        AddBulkTagMaster: '/api/AddBulkTagMaster',
        getSpecificTagBYTagType:'/api/getSpecificTagBYTagType/',
        //getalltag count an typed
        getAllTagCount: '/api/getAllTagCount',
        getallTagMasterbyTagTypeId: '/api/getallTagMasterbyTagTypeId/',
        getallCustomerAssignedTagIdDetailsbytagQtyAssignmentId: '/api/getallCustomerAssignedTagIdDetailsbytagQtyAssignmentId/'
      }

      
      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }

      var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
      }
      
      var service = {
        GetAllTagType: GetAllTagType,
        DeleteTagType: DeleteTagType,
        AddTagType: AddTagType,
        UpdatetagType: UpdatetagType,
        // Tag Quantity Assignment
        GetAllTagQtyAssignment: GetAllTagQtyAssignment,
        DeleteTagQtyAssignment: DeleteTagQtyAssignment,
        AddTagQtyAssignment: AddTagQtyAssignment,
        UpdatetagQtyAssignment: UpdatetagQtyAssignment,
        // For Tag Details
        GetAllTagMaster: GetAllTagMaster,
        DeleteTagMaster: DeleteTagMaster,
        AddTagMaster: AddTagMaster,
        UpdatetagMaster: UpdatetagMaster,
        // get Cutomer Details
        GetAllCustomerDetails: GetAllCustomerDetails,
        //file upload and convert
        FileUploadandConvert: FileUploadandConvert,
        AddBulkTagMaster: AddBulkTagMaster,
        GetSpecificTagMaster: GetSpecificTagMaster,
        //getAllTagCount
        GetAllTagCount: GetAllTagCount,
        GetallTagMasterbyTagTypeId: GetallTagMasterbyTagTypeId,
        GetallCustomerAssignedTagIdDetailsbytagQtyAssignmentId:GetallCustomerAssignedTagIdDetailsbytagQtyAssignmentId
      };

      return service;

      function GetUserData(usernameoremail, callback) {
        ////console.log(my_headers);
        $http.get(serverIp+apiUrls.getUserProfile+usernameoremail)
        .success(function (response){
            if(response.success==true){
              localStorage.setObject('CurrUserData', response.data);
            }
            callback(response);
        }).error(function (response){
            callback(response);
        })
      }
      //getAllTagCount
      function GetAllTagCount(callback) {
        $http.get(serverIp+apiUrls.getAllTagCount, { 
            headers: {  
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            },
        })
        .success(function (response){
            if(response.success==true){
            }
            callback(response);
        }).error(function (response){
            callback(response);
        })
      }
      //for Tag details
      function GetAllTagType(callback) {
          $http.get(serverIp+apiUrls.getalltagtype, { })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function AddTagType(TagType, TagTypeName, TagTypeDescription, callback) {
          $http.post(serverIp+apiUrls.addtagtype, { TagType: TagType, TagTypeName: TagTypeName, TagTypeDescription: TagTypeDescription })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      function DeleteTagType(tagTypeId,callback){
          $http.delete(serverIp+apiUrls.deletetagtypeById+tagTypeId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdatetagType(tagTypeId, TagType, TagTypeName, TagTypeDescription,callback){
          $http.put(serverIp+apiUrls.updatetagTypeById+tagTypeId, { TagType: TagType, TagTypeName: TagTypeName, TagTypeDescription: TagTypeDescription })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      //for Tag Assignment Details
      function GetAllTagQtyAssignment(callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.getallTagQtyAssignment,config, { })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        })
    }

    function AddTagQtyAssignment(customerId, tagTypeId, tagQty, callback) {
        var data = $.param({
            customerId: customerId, tagTypeId: tagTypeId, tagQty: tagQty
        });
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.post(serverIp+apiUrls.addTagQtyAssignment, data, config)
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    function DeleteTagQtyAssignment(tagQtyAssignId,callback){
        var data = $.param({
            
        });
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.delete(serverIp+apiUrls.deleteTagQtyAssignmentById+tagQtyAssignId,config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function UpdatetagQtyAssignment(tagQtyAssignId, customerId, tagTypeId, tagQty,callback){
        var data = $.param({
            customerId: customerId, 
            tagTypeId: tagTypeId, tagQty: parseInt(tagQty)
        });
        $http.put(serverIp+apiUrls.updateTagQtyAssignmentById+tagQtyAssignId, data,config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

          

      //for Tag Master Details
      function GetAllTagMaster(callback) {
        $http.get(serverIp+apiUrls.getallTagMaster, { })
            .success(function (response) {
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    // for add Tag Master details
    function AddTagMaster(tagPacket, tagId, tagType,manufacturingDate, callback) {
        $http.post(serverIp+apiUrls.addTagMaster, { tagPacket: tagPacket, tagId: tagId, tagType: tagType,
            manufacturingDate:manufacturingDate })
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }

    function DeleteTagMaster(tagmasterId,callback){
        $http.delete(serverIp+apiUrls.deleteTagbyId+tagmasterId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

    function UpdatetagMaster(tagmasterId,tagPacket, tagId, tagType, manufacturingDate, callback){
        $http.put(serverIp+apiUrls.updateTagbyId+tagmasterId, { tagPacket: tagPacket, 
            tagId: tagId, tagType: tagType,manufacturingDate:manufacturingDate })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    

    // Get All CustomerDetails
    function GetAllCustomerDetails(callback) {
        $http.get(serverIp+apiUrls.getallCustomers, { })
            .success(function (response) { 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    // Get All CustomerDetails
    function FileUploadandConvert(formData,callback) {
        $http.post(serverIp+apiUrls.fileUploadandConvert, formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(response){
            callback(response);
        })
        .error(function(response){
            callback(response);
        });
    }
    //add bulk tags to db
    function AddBulkTagMaster(data,callback){
        $http.post(serverIp+apiUrls.AddBulkTagMaster,data )
        .success(function (response) { 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    ///getSpecificTagBYTagType/:size/:tagTypeId
    function GetSpecificTagMaster(size,tagtype,id,callback){
        //{{url_tmass_local_server}}/api/getSpecificTagBYTagType/5b47e6aa21e42951cf99b04d?size=2
        //var data = $.param({});
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        //$http.get(serverIp+apiUrls.getTireDetails+tireSerialNumber, config)
        $http.get(serverIp+apiUrls.getSpecificTagBYTagType+tagtype+'?size='+size+'&id='+id, config)
        //$http.get(serverIp+apiUrls.getSpecificTagBYTagType+tagtype+'?size='+size+'&id='+id )
        .success(function (response) { 
            callback(response);
        }).error(function (response) {
            callback(response);
        });
    }

    function GetallTagMasterbyTagTypeId(tagTypeId,pageNo,size,callback){
        //getallTagMasterbyTagTypeId
        $http.get(serverIp+apiUrls.getallTagMasterbyTagTypeId+tagTypeId+'?pageNo='+pageNo+'&size='+size )
        .success(function (response) { 
            callback(response);
        }).error(function (response) {
            callback(response);
        });
    }
    function GetallCustomerAssignedTagIdDetailsbytagQtyAssignmentId(tagQtyAssignmentId,callback){
        //getallCustomerAssignedTagIdDetailsbytagQtyAssignmentId
        var data = $.param({

        });
        //$http.put(serverIp+apiUrls.updateTagQtyAssignmentById+tagQtyAssignId, data,config)
        $http.get(serverIp+apiUrls.getallCustomerAssignedTagIdDetailsbytagQtyAssignmentId+tagQtyAssignmentId )
        .success(function (response) { 
            callback(response);
        }).error(function (response) {
            callback(response);
        });
    }
  }  
  })();