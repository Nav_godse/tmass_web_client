(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('issueReportService', issueReportService);
  
    /** @ngInject */
    function issueReportService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
          getAllIssueReport:'/api/getAllIssueReport'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }
      
      var service = {
        GetAllIssueReport: GetAllIssueReport,
      };

      return service;

    //for get tire report details
    function GetAllIssueReport(callback) {
        $http.get(serverIp+apiUrls.getAllIssueReport, { 
            headers: {  
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            },
        })
        .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    
  }
  
  })();