(function() {
  'use strict';

  angular.module('TmassAdmin.pages.services', [])
    //.config(routeConfig)
    .factory('AuthenticationService', Service);
  /** @ngInject */
  // function routeConfig() {

  // }
  function Service($http, localStorage,config) {
        var serverIp = config.apiUrl;

        var authUrls = {
            login :'/api/login',
            forgotPassword:'/api/forgotPassword',
            resetPassword:'/api/resetpassword',
            getUserProfile:'/api/getUserProfile',
            //get my user details
            getCurrentUserProfile: "/api/getCurrentUserProfile/",
        }

        var service = {
            Login : Login,
            Forgot : Forgot,
            ResetPassword : ResetPassword,
            Logout : Logout,
            GetCurrentUserDetails: GetCurrentUserDetails,
            GetMasterRoles: GetMasterRoles,
        };
        return service;
        function Login(usernameoremail, password, callback) {
            $http.post(serverIp+authUrls.login, { usernameoremail: usernameoremail, password: password })
                .success(function (response) {
                    console.log(response);
                // login successful if there's a token in the response
                //var test = GetCurrentUserDetails(usernameoremail,response.token);
                //   console.log(test);
                var currResponseToken = response.token;
                if(currResponseToken==undefined){
                    callback(response.message);
                    return
                }
                //if( response.data.roleName=="OEM User" || response.data.roleName=="Client User" ){
                if( response.data.roleName=="Client User" ){
                    callback('You do not have permission to login web');
                    return
                }
                var currUserLicenseType= false;
                //if( response.data.isCustomer==false && response.data.roleName=="OEM Manager" || response.data.roleName=="Client Manager" || response.data.roleName=="System User" || response.data.roleName=="System Manager" || response.data.roleName=="System Super Admin" || response.data.roleName=="System Admin" || response.data.roleName=="OEM Admin" ){
                if( response.data.isCustomer==false && response.data.roleName=="OEM Manager" || response.data.roleName=="OEM User" || response.data.roleName=="Client Manager" || response.data.roleName=="System User" || response.data.roleName=="System Manager" || response.data.roleName=="System Super Admin" || response.data.roleName=="System Admin" || response.data.roleName=="OEM Admin" ){
                    currUserLicenseType= true;
                }else if(response.data.isCustomer==true || response.data.roleName=="Client Admin" ){
                    var loginUserResponse= response.data.CustomerLicenseDetails;
                    for(var i=0;i<loginUserResponse.length;i++){
                        //if(loginUserResponse[i]["licenseTypeName"] == "Connected" && response.data.roleName!="OEM Manager" || response.data.roleName!="Client Manager" || response.data.roleName!="System Manager" || response.data.roleName=="Client Admin"){
                        if(loginUserResponse[i]["licenseTypeName"] == "Connected" && response.data.roleName!="Client Manager" || response.data.roleName!="System Manager" || response.data.roleName=="Client Admin" || response.data.roleName=="OEM User" ){
                            currUserLicenseType= true;
                        }                        
                    }
                }
                    //var licenseTypeName = response.data.licenseTypeName;
                    if (currResponseToken) {
                        console.log('response.data');
                        console.log(response.data);
                            // store username and token in local storage to keep user logged in between page refreshes
                            //localStorage.currentUser = { usernameoremail: usernameoremail, token: response.token };   
                            // add jwt token to auth header for all requests made by the $http service
                            //$http.defaults.headers.common.Authorization = 'Bearer ' + response.token;
                            $http.defaults.headers.common.Authorization = response.token;    
                            // execute callback with true to indicate successful login
                            if(currUserLicenseType == true){
                                var loginUser = {
                                    usernameoremail: usernameoremail, token: currResponseToken, otherData: response.data
                                };
                                localStorage.setObject('dataUser', loginUser);
                                callback(response);
                            }
                            // else if(response.data[0].isCustomer==false){
                            //    callback('You do not have permission and cannot login to web');
                            //    return
                            // }                          
                        } else {
                            // execute callback with false to indicate failed login
                            callback(response);
                        }
                    //callback(response);
                // })

                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }

        function Forgot(usernameoremail, mobile, callback) {
            $http.post(serverIp+authUrls.forgotPassword, { usernameoremail: usernameoremail, mobile: mobile })
                .success(function (response) {
                    if(response.success==true){
                    localStorage.currentUser = { usernameoremail: usernameoremail, mobile: mobile};
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }

        function ResetPassword(usernameoremail, mobile,otp,password, callback) {
            $http.post(serverIp+authUrls.resetPassword, { usernameoremail: usernameoremail, mobile: mobile, otp: otp, password: password })
                .success(function (response) {
                    if(response.success==true){
                    localStorage.currentUser = { usernameoremail: usernameoremail, mobile: mobile};
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }

        function getUserData(usernameoremail, mobile, callback) {
            $http.get(serverIp+authUrls.getUserProfile, { usernameoremail: usernameoremail, mobile: mobile })
            .success(function (response){
                if(response.success==true){
                    localStorage.currentUser = { usernameoremail: usernameoremail, mobile: mobile};
                }
                callback(response);
            }).error(function (response){
                callback(response);
            })
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            delete localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        }

        function GetCurrentUserDetails(data1,callback){
            //getCurrentUserProfile
            $http.get(serverIp+authUrls.getCurrentUserProfile+data1)
            .success(function (response,headers,config) { 
            callback(response);
            }).error(function (response,headers,config) {
                callback(response);
            });
        }

        function GetMasterRoles(){
            return authRoles=['System Admin', 'System Super Admin', 'System Manager', 'System User', 'OEM Admin', 'Client Admin']   // <-- roles allowed for this module
        }
    
  }
})();