(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('allDashboardService', allDashboardService);
  
    /** @ngInject */
    function allDashboardService($window,$http,localStorage,config) {
      var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
          //tag type
          getAllTireReport:'/api/getAllTireReport',
          getallVehicleAlertsReport:'/api/getallVehicleAlertsReport',
          showTireDashboard:'/api/showTireDashboard',
          showVehicleDashboard:'/api/showVehicleDashboard',
          showTireDashboardfAdmins: '/api/showTireDashboardfAdmins',
          showTireDashboardforMangers: '/api/showTireDashboardforMangers',
          showTireDashboardforSystem: '/api/showTireDashboardforSystem',
          sendMail: '/api/sendMail',
          showAlertDashboard: '/api/showAlertDashboard',
          showPerformanceDashboard:'/api/showPerformanceDashboard',
          getAvgCostPerKm:'/api/getAvgCostPerKm',
          getAvgdistanceTravelledwithoutissue:'/api/getAvgdistanceTravelledwithoutissue',
          getAvgmaintainancecost:'/api/getAvgmaintainancecost',
          getalertCrons:'/api/getalertCrons',
          alertcronvehiclealertmaster:'/api/alertcronvehiclealertmaster',
          allmyusersandcustomer:'/api/getusersandcustomers',
          saveMyalertsSettings:'/api/addalertCron',
          deletealertCron:'/api/deletealertCron/'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }
      
      var service = {
        getAllTireReport: getAllTireReport,
        GetAllVehicleAlertsReport: GetAllVehicleAlertsReport,
        //showTireDashboard:showTireDashboard,
        showVehicleDashboard: showVehicleDashboard,
        showTireDashboardMgrs:showTireDashboardMgrs,
        showTireDashboardAdmins: showTireDashboardAdmins,
        showTireDashboardforSystem:showTireDashboardforSystem,
        SendMail:SendMail,
        showAlertDashboard: showAlertDashboard,
        getAvgCostPerKm:getAvgCostPerKm,
        getAvgdistanceTravelledwithoutissue:getAvgdistanceTravelledwithoutissue,
        getAvgmaintainancecost:getAvgmaintainancecost,
        getalertCrons:getalertCrons,
        alertcronvehiclealertmaster:alertcronvehiclealertmaster,
        allmyusersandcustomer:allmyusersandcustomer,
        saveMyalertsSettings:saveMyalertsSettings,
        deletealertCron:deletealertCron,
        showPerformanceDashboard:showPerformanceDashboard
      };

      return service; 
      //for get tire report details
    //   function getAllTireReport(callback) {
    //       $http.get(serverIp+apiUrls.getAllTireReport, { })
    //           .success(function (response) {
    //               if(response.success==true){
    //                   //console.log(response);
    //               }
    //               // login successful if there's a token in the response
    //               callback(response);
    //           }).error(function (response) {
    //               // handle error things
    //               //console.log(response);
    //               callback(response);
    //           });
    //   }

        //with headers
      //for get tire report details
    function getAllTireReport(callback) {
        $http.get(serverIp+apiUrls.getAllTireReport, { 
            headers: {  
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            },
        })
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //allmyusersandcustomer
    function allmyusersandcustomer(callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.allmyusersandcustomer, config)
            .success(function (response) {
            if(response.success==true){
                console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //Delete all my alert crons
    function deletealertCron(id,vehicleId,alertType,callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.delete(serverIp+apiUrls.deletealertCron+id+'/'+alertType+'/'+vehicleId, config)
            .success(function (response) {
            if(response.success==true){
                console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //saveMyalertsSettings
    function saveMyalertsSettings(myData,callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        var data = $.param({
            "vehicleData":myData.vehicleData,
            "emails":myData.emails,
            "freq":myData.freq
            
        })
        console.log(data);
        $http.post(serverIp+apiUrls.saveMyalertsSettings, data, config)
            .success(function (response) {
            if(response.success==true){
                console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //getAvgCostPerKm
    function getAvgCostPerKm(callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.getAvgCostPerKm, config)
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //getAvgdistanceTravelledwithoutissue
    function getAvgdistanceTravelledwithoutissue(callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.getAvgdistanceTravelledwithoutissue, config)
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //getAvgmaintainancecost
    function getAvgmaintainancecost(callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.getAvgmaintainancecost, config)
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //getalertCrons
    function getalertCrons(callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.getalertCrons, config)
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //alertcronvehiclealertmaster
    function alertcronvehiclealertmaster(callback) {
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.alertcronvehiclealertmaster, config)
            .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //for get tire report details
    function GetAllVehicleAlertsReport(callback) {
        $http.get(serverIp+apiUrls.getallVehicleAlertsReport, {})
        .success(function (response) {
            if(response.success==true){
            ////console.log(response);
            } 
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

      function AddRole(roleName, roleLevel, callback) {
          $http.post(serverIp+apiUrls.addrole, { roleName: roleName, roleLevel: roleLevel })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      function DeleteRole(roleId,callback){
          $http.delete(serverIp+apiUrls.deleteroleById+roleId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateRole(roleId, roleName, roleLevel,callback){
          $http.put(serverIp+apiUrls.updateroleById+roleId, {  roleName: roleName, roleLevel: roleLevel })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function showTireDashboardMgrs(callback){
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.showTireDashboardforMangers, config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    
    function showTireDashboardAdmins(callback){
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.showTireDashboardfAdmins, config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //showTireDashboardforSystem
    function showTireDashboardforSystem(callback){
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.showTireDashboardforSystem, config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //showVehicleDashboard
    function showVehicleDashboard(callback){
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.showVehicleDashboard, config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }

    //showPerformanceDashboard
    function showPerformanceDashboard(callback){
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.showPerformanceDashboard, config)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
}

    //showAlertDashboard
    function showAlertDashboard(callback){
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.showAlertDashboard, config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //sendmail
    function SendMail(toEmail,subject,dashboardName,mailBody,fileData,callback){
            var data = $.param({
                toEmail:toEmail,
                subject:subject,
                dashboardName: dashboardName,
                mailBody:mailBody,
                filepath:fileData
            })
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.post(serverIp+apiUrls.sendMail,data, config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
  }
  
  })();