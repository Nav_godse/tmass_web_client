(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('customerService', customerService);
  
    /** @ngInject */
    function customerService($window,$http,localStorage,config) {
      var serverIp = config.apiUrl;
      var my_user = {};

      var apiUrls = {
          getUserProfile:'/api/getCurrentUserProfile/',
          getallCustomers:'/api/getallCustomerswithToken',
         // addCustomerDetails: '/api/addCustomerDetails',
         addCustomerDetails: '/api/addCustomerDetailsWithToken', 
         updateCutomerbyId: '/api/updateCustomerbyId/',
          deleteCutomerbyId: '/api/deleteCutomerbyId/',
          //state ans city
          getallStates:'/api/getallStates',
          getallCities:'/api/getallCities',
          //license type details
          getalllicensetype:'/api/getallLicenseType',
          addlicensetype:'/api/addLicenseType',
          deletelicenseTypeById:'/api/deleteLicenseTypebyId/',
          updatelicenseTypeById:'/api/updateLicenseTypebyId/',
          //customer types
          getallcustomertype:'/api/getallCustomerType',
          addcustomertype:'/api/addCustomerType',
          deletecustomertypeById:'/api/deleteCustomerType/',
          updatecustomertypeById:'/api/updateCustomerType/',
          //roles
          addroles:'/api/addRole',
          getallroles:'/api/getallRoles',
          updaterolebyid:'/api/updaterolebyid/',
          deleterolebyid:'/api/deleterolebyid/',
          //CustomerAssignedTagIdDetails
          AddCustomerAssignedTagIdDetails:'/api/AddCustomerAssignedTagIdDetails',
          AddCustomerAssignedTagIdDetailsbulk:'/api/AddCustomerAssignedTagIdDetailsbulk',
          getallCustomerAssignedTagIdDetails: '/api/getallCustomerAssignedTagIdDetails',
          updateCustomerAssignedTagIdById: '/api/updateCustomerAssignedTagIdById/',
          deleteCustomerAssignedTagIdById: '/api/deleteCustomerAssignedTagIdById/',
          //CustomerTagAllocatesToDepot
          AddCustomerTagAllocatesToDepot:'/api/AddCustomerTagAllocatesToDepot',
          getallCustomerTagAllocatesToDepot: '/api/getallCustomerTagAllocatesToDepot',
          updateCustomerTagAllocatesToDepot: '/api/updateCustomerTagAllocatesToDepot/',
          deleteCustomerTagAllocatesToDepot: '/api/deleteCustomerTagAllocatesToDepot/',
          //license details
          AddCustomerLicenseDetail:'/api/AddCustomerLicenseDetail',
          getallCustomerLicenseDetail: '/api/getallCustomerLicenseDetail',
          updateCustomerLicenseDetailById: '/api/updateCustomerLicenseDetailById/',
          deleteCustomerLicenseDetailById: '/api/deleteCustomerLicenseDetailById/',
          //get count of the customer tag
          getTagAllocatedTocustomer: '/api/getTagAllocatedTocustomer',
          homePageSettings: '/api/homePageSettings',
          addsettings: '/api/addsettings',
          deleteSettings:'/api/deleteSettings/',
          // get users and customers of the system
          getAllUserInSystem: '/api/getAllUserInSystem',
          getAllCustomerInSystem: '/api/getAllCustomerInSystem',
          showadminsOfOEM: '/api/showadminsOfOEM',
          updateCutomerProfilebyId: '/api/updateCutomerProfilebyId/',
          getallTransporters: '/api/getallTransporters',
          firstTImeLogin:'/api/firstTImeLogin'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);

        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
      var service = {
        GetUserData: GetUserData,
        GetAllCustomers: GetAllCustomers,
        DeleteCustomer: DeleteCustomer,
        AddCustomer:AddCustomer,
        updateCustomer:updateCustomer,
        GetAllCustomerType: GetAllCustomerType,
        AddCustomerType: AddCustomerType,
        GetAllLicenseType: GetAllLicenseType,
        DeleteCustomerType: DeleteCustomerType,
        UpdateCustomerType: UpdateCustomerType,
        AddLicenseType: AddLicenseType,
        DeleteLicenseType: DeleteLicenseType,
        UpdateLicenseType: UpdateLicenseType,
        GetAllStates: GetAllStates,
        GetAllCities: GetAllCities,
        DeleteCustomerAssignedTagIdById: DeleteCustomerAssignedTagIdById,
        UpdateCustomerAssignedTagIdById: UpdateCustomerAssignedTagIdById,
        AddCustomerAssignedTagDetails: AddCustomerAssignedTagDetails,
        GetAllCustomerAssignedTagId: GetAllCustomerAssignedTagId,
        //CustomerTagAllocatesToDepot
        AddCustomerTagAllocatesToDepot: AddCustomerTagAllocatesToDepot,
        GetallCustomerTagAllocatesToDepot: GetallCustomerTagAllocatesToDepot,
        DeleteCustomerTagAllocatesToDepot: DeleteCustomerTagAllocatesToDepot,
        UpdateCustomerTagAllocatesToDepot: UpdateCustomerTagAllocatesToDepot,
        //licenseDetails
        GetallCustomerLicenseDetail: GetallCustomerLicenseDetail,
        AddCustomerLicenseDetail: AddCustomerLicenseDetail,
        UpdateCustomerLicenseDetail: UpdateCustomerLicenseDetail,
        DeleteCustomerLicense: DeleteCustomerLicense,
        AddCustomerAssignedTagDetailsBulk:AddCustomerAssignedTagDetailsBulk,
        //get COunt for customer tags
        GetTagAllocatedTocustomer : GetTagAllocatedTocustomer,
        addhomePageSettings: addhomePageSettings,
        deletehomePageSettings: deletehomePageSettings,
        updatehomePageSettings: updatehomePageSettings,
        gethomePageSettings: gethomePageSettings,
        //get all users and customers in system
        GetAllUsersInsystem: GetAllUsersInsystem,
        GetAllCusotmersInsystem: GetAllCusotmersInsystem,
        ShowadminsOfOEM:ShowadminsOfOEM,
        updateCustomerProfile:updateCustomerProfile,
        GetallTransporters:GetallTransporters,
        firstTImeLogin:firstTImeLogin
      };

      return service;

      function GetUserData(usernameoremail, callback) {
        ////console.log(my_headers);
        $http.get(serverIp+apiUrls.getUserProfile+usernameoremail)
        .success(function (response){
            if(response.success==true){
              localStorage.setObject('CurrUserData', response.data);
            }
            callback(response);
        }).error(function (response){
            callback(response);
        })
      }
      function firstTImeLogin(isCustomer,id, callback) {
        var data = $.param({
            isCustomer: isCustomer,
            _id:id,
        });
        $http.post(serverIp+apiUrls.firstTImeLogin, data, config)
            .success(function (response, config) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response, config) {
                // handle error things
                callback(response);
            });
      }
      //for customer details
      function GetAllCustomers(callback) {
        var data = $.param({});
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.getallCustomers,config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
      }

      //get all transporter
     //for customer details
     function GetallTransporters(callback) {
        var data = $.param({});
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.get(serverIp+apiUrls.getallTransporters,config)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

      //for adding customer
    function AddCustomer(custdata, callback) {
        var data = $.param({
            isEnable: custdata.isEnable,
            customerName:custdata.customerName,
            email:custdata.email,
            addressLine1:custdata.addressLine1,
            addressLine2:custdata.addressLine2,
            pincode:custdata.pincode,
            cityId:custdata.cityId,
            stateId:custdata.stateId,
            country:custdata.country,
            customerTypeId:custdata.customerTypeId
        });
        $http.post(serverIp+apiUrls.addCustomerDetails, data, config)
            .success(function (response, config) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response, config) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //for adding customer
    function updateCustomer(custdata, callback) {
        console.log("inside update customer");
        console.log(custdata);
        $http.put(serverIp+apiUrls.updateCutomerbyId+custdata._id, {
            isEnable: custdata.isEnable,
            customerName:custdata.customerName,
            email:custdata.email,
            addressLine1:custdata.addressLine1,
            addressLine2:custdata.addressLine2,
            pincode:custdata.pincode,
            cityId:custdata.cityId,
            stateId:custdata.stateId,
            country:custdata.country,
            customerTypeId:custdata.customerTypeId,
            createdByCustomerId:custdata.createdByCustomerId,
            isnewEmail:custdata.isnewEmail,
            username: custdata.customerName,
            name: custdata.customerName,
            // roleId:custdata.roleId,
            // roleName:custdata.,
            // password: bcrypt.hashSync(currpassword, bcrypt.genSaltSync(10))
        })
        .success(function (response) {
            if(response.success==true){
                ////console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //updateCustomerProfile updateCutomerProfilebyId
    function updateCustomerProfile(custdata, callback) {
        console.log("inside update customer");
        console.log(custdata);
        $http.put(serverIp+apiUrls.updateCutomerProfilebyId+custdata._id, {
            isEnable: custdata.isEnable,
            customerName:custdata.customerName,
            email:custdata.email,
            addressLine1:custdata.addressLine1,
            addressLine2:custdata.addressLine2,
            pincode:custdata.pincode,
            cityId:custdata.cityId,
            stateId:custdata.stateId,
            country:custdata.country,
            customerTypeId:custdata.customerTypeId,
            createdByCustomerId:custdata.createdByCustomerId,
            isnewEmail:custdata.isnewEmail,
            username: custdata.customerName,
            name: custdata.customerName,
            password: custdata.password
        })
        .success(function (response) {
            if(response.success==true){
                ////console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
     //for customer delete
     function DeleteCustomer(customerId,callback){
        var data = $.param({});
        var config = {
            headers : {
                'x-access-token':my_token.token,
                'Content-Type':'application/x-www-form-urlencoded'
            }
        }
        $http.delete(serverIp+apiUrls.deleteCutomerbyId+customerId,config)
        .success(function (response,headers, config) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response,headers, config) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
      //get all cust types
      function GetAllCustomerType(callback) {
          $http.get(serverIp+apiUrls.getallcustomertype, { })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      
      //get state and city
        function GetAllStates(callback) {
            $http.get(serverIp+apiUrls.getallStates, { })
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
        function GetAllCities(callback) {
            $http.get(serverIp+apiUrls.getallCities, { })
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
      //for customer types
      function AddCustomerType(customerTypeName, customerTypeDesc, callback) {
          $http.post(serverIp+apiUrls.addcustomertype, { customerTypeName: customerTypeName, customerTypeDesc: customerTypeDesc })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      //for customer type delete
      function DeleteCustomerType(customerTypeId,callback){
        $http.delete(serverIp+apiUrls.deletecustomertypeById+customerTypeId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    //for customer type update
    function UpdateCustomerType(customerid,customerTypeName,customerTypeDesc,callback){
        $http.put(serverIp+apiUrls.updatecustomertypeById+customerid, { customerTypeName: customerTypeName, customerTypeDesc: customerTypeDesc })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

        //for license details
        function GetAllLicenseType(callback) {
          $http.get(serverIp+apiUrls.getalllicensetype, { })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function AddLicenseType(licenseTypeName, licenseTypeDesc, callback) {
          $http.post(serverIp+apiUrls.addlicensetype, { licenseTypeName: licenseTypeName, licenseTypeDesc: licenseTypeDesc })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function DeleteLicenseType(licenseTypeId,callback){
          $http.delete(serverIp+apiUrls.deletelicenseTypeById+licenseTypeId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateLicenseType(licenseTypeId,licenseTypeName,licenseTypeDesc,callback){
          $http.put(serverIp+apiUrls.updatelicenseTypeById+licenseTypeId, { licenseTypeName: licenseTypeName, licenseTypeDesc: licenseTypeDesc })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }
      //customer tag assignemnet data
            //for customer details
            function GetAllCustomerAssignedTagId(callback) {
                $http.get(serverIp+apiUrls.getallCustomerAssignedTagIdDetails, { })
                    .success(function (response) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
              }
        
              //for adding customer
            function AddCustomerAssignedTagDetails(tagAssignmentData, callback) {
                $http.post(serverIp+apiUrls.AddCustomerAssignedTagIdDetails,{ 
                    tagQtyAssignmentId:tagAssignmentData.tagQtyAssignmentId,
                    tagmasterId:tagAssignmentData.tagmasterId,
                    depotId:tagAssignmentData.depotId,
                    tagTypeId:tagAssignmentData.tagTypeId
                 })
                    .success(function (response) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }

            function AddCustomerAssignedTagDetailsBulk(tagAssignmentData, callback) {
                $http.post(serverIp+apiUrls.AddCustomerAssignedTagIdDetailsbulk,tagAssignmentData)
                    .success(function (response) {
                        if(response.success==true){
                            console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }
            //for adding customer
            function UpdateCustomerAssignedTagIdById(tagAssignmentData, callback) {
                //console.log(tagAssignmentData);
                $http.put(serverIp+apiUrls.updateCustomerAssignedTagIdById+tagAssignmentData._id,
                    { 
                        tagQtyAssignmentId:tagAssignmentData.tagQtyAssignmentId,
                        tagmasterId:tagAssignmentData.tagmasterId,
                        depotId:tagAssignmentData.depotId,
                        tagTypeId:tagAssignmentData.tagTypeId 
                    })
                    .success(function (response) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }
             //for customer delete
             function DeleteCustomerAssignedTagIdById(id,callback){
                $http.delete(serverIp+apiUrls.deleteCustomerAssignedTagIdById+id)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }

      //customer tag allocate to depot data
            //for customer details
            function GetallCustomerTagAllocatesToDepot(callback) {
                //add header
                var configAllocateHeader = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                $http.get(serverIp+apiUrls.getallCustomerTagAllocatesToDepot,configAllocateHeader, { })
                .success(function (response) {
                    //if(response.success==true){
                        // login successful if there's a token in the response   
                        callback(response);
                        //console.log(response);
                    //}              
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
              }
        
              //for adding customer

            function AddCustomerTagAllocatesToDepot(tagdepotData, callback) {
                var data = $.param({
                    customerId:tagdepotData.customerId,
                    depotId:tagdepotData.depotId,
                    tagQty:tagdepotData.tagQty,
                    tagTypeId:tagdepotData.tagTypeId
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                $http.post(serverIp+apiUrls.AddCustomerTagAllocatesToDepot, data, config)
                    .success(function (response) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }
            //for adding customer'
            
            function UpdateCustomerTagAllocatesToDepot(tagdepotData, callback) {
                //console.log(tagdepotData);.
                var data = $.param({
                    customerId:tagdepotData.customerId,
                    depotId:tagdepotData.depotId,
                    tagQty:tagdepotData.tagQty,
                    tagTypeId:tagdepotData.tagTypeId
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                $http.put(serverIp+apiUrls.updateCustomerTagAllocatesToDepot+tagdepotData._id, data,config)
                    .success(function (response) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }
             //for customer delete
             function DeleteCustomerTagAllocatesToDepot(id,callback){
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                $http.delete(serverIp+apiUrls.deleteCustomerTagAllocatesToDepot+id,config,{})
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }
            //customerLicenseDetail
            //for customer LicenseDetail LicenseDetail

            function GetallCustomerLicenseDetail(callback) {
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                $http.get(serverIp+apiUrls.getallCustomerLicenseDetail,config)
                    .success(function (response, config) {
                        if(response.success==true){
    
                        }
                        callback(response);
                    }).error(function (response, config) {
                        callback(response);
                    });
            }


            // function GetallCustomerLicenseDetail(callback) {
            //     var config = {
            //         headers : {
            //             'x-access-token':my_token.token,
            //             'Content-Type':'application/x-www-form-urlencoded'
            //         }
            //     }
            //     $http.get(serverIp+apiUrls.getallCustomerLicenseDetail,config)
            //         .success(function (response,headers,config) {
            //             if(response.success==true){
            //                 //console.log(response);
            //             }
            //             // login successful if there's a token in the response
            //             callback(response);
            //         }).error(function (response, headers,config) {
            //             // handle error things
            //             //console.log(response);
            //             callback(response);
            //         });
            //   }
        
              //for adding customer LicenseDetail

            function AddCustomerLicenseDetail(licenseData, callback) {

                var data = $.param({
                    customerId: licenseData.customerId,
                    licenseDate: licenseData.licenseDate,
                    licenseTypeId: licenseData.licenseTypeId,
                    tireQty: licenseData.tireQty,
                    vehicleQty: licenseData.vehicleQty
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }

                $http.post(serverIp+apiUrls.AddCustomerLicenseDetail, data, config)
                    .success(function (response, config) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response, config) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }
            //for adding customer'

            function UpdateCustomerLicenseDetail(licenseData, callback) {
                var data = $.param({
                    customerId: licenseData.customerId,
                    licenseDate: licenseData.licenseDate,
                    licenseTypeId: licenseData.licenseTypeId,
                    tireQty: licenseData.tireQty,
                    vehicleQty: licenseData.vehicleQty
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }

                $http.put(serverIp+apiUrls.updateCustomerLicenseDetailById+licenseData._id, data, config)
                    .success(function (response, config) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response, config) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }
             //for customer delete
             
            function DeleteCustomerLicense(id,callback){
                $http.delete(serverIp+apiUrls.deleteCustomerLicenseDetailById+id, config)
                .success(function (response, config) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response, config) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }

            // for count of tags for particular tags
            function GetTagAllocatedTocustomer(customerId,tagtypeId,callback){
                var data = $.param({
            
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                //?depotId=5b51c752f583b10a5c2a4f22&customerId=5b51ab21a2022208939802a0&tagTypeId=5b47e6aa21e42951cf99b04d
                $http.get(serverIp+apiUrls.getTagAllocatedTocustomer+'?customerId='+customerId+'&tagTypeId='+tagtypeId, config)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }

            function addhomePageSettings(settingData,callback){
                var data = $.param(settingData);
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                $http.post(serverIp+apiUrls.addsettings,data, config)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }
            function updatehomePageSettings(callback){
                var data = $.param({
            
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                //?depotId=5b51c752f583b10a5c2a4f22&customerId=5b51ab21a2022208939802a0&tagTypeId=5b47e6aa21e42951cf99b04d
                $http.put(serverIp+apiUrls.homePageSettings, config)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }
            function deletehomePageSettings(id,callback){
                var data = $.param({
            
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                //?depotId=5b51c752f583b10a5c2a4f22&customerId=5b51ab21a2022208939802a0&tagTypeId=5b47e6aa21e42951cf99b04d
                $http.delete(serverIp+apiUrls.deleteSettings+id, config)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }

            function gethomePageSettings(callback){
                var data = $.param({
            
                });
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                //?depotId=5b51c752f583b10a5c2a4f22&customerId=5b51ab21a2022208939802a0&tagTypeId=5b47e6aa21e42951cf99b04d
                $http.get(serverIp+apiUrls.homePageSettings, config)
                .success(function (response) {
                    // if(response.success==true){
                    //     //console.log(response);
                    // }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }

            function GetAllUsersInsystem(callback) {
                $http.get(serverIp+apiUrls.getAllUserInSystem)
                    .success(function (response) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        callback(response);
                    }).error(function (response) {
                        //console.log(response);
                        callback(response);
                    });
            }

            function GetAllCusotmersInsystem(callback) {
                $http.get(serverIp+apiUrls.getAllCustomerInSystem, { })
                    .success(function (response) {
                        if(response.success==true){
                            //console.log(response);
                        }
                        // login successful if there's a token in the response
                        callback(response);
                    }).error(function (response) {
                        // handle error things
                        //console.log(response);
                        callback(response);
                    });
            }

            function ShowadminsOfOEM(callback){
                //showadminsOfOEM
                var config = {
                    headers : {
                        'x-access-token':my_token.token,
                        'Content-Type':'application/x-www-form-urlencoded'
                    }
                }
                //?depotId=5b51c752f583b10a5c2a4f22&customerId=5b51ab21a2022208939802a0&tagTypeId=5b47e6aa21e42951cf99b04d
                $http.get(serverIp+apiUrls.showadminsOfOEM, config)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
            }

  }
  
  })();