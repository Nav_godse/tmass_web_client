(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.services')
      .service('tireService', tireService);
  
    /** @ngInject */
    function tireService($window,$http,localStorage,config) {
        var serverIp = config.apiUrl;

      var my_user = {};

      var apiUrls = {
          //tag type
          getalltiretype:'/api/getallTireType',
          addtiretype: '/api/addTireType',
          deletetiretypeById: '/api/deleteTireType/',
          updatetireTypeById: '/api/updateTireType/',
          // Tire Manufacture
          getalltiremanufacture:'/api/getallTireManufacture',
          addtiremanufacture: '/api/addTireManufacture',
          deleteTireManufactureById: '/api/deleteTireManufactureById/',
          updateTireManufactureById: '/api/updateTireManufactureById/',
           // Tire Status
           getalltirestatus:'/api/getallTireStatus',
           addtirestatus: '/api/addTireStatus',
           deleteTireStatusById: '/api/deleteTireStatusById/',
           updateTireStatusById: '/api/updateTireStatusById/',
           // Tire RecommendedPosition
           getAllTireRecommendedPosition:'/api/getallTireRecommendedPosition',
           addTireRecommendedPosition: '/api/addtireRecommendedPosition',
           deleteTireRecommendedPositionById: '/api/deleteTireRecommendedPositionById/',
           updateTireRecommendedPositionById: '/api/updateTireRecommendedPositionById/',
            // Tire Model
            getallTireModel:'/api/getallTireModel',
            addTireModel: '/api/addTireModel',
            deleteTireModelById: '/api/deleteTireModelById/',
            updateTireModelById: '/api/updateTireModelById/',
            // Tire Specifications
            getallTireSpecification:'/api/getalltireSpecification',
            addTireSpecification: '/api/addtireSpecification',
            deleteTireSpecificationById: '/api/deletetireSpecificationById/',
            updateTireSpecificationById: '/api/updatetireSpecificationById/',
            // Tire Details
            getallTireDetails:'/api/getTires',
            addTireDetails: '/api/addTire',
            deleteTireDetailsById: '/api/deletetireDetailsById/',
            updateTireDetailsById: '/api/updatetireDetailsById/',
            getTireDetails: '/api/getTireDetails/',
            //Tire all Data
            getAllTireDataUrl:'/api/getAllTireReport',
            getAllTireReportofSystem: '/api/getAllTireReportofSystem',
            getThreadDepthDetails:'/api/getThreadDepthDetails/',
            addBulkTireSpecMaster:'/api/AddBulkTireSpecMaster',
            GetTireSpecificationbyTireManufacurerandModel: '/api/GetTireSpecificationbyTireManufacurerandModel/'
      }

      my_user = $window.localStorage.dataUser;
      var my_token = JSON.parse(my_user);
      //console.log( my_token.token );

      var my_headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token':my_token.token
      }

        var header_config= {
        headers: my_headers
        }
      
      var service = {
            // Tire Type
            GetAllTireType: GetAllTireType,
            DeleteTireType: DeleteTireType,
            AddTireType: AddTireType,
            UpdateTireType: UpdateTireType,
            // Tire Manufacture
            GetallTireManufacture: GetallTireManufacture,
            AddTireManufacture: AddTireManufacture,
            DeleteTireManufacture: DeleteTireManufacture,
            UpdateTireManufacture: UpdateTireManufacture,
            // Tire Status
            GetallTireStatus: GetallTireStatus,
            AddTireStatus: AddTireStatus,
            DeleteTireStatus: DeleteTireStatus,
            UpdateTireStatus: UpdateTireStatus,
            // Tire RecommendedPosition
            GetallTireRecommendedPosition: GetallTireRecommendedPosition,
            AddTireRecommendedPosition: AddTireRecommendedPosition,
            DeleteTireRecommendedPosition: DeleteTireRecommendedPosition,
            UpdateTireRecommendedPosition: UpdateTireRecommendedPosition,
            // Tire Model
            GetallTireModel: GetallTireModel,
            AddTireModel: AddTireModel,
            DeleteTireModel: DeleteTireModel,
            UpdateTireModel: UpdateTireModel,
            //Tire Specfication
            GetallTireSpecification: GetallTireSpecification,
            AddTireSpecification: AddTireSpecification,
            DeleteTireSpecification: DeleteTireSpecification,
            UpdateTireSpecification: UpdateTireSpecification,
            //Tire Details
            GetallTireDetails: GetallTireDetails,
            GetTireDetails: GetTireDetails,
            AddTireDetails: AddTireDetails,
            DeleteTireDetails: DeleteTireDetails,
            UpdateTireDetails: UpdateTireDetails,
            //Tire Data
            GetAllTireData: GetAllTireData,
            GetTireReportofSystem:GetTireReportofSystem,
            GetThreadDepthDetails: GetThreadDepthDetails,
            AddBulkTireSpecMaster:AddBulkTireSpecMaster,
            GetTireSpecificationbyTireManufacurerandModel:GetTireSpecificationbyTireManufacurerandModel
      };
      return service; 




      var config = {
        headers : {
            'x-access-token':my_token.token,
            'Content-Type':'application/x-www-form-urlencoded'
        }
    }

    function AddBulkTireSpecMaster(tiredata, callback) {
        // var data1 = [];
        // var data1 = $.param({tiredata});
        // console.log(data1);
        $http.post(serverIp+apiUrls.addBulkTireSpecMaster, tiredata)
            .success(function (data1, status, config) { 
                callback(data1);
            })
            .error(function (data1, status, config) {
            callback(data1);
        });
    }

    function GetTireSpecificationbyTireManufacurerandModel(tireManufacture,tiremodelno, callback){
        $http.get(serverIp+apiUrls.GetTireSpecificationbyTireManufacurerandModel+tireManufacture+'/'+tiremodelno)
        .success(function (data1, status, config) { 
            callback(data1);
        })
        .error(function (data1, status, config) {
            callback(data1);
        });
    }
        //for Tire Specification
        function GetallTireDetails(callback) {
            $http.get(serverIp+apiUrls.getallTireSpecification, { })
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }




        function GetTireDetails(tireSerialNumber,callback) {
            var data = $.param({});
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.getTireDetails+tireSerialNumber, config)
            .success(function (response) {
                if(response.success==true){
                    ////console.log(response);
                } 
                callback(response);
            }).error(function (response) {
                callback(response);
            });
        }
        
        function GetAllTireData(callback) {
            var data = $.param({});
            var config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            }
            $http.get(serverIp+apiUrls.getAllTireDataUrl, config)
            .success(function (response) {
                if(response.success==true){
                    ////console.log(response);
                } 
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        function AddTireDetails(tireData, callback) {
            var data = $.param({
                tireManufacturerId:tireData.tireManufacturerId,
                tagId:tireData.tagId,
                tireSerialNumber:tireData.tireSerialNumber,
                isTireModelNew:tireData.isTireModelNew,
                tireModelNo:tireData.tireModelNo,
                tireThreadDepth_t1:tireData.tireThreadDepth_t1,
                tireThreadDepth_t2:tireData.tireThreadDepth_t2,
                tireThreadDepth_t3:tireData.tireThreadDepth_t3,
                tireWearIndicator_twi:tireData.tireWearIndicator_twi,
                maxPressureThreshold:tireData.maxPressureThreshold,
                minpressureThreshold:tireData.minPressureThreshold,
                maxTemperatureThreshold:tireData.maxTempratureThreshold,
                tireCost_inr:tireData.tireCost_inr,
                tireRemouldCost:tireData.tireRemouldCost,
                expectedTireLife_km:tireData.expectedTireLife_km,
                recommendedcoldInflationPressure:tireData.recommendedcoldInflationPressure,
                remouldCount:tireData.remouldCount,
                recommendedPositionName:tireData.recommendedPositionName,
                cusotmerAssignedTagId:tireData.cusotmerAssignedTagId,
                tireStatusName:tireData.tireStatusName,
                tireTypeName:tireData.tireTypeName,
                kmTravelled:tireData.kmTravelled,
                depotId:tireData.depotId
            });
    
            var my_header_config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            };

            $http.post(serverIp+apiUrls.addTireDetails, data, my_header_config)
            .success(function (response,status, headers, my_header_config) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response,status, headers, my_header_config) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
        function DeleteTireDetails(id,callback){
            console.log(id);
            var my_header_config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            };
            $http.delete(serverIp+apiUrls.deleteTireDetailsById+id,my_header_config)
            .success(function (response,headers, my_header_config) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response,headers, my_header_config) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        function UpdateTireDetails(tireData,callback){
            var data = $.param({
                tireManufacturerId:tireData.tireManufacturerId,
                tagId:tireData.tagId,
                tireSerialNumber:tireData.tireSerialNumber,
                tireModelNo:tireData.tireModelNo,
                kmTravelled:tireData.kmTravelled,
                depotId:tireData.depotId,
                tireStatusId :tireData.tireStatusId,
                tireTypeId :tireData.tireTypeId,
                tireModelId :tireData.tireModelId,
                tireWearIndicator: tireData.tireWearIndicator_twi,
                expectedTireLife_km: tireData.expectedTireLife_km,
                remoldCount: tireData.remouldCount,
                tireRemouldCost: tireData.tireRemouldCost
            });
            var my_header_config = {
                headers : {
                    'x-access-token':my_token.token,
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            };
            $http.put(serverIp+apiUrls.updateTireDetailsById+tireData._id,data,my_header_config)
            .success(function (response,headers, my_header_config) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response,headers, my_header_config) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }  
      //for Tire Specification
        function GetallTireSpecification(callback) {
            $http.get(serverIp+apiUrls.getallTireSpecification, { })
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }

        function AddTireSpecification(tireData, callback) {
            $http.post(serverIp+apiUrls.addTireSpecification, { 
                tireManufacturerName:tireData.tireManufacturerName,
                tireModelNo:tireData.tireModelNo,
                tireThreadDepth_t1:tireData.tireThreadDepth_t1,
                tireThreadDepth_t2:tireData.tireThreadDepth_t2,
                tireThreadDepth_t3:tireData.tireThreadDepth_t3,
                tireWearIndicator_twi:tireData.tireWearIndicator_twi,
                maxPressureThreshold:tireData.maxPressureThreshold,
                minpressureThreshold:tireData.minpressureThreshold,
                maxTemperatureThreshold:tireData.maxTemperatureThreshold,
                tireCost:tireData.tireCost,
                tireRemouldCost:tireData.tireRemouldCost,
                expectedTireLife_km:tireData.expectedTireLife_km,
                recommendedcoldInflationPressure:tireData.recommendedcoldInflationPressure,
                remouldCount:tireData.remouldCount,
                recommendedPositionName:tireData.recommendedPositionName,
                tireTypeName:tireData.tireTypeName
            })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
        function DeleteTireSpecification(id,callback){
            $http.delete(serverIp+apiUrls.deleteTireSpecificationById+id)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        function UpdateTireSpecification(tireData,callback){
            $http.put(serverIp+apiUrls.updateTireSpecificationById+tireData._id, { 
                tireManufacturerName:tireData.tireManufacturerName,
                tireModelNo:tireData.tireModelNo,
                tireThreadDepth_t1:tireData.tireThreadDepth_t1,
                tireThreadDepth_t2:tireData.tireThreadDepth_t2,
                tireThreadDepth_t3:tireData.tireThreadDepth_t3,
                tireWearIndicator_twi:tireData.tireWearIndicator_twi,
                maxPressureThreshold:tireData.maxPressureThreshold,
                minpressureThreshold:tireData.minpressureThreshold,
                maxTemperatureThreshold:tireData.maxTemperatureThreshold,
                tireCost:tireData.tireCost,
                tireRemouldCost:tireData.tireRemouldCost,
                expectedTireLife_km:tireData.expectedTireLife_km,
                recommendedcoldInflationPressure:tireData.recommendedcoldInflationPressure,
                remouldCount:tireData.remouldCount,
                recommendedPositionName:tireData.recommendedPositionName,
                tireTypeName:tireData.tireTypeName
            })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
      //for Tire Type details
      function GetAllTireType(callback) {
          $http.get(serverIp+apiUrls.getalltiretype, { })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }

      function AddTireType(tireTypeName, tireTypeDesc, callback) {
          $http.post(serverIp+apiUrls.addtiretype, { tireTypeName: tireTypeName, tireTypeDesc: tireTypeDesc })
              .success(function (response) {
                  if(response.success==true){
                      //console.log(response);
                  }
                  // login successful if there's a token in the response
                  callback(response);
              }).error(function (response) {
                  // handle error things
                  //console.log(response);
                  callback(response);
              });
      }
      function DeleteTireType(tireTypeId,callback){
          $http.delete(serverIp+apiUrls.deletetiretypeById+tireTypeId)
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      function UpdateTireType(tireTypeId, tireTypeName, tireTypeDesc,callback){
          $http.put(serverIp+apiUrls.updatetireTypeById+tireTypeId, { tireTypeName: tireTypeName, tireTypeDesc: tireTypeDesc })
          .success(function (response) {
              if(response.success==true){
                  //console.log(response);
              }
              // login successful if there's a token in the response
              callback(response);
          }).error(function (response) {
              // handle error things
              //console.log(response);
              callback(response);
          });
      }

      //for Tire Manucture details
      function GetallTireManufacture(callback) {
        $http.get(serverIp+apiUrls.getalltiremanufacture, { })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
   //for Add Tire Manufacture details
    function AddTireManufacture(tireManufacturerName, tireManufacturerPlace, callback) {
        $http.post(serverIp+apiUrls.addtiremanufacture, { tireManufacturerName: tireManufacturerName, tireManufacturerPlace: tireManufacturerPlace })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
     //for Delete Tire Manufacture details
    function DeleteTireManufacture(tireManufactureId,callback){
        $http.delete(serverIp+apiUrls.deleteTireManufactureById+tireManufactureId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
     //for Update Tire Manufacture details
    function UpdateTireManufacture(tireManufactureId, tireManufacturerName, tireManufacturerPlace,callback){
        $http.put(serverIp+apiUrls.updateTireManufactureById+tireManufactureId, { tireManufacturerName: tireManufacturerName, tireManufacturerPlace: tireManufacturerPlace })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

     //for Get Tire Status details
     function GetallTireStatus(callback) {
        $http.get(serverIp+apiUrls.getalltirestatus, { })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }

      //for Add Tire Status details
      function AddTireStatus(tireStatusName, tireStatusDesc, callback) {
        $http.post(serverIp+apiUrls.addtirestatus, { tireStatusName: tireStatusName, tireStatusDesc: tireStatusDesc })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }
    //for Delete Tire Status details
    function DeleteTireStatus(tireStatusId,callback){
        $http.delete(serverIp+apiUrls.deleteTireStatusById+tireStatusId)
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }
    
    //for Update Tire Manufacture details
    function UpdateTireStatus(tireStatusId, tireStatusName, tireStatusDesc,callback){
        $http.put(serverIp+apiUrls.updateTireStatusById+tireStatusId, { tireStatusName: tireStatusName, tireStatusDesc: tireStatusDesc })
        .success(function (response) {
            if(response.success==true){
                //console.log(response);
            }
            // login successful if there's a token in the response
            callback(response);
        }).error(function (response) {
            // handle error things
            //console.log(response);
            callback(response);
        });
    }

     //for Get Tire Status details
     function GetallTireRecommendedPosition(callback) {
        $http.get(serverIp+apiUrls.getAllTireRecommendedPosition, { })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
    }

        //for Add Tire Status details
        function AddTireRecommendedPosition(tireRecommendedPositionName, tireRecommendedPositionDescription, callback) {
            $http.post(serverIp+apiUrls.addTireRecommendedPosition, { tireRecommendedPositionName: tireRecommendedPositionName, tireRecommendedPositionDescription: tireRecommendedPositionDescription })
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
        //for Delete Tire Status details
        function DeleteTireRecommendedPosition(tireRecId,callback){
            $http.delete(serverIp+apiUrls.deleteTireRecommendedPositionById+tireRecId)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
        
        //for Update Tire Manufacture details
        function UpdateTireRecommendedPosition(tireRecId, tireRecommendedPositionName, tireRecommendedPositionDescription,callback){
            $http.put(serverIp+apiUrls.updateTireRecommendedPositionById+tireRecId, { tireRecommendedPositionName: tireRecommendedPositionName, tireRecommendedPositionDescription: tireRecommendedPositionDescription })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
       
        //for Get Tire Model details
        function GetallTireModel(callback) {
            $http.get(serverIp+apiUrls.getallTireModel, { })
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }

        //for Add Tire Model details
        function AddTireModel(tireManufacturerId, tireModelNo,tireModelDescription, callback) {
            $http.post(serverIp+apiUrls.addTireModel, { tireManufacturerId: tireManufacturerId, tireModelNo: tireModelNo,tireModelDescription:tireModelDescription })
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
        //for Delete Tire Model details
        function DeleteTireModel(tireModelId,callback){
            $http.delete(serverIp+apiUrls.deleteTireModelById+tireModelId)
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }
        
        //for Update Tire Model details
        function UpdateTireModel(tireModelId, tireManufacturerId, tireModelNo,tireModelDescription,callback){
            $http.put(serverIp+apiUrls.updateTireModelById+tireModelId, { tireManufacturerId: tireManufacturerId, tireModelNo: tireModelNo,tireModelDescription:tireModelDescription })
            .success(function (response) {
                if(response.success==true){
                    //console.log(response);
                }
                // login successful if there's a token in the response
                callback(response);
            }).error(function (response) {
                // handle error things
                //console.log(response);
                callback(response);
            });
        }

        //getAllTireReportofSystem
        function GetTireReportofSystem(callback) {
            $http.get(serverIp+apiUrls.getAllTireReportofSystem)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
        //getThreadDepthDetails
        function GetThreadDepthDetails(tireId,callback) {
            $http.get(serverIp+apiUrls.getThreadDepthDetails+tireId)
                .success(function (response) {
                    if(response.success==true){
                        //console.log(response);
                    }
                    // login successful if there's a token in the response
                    callback(response);
                }).error(function (response) {
                    // handle error things
                    //console.log(response);
                    callback(response);
                });
        }
  }
  
  })();