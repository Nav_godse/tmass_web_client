(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.vehicleTpms', ['ui.select', 'ngSanitize'])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider,$httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
      $stateProvider
        .state('main.vehicletpms', {
          url: '/vehicle_tpms',
          //app/pages/settings/settingDetails/settingDetails.html
          templateUrl: 'app/pages/vehicletpms/vehicleTpms.html',
          controller: "VehicleTpmsCtrl",
          abstract: false,
          title: 'Vehicle TPMS View',
          sidebarMeta: {
            icon: 'icoTPMSView',
            order: 5,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager']   // <-- roles allowed for this module
          }
        });
    }
  })();