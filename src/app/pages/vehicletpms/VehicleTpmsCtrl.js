(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleTpmsCtrl', VehicleTpmsCtrl);
  
    /** @ngInject */
    function VehicleTpmsCtrl($scope,podService,vehicleService,tireService,$http,toastr, animation) {
        var vm = this; 
        
        vm.VehicleSpeclId=""; 
        $scope.currentMgr="";
        $scope.startDayMgr=0;
        $scope.startMonthMgr=0;
        $scope.MgrValue="0";
        initController();  

        $scope.selectedVehicleNo = ""; 
        $scope.selectedAxelConfig = "none";
        function initController() {
            getAllDropdowns()            
            // For Open Popup for Add and Edit
            //$("#jqxwindowVehicleSpec").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function getAllDropdowns(){  
            getAllVehicleList(); 
           // GetAllTPMSViewDetails();
        }
        //http://demo.tmaas.in/treel_data_lg/device_details/get_device_info_deviceid.php?DeviceID=865794033919954&deviceType=treel&MgrValue=0
          //to get all Vehicle  Axel Config details.
        function getAllVehicleList() {        
               
            vehicleService.getAllVehicleList("tubedemo",function (result) { 
                    $scope.vehicles=result;
                    $scope.selectedVehicleNo = result.data.data[0].VehicleID; 
                    $scope.myvehicle(result.data.data[0].VehicleID,result.data.data[0].AxelConfig,
                        result.data.data[0].DeviceType,result.data.data[0].DeviceID,result.data.data[0].MgrValue);   
               
            });
        };
        $scope.tagInfo = [];
        $scope.myvehicle= function(vehicleId,manufactureId,modelid,AxelConfig,DeviceID,MgrValue,vehicleRegNo){ 
            //animation.myFunc('showvehicle');
            $scope.tagInfo = [];
           // function GetAllTPMSViewDetails(podDeviceNo,currVehicleAxel,vehicleRegNo) {            
                //vehicleService.GetAllTPMSViewDetails("865794033871189","treel",function (result) {
                tireService.GetTireSpecificationbyTireManufacurerandModel(manufactureId,modelid,function(result0){
                    podService.getpodTPMSdetails(DeviceID,function (result) {
                        console.log("result0",result0);
                        console.log("result",result);
                        // $scope.currentMgr="";
                        // $scope.startDayMgr=0;
                        // $scope.startMonthMgr=0;
                        $scope.selectedVehicleId= vehicleId;
                        $scope.selectedVehicleNo = vehicleRegNo;
                        $scope.selectedAxelConfig = AxelConfig;
                        // $scope.deviceType = "treel";
                        // $scope.MgrValue = 0;                  
                        console.log("result.data.TagInfo");                    
                        if(result.data!=undefined){
                            console.log(result.data.TagInfo);
                            //for testing
                            var dataObj=[{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "146",
                                TPMSTemperature: "146",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "111",
                                TPMSTemperature: "89",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "111",
                                TPMSTemperature: "25",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "146",
                                TPMSTemperature: "146",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "146",
                                TPMSTemperature: "146",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "146",
                                TPMSTemperature: "146",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "146",
                                TPMSTemperature: "146",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "146",
                                TPMSTemperature: "146",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            },{
                                IDTagStatus: "146",
                                TPMSAlert: "146",
                                TPMSBatteryLevel: "146",
                                TPMSPressure: "146",
                                TPMSTemperature: "146",
                                _id: "5c03f0a08323d926ac0ef9ec"
                            }
                        ]
                            $scope.vehicleDetails=result.data.TagInfo;
                            $scope.tagInfo = result.data.TagInfo;
                            //$scope.tagInfo = dataObj;
                            //$scope.vehicleDetails = dataObj;
                            //$('#showvehicle').waitMe("hide");
                        }
                        console.log("result.data.TagInfo");
                        //$('#showvehicle').waitMe("hide");
                    });
                })
        };
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myList li").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > 1461);
            });
        });
        // in controller
        $scope.isValidFoo = function () {
            return $scope.foo > 0;
        }
        //for  Get Vehicle Vehicle List details
        function getAllVehicleList() {
            animation.myFunc('showvehicleList'); 
            podService.showVehicleandPodDetails(function (result) {
                $scope.tpmsGridData=[];
                
                console.log("getGridtpmsData",result.data);
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tpmsGridData.push(result.data[i]);    
                    }
                    //$scope.tpmsGridData=[]; 
                    console.log($scope.tpmsGridData);
                    $('#showvehicleList').waitMe('hide');
                } else {
                    $scope.tpmsGridData=[];         
                    $('#showvehicleList').waitMe('hide');           
                    //toastr.error(result.message);       
                }            
            });
        }
    }
 
  
})();
  
  