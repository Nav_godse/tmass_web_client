/**
 * @author l.azevedo
 * created on 29.07.2017
 */
(function() {
  'use strict';

  angular.module('TmassAdmin.pages.main', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $locationProvider) {
    $stateProvider
      .state('main', {
        url: '/main',
        templateUrl: 'app/pages/main/main.html',
        redirectTo: 'main.dashboard',
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'System User', 'OEM Admin','OEM Manager', 'OEM User',  'Client Admin', 'Client User', 'Client Manager', 'Depot Manager', 'Depot User']   // <-- roles allowed for this module
        }
      });
      $locationProvider.html5Mode(true);
  }

})();