(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.dashboard')
        .controller('VehicledashCtrl', VehicledashCtrl);
  
    /** @ngInject */
    function VehicledashCtrl($scope,allDashboardService,customerService,tireService,vehicleService,toastr, $uibModal,localStorage,$http,config) {
        var vm = this;
        // $scope.oneAtATime = true;
        $scope.status = {
          isCustomHeaderOpen: false,
          isFirstOpen: true,
          isFirstDisabled: false,
          open: true
        }
        vm.currUserData = localStorage.getObject('dataUser');
        console.log(vm.currUserData);
        $scope.vehicleItems = [
          {name: 'Total Vehicle'},
          {name: 'Vehicle TD 80%'},
          {name: 'Vehicle TD 50%'},
          {name: 'Vehicle TD 20%'},
        ];
        initController();
        function initController() {
            gettireDashboard();
            getOEMAdmins();
        }
        $scope.allOemAdminsId = [];
        function getOEMAdmins() {
            customerService.ShowadminsOfOEM(function(result){
                if (result.success === true) {
                    console.log(result.data);
                    for(var i=0;i<result.data.length;i++){
                        $scope.allOemAdminsId.push(result.data[i]._id);
                    }                    
                }else{
                    //toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }
        $scope.threadDeapth =[]
        function getAllthreadDepth(tireId){
            tireService.GetThreadDepthDetails(tireId,function(result){
                if (result.success === true) {
                    console.log(result.data);
                    for(var i=0;i<result.data.length;i++){
                        $scope.threadDeapth.push(result.data[i]);
                    }                    
                    return result.data;
                }else{
                    ////toastr.error(result.message);
                   // vm.error = result.message;                    
                }
            })
        }

        $scope.configuredVehicles = []
        $scope.nonConfiguredVehicles = [];
        $scope.configuredVehiclesCount = 0
        $scope.nonConfiguredVehiclesCount = 0;

        var totalAvg=0;
        var oldvalueavg =0 ;
        var newvalueavg = 0
        var newval =0;
        var vehicleThreadDepth = 0;
        var vehicleAvgThreadDepth = 0;
        var totalgt80TiresData =[];
        var totalgt50TiresData  =[];
        var totalgt20TiresData =[];
        var totallt20TiresData =[];
        $scope.realData = [];
        function gettireDashboard () {
            allDashboardService.showVehicleDashboard(function(result){
                if (result.success === true) { 
                    console.log('result.data');
                    console.log(result.data);
                     var vehicleCount =0;
                     var currResult = result.data;
                     $scope.realData= result.data;
                     vehicleCount =currResult.length;
                     for(var q=0 ;q<currResult.length ;q++){
                         console.log(currResult[q].TireConfigdetails);
                        if(currResult[q].TireConfigdetails==undefined || currResult[q].TireConfigdetails.length==0 ){
                            $scope.nonConfiguredVehicles.push(currResult[q]);                            
                        }else{
                        $scope.configuredVehicles.push(currResult[q]);
                        }
                     }

                     for(var w=0;w<$scope.configuredVehicles.length;w++){ 
                        vehicleThreadDepth=0;
                        var currtireDetail =$scope.configuredVehicles[w].TireConfigdetails; 
                        for(var e=0 ;e<currtireDetail.length ;e++){ 
                            var currtirethreaddepth =currtireDetail[e].tireThreadDepthDetails;
                            console.log(currtireDetail[e].tirePositionName); 
                                totalAvg=0;
                                if(currtirethreaddepth.length>1){
                                    newval = currtirethreaddepth[currtirethreaddepth.length - 1];
                                    oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                    newvalueavg=(newval.tireThreadDepth_t1+newval.tireThreadDepth_t2+newval.tireThreadDepth_t3)/3;
                                    totalAvg =newvalueavg*100/oldvalueavg ; 
                                    currtireDetail[e].TD= totalAvg;
                                }else{ 
                                    oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                    newvalueavg=oldvalueavg; 
                                    totalAvg =newvalueavg*100/oldvalueavg;  
                                    currtireDetail[e].TD= totalAvg;
                                } 
                            vehicleThreadDepth = totalAvg +vehicleThreadDepth; 
                            vehicleAvgThreadDepth=vehicleThreadDepth/currtireDetail.length;
                            $scope.configuredVehicles[w].vehicleTD = Math.round(vehicleAvgThreadDepth * 100) / 100 ;
                        }
                     }
                     console.log($scope.configuredVehicles);
                     for(var t=0;t<$scope.configuredVehicles.length;t++){
                        if($scope.configuredVehicles[t].vehicleTD>=80){
                            totalgt80TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<80 && $scope.configuredVehicles[t].vehicleTD>=50){
                            totalgt50TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<50 && $scope.configuredVehicles[t].vehicleTD>=20){
                            totalgt20TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<20 ){
                            totallt20TiresData.push($scope.configuredVehicles[t])
                        }
                     }
                    $scope.tireDashboard={
                        totalCount: vehicleCount,
                        totalgt80: totalgt80TiresData.length,
                        totalgt50: totalgt50TiresData.length,
                        totalgt20: totalgt20TiresData.length,
                        totallt20: totallt20TiresData.length,
                        totalData: currResult,
                        totalgt80Data: totalgt80TiresData,
                        totalgt50Data: totalgt50TiresData,
                        totalgt20Data: totalgt20TiresData,
                        totallt20Data: totallt20TiresData,
                    };
                } else {
                    $scope.tireDashboard={
                        totalCount: 0,
                        totalgt80: 0,
                        totalgt50: 0,
                        totalgt20: 0,
                        totallt20: 0
                    };
                    //toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        }

        //for printing
        $('#save').on('click', function () {
            printData();
        });

        $scope.demoFromHTML11=function(tablename) {
            printData(tablename);
        }
        var externalDataRetrievedFromServer = [];
        //create table 
        function buildTableBody(data, columns) {
            var body = [];
        
            body.push(columns);
        
            data.forEach(function(row) {
                var dataRow = [];
        
                columns.forEach(function(column) {
                    dataRow.push(row[column].toString());
                })
        
                body.push(dataRow);
            });
        
            return body;
        }
        //get table body
        function table(data, columns) {
            return {
                table: {
                    headerRows: 1,
                    widths:['25%','25%','25%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        function table1(data, columns) {
            return {
                table: {
                    headerRows: 1,
                    widths:['25%','25%','25%','25%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        //for printing in pdf
        $scope.demoFromHTML=function(dashboardvariable) {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = date.toDateString();
            if(dashboardvariable=='divtable0'){
                dashheader='Total Vehicles in Vehicle Dashboard \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'];
                for(var i=0;i<$scope.tireDashboard.totalData.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalData[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalData[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalData[i].vehicleAxel
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).download('Vehicle_Dashboard.pdf'); //pdf
            }else if(dashboardvariable=='totalgt80Data'){
                dashheader='Vehicle Dashboard greater than 80% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt80Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt80Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt80Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt80Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt80Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                pdfMake.createPdf(dd).download('Vehicle_Dashboard1.pdf'); //pdf
            }else if(dashboardvariable=='tdgt50'){
                dashheader='Vehicle Dashboard TD greater than 50% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt50Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt50Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt50Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt50Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt50Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                pdfMake.createPdf(dd).download('Vehicle_Dashboard2.pdf'); 
            }else if(dashboardvariable=='tdgt20'){
                dashheader='Vehicle Dashboard TD greater than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                pdfMake.createPdf(dd).download('Vehicle_Dashboard3.pdf'); 
            }else if(dashboardvariable=='tdlt20'){
                dashheader='Vehicle Dashboard TD less than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totallt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totallt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totallt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totallt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totallt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                pdfMake.createPdf(dd).download('Vehicle_Dashboard4.pdf');   
            }else if(dashboardvariable=='tdnonConfigured'){
                dashheader='Vehicle Dashboard for Non Configured Vehicles \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'];
                for(var i=0;i<$scope.nonConfiguredVehicles.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.nonConfiguredVehicles[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.nonConfiguredVehicles[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.nonConfiguredVehicles[i].vehicleAxel
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                pdfMake.createPdf(dd).download('Vehicle_Dashboard5.pdf');   
            } 
        }

        function printData(tablename)
        {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = date.toDateString();
            if(tablename=='divtable0'){
                for(var i=0;i<$scope.tireDashboard.totalData.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalData[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalData[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalData[i].vehicleAxel
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: 'Total Vehicles in Vehicle Dashboard \n',bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, ['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'])                        
                    ]
                } 
                pdfMake.createPdf(dd).print(); //print
            }else if(tablename=='totalgt80Data'){
                dashheader='Vehicle Dashboard greater than 80% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt80Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt80Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt80Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt80Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt80Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                pdfMake.createPdf(dd).print(); //print
            }else if(tablename=='tdgt50'){
                dashheader='Vehicle Dashboard TD greater than 50% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt50Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt50Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt50Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt50Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt50Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                pdfMake.createPdf(dd).print(); //print
            }else if(tablename=='tdgt20'){
                dashheader='Vehicle Dashboard TD greater than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                pdfMake.createPdf(dd).print(); //print
            }else if(tablename=='tdlt20'){
                dashheader='Vehicle Dashboard TD less than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totallt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totallt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totallt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totallt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totallt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                pdfMake.createPdf(dd).print(); //print
            }else if(tablename=='tdnonConfigured'){
                dashheader='Vehicle Dashboard for Non Configured Vehicles \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'];
                for(var i=0;i<$scope.nonConfiguredVehicles.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.nonConfiguredVehicles[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.nonConfiguredVehicles[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.nonConfiguredVehicles[i].vehicleAxel
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                pdfMake.createPdf(dd).print(); //print
            }    
        }
        //end of printing
        //export data to excel sheet
        $scope.exportData = function (tablename) {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = date.toDateString();
            if(tablename=='divtable0'){
                dashheader='Total Vehicles in Vehicle Dashboard \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'];
                for(var i=0;i<$scope.tireDashboard.totalData.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalData[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalData[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalData[i].vehicleAxel,
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Vehicle Report", true);//excel
            }else if(tablename=='totalgt80Data'){
                dashheader='Vehicle Dashboard TD greater than 80% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                
                for(var i=0;i<$scope.tireDashboard.totalgt80Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt80Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt80Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt80Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt80Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Vehicle Report", true);//excel
            }else if(tablename=='tdgt50'){
                dashheader='Vehicle Dashboard TD greater than 50% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt50Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt50Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt50Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt50Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt50Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Vehicle Report", true);//excel
            }else if(tablename=='tdgt20'){
                dashheader='Vehicle Dashboard TD greater than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Vehicle Report", true);//excel
            }else if(tablename=='tdlt20'){
                dashheader='Vehicle Dashboard TD less than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totallt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totallt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totallt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totallt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totallt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Vehicle Report", true);//excel
            }else if(tablename=='tdnonConfigured'){
                dashheader='Vehicle Dashboard for Non Configured Vehicles \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'];
                for(var i=0;i<$scope.nonConfiguredVehicles.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.nonConfiguredVehicles[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.nonConfiguredVehicles[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.nonConfiguredVehicles[i].vehicleAxel
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Vehicle Report", true);//excel
            }
        };

        $scope.myalertArr=[];
        $scope.currvehicleData='';
        $scope.openRoleMasterModal = function (id) {
            //alert(id);
            $scope.myalertArr=[];
            $scope.myalertObj ={};
            for(var g=0;g<$scope.realData.length;g++){
                if($scope.realData[g]._id==id){
                    for(var x=0;x<$scope.realData[g].TireConfigdetails.length;x++){
                        $scope.currvehicleData=$scope.realData[g].vehicleRegistrationNumber;
                        $scope.myalertArr.push($scope.realData[g].TireConfigdetails[x]) 
                    }                       
                }
            }
            $uibModal.open({
                template: "<div ng-include src=\"'roleMasterPopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        //test xls
        function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
            
            var CSV = '';    
            //Set Report title in first row or line
            
            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";
                
                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {
                    
                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);
                
                //append Label row with line break
                CSV += row + '\r\n';
            }
            
            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";
                
                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);
                
                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {        
                alert("Invalid data");
                return;
            }   
            
            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g,"_");   
            
            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            
            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    
            
            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");    
            link.href = uri;
            
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";
            
            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            console.log('link');
            console.log(link)
            link.click();
            document.body.removeChild(link);
        }

        //send mail with html table
        $scope.sendMail = function(tablename)
        {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = date.toDateString();
            if(tablename=='divtable0'){
                dashheader='Total Vehicles in Vehicle Dashboard \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'];
                for(var i=0;i<$scope.tireDashboard.totalData.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalData[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalData[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalData[i].vehicleAxel,
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Vehicle Dashboard", true,"Dashboard : Vehicle Dashboard",dashheader);
            }else if(tablename=='totalgt80Data'){
                dashheader='Vehicle Dashboard TD greater than 80% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                
                for(var i=0;i<$scope.tireDashboard.totalgt80Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt80Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt80Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt80Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt80Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Vehicle Dashboard", true,"Dashboard : Vehicle Dashboard",dashheader);
            }else if(tablename=='tdgt50'){
                dashheader='Vehicle Dashboard TD greater than 50% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt50Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt50Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt50Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt50Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt50Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Vehicle Dashboard", true,"Dashboard : Vehicle Dashboard",dashheader);
            }else if(tablename=='tdgt20'){
                dashheader='Vehicle Dashboard TD greater than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totalgt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totalgt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totalgt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totalgt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totalgt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Vehicle Dashboard", true,"Dashboard : Vehicle Dashboard",dashheader);
            }else if(tablename=='tdlt20'){
                dashheader='Vehicle Dashboard TD less than 20% \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel','Vehicle TD'];
                for(var i=0;i<$scope.tireDashboard.totallt20Data.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.tireDashboard.totallt20Data[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.tireDashboard.totallt20Data[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.tireDashboard.totallt20Data[i].vehicleAxel,
                        'Vehicle TD':$scope.tireDashboard.totallt20Data[i].vehicleTD
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table1(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Vehicle Dashboard", true,"Dashboard : Vehicle Dashboard",dashheader);
            }else if(tablename=='tdnonConfigured'){
                dashheader='Vehicle Dashboard for Non Configured Vehicles \n';
                headerArr=['Vehicle Serial No', 'Vehicle Group Name','Vehicle Axel'];
                for(var i=0;i<$scope.nonConfiguredVehicles.length;i++){
                    var a={ 
                        'Vehicle Serial No': $scope.nonConfiguredVehicles[i].vehicleRegistrationNumber, 
                        'Vehicle Group Name':$scope.nonConfiguredVehicles[i].vehicleGroupName ,
                        'Vehicle Axel':$scope.nonConfiguredVehicles[i].vehicleAxel
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)
                    ]
                }   
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Vehicle Dashboard", true,"Dashboard : Vehicle Dashboard",dashheader);
            }
        }
        
        $scope.uploadFile = function(){
            var file = $scope.myFile;
            var uploadUrl = "/savedata";
            fileUpload.uploadFileToUrl(file, uploadUrl);
        };
        ///===========================================
        function dataURItoBlob(dataURI, callback) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var bb = new Blob([ab]);
            return bb;
        }
        function JSONToCSVConvertor2(JSONData,ReportTitle, ShowLabel,subject,dashboardName) {
            //alert("in");
            // var JSONData = [
            //     { "Vehicle": "BMW", "Date": "30, Jul 2013 09:24 AM", "Location": "Hauz Khas, Enclave, New Delhi, Delhi, India", "Speed": 42 }, 
            //     { "Vehicle": "Honda CBR", "Date": "30, Jul 2013 12:00 AM", "Location": "Military Road,  West Bengal 734013,  India", "Speed": 0 }, 
            //     { "Vehicle": "Supra", "Date": "30, Jul 2013 07:53 AM", "Location": "Sec-45, St. Angel's School, Gurgaon, Haryana, India", "Speed": 58 }, 
            // ];
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            // var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            var uri = 'data:text/csv;charset=utf-8,' + btoa(CSV);
            //  btoa(string)
            var block = uri.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1]
            console.log(realData);
            console.log(realData);
            var link = document.createElement("a");
            link.href = uri;
            console.log(link);

            console.log(dataURItoBlob(link.href));

            var fd = new FormData();
            fd.append('fname', 'test.csv');
            fd.append('data', dataURItoBlob(uri));

            ///console.log(fd);

            $scope.uploadInfo = {
                scaleId: 29719,
                fileDate: "23/09/1992"
            };

            var data = $scope.uploadInfo;
            $scope.uploadInfo.fileDate = "23/09/1992";
            $scope.uploadInfo.fileDate = new Date($scope.uploadInfo.fileDate);
            $scope.uploadInfo.fileDate = new Date();
            data.fileDate = $scope.uploadInfo.fileDate.toISOString();


            $scope.uploadAttachDetails(uri,subject,dashboardName);
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
        }

        $scope.uploadAttachDetails = function(file,subject,dashboardName) { 
            var file_obj=dataURItoBlob(file) 
            console.log(file_obj);
            var currData = {};
        
            var fd = new FormData();
            fd.append('file', file_obj);
            fd.append('data', 'string');
            $http.post(config.apiUrl+'/api/upload_mail_attach', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(response){
                    console.log(response);
                    allDashboardService.SendMail(vm.currUserData.otherData.email,subject,dashboardName,'',response.data,function(result){
                        if (result.success === true) {
                            console.log(result);   
                            toastr.success(result.message);      
                        }else{
                            //toastr.error(result.message);
                            vm.error = result.message;                    
                        }
                    })
            })
            .error(function(response){
                console.log(response);
            });
        }

        //=============================================
    }

})();
  
  