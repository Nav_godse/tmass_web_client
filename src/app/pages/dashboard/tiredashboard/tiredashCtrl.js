(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.dashboard')
        .controller('TiredashCtrl', TiredashCtrl);
  
    /** @ngInject */
    function TiredashCtrl($scope,allDashboardService,tireReportService,customerService,tagService,vehicleService,toastr, $uibModal,localStorage,$http,config) {
        var vm = this;
        // $scope.oneAtATime = true;
        $scope.tireDashboard={
            totalCount:0,
            totalRemouldedCount:0,
            totalRepariedCount:0,
            totalNewCount:0,
            totalTireData: [],
            totalRemouldedTiresData: [],
            totalNewTiresData: [],
            totalRepairedTiresData : []
        };
        vm.currUserData = localStorage.getObject('dataUser');

        $scope.currUserRole = vm.currUserData.otherData.roleName;

        console.log(vm.currUserData);
        $scope.status = {
          isCustomHeaderOpen: false,
          isFirstOpen: true,
          isFirstDisabled: false,
          open: true
        }

        initController();

        function initController() {
            if(vm.currUserData.otherData.roleName=="OEM Admin"){
                getOEMAdmins();
            }
            
            gettireDashboard();
        }
        $scope.allOemAdminsId = [];
        function getOEMAdmins() {
            customerService.ShowadminsOfOEM(function(result){
                if (result.success === true) {
                    console.log(result.data);
                    for(var i=0;i<result.data.length;i++){
                        $scope.allOemAdminsId.push(result.data[i]._id);
                    }                    
                }else{
                    //toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }
        function gettireDashboard () {
            if(vm.currUserData.otherData.roleName=="System Admin" || vm.currUserData.otherData.roleName=="System Super Admin" || vm.currUserData.otherData.roleName=="System Manager"){
                allDashboardService.showTireDashboardforSystem(function(result){
                    if (result.success === true) { 
                        console.log("tire dashboard");
                        console.log(result);

                        var resultnewTireDepotObj = {};
                        var resultrepairedTireDepotObj = {};
                        var resultRemouldedTireObj = {};
                        var newTireAgainstDepot = [];

                        vm.newTireDepot = result.data.totalNewTiresData;
                        vm.repairedTireDepot = result.data.totalRepairedTiresData;
                        vm.remouldedTireDepot = result.data.totalRemouldedTiresData;
                        //newTireDepot
                        if(result.data.totalNewTiresData.length>0){
                            for(var i in vm.newTireDepot) {
                                resultnewTireDepotObj[vm.newTireDepot[i].depotName] = null;
                            }
                            resultnewTireDepotObj = Object.keys(resultnewTireDepotObj);
                            console.log(resultnewTireDepotObj);
                            newTireAgainstDepot.push(result.data.totalNewTiresData)
                        }

                        //for repairedTire
                        if(result.data.totalRepairedTiresData.length>0){
                            for(var i in vm.repairedTireDepot) {
                                resultrepairedTireDepotObj[vm.repairedTireDepot[i].depotName] = null;
                            }
                            resultrepairedTireDepotObj = Object.keys(resultrepairedTireDepotObj);
                            console.log(resultrepairedTireDepotObj);
                        }

                        //for remoulded Tire
                        if(result.data.totalRemouldedTiresData.length>0){
                            for(var i in vm.remouldedTireDepot) {
                                resultRemouldedTireObj[vm.remouldedTireDepot[i].depotName] = null;
                            }
                            resultRemouldedTireObj = Object.keys(resultRemouldedTireObj);
                            console.log(resultRemouldedTireObj);
                        }
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };

                        console.log($scope.tireDashboard, "system admin data");
                    } else {
                        $scope.tireDashboard={
                            totalCount:0,
                            totalRemouldedCount:0,
                            totalRepariedCount:0,
                            totalNewCount:0,
                            totalConfiguredCount:0
                        };
                        ////toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                   // //console.log($scope.allDepo);
                });
            }else if(vm.currUserData.otherData.roleName=="Client Admin" ){
                allDashboardService.showTireDashboardAdmins(function(result){
                    if (result.success === true) { 
                        console.log("tire dashboard");
                        console.log(result);
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };
                        console.log($scope.tireDashboard);
                    } else {
                        $scope.tireDashboard={
                            totalCount:0,
                            totalRemouldedCount:0,
                            totalRepariedCount:0,
                            totalNewCount:0,
                            totalConfiguredCount:0
                        };
                        ////toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                   // //console.log($scope.allDepo);
                });
            }else if(vm.currUserData.otherData.roleName=="OEM Admin"){
                // $scope.allOemAdminsId = [];
                allDashboardService.showTireDashboardforSystem(function(result){
                    if (result.success === true) { 
                        console.log("result.data");
                        console.log(result.data);
                        var TotalCountofTire = 0;
                        var totalRemouldtireOemCount = 0;
                        var totalRepairTireOemCount = 0;
                        var totalNewTireOemCount = 0;
                        var totalConfiguredCount =0;
                        var totaltireofOEM =[];
                        var totalNewTireOfOem= [];
                        var totalConfiguredTires=[];
                        var totalRepairedTireOdOem =[]; 
                        var totalRemouldedTireOdOem =[]; 
                        console.log('$scope.allOemAdminsId');
                        console.log($scope.allOemAdminsId);
                        for(var i=0;i<result.data.totalTireData.length;i++){
                             for(var j=0;j<$scope.allOemAdminsId.length;j++){
                                 if(result.data.totalTireData[i].customerId == $scope.allOemAdminsId[j]){
                                    TotalCountofTire = TotalCountofTire+1;
                                    totaltireofOEM.push(result.data.totalTireData[i]);
                                    if(result.data.totalTireData[i].tireStatusName=="New"){
                                        totalNewTireOemCount = totalNewTireOemCount+1;
                                        totalNewTireOfOem.push(result.data.totalTireData[i]);
                                    }else if(result.data.totalTireData[i].tireStatusName=="Remoulded"){
                                        totalRemouldtireOemCount = totalRemouldtireOemCount+1;
                                        totalRemouldedTireOdOem.push(result.data.totalTireData[i]);
                                    }else if(result.data.totalTireData[i].tireStatusName=="Repaired"){
                                        totalRepairTireOemCount = totalRepairTireOemCount+1;
                                        totalRepairedTireOdOem.push(result.data.totalTireData[i]);
                                    }else if(result.data.totalTireData[i].vehicleTireConfigData!=null){
                                        totalConfiguredCount = totalConfiguredCount+1;
                                        totalConfiguredTires.push(result.data.totalTireData[i]);
                                    }
                                        
                                    //}
                                 }
                                    
                             }
                        }
                        console.log(totaltireofOEM);
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };
                        console.log($scope.tireDashboard);
                    } else {
                        $scope.tireDashboard={
                            totalCount:0,
                            totalRemouldedCount:0,
                            totalRepariedCount:0,
                            totalNewCount:0,
                            totalConfiguredCount:0
                        };
                        ////toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                   //console.log($scope.allDepo);
                });
            }else{
                allDashboardService.showTireDashboardMgrs(function(result){
                    if (result.success === true) { 
                        console.log("tire dashboard");
                        console.log(result);
                        var totalConfiguredCount =0;
                        var totalConfiguredTires=[];
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };
                        console.log($scope.tireDashboard);
                    } else {
                        $scope.tireDashboard={
                            totalCount:0,
                            totalRemouldedCount:0,
                            totalRepariedCount:0,
                            totalNewCount:0,
                            totalConfiguredCount:0
                        };
                        ////toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                   // //console.log($scope.allDepo);
                });
            }
        }
        //for printing
        $('#save').on('click', function () {
            printData();
        });

        //for page print
        // $scope.demoFromHTML11=function(tablename) {
        //     printData(tablename);
        // }

        // //for printing in pdf
        // $scope.demoFromHTML=function(divname) {
        //     var pdf = new jsPDF('p', 'pt', 'letter');
        //     // source can be HTML-formatted string, or a reference
        //     // to an actual DOM element from which the text will be scraped.
        //     var source = $('#'+divname)[0];
        
        //     // we support special element handlers. Register them with jQuery-style 
        //     // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        //     // There is no support for any other type of selectors 
        //     // (class, of compound) at this time.
        //     var specialElementHandlers = {
        //         // element with id of "bypass" - jQuery style selector
        //         '#bypassme': function (element, renderer) {
        //             // true = "handled elsewhere, bypass text extraction"
        //             return true
        //         }
        //     };
        //     var margins = {
        //         top: 80,
        //         bottom: 60,
        //         left: 50,
        //         width: 700
        //     };
        //     // all coords and widths are in jsPDF instance's declared units
        //     // 'inches' in this case
        //     pdf.fromHTML(
        //     source, // HTML string or DOM elem ref.
        //     margins.left, // x coord
        //     margins.top, { // y coord
        //         'width': margins.width, // max width of content on PDF
        //         'elementHandlers': specialElementHandlers
        //     },
        
        //     function (dispose) {
        //         // dispose: object with X, Y of the last line add to the PDF 
        //         //          this allow the insertion of new lines after html
        //         pdf.save('TireDashboard'+Date.now()+'.pdf');
        //     }, margins);
        // }

        // function printData(tablename)
        // {
        //     var divToPrint=document.getElementById(tablename);
        //     var newWin= window.open("");
        //     newWin.document.write("Tire Data<br>  \n");
        //     newWin.document.write( divToPrint.outerHTML);
        //     newWin.print();
        //     newWin.close();
        // }
        // //end of printing
        // //export data to excel sheet
        // $scope.exportData = function (exportable) {
        //     $("#"+exportable).table2excel({
        //         // exclude CSS class
        //         exclude: ".noExl",
        //         name: "Worksheet Name",
        //         filename: "TireDashboard" //do not include extension
        //       }); 
        // };
        //new print pdf and excel
        $scope.demoFromHTML11=function(tablename) {
            printData(tablename);
        }
        var externalDataRetrievedFromServer = [];
        //create table 
        function buildTableBody(data, columns) {
            var body = [];

            body.push(columns);

            data.forEach(function(row) {
                var dataRow = [];

                columns.forEach(function(column) {
                    dataRow.push(row[column].toString());
                })

                body.push(dataRow);
            });

            return body;
        }
        //get table body
        function table(data, columns) {
            return {
                table: {
                    headerRows: 1,
                    widths: ['auto', 'auto', 'auto','auto','auto','auto','auto','auto','auto'],
                    //widths:['9%','11.2%','11.2%','11.2%','11.2%','11.2%','11.2%','11.2%','11.2%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        //for non configured tires
        function table2(data, columns) {
            return {
                table: {
                    headerRows: 1,
                    widths: ['auto', 'auto', 'auto','auto','auto','auto','auto','auto','auto','auto'],
                    //widths:['9%','11.2%','11.2%','11.2%','11.2%','11.2%','11.2%','11.2%','11.2%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        //for printing in pdf
        $scope.demoFromHTML=function(dashboardvariable) {
            
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            var n = date.toDateString();
            if(dashboardvariable=='totalCount'){
                dashheader='Total Tyres in Tyres Dashboard \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalTireData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalTireData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalTireData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalTireData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalTireData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalTireData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).download('Tyres_Dashboard.pdf'); //pdf
            }else if(dashboardvariable=='totalNew'){
                dashheader='Tyres Dashboard : Total New Tires \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalNewTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalNewTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalNewTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalNewTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalNewTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalNewTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).download('Tyres_Dashboard.pdf'); //pdf
            }else if(dashboardvariable=='totalRemoulded'){
                dashheader='Tyres Dashboard : Total Remolded Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRemouldedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRemouldedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRemouldedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRemouldedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRemouldedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRemouldedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).download('Tyres_Dashboard.pdf'); //pdf
            }else if(dashboardvariable=='tirerepair'){
                dashheader='Tyres Dashboard : Total Repaired Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRepairedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRepairedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRepairedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRepairedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRepairedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRepairedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).download('Tyres_Dashboard.pdf'); //pdf
            }else if(dashboardvariable=='tiresconfigured'){
                dashheader='Tyres Dashboard : Total Configured Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status','Vehicle No', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalConfiguredTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalConfiguredTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalConfiguredTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalConfiguredTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalConfiguredTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalConfiguredTiresData[i].tireStatusName, 
                        'Vehicle No':$scope.tireDashboard.totalConfiguredTiresData[i].VehicleTireConfigureDetails.vehicleDetails.vehicleRegistrationNumber,
                        'TD T1':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table2(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).download('Tyres_Dashboard.pdf'); //pdf
            }
        }

        function printData(tablename)
        {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = date.toDateString();
            if(tablename=='totalCount'){
                dashheader='Total Tyres in Tyres Dashboard \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalTireData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalTireData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalTireData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalTireData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalTireData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalTireData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).print() //pdf
            }else if(tablename=='totalNew'){
                dashheader='Tyres Dashboard : Total New Tires \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalNewTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalNewTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalNewTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalNewTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalNewTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalNewTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).print(); //pdf
            }else if(tablename=='totalRemoulded'){
                dashheader='Tyres Dashboard : Total Remolded Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRemouldedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRemouldedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRemouldedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRemouldedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRemouldedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRemouldedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).print(); //pdf
            }else if(tablename=='tirerepair'){
                dashheader='Tyres Dashboard : Total Repaired Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRepairedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRepairedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRepairedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRepairedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRepairedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRepairedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).print(); //pdf
            }else if(tablename=='tiresconfigured'){
                dashheader='Tyres Dashboard : Total Configured Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status','Vehicle No', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalConfiguredTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalConfiguredTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalConfiguredTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalConfiguredTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalConfiguredTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalConfiguredTiresData[i].tireStatusName, 
                        'Vehicle No':$scope.tireDashboard.totalConfiguredTiresData[i].VehicleTireConfigureDetails.vehicleDetails.vehicleRegistrationNumber,
                        'TD T1':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table2(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                pdfMake.createPdf(dd).print(); //pdf
            }
        }
        //end of printing
        //export data to excel sheet
        $scope.exportData = function (exportable) {
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = date.toDateString();
            //  JSONToCSVConvertor(externalDataRetrievedFromServer, "Tire Dashboard", true);//excel
            if(exportable=='totalCount'){
                dashheader='Total Tyres in Tyres Dashboard \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalTireData.length;i++){
                    console.log(i +'count');
                    console.log($scope.tireDashboard.totalTireData[i]);
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalTireData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalTireData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalTireData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalTireData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalTireData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Tire Dashboard", true);//excel
            }else if(exportable=='totalNew'){
                dashheader='Tyres Dashboard : Total New Tires \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalNewTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalNewTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalNewTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalNewTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalNewTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalNewTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Tire Dashboard", true);//excel
            }else if(exportable=='totalRemoulded'){
                dashheader='Tyres Dashboard : Total Remolded Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRemouldedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRemouldedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRemouldedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRemouldedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRemouldedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRemouldedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Tire Dashboard", true);//excel
            }else if(exportable=='tirerepair'){
                dashheader='Tyres Dashboard : Total Repaired Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRepairedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRepairedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRepairedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRepairedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRepairedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRepairedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Tire Dashboard", true);//excel
            }else if(exportable=='tiresconfigured'){
                dashheader='Tyres Dashboard : Total Configured Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status','Vehicle No', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalConfiguredTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalConfiguredTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalConfiguredTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalConfiguredTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalConfiguredTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalConfiguredTiresData[i].tireStatusName, 
                        'Vehicle No':$scope.tireDashboard.totalConfiguredTiresData[i].VehicleTireConfigureDetails.vehicleDetails.vehicleRegistrationNumber,
                        'TD T1':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table2(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor(externalDataRetrievedFromServer, "Tire Dashboard", true);//excel
            }
        };
        //test xls
        function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
            
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
            
            var CSV = '';    
            //Set Report title in first row or line
            
            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";
                
                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {
                    
                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);
                
                //append Label row with line break
                CSV += row + '\r\n';
            }
            
            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";
                
                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);
                
                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {        
                alert("Invalid data");
                return;
            }   
            
            //Generate a file name
            var fileName = "tireDashboard_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g,"_");   
            
            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            
            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    
            
            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");    
            link.href = uri;
            
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";
            
            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            console.log('link');
            console.log(link)
            link.click();
            document.body.removeChild(link);
        }

//end of new print pdf and excel
        //send mail with html table
        $scope.sendMail = function(tablename)
        {
            //JSONToCSVConvertor2("dfrefer",true);
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = date.toDateString();
            if(tablename=='totalCount'){
                dashheader='Total Tyres in Tyres Dashboard \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalTireData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalTireData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalTireData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalTireData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalTireData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalTireData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalTireData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Tire Dashboard", true,"Dashboard : Tire Dashboard",dashheader);
                //JSONToCSVConvertor2(externalDataRetrievedFromServer, "Tire Dashboard", true);//excel
            }else if(tablename=='totalNew'){
                dashheader='Tyres Dashboard : Total New Tires \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalNewTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalNewTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalNewTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalNewTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalNewTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalNewTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalNewTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Tire Dashboard", true,"Dashboard : Tire Dashboard",dashheader);
            }else if(tablename=='totalRemoulded'){
                dashheader='Tyres Dashboard : Total Remolded Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRemouldedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRemouldedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRemouldedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRemouldedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRemouldedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRemouldedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRemouldedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Tire Dashboard", true,"Dashboard : Tire Dashboard",dashheader)
            }else if(tablename=='tirerepair'){
                dashheader='Tyres Dashboard : Total Repaired Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalRepairedTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalRepairedTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalRepairedTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalRepairedTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalRepairedTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalRepairedTiresData[i].tireStatusName, 
                        'TD T1':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalRepairedTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Tire Dashboard", true,"Dashboard : Tire Dashboard",dashheader);
            }else if(tablename=='tiresconfigured'){
                dashheader='Tyres Dashboard : Total Configured Tyres \n';
                headerArr=['Tyres Serial No', 'Tyres Depot Name','Tyres Model', 'Tyres Manufacturer', 'Tyres Status','Vehicle No', 'TD T1','TD T2', 'TD T3', 'Avg TD'];
                for(var i=0;i<$scope.tireDashboard.totalConfiguredTiresData.length;i++){
                    var a={ 
                        'Tyres Serial No': $scope.tireDashboard.totalConfiguredTiresData[i].tireSerialNumber, 
                        'Tyres Depot Name':$scope.tireDashboard.totalConfiguredTiresData[i].depotName ,
                        'Tyres Model':$scope.tireDashboard.totalConfiguredTiresData[i].tireModelNo,
                        'Tyres Manufacturer':$scope.tireDashboard.totalConfiguredTiresData[i].tireManufacturerName,
                        'Tyres Status':$scope.tireDashboard.totalConfiguredTiresData[i].tireStatusName, 
                        'Vehicle No':$scope.tireDashboard.totalConfiguredTiresData[i].VehicleTireConfigureDetails.vehicleDetails.vehicleRegistrationNumber,
                        'TD T1':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1,
                        'TD T2':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2, 
                        'TD T3':$scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3,
                        'Avg TD':Math.round(((parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t1)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t2)+parseFloat($scope.tireDashboard.totalConfiguredTiresData[i].tireThreadDepthDetailsData.tireThreadDepth_t3))/3)* 100) / 100
                        //Math.round(num * 100) / 100
                    }
                    externalDataRetrievedFromServer.push(a);
                }
                var dd = {
                    content: [
                        { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                        { text: 'Date: '+n,bold: true, alignment:'left' },
                        { text: '\n'},
                        table2(externalDataRetrievedFromServer, headerArr)                        
                    ]
                } 
                JSONToCSVConvertor2(externalDataRetrievedFromServer,"Tire Dashboard", true,"Dashboard : Tire Dashboard",dashheader);
            }
        }
        
        $scope.uploadFile = function(){
            var file = $scope.myFile;
            var uploadUrl = "/savedata";
            fileUpload.uploadFileToUrl(file, uploadUrl);
        };
        ///===========================================
        function dataURItoBlob(dataURI, callback) {
            // convert base64 to raw binary data held in a string
            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
            var byteString = atob(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

            // write the bytes of the string to an ArrayBuffer
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            // write the ArrayBuffer to a blob, and you're done
            var bb = new Blob([ab]);
            return bb;
        }
        function JSONToCSVConvertor2(JSONData,ReportTitle, ShowLabel,subject,dashboardName) {
            //alert("in");
            // var JSONData = [
            //     { "Vehicle": "BMW", "Date": "30, Jul 2013 09:24 AM", "Location": "Hauz Khas, Enclave, New Delhi, Delhi, India", "Speed": 42 }, 
            //     { "Vehicle": "Honda CBR", "Date": "30, Jul 2013 12:00 AM", "Location": "Military Road,  West Bengal 734013,  India", "Speed": 0 }, 
            //     { "Vehicle": "Supra", "Date": "30, Jul 2013 07:53 AM", "Location": "Sec-45, St. Angel's School, Gurgaon, Haryana, India", "Speed": 58 }, 
            // ];
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            // var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
            var uri = 'data:text/csv;charset=utf-8,' + btoa(CSV);
            //  btoa(string)
            var block = uri.split(";");
            // Get the content type of the image
            var contentType = block[0].split(":")[1];// In this case "image/gif"
            // get the real base64 content of the file
            var realData = block[1].split(",")[1]
            console.log(realData);
            console.log(realData);
            var link = document.createElement("a");
            link.href = uri;
            console.log(link);

            console.log(dataURItoBlob(link.href));

            var fd = new FormData();
            fd.append('fname', 'test.csv');
            fd.append('data', dataURItoBlob(uri));

            ///console.log(fd);

            $scope.uploadInfo = {
                scaleId: 29719,
                fileDate: "23/09/1992"
            };

            var data = $scope.uploadInfo;
            $scope.uploadInfo.fileDate = "23/09/1992";
            $scope.uploadInfo.fileDate = new Date($scope.uploadInfo.fileDate);
            $scope.uploadInfo.fileDate = new Date();
            data.fileDate = $scope.uploadInfo.fileDate.toISOString();


            $scope.uploadAttachDetails(uri,subject,dashboardName);
            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
        }

        $scope.uploadAttachDetails = function(file,subject,dashboardName) { 
            var file_obj=dataURItoBlob(file) 
            console.log(file_obj);
            var currData = {};
        
            var fd = new FormData();
            fd.append('file', file_obj);
            fd.append('data', 'string');
            $http.post(config.apiUrl+'/api/upload_mail_attach', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(response){
                    console.log(response);
                    allDashboardService.SendMail(vm.currUserData.otherData.email,subject,dashboardName,'',response.data,function(result){
                        if (result.success === true) {
                            console.log(result);   
                            toastr.success(result.message);      
                        }else{
                            //toastr.error(result.message);
                            vm.error = result.message;                    
                        }
                    })
            })
            .error(function(response){
                console.log(response);
            });
        }

        //=============================================


    }

})();
  
  