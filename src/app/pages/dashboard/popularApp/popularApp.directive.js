/**
 * @author Nav_god
 * created on 16.3.2018
 */
(function () {
  'use strict';

  angular.module('TmassAdmin.pages.dashboard')
      .directive('popularApp', popularApp);

  /** @ngInject */
  function popularApp() {
    return {
      restrict: 'E',
      templateUrl: 'app/pages/dashboard/popularApp/popularApp.html'
    };
  }
})();