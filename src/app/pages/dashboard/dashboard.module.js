(function() {
  'use strict';

  angular.module('TmassAdmin.pages.dashboard', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.home', {
        url: '/Home',
        templateUrl: 'app/pages/dashboard/dashboard.html',
        
        title: 'Main',  
        sidebarMeta: {
          icon: 'iconHome',
          order: 0,
        },
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin', 'System User', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager','System Manager']   // <-- roles allowed for this module
        }
      })
      .state('main.dashboard', {
        url: '/dashboard',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        title: 'Dashboard',
        sidebarMeta: {
          icon: 'icoDash',
          order: 1,
        },
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager','System Manager']   // <-- roles allowed for this module
        }
      })
      .state('main.dashboard.home', {
        url: '/home',
        templateUrl: 'app/pages/dashboard/home/home.html',
        controller: 'HomeCtrl',
        title: 'All In One',
        sidebarMeta: {
          icon: 'icoDash',
          order: 0,
        },
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager']   // <-- roles allowed for this module
        }
      })
      .state('main.dashboard.tireDashboard', {
        url: '/tyre_Dashboard',
        templateUrl: 'app/pages/dashboard/tiredashboard/tiredash.html',
        controller: 'TiredashCtrl',
        title: 'Tyre Dashboard',
        sidebarMeta: {
          icon: '',
          order: 1,
        },
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin','System Manager', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager']  // <-- roles allowed for this module
        }
      })
      .state('main.dashboard.vehicleDashboard', {
        url: '/vehicle_Dashboard',
        templateUrl: 'app/pages/dashboard/vehicledashboard/vehicledash.html',
        controller: "VehicledashCtrl",
        title: 'Vehicle Dashboard',
        sidebarMeta: {
          icon: '',
          order: 2,
        },
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin', 'OEM Admin','System Manager','OEM Manager', 'Client Admin', 'Client Manager']   // <-- roles allowed for this module
        }
      })
      .state('main.dashboard.alertDashboard', {
        url: '/alert_Dashboard',
        templateUrl: 'app/pages/dashboard/alertdashboard/alertdash.html',
        controller: "AlertdashCtrl",
        title: 'Alert Dashboard',
        sidebarMeta: {
          icon: '',
          order: 3,
        },
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin', 'OEM Admin','OEM Manager', 'Client Admin']   // <-- roles allowed for this module
        }
      })
      .state('main.dashboard.performanceDashboard', {
        url: '/performance_Dashboard',
        templateUrl: 'app/pages/dashboard/performancedashboard/performancedash.html',
        controller: "PerformancedashCtrl",
        title: 'Performance Dashboard',
        sidebarMeta: {
          icon: '',
          order: 4,
        },
        authenticate: true,
        params: {                // <-- focusing this one
          authRoles: ['System Admin', 'System Super Admin', 'OEM Admin','OEM Manager', 'Client Admin', 'Client Manager']  
        }
      });
  }

})();