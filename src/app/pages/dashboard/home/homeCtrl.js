(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.dashboard')
        .controller('HomeCtrl', HomeCtrl);
  
    /** @ngInject */
    function HomeCtrl($scope,tireReportService,customerService,tagService,vehicleService,toastr, $uibModal) {
        var vm = this;
        // $scope.oneAtATime = true;
        $scope.switchVisible = function(div){
            if (div == 'tire'){
                $('#tireDiv').toggleClass('fullscreen'); 
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            } else if(div == 'vehicle'){
                $('#vehicleDiv').toggleClass('fullscreen');
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
             }else if(div == 'alert'){
                $('#alertDiv').toggleClass('fullscreen');
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }else if(div == 'performance'){
                $('#performanceDiv').toggleClass('fullscreen');
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }else {
            }
        }

        $scope.div1 = false;
        $scope.div2 = false;
    }

})();
  
  