/**
 * @author Nav_god
 * created on 16.3.2018
 */
(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.dashboard')
        .controller('PieChartCtrl', PieChartCtrl);
  
    /** @ngInject */
    function PieChartCtrl($scope, $timeout,$uibModal,vehicleService,podService,customerService, baConfig,allDashboardService ,toastr,localStorage,$http,$state,config,animation) {
        console.log('in piecharts');
        var vm = this;
        vm.currUserData = localStorage.getObject('dataUser');
        vm.avgcostperkm = 0;
        vm.avgMaintenancecost = 0;
        vm.avgdistanceTravelled =0;
        $scope.currUserRole = vm.currUserData.otherData.roleName;
        var layoutColors = baConfig.colors;

        vm.VehicleSpeclId=""; 
        $scope.currentMgr="";
        $scope.startDayMgr=0;
        $scope.startMonthMgr=0;
        $scope.MgrValue="0";
        
        $scope.vehicleAxelConfigs = [];
        $scope.selectedAxelConfig = "2x4x4";
        
        initController();

        function initController() {
            vm.currUserData = localStorage.getObject('dataUser');
            console.log(vm.currUserData.otherData.isloginNew);
            if(vm.currUserData.otherData.isloginNew==true){
                $state.go('main.profile');
                alertify.alert('Please change your Password now or To change your password, Click on User avatar on the top right menu, in drop-down click profile page!', function(){ 
                    firstTimeLogin()
                });
            }

            if(vm.currUserData.otherData.roleName=="OEM Admin"){
                getOEMAdmins();
            }
            getGridHomepageData();
            // gettireDashboard();
            // getVehicleData();
            // getPerformanceData();
            avgcostperkm();
            getAvgmaintainancecost();
            avgdistanceTravelledwithoutissue();
            /////////////////////////////////////
            // getAllVehicleAxelConfigDetails()
            // GetAllTPMSViewDetails();
            
        }

        function firstTimeLogin(){
            console.log("vm.currUserData.otherData.isCustomer,vm.currUserData.otherData._id");  
            console.log(vm.currUserData.otherData.isCustomer,vm.currUserData.otherData._id);
            customerService.firstTImeLogin(vm.currUserData.otherData.isCustomer,vm.currUserData.otherData._id,function(result){
                console.log("firsttimelogin",result);
                // Get the existing data
                var existing = localStorage.getObject('dataUser');

                // If no existing data, create an array
                // Otherwise, convert the localStorage string to an array
                //existing = existing ? JSON.parse(existing) : {};

                // Add new data to localStorage Array //isloginNew: true
                existing['otherData' ].isloginNew= false;

                // Save back to localStorage
                localStorage.setObject('dataUser', existing);
                window.location.reload();
            })
        }
        
        function getOEMAdmins() {
            customerService.ShowadminsOfOEM(function(result){
                if (result.success === true) {
                    console.log(result.data);
                    for(var i=0;i<result.data.length;i++){
                        $scope.allOemAdminsId.push(result.data[i]._id);
                    }                    
                }else{
                    //toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }

        //for getting all the tire for pie chart
        function gettireDashboard () {
            if(vm.currUserData.otherData.roleName=="System Admin" || vm.currUserData.otherData.roleName=="System Super Admin" || vm.currUserData.otherData.roleName=="System Manager"){
                allDashboardService.showTireDashboardforSystem(function(result){
                    if (result.success === true) { 
                        console.log("tire dashboard");
                        console.log(result);

                        var resultnewTireDepotObj = {};
                        var resultrepairedTireDepotObj = {};
                        var resultRemouldedTireObj = {};
                        var newTireAgainstDepot = [];

                        vm.newTireDepot = result.data.totalNewTiresData;
                        vm.repairedTireDepot = result.data.totalRepairedTiresData;
                        vm.remouldedTireDepot = result.data.totalRemouldedTiresData;
                        //newTireDepot
                        if(result.data.totalNewTiresData.length>0){
                            for(var i in vm.newTireDepot) {
                                resultnewTireDepotObj[vm.newTireDepot[i].depotName] = null;
                            }
                            resultnewTireDepotObj = Object.keys(resultnewTireDepotObj);
                            console.log(resultnewTireDepotObj);
                            newTireAgainstDepot.push(result.data.totalNewTiresData)
                        }

                        //for repairedTire
                        if(result.data.totalRepairedTiresData.length>0){
                            for(var i in vm.repairedTireDepot) {
                                resultrepairedTireDepotObj[vm.repairedTireDepot[i].depotName] = null;
                            }
                            resultrepairedTireDepotObj = Object.keys(resultrepairedTireDepotObj);
                            console.log(resultrepairedTireDepotObj);
                        }

                        //for remoulded Tire
                        if(result.data.totalRemouldedTiresData.length>0){
                            for(var i in vm.remouldedTireDepot) {
                                resultRemouldedTireObj[vm.remouldedTireDepot[i].depotName] = null;
                            }
                            resultRemouldedTireObj = Object.keys(resultRemouldedTireObj);
                            console.log(resultRemouldedTireObj);
                        }
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };
                        $scope.tirelabels = ["New", "Remoulded", "Repaired"]
                        if($scope.tireDashboard.totalNewCount==0 && $scope.tireDashboard.totalRemouldedCount==0 && $scope.tireDashboard.totalRepariedCount==0){
                        $scope.tirechartdata= 0;
                        }else{
                        $scope.tirechartdata = [$scope.tireDashboard.totalNewCount, $scope.tireDashboard.totalRemouldedCount, $scope.tireDashboard.totalRepariedCount];
                        }
                        console.log($scope.tireDashboard);
                    } else {
                        //toastr.error(result.message);
                        $scope.tirechartdata= 0;
                        vm.error = result.message;                    
                    }
                // //console.log($scope.allDepo);
                });
            }else if(vm.currUserData.otherData.roleName=="Client Admin" ){
                allDashboardService.showTireDashboardAdmins(function(result){
                    if (result.success === true) { 
                        console.log("tire dashboard");
                        console.log(result);
                        // $scope.tireDashboard={
                        //     totalCount:result.data.totalCount,
                        //     totalRemouldedCount:result.data.totalRemouldedCount,
                        //     totalRepariedCount:result.data.totalRepariedCount,
                        //     totalNewCount:result.data.totalNewCount,
                        //     totalTireData: result.data.totalTireData,
                        //     totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                        //     totalNewTiresData: result.data.totalNewTiresData,
                        //     totalRepairedTiresData : result.data.totalRepairedTiresData
                        // };
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };
                        console.log($scope.tireDashboard);
                        $scope.tirelabels = ["New", "Remoulded", "Repaired"];
                        if($scope.tireDashboard.totalNewCount==0 && $scope.tireDashboard.totalRemouldedCount==0 && $scope.tireDashboard.totalRepariedCount==0){
                        $scope.tirechartdata= 0;
                        }else{
                        $scope.tirechartdata = [$scope.tireDashboard.totalNewCount, $scope.tireDashboard.totalRemouldedCount, $scope.tireDashboard.totalRepariedCount];
                        }
                    } else {
                        //toastr.error(result.message);
                        $scope.tirechartdata= 0;
                        vm.error = result.message;                    
                    }
                // //console.log($scope.allDepo);
                });
            }else if(vm.currUserData.otherData.roleName=="OEM Admin"){
                // $scope.allOemAdminsId = [];
                allDashboardService.showTireDashboardforSystem(function(result){
                    if (result.success === true) { 
                        console.log("result.data");
                        console.log(result.data);
                        var TotalCountofTire = 0;
                        var totalRemouldtireOemCount = 0;
                        var totalRepairTireOemCount = 0;
                        var totalNewTireOemCount = 0;
                        var totalConfiguredCount =0;
                        var totaltireofOEM =[];
                        var totalNewTireOfOem= [];
                        var totalConfiguredTires=[];
                        var totalRepairedTireOdOem =[]; 
                        var totalRemouldedTireOdOem =[]; 
                        console.log('$scope.allOemAdminsId');
                        console.log($scope.allOemAdminsId);
                        for(var i=0;i<result.data.totalTireData.length;i++){
                            for(var j=0;j<$scope.allOemAdminsId.length;j++){
                                if(result.data.totalTireData[i].customerId == $scope.allOemAdminsId[j]){
                                    TotalCountofTire = TotalCountofTire+1;
                                    totaltireofOEM.push(result.data.totalTireData[i]);
                                    if(result.data.totalTireData[i].tireStatusName=="New"){
                                        totalNewTireOemCount = totalNewTireOemCount+1;
                                        totalNewTireOfOem.push(result.data.totalTireData[i]);
                                    }else if(result.data.totalTireData[i].tireStatusName=="Remoulded"){
                                        totalRemouldtireOemCount = totalRemouldtireOemCount+1;
                                        totalRemouldedTireOdOem.push(result.data.totalTireData[i]);
                                    }else if(result.data.totalTireData[i].tireStatusName=="Repaired"){
                                        totalRepairTireOemCount = totalRepairTireOemCount+1;
                                        totalRepairedTireOdOem.push(result.data.totalTireData[i]);
                                    }else if(result.data.totalTireData[i].vehicleTireConfigData!=null){
                                        totalConfiguredCount = totalConfiguredCount+1;
                                        totalConfiguredTires.push(result.data.totalTireData[i]);
                                    }
                                        
                                    //}
                                }
                                    
                            }
                        }
                        console.log(totaltireofOEM);
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };
                        console.log($scope.tireDashboard);
                        $scope.tirelabels = ["New", "Remoulded", "Repaired"];
                        if($scope.tireDashboard.totalNewCount==0 && $scope.tireDashboard.totalRemouldedCount==0 && $scope.tireDashboard.totalRepariedCount==0){
                        $scope.tirechartdata= 0;
                        }else{
                        $scope.tirechartdata = [$scope.tireDashboard.totalNewCount, $scope.tireDashboard.totalRemouldedCount, $scope.tireDashboard.totalRepariedCount];
                        }
                    } else {
                    $scope.tirechartdata=0
                        //toastr.error(result.message);
                        
                        vm.error = result.message;                    
                    }
                //console.log($scope.allDepo);
                });
            }else{
                allDashboardService.showTireDashboardMgrs(function(result){
                    if (result.success === true) { 
                        console.log("tire dashboard");
                        console.log(result);
                        var totalConfiguredCount =0;
                        var totalConfiguredTires=[];
                        $scope.tireDashboard={
                            totalCount:result.data.totalCount,
                            totalRemouldedCount:result.data.totalRemouldedCount,
                            totalRepariedCount:result.data.totalRepariedCount,
                            totalNewCount:result.data.totalNewCount,
                            totalConfiguredCount:result.data.configTiresCount,
                            totalTireData: result.data.totalTireData,
                            totalRemouldedTiresData: result.data.totalRemouldedTiresData,
                            totalNewTiresData: result.data.totalNewTiresData,
                            totalRepairedTiresData : result.data.totalRepairedTiresData,
                            totalConfiguredTiresData: result.data.configTires
                        };
                        console.log($scope.tireDashboard ,"tire dashboard");
                        $scope.tirelabels = ["New", "Remoulded", "Repaired"];
                        if($scope.tireDashboard.totalNewCount==0 && $scope.tireDashboard.totalRemouldedCount==0 && $scope.tireDashboard.totalRepariedCount==0){
                        $scope.tirechartdata= 0;
                        }else{
                        $scope.tirechartdata = [$scope.tireDashboard.totalNewCount, $scope.tireDashboard.totalRemouldedCount, $scope.tireDashboard.totalRepariedCount];
                        }
                        //$scope.tirechartdata = [result.data.totalNewCount, result.data.totalRemouldedCount, result.data.totalRepariedCount];
                    } else {
                        //toastr.error(result.message);
                        $scope.tirechartdata = 0;
                        vm.error = result.message;                    
                    }
                // //console.log($scope.allDepo);
                });
            }
        }

        //for getting all the vehicle data for pie chart
        function getVehicleData () {
            allDashboardService.showVehicleDashboard(function(result){
            $scope.configuredVehicles=[];
            $scope.realData=[];
            $scope.nonConfiguredVehicles = [];
            var vehicleThreadDepth=0;
            var totalAvg=0;
            var newval=0;
            var oldvalueavg=0;
            var newvalueavg=0;
            var vehicleAvgThreadDepth=0;
            var totalgt80TiresData =[];
            var totalgt50TiresData  =[];
            var totalgt20TiresData =[];
            var totallt20TiresData =[];
                if (result.success === true) { 
                    console.log('result.data');
                    console.log(result.data);
                    var vehicleCount =0;
                    var currResult = result.data;
                    $scope.realData= result.data;
                    vehicleCount =currResult.length;
                    for(var q=0 ;q<currResult.length ;q++){
                        console.log(currResult[q].TireConfigdetails);
                        if(currResult[q].TireConfigdetails==undefined || currResult[q].TireConfigdetails.length==0 ){
                            $scope.nonConfiguredVehicles.push(currResult[q]);                            
                        }else{
                        $scope.configuredVehicles.push(currResult[q]);
                        }
                    }

                    for(var w=0;w<$scope.configuredVehicles.length;w++){ 
                        vehicleThreadDepth=0;
                        var currtireDetail =$scope.configuredVehicles[w].TireConfigdetails; 
                        for(var e=0 ;e<currtireDetail.length ;e++){ 
                            var currtirethreaddepth =currtireDetail[e].tireThreadDepthDetails;
                            console.log(currtireDetail[e].tirePositionName); 
                                totalAvg=0;
                                if(currtirethreaddepth.length>1){
                                    newval = currtirethreaddepth[currtirethreaddepth.length - 1];
                                    oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                    newvalueavg=(newval.tireThreadDepth_t1+newval.tireThreadDepth_t2+newval.tireThreadDepth_t3)/3;
                                    totalAvg =newvalueavg*100/oldvalueavg ; 
                                    currtireDetail[e].TD= totalAvg;
                                }else{ 
                                    oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                    newvalueavg=oldvalueavg; 
                                    totalAvg =newvalueavg*100/oldvalueavg;  
                                    currtireDetail[e].TD= totalAvg;
                                } 
                            vehicleThreadDepth = totalAvg +vehicleThreadDepth; 
                            vehicleAvgThreadDepth=vehicleThreadDepth/currtireDetail.length;
                            $scope.configuredVehicles[w].vehicleTD = Math.round(vehicleAvgThreadDepth * 100) / 100 ;
                        }
                    }
                    console.log($scope.configuredVehicles);
                    for(var t=0;t<$scope.configuredVehicles.length;t++){
                        if($scope.configuredVehicles[t].vehicleTD>=80){
                            totalgt80TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<80 && $scope.configuredVehicles[t].vehicleTD>=50){
                            totalgt50TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<50 && $scope.configuredVehicles[t].vehicleTD>=20){
                            totalgt20TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<20 ){
                            totallt20TiresData.push($scope.configuredVehicles[t])
                        }
                    }
                    $scope.tireDashboard={
                        totalCount: vehicleCount,
                        totalgt80: totalgt80TiresData.length,
                        totalgt50: totalgt50TiresData.length,
                        totalgt20: totalgt20TiresData.length,
                        totallt20: totallt20TiresData.length,
                        totalData: currResult,
                        totalgt80Data: totalgt80TiresData,
                        totalgt50Data: totalgt50TiresData,
                        totalgt20Data: totalgt20TiresData,
                        totallt20Data: totallt20TiresData,
                    };
                    $scope.vehiclelabels = [">80%", ">50%", "<20%"];
                    if($scope.tireDashboard.totalgt80==0 && $scope.tireDashboard.totalgt50==0 && $scope.tireDashboard.totallt20==0){
                    $scope.vehiclechartdata= 0;
                    }else{
                    $scope.vehiclechartdata = [$scope.tireDashboard.totalgt80, $scope.tireDashboard.totalgt50, $scope.tireDashboard.totallt20];
                    }
                    
                    console.log($scope.tireDashboard, "inside vehicle")    
                } else {
                    $scope.vehiclechartdata= 0;
                    //toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        }

        //for getting all performance of vehicle
        function getPerformanceData () {
        allDashboardService.showPerformanceDashboard(function(result){
            $scope.configuredVehicles=[];
            $scope.realData=[];
            $scope.nonConfiguredVehicles = [];
            var vehicleThreadDepth=0;
            var totalAvg=0;
            var newval=0;
            var oldvalueavg=0;
            var newvalueavg=0;
            var vehicleAvgThreadDepth=0;
            var totalgt80TiresData =[];
            var totalgt50TiresData  =[];
            var totalgt20TiresData =[];
            var totallt20TiresData =[];
            if (result.success === true) { 
                console.log('result.data');
                console.log(result.data);
                var vehicleCount =0;
                var currResult = result.data;

                if(result.data==null || result.data==undefined){                        
                    $scope.performancechartdata=0;
                }else{
                    $scope.realData= result.data;
                    vehicleCount =currResult.length;
                    for(var q=0 ;q<currResult.length ;q++){
                        console.log(currResult[q].TireConfigdetails);
                        if(currResult[q].TireConfigdetails==undefined || currResult[q].TireConfigdetails.length==0 ){
                            $scope.nonConfiguredVehicles.push(currResult[q]);                            
                        }else{
                        $scope.configuredVehicles.push(currResult[q]);
                        }
                    }

                    for(var w=0;w<$scope.configuredVehicles.length;w++){ 
                        vehicleThreadDepth=0;
                        var currtireDetail =$scope.configuredVehicles[w].TireConfigdetails; 
                        for(var e=0 ;e<currtireDetail.length ;e++){ 
                            var currtirethreaddepth =currtireDetail[e].tireThreadDepthDetails;
                            console.log(currtireDetail[e].tirePositionName); 
                                totalAvg=0;
                                if(currtirethreaddepth.length>1){
                                    newval = currtirethreaddepth[currtirethreaddepth.length - 1];
                                    oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                    newvalueavg=(newval.tireThreadDepth_t1+newval.tireThreadDepth_t2+newval.tireThreadDepth_t3)/3;
                                    totalAvg =newvalueavg*100/oldvalueavg ; 
                                    currtireDetail[e].TD= totalAvg;
                                }else{ 
                                    oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                    newvalueavg=oldvalueavg; 
                                    totalAvg =newvalueavg*100/oldvalueavg;  
                                    currtireDetail[e].TD= totalAvg;
                                } 
                            vehicleThreadDepth = totalAvg +vehicleThreadDepth; 
                            vehicleAvgThreadDepth=vehicleThreadDepth/currtireDetail.length;
                            $scope.configuredVehicles[w].vehicleTD = Math.round(vehicleAvgThreadDepth * 100) / 100 ;
                        }
                    }
                    console.log($scope.configuredVehicles);
                    for(var t=0;t<$scope.configuredVehicles.length;t++){
                        if($scope.configuredVehicles[t].vehicleTD>=80){
                            totalgt80TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<80 && $scope.configuredVehicles[t].vehicleTD>=50){
                            totalgt50TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<50 && $scope.configuredVehicles[t].vehicleTD>=20){
                            totalgt20TiresData.push($scope.configuredVehicles[t])
                        }else if($scope.configuredVehicles[t].vehicleTD<20 ){
                            totallt20TiresData.push($scope.configuredVehicles[t])
                        }
                    }
                    $scope.performancepieboard={
                        totalCount: vehicleCount,
                        totalgt80: totalgt80TiresData.length,
                        totalgt50: totalgt50TiresData.length,
                        totalgt20: totalgt20TiresData.length,
                        totallt20: totallt20TiresData.length,
                        totalData: currResult,
                        totalgt80Data: totalgt80TiresData,
                        totalgt50Data: totalgt50TiresData,
                        totalgt20Data: totalgt20TiresData,
                        totallt20Data: totallt20TiresData,
                    };
                    $scope.performancelabels = [">90%",">75%", ">50%", "<25%"];
                    if($scope.performancepieboard.totalgt80==0 && $scope.performancepieboard.totalgt50==0 && $scope.performancepieboard.totallt20==0){
                        $scope.performancechartdata= 0;
                        }else{
                        $scope.performancechartdata = [$scope.performancepieboard.totalgt80,0, $scope.performancepieboard.totalgt50, $scope.performancepieboard.totallt20];
                        }
                    
                    console.log($scope.performancepieboard, "inside vehicle")   
                }

            } else {
                $scope.performancechartdata=0;
                $scope.performancechartdata = 0;
                //toastr.error(result.message);
                vm.error = result.message;                    
            }
        });
        }

        //for getting the avg cost per km 
        function avgcostperkm() {
            console.log("Inside avgcostperkm 000")
            vm.avgcostperkm=0;
            $scope.avgCostperKmData = 0
                allDashboardService.getAvgCostPerKm(function(result){
                    console.log(result,"Inside avgcostperkm 000")
                    if (result.success === true) { 
                        console.log(result.data, "inside avg cost km000");
                        if(result.data!=null){
                            vm.avgcostperkm=result.data;
                            $scope.avgCostperKmData = result.data.totalNewCount
                        }else{
                            vm.avgcostperkm=0;
                            $scope.avgCostperKmData = 0
                        }
                    } else {
                        $scope.avgCostperKmData=0;
                        vm.avgcostperkm=0;
                        //toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                })
        }

        //for getting the avg distance travelled without issue
        function avgdistanceTravelledwithoutissue() {
            vm.avgdistanceTravelledwithoutissue=0;
            $scope.avgdistanceTravelledwithoutissueData = 0
            allDashboardService.getAvgdistanceTravelledwithoutissue(function(result){
                if (result.success === true) { 
                    console.log(result.data, "inside a getAvgdistanceTravelledwithoutissue")    
                    if(result.data!=null){
                        vm.avgdistanceTravelledwithoutissue=result.data;
                        $scope.avgdistanceTravelledwithoutissueData = result.data
                    }else{
                        vm.avgdistanceTravelledwithoutissue=0;
                        $scope.avgdistanceTravelledwithoutissueData = 0
                    }
                
                } else {
                    vm.avgdistanceTravelledwithoutissue=0
                    $scope.avgdistanceTravelledwithoutissueData=0;
                    //toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }

        //getAvgmaintainancecost
        function getAvgmaintainancecost() {
            allDashboardService.getAvgmaintainancecost(function(result){
                if (result.success === true) { 
                    console.log(result.data, "inside avg cost maintainancecost")    
                    if(result.data!=null){
                        vm.getAvgmaintainancecost=result.data;
                        $scope.getAvgmaintainancecostData = result.data
                    }else{
                        vm.getAvgmaintainancecost=0;
                        $scope.getAvgmaintainancecostData = 0;
                    }
                } else {
                    vm.getAvgmaintainancecostData==0
                    $scope.getAvgmaintainancecostData=0;
                    //toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }

        $scope.labels = ["Green"],
        $scope.data = [12];

        $scope.options = {
            elements: {
                arc: {
                borderWidth: 0
                }
            },
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                fontColor: layoutColors.defaultText
                }
            },
            responsive: true,
            // title:{
            //     display: true,
            //     text: "Color test"
            // }
        };

        $scope.donutoptions = {
            elements: {
                arc: {
                borderWidth: 0
                }
            },
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                fontColor: layoutColors.defaultText
                }
            },
            responsive: true,
            // title:{
            //     display: true,
            //     text: "Color test"
            // }
        };

        // $scope.changeData = function () {
        //   //$scope.data = shuffle($scope.data);
        // };

        // function shuffle(o){
        //   for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x){}
        //   return o;
        // }
        $scope.gridData=[];
        $scope.alertdetails=[];
        var totalAvg=0;
        var oldvalueavg =0 ;
        var newvalueavg = 0
        var newval =0;
        var vehicleThreadDepth = 0;
        var vehicleAvgThreadDepth = 0;
        var vehicleIssues=0;
        var totalgt80TiresData =[];
        var totalgt50TiresData  =[];
        var totalgt20TiresData =[];
        var totallt20TiresData =[];
        function getGridHomepageData(){
            animation.myFunc('loadmegrid');
            podService.showVehicleandPodDetails(function (result) {
                $scope.homepageGridData=[];

                console.log("getGridHomepageData",result.data);
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.homepageGridData.push(result.data[i]);    
                        $scope.gridData.push(result.data[i]);
                        //for vehcileTd
                        for(var w=0;w<$scope.homepageGridData.length;w++){ 
                            vehicleThreadDepth=0;
                            var totaltyres=0;
                            var totaltyreswiththreadepth= 0;
                            var totalexpectedistance=0;
                            var totaldistancetravelled = 0;
                            if($scope.homepageGridData[w].TireConfigdetails.length>0){
                                totaltyres+=$scope.homepageGridData[w].TireConfigdetails.length;
                                var currtireDetail =$scope.homepageGridData[w].TireConfigdetails; 
                                for(var e=0 ;e<currtireDetail.length ;e++){ 
                                    var currtirethreaddepth =currtireDetail[e].tireThreadDepthDetails;
                                        console.log(currtireDetail[e]); 
                                        totalAvg=0;
                                        if(currtirethreaddepth.length>1){
                                            if(currtireDetail[e].expectedTireLife_km){
                                                totalexpectedistance+=currtireDetail[e].expectedTireLife_km;
                                            }else{
                                                totalexpectedistance+=0;
                                            }
                                            if(currtireDetail[e].kmTravelled){
                                                totaldistancetravelled+=currtireDetail[e].kmTravelled;
                                            }else{
                                                totaldistancetravelled+=0;
                                            }
                                            
                                            totaltyreswiththreadepth+=1;
                                            newval = currtirethreaddepth[currtirethreaddepth.length - 1];
                                            oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                            newvalueavg=(newval.tireThreadDepth_t1+newval.tireThreadDepth_t2+newval.tireThreadDepth_t3)/3;
                                            totalAvg =newvalueavg*100/oldvalueavg ; 
                                            currtireDetail[e].TD= totalAvg;
                                        }else{ 
                                            oldvalueavg= (currtirethreaddepth[0].tireThreadDepth_t1+currtirethreaddepth[0].tireThreadDepth_t2+currtirethreaddepth[0].tireThreadDepth_t3)/3;
                                            newvalueavg=oldvalueavg; 
                                            totalAvg =newvalueavg*100/oldvalueavg;  
                                            currtireDetail[e].TD= totalAvg;
                                        } 
                                    vehicleThreadDepth = totalAvg +vehicleThreadDepth; 
                                    vehicleAvgThreadDepth=vehicleThreadDepth/currtireDetail.length;
                                    $scope.homepageGridData[w].vehicleTD = Math.round(vehicleAvgThreadDepth * 100) / 100 ;

                                }
                                var test = ((100 * totaldistancetravelled/totalexpectedistance)/totaltyres)*totaltyreswiththreadepth;
                                if(isNaN(test)){
                                    var calofeffiecency = 100;
                                }else{
                                    var calofeffiecency = 100 -  Math.round(test * 100) / 100 ;;
                                }
                                
                                $scope.homepageGridData[w].effiecency = Math.round(calofeffiecency * 100) / 100 ;
                            }else{
                                $scope.homepageGridData[w].vehicleTD = 0;
                            }                            
                         }

                         console.log("gridData", $scope.homepageGridData);
                    }
                    //$scope.homepageGridData=[]; 
                    console.log($scope.homepageGridData);
                    $scope.allTireCOnfigs=[];
                    for(var x in $scope.homepageGridData){
                        $scope.allTireCOnfigs.push($scope.homepageGridData[x].TireConfigdetails);
                    }
                    console.log("allTireCOnfigs",$scope.allTireCOnfigs);
                    $('#loadmegrid').waitMe('hide');
                } else {
                    $('#loadmegrid').waitMe('hide');
                    $scope.homepageGridData=[];                    
                    //toastr.error(result.message);       
                }            
                mygridHomepage();
            });
        }

        function mygridHomepage(){ 
            var source =
            {
                localdata: $scope.homepageGridData,
                datatype: "array",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'vehicleRegistrationNumber', type: 'string' },
                    { name: 'vehicleGroupName', type: 'string' },
                    { name: 'vehicleManufacturer', type: 'string' },
                    { name: 'vehicleModel', type: 'string' },
                    { name: 'vehicleAxel', type: 'string' },
                    { name: 'totalvehicleAlert', type: 'number' },
                    { name: 'vehicleId', type: 'string'},
                    { name: 'podDateTime', type: 'string' },
                    { name: "podDevice", type: 'string' },
                    { name: 'vehicleTD', type: 'string' },
                    { name: 'issues', type: 'string' },
                    { name: 'effiecency', type: 'string' },
                    { name: 'location', type: 'string' },
                    { name: 'podLattitude', type: 'string' },
                    { name: 'podLongitude', type: 'string' },
                    { name: 'tpms', type: 'string' },
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#gridHomePage").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Vehicle Id',hidden:true, datafield: '_id',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'PodDevice',hidden:true, datafield: 'podDevice',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'podLattitude',hidden:true, datafield: 'podLattitude',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'podLongitude',hidden:true, datafield: 'podLongitude',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'Vehicle No', datafield: 'vehicleRegistrationNumber',align: 'center',cellsalign: 'center', width: '12%' },
                    { text: 'Date and Time Of POD',hidden:false, datafield: 'podDateTime',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'Avg TD',hidden:false, datafield: 'vehicleTD',align: 'center',cellsalign: 'center', width: '15%',cellsRenderer: function (row, column, value) {
                        return '<div class="text-center" style="padding:15px;"> <a style="margin-right:5px;text-decoration: underline !important;" data-row="' + row + '" class="avgTdButtons"  >' + value + ' </a>';   
                        } 
                    },
                    {
                        text: 'Issues', cellsAlign: 'center', align: "center",width: '8%', filterable: false, editable: false, sortable: false, dataField: 'issues', cellsRenderer: function (row, column, value) {
                        return '<div class="text-center" style="padding:15px;"> <a style="margin-right:5px;text-decoration: underline !important;" data-row="' + row + '" class="issueButtons"  >' + value + ' </a>';   
                        }
                    },
                    { text: 'Vehicle Group',hidden:false, datafield: 'vehicleGroupName',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'Efficiency',hidden:false, datafield: 'effiecency',align: 'center',cellsalign: 'center', width: '13.9%' },
                    {
                        text: 'Location', cellsAlign: 'center', align: "center",width: '8%', filterable: false, editable: false, sortable: false, dataField: 'location', cellsRenderer: function (row, column, value) {
                        return '<div class="text-center" style="padding:15px;"> <a style="margin-right:5px;text-decoration: underline !important;" data-row="' + row + '" class="locationButtons "  ><img src="../../../../assets/img/imgtpms/location.gif" style="width:30px;height:30px;"> </a>';   
                        }
                    },
                    {
                        text: 'TPMS View', cellsAlign: 'center', align: "center",width: '8%', filterable: false, editable: false, sortable: false, dataField: 'tpmsview', cellsRenderer: function (row, column, value) {
                        return '<div class="text-center" style="padding:15px;"> <a style="margin-right:5px;text-decoration: underline !important;" data-row="' + row + '" class="tpmsviewButtons "  ><img src="../../../../assets/img/icons/TPMS_withouttext.png" style="width:30px;height:30px;"> </a>';   
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
            $("#gridHomePage").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }

        function buttonsEvents() 
        {
                // For Edit Details
                var tpmsviewButtons = document.getElementsByClassName('tpmsviewButtons');
                for (var i = 0; i < tpmsviewButtons.length; i+=1) {
                    tpmsviewButtons[i].addEventListener('click', tpmsDetails);
                }
                //for showing avgTd
                var avgTdButtons = document.getElementsByClassName('avgTdButtons');
                for (var i = 0; i < avgTdButtons.length; i+=1) {
                    avgTdButtons[i].addEventListener('click', avgTdDetails);
                }
                // For Delete Details
                var deletebuttons = document.getElementsByClassName('deleteButtons');
                for (var i = 0; i < deletebuttons.length; i+=1) {
                    deletebuttons[i].addEventListener('click', deleteDetails);
                }

                var locationButtons = document.getElementsByClassName('locationButtons');
                for (var i = 0; i < locationButtons.length; i+=1) {
                    locationButtons[i].addEventListener('click', locationDetails);
                }
                var issueButtons = document.getElementsByClassName('issueButtons');
                for (var i = 0; i < issueButtons.length; i+=1) {
                    issueButtons[i].addEventListener('click', issuesDetails);
                }
        }

        // For Edit Details
        function tpmsDetails() { 
                var getselectedrowindexes = $('#gridHomePage').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                { 
                    var selectedRowData = $('#gridHomePage').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                console.log("openTPMSViewModal",selectedRowData);     
                //alert("called poupup");
                getAllVehicleAxelConfigDetails();
                GetAllTPMSViewDetails(selectedRowData.podDevice,selectedRowData.vehicleAxel,selectedRowData.vehicleRegistrationNumber);
                var configdetails =[];
                for(var r in $scope.gridData){
                    if(selectedRowData._id==$scope.gridData[r]._id){
                        configdetails=$scope.gridData[r].TireConfigdetails;
                    }
                }
                if(configdetails.length>0){
                    $scope.openTPMSViewModal();
                }else{
                    alertify.alert("Vehicle Not configured");
                }
                
        }  
        // For Edit Details
        function avgTdDetails() { 
            var getselectedrowindexes = $('#gridHomePage').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#gridHomePage').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            console.log("openTPMSViewModalavgTdDetailsavgTdDetails",selectedRowData);     
            //getvehicleTdDetails();
            console.log("$scope.homepageGridDataavgTdDetailsavgTdDetails",$scope.homepageGridData);
            var configdetails =[];
            for(var r in $scope.gridData){
                if(selectedRowData._id==$scope.gridData[r]._id){
                    configdetails=$scope.gridData[r].TireConfigdetails;
                }
            }
            if(configdetails.length>0){
                $scope.openavgTdModal(selectedRowData._id);
            }else{
                alertify.alert("Vehicle Not configured");
            }
            
        }  
        
        function issuesDetails() { 
            $scope.alertdetails=[];
            var getselectedrowindexes = $('#gridHomePage').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#gridHomePage').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            console.log("openissuesDetails",selectedRowData);     
            //alert("called poupup");
            var vehicleId = selectedRowData._id;
            $scope.currvehicleData=selectedRowData.vehicleRegistrationNumber;
            vehicleService.getVehicleAlertswithin24hrs(vehicleId,function(result){
                console.log("result.data",result.data);
                $scope.alertdetails=result.data;
                if($scope.alertdetails.length>0){
                    $scope.openIssuesModal();
                    //alertify.alert("No Alerts Found");
                }else{
                    alertify.alert("Vehicle Not configured");
                }
            })            
        } 
        // For Edit Details
        function locationDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#gridHomePage').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#gridHomePage').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            var lat= 0;
            var long = 0;
            if(parseFloat(selectedRowData.podLattitude)==NaN){
                lat= selectedRowData.podLattitude;
            }
            if(parseFloat(selectedRowData.podLongitude)==NaN){
                long = selectedRowData.podLongitude;
            }
            window.open(
                "https://www.google.com/maps?q="+lat+","+long+"%20&t=m&z=16",
                '_blank' // <- This is what makes it open in a new window.
            );
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#gridHomePage').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#gridHomePage').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete this record?", function () {
                // user clicked "ok"
                deleteVehicleAxelConfigDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });            
        }
            
        $("#gridHomePage").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
        });

        $scope.openTPMSViewModal = function () {
            //alert("in hereropenTPMSViewModal");
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTPMSView.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
            
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    //$scope.clearData();
                };
            },
            scope: $scope,
            backdrop:'static'
            })
        };

        $scope.openIssuesModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowIssuesModal.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
            
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    //$scope.clearData();
                };
            },
            scope: $scope,
            backdrop:'static'
            })
        };

        //to get all Vehicle  Axel Config details.
        function getAllVehicleAxelConfigDetails() {            
            vehicleService.GetAllVehicleAxelConfiguration(function (result) {
                if (result.success === true) {
                    vm.userVehicleAxelConfig=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleAxelConfigs.push({value: result.data[i]._id, text: result.data[i].vehicleAxelConfiguration});    
                    }
                    //console.log($scope.vehicleAxelConfigs);      
                } else {
                    toastr.error(result.message);
                    //console.log(result);         
                }
            });
        };

        function GetAllTPMSViewDetails(podDeviceNo,currVehicleAxel,vehicleRegNo) {            
            //vehicleService.GetAllTPMSViewDetails("865794033871189","treel",function (result) {
            podService.getpodTPMSdetails(podDeviceNo,function (result) {
                $scope.currentMgr="";
                $scope.startDayMgr=0;
                $scope.startMonthMgr=0;
                $scope.tagInfo = [];
                $scope.selectedVehicleNo = vehicleRegNo;
                $scope.selectedAxelConfig = currVehicleAxel;
                $scope.deviceType = "treel";
                $scope.MgrValue = 0;
                if(result.data!=undefined){
                    $scope.vehicleDetails=result.data.TagInfo;
                    console.log("result.data.TagInfo");
                    console.log(result.data.TagInfo);
                    $scope.tagInfo = result.data.TagInfo;
                }   
            });
        };

        $scope.openavgTdModal = function (vehcileId) {
            $scope.myalertArr=[];
            $scope.myalertObj ={};
            for(var g=0;g<$scope.gridData.length;g++){
                console.log("$scope.gridData[g]",$scope.gridData[g]._id,$scope.gridData[g].TireConfigdetails);
                if($scope.gridData[g]._id==vehcileId){
                    for(var x=0;x<$scope.gridData[g].TireConfigdetails.length;x++){
                        $scope.currvehicleData=$scope.gridData[g].vehicleRegistrationNumber;
                        $scope.myalertArr.push($scope.gridData[g].TireConfigdetails[x]) 
                    }                       
                }
            }
            $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowavgTdModal.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };
  }
  })();