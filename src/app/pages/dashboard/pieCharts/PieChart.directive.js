
(function () {
  'use strict';

  angular.module('TmassAdmin.pages.dashboard')
      .directive('pieChart', pieChart);

  /** @ngInject */
  function pieChart() {
    return {
      restrict: 'E',
      controller: 'PieChartCtrl',
      templateUrl: 'app/pages/dashboard/pieCharts/piechart.html'
    };
  }
})();