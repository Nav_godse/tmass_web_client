(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.resetPassword')
      .controller('resetPasswordCtrl', resetPasswordCtrl);
  
    /** @ngInject */
    function resetPasswordCtrl($scope,AuthenticationService,localStorage,$stateParams,toastr,toastrConfig,$state) {
      var vm = this;

      vm.resetpass = resetpass;
      //vm.mobile='';
      init();

      // console.log($stateParams);
  
      function init() {
        //localStorage.clear();
        $scope.resetData = localStorage.getObject('forgetData');
        console.log($scope.resetData);
        vm.user = $scope.resetData.usernameoremail;
        vm.mobile = $scope.resetData.mobile;
      }
  
      angular.extend(toastrConfig, {
        timeOut: 1000,
        autoDismiss: true,
        containerId: 'toast-container',
        maxOpened: 0,    
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
      });
      $scope.samepass=false;
      // $('#confirmpassword').on('keyup keydown focusout', function () {
      //   if ($('#password').val() == $('#confirmpassword').val()) {
      //     $scope.samepass=true
      //   }else{
      //     $scope.samepass=false
      //   }
      // });

      function resetpass() {
        if($('#password').val().length>=6){
          if(vm.user, vm.mobile, vm.otp, vm.password == undefined ){
            toastr.warning("Please Fill all info");
            return
          }
          var mobile =""
          // if(vm.mobile!="" && parseInt(vm.mobile)>0 ){
          //   if(parseInt(vm.mobile).length==10){
          //     var mobile = parseInt(vm.mobile)
          //   }else{
          //     alertify.alert("Please enter correct mobile no");
          //     return
          //   }
              
          // }
          console.log(vm.user, mobile, vm.otp, vm.confirmpassword);
          AuthenticationService.ResetPassword(vm.user,mobile ,vm.otp, vm.password, function (result) {
              if (result.success === true) {
                  toastr.success(result.message);
                  $state.go('main.dashboard');
              } else {
                  //console.log(result);
                  toastr.error(result.message);
                  return
              }
          });
        }else{
          alertify.alert("Password should not be less than 6 characters");
          return;
        }

      };
  
    }
  
  })();