(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.resetPassword', [])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('resetPassword', {
          //url: '/resetPassword/:user',
          url: '/resetPassword',
          templateUrl: 'app/pages/resetPassword/resetPassword.html',
          title: 'Forgot Password',
          controller: 'resetPasswordCtrl',
          sidebarMeta: {
            order: 800,
          },
          authenticate: false
        });
    }
  
  })();