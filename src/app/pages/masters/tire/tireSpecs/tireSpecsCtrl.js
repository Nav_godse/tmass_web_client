
(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireSpecsCtrl', TireSpecsCtrl);
  
    /** @ngInject */
    function TireSpecsCtrl($scope,tireService,toastr, $uibModal,$http,config,animation) {
        var vm = this;

        $scope.openModal = function () {
            vm.Modalinstance=$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowTireSpec.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.getAllTireSpecDetails = getAllTireSpecDetails;
        vm.addTireSpec = addTireSpec;
        vm.TireSpeclId="";
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id: "",
            tiremanufacturerId: "",
            tiremnaufacturename: "",
            tiremodelId: "",
            tiremodelNo: "",
            tireTypeId: "",
            tireTypeName: "",
            tireThreadDepth_t1: "",
            tireThreadDepth_t2: "",
            tireThreadDepth_t3: "",
            tireWearIndicator_twi: "",
            maxPressureThreshold: "",
            minpressureThreshold: "",
            maxTemperatureThreshold: "",
            tireCost: "",
            tireRemouldCost: "",
            expectedTireLife_km: "",
            recommendedcoldInflationPressure: "",
            remouldCount: "",
            recommendedPositionId: "",
            tireRecommendedPositionName: ""
        }
        initController();
        $scope.tireSpecications = [];
        $scope.tireRecommendedPositions = [];
        $scope.tireManufacturers = [];
        $scope.tireModels = [];
        $scope.tiretypes = [];
        $scope.globalManufacturerId = '';
        function initController() {
            getAllDropdowns()            
            // For Open Popup for Add and Edit
            //$("#jqxwindowTireSpec").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function getAllDropdowns(){
            validateFeilds();
            getAllTireType();
            getAllTireManufacturerDetails();
            getAllTireModelDetails();
            getAllTireRecommendedPositionDetails();
            getAllTireSpecDetails();
        }
        function validateFeilds(){
            console.log("inside validate feilds");
            $("#remouldCount #expectedTireLife_km #recommendedcoldInflationPressure").keypress(function (e) {     
                var maxlengthNumber = parseInt($('#remouldCount #expectedTireLife_km #recommendedcoldInflationPressure').attr('maxlength'));
                var inputValueLength = $('#remouldCount #expectedTireLife_km #recommendedcoldInflationPressure').val().length + 1;
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {                   
                          return false;
               }
               if(maxlengthNumber < inputValueLength) {
                   return false;
               }
              });
              
        }
        function addTireSpec(data) {            
            tireService.AddTireSpecification(data, function (result) {
                if (result.success === true) {
                    getAllDropdowns();
                    toastr.success(result.message);
                    vm.Modalinstance.close();
                    $scope.isValidate=true;
                    $scope.clearData();
                } else {
                    toastr.error(result.message); 
                    $scope.isValidate=false;
                    //console.log(result);           
                }
            });
        };

        function getAllTireSpecDetails() { 
            $scope.tireSpecications = [];           
            tireService.GetallTireSpecification(function (result) {
                //console.log(result);
                if (result.success === true) {                    
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireSpecications.push(result.data[i]);    
                    }
                    myGrid();
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }

        $scope.changeId = function(manufactureId) {
            console.log(manufactureId);
            $scope.globalManufacturerId = manufactureId;
        }
        //getting dropdown values
        //to get all tire manufacurer details.
        
        function getAllTireManufacturerDetails() {    
            $scope.tireManufacturers =[];        
            tireService.GetallTireManufacture(function (result) {
                console.log(result); 
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireManufacturers.push({value: result.data[i]._id, text: result.data[i].tireManufacturerName});    
                    }
                } else {
                    toastr.error(result.message);
                          
                }
            });
        };
        function getAllTireModelDetails() {          
            $scope.tireModels=[];  
            tireService.GetallTireModel(function (result) {
                console.log(result);
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireModels.push({value: result.data[i]._id, text: result.data[i].tireModelNo, manufactureId: result.data[i].tireManufacturerId});    
                    }
                } else {
                    toastr.error(result.message);
                           
                }
            });
        };
        function getAllTireRecommendedPositionDetails() {    
            $scope.tireRecommendedPositions = [];        
            tireService.GetallTireRecommendedPosition(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireRecommendedPositions.push({value: result.data[i]._id, text: result.data[i].tireRecommendedPositionName});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };
        function getAllTireType() {         
            $scope.tiretypes = [];   
            tireService.GetAllTireType(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tiretypes.push({value: result.data[i]._id, text: result.data[i].tireTypeName});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };
        //end of dropdown values

        function deleteTireSpecDetails(id) {            
            tireService.DeleteTireSpecification(id,function (result) {
                if (result.success === true) {
                    $scope.tireSpecications=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.clearData();
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };

        function updateTireSpecDetails(data) {            
            tireService.UpdateTireSpecification(data,function (result) {
                if (result.success === true) {
                    $scope.vccUser = {};
                    $scope.tireSpecications=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.isValidate=true;
                    vm.Modalinstance.close();                    
                    $scope.clearData();
                } else {
                    $scope.isValidate=false;
                    toastr.error(result.message);
                    console.log(result);   
                }
            })
        }

        $scope.currRowData = function(rowform) {
            console.log(rowform);
            var constVal =30;
            var x3=parseFloat($("#tireThreadDepth_t3").val());
            var x2=parseFloat($("#tireThreadDepth_t2").val());
            var x1=parseFloat($("#tireThreadDepth_t1").val());
            var twival = parseFloat($('#tireWearIndicator_twi').val());
            console.log(x3,x2,x1,twival)
            console.log(x3,x2,x1);
            if(x1 > 30.00 || x1 <= 0){
                alertify.alert("Thread Depth 1 cannot be greater than 30.00 and 0");
                $scope.isValidate = false;
                return;
            }else if(x2 > 30.00 || x2 <= 0){
                alertify.alert("Thread Depth 2 cannot be greater than 30.00 and 0");
                $scope.isValidate = false;
                return;
            }else if(x3 > 30.00 || x3 <= 0 ){
                alertify.alert("Thread Depth 3 cannot be greater than 30.00 and 0");
                $scope.isValidate = false;
                return;
            }else if(twival > 2){
                alertify.alert("TWI cannot be greater than 2");
                $scope.isValidate = false;
                return;
            }else if(parseInt($("#minpressureThreshold").val()) >= parseInt($("#maxPressureThreshold").val())){
                alertify.alert("Minimum pressure Value should be less than the Max Pressure");
                $scope.isValidate = false;
                return;
            }
            //for validation if data is blank or not.
            var count=0;
            $scope.isValidate = false;
            if(rowform){
                $(".data_field").each(function() {
                    //console.log($(this));
                    var dataValue= $(this).val(); 
                    //console.log(dataValue);
                    count++;
                    if(dataValue==""){
                        toastr.error("Please Fill valid data in required fields");
                        $scope.isValidate = false;
                        return false;
                    }
                })
            } else {
                //$scope.isValidate = true;
            }
            var data = {};
            if(count>=16){
                //$scope.isValidate = true;
                if(rowform._id == "" || rowform._id == undefined){
                    data={
                        tireManufacturerName:$("#tiremanufacturerId option:selected").text(),
                        tireModelNo:$("#tireModelId option:selected").text(),
                        tireThreadDepth_t1:rowform.tireThreadDepth_t1,
                        tireThreadDepth_t2:rowform.tireThreadDepth_t2,
                        tireThreadDepth_t3:rowform.tireThreadDepth_t3,
                        tireWearIndicator_twi:rowform.tireWearIndicator_twi,
                        maxPressureThreshold:rowform.maxPressureThreshold,
                        minpressureThreshold:rowform.minpressureThreshold,
                        maxTemperatureThreshold:rowform.maxTemperatureThreshold,
                        tireCost:rowform.tireCost,
                        tireRemouldCost:rowform.tireRemouldCost,
                        expectedTireLife_km:rowform.expectedTireLife_km,
                        recommendedcoldInflationPressure:rowform.recommendedcoldInflationPressure,
                        remouldCount:rowform.remouldCount,
                        recommendedPositionName:$("#recommendedPositionId option:selected").text(),
                        tireTypeName:$("#tireTypeId option:selected").text() 
                    }
                    console.log("add data", data);
                    addTireSpec(data);
                }else{
                    data={
                        _id:rowform._id,
                        tireManufacturerName:$("#tiremanufacturerId option:selected").text(),
                        tireModelNo:$("#tireModelId option:selected").text(),
                        tireThreadDepth_t1:rowform.tireThreadDepth_t1,
                        tireThreadDepth_t2:rowform.tireThreadDepth_t2,
                        tireThreadDepth_t3:rowform.tireThreadDepth_t3,
                        tireWearIndicator_twi:rowform.tireWearIndicator_twi,
                        maxPressureThreshold:rowform.maxPressureThreshold,
                        minpressureThreshold:rowform.minpressureThreshold,
                        maxTemperatureThreshold:rowform.maxTemperatureThreshold,
                        tireCost:rowform.tireCost,
                        tireRemouldCost:rowform.tireRemouldCost,
                        expectedTireLife_km:rowform.expectedTireLife_km,
                        recommendedcoldInflationPressure:rowform.recommendedcoldInflationPressure,
                        remouldCount:rowform.remouldCount,
                        recommendedPositionName:$("#recommendedPositionId option:selected").text(),
                        tireTypeName:$("#tireTypeId option:selected").text()
                    }
                    updateTireSpecDetails(data);
                }
            }
        }
        // For Loading the Grid Widgets
        function myGrid(){ 
        var source =
        {
            localdata: $scope.tireSpecications,
            datatype: "json",
            datafields:
            [
                { name: '_id', type: 'string' },
                { name: 'tiremanufacturerId', type: 'string' },
                { name: 'tiremnaufacturename', type: 'string' },
                { name: 'tiremodelId', type: 'string' },
                { name: 'tiremodelNo', type: 'string' },
                { name: 'tireTypeId', type: 'string' },
                { name: 'tireTypeName', type: 'string' },
                { name: 'tireThreadDepth_t1', type: 'number' },
                { name: 'tireThreadDepth_t2', type: 'number' },
                { name: 'tireThreadDepth_t3', type: 'number' },
                { name: 'tireWearIndicator_twi', type: 'number' },
                { name: 'maxPressureThreshold', type: 'number' },
                { name: 'minpressureThreshold', type: 'number' },
                { name: 'maxTemperatureThreshold', type: 'number' },
                { name: 'tireCost', type: 'number' },
                { name: 'tireRemouldCost', type: 'number' },
                { name: 'expectedTireLife_km', type: 'number' },
                { name: 'recommendedcoldInflationPressure', type: 'number' },
                { name: 'remouldCount', type: 'number' },
                { name: 'recommendedPositionId', type: 'string' },
                { name: 'tireRecommendedPositionName', type: 'string' }
            ]
        };

        var dataAdapter = new $.jqx.dataAdapter(source);         
        $("#tirespecgrid").jqxGrid(
        {
            width: '100%',
            theme: 'dark',
            source: dataAdapter,
            columnsresize: false,
            //selectionmode: 'checkbox',
            sortable: true,
            pageable: true,
            pagesize:5,
            autoheight: true,
            columnsheight: 60,
            rowsheight: 50,
            columns: [
                {
                    text: 'Sr No',sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                    cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                    }
                },
                { text: 'Tyre ManufacturerId', datafield: 'tiremanufacturerId',hidden:true,align: 'center',cellsalign: 'center', width: '5%' },
                { text: 'Tyre Manufacture', datafield: 'tiremnaufacturename',align: 'center',cellsalign: 'center', width: '10%' },
                { text: 'Tyre model Id', datafield: 'tiremodelId',hidden:true,align: 'center',cellsalign: 'center', width: '5%' },
                { text: 'Tyre Model No', datafield: 'tiremodelNo',align: 'center',cellsalign: 'center', width: '20%' },
                { text: 'Tyre Type Id', datafield: 'tireTypeId',hidden:true,align: 'center', width: '5%' },
                { text: 'Tyre Type', datafield: 'tireTypeName',align: 'center',cellsalign: 'center', width: '10%' },

                { text: 'Thread Depth T1', datafield: 'tireThreadDepth_t1',align: 'center',cellsalign: 'center', width: '10%' },
                { text: 'Thread Depth T2', datafield: 'tireThreadDepth_t2',align: 'center',cellsalign: 'center', width: '10%' },
                { text: 'Thread Depth T3', datafield: 'tireThreadDepth_t3',align: 'center',cellsalign: 'center', width: '10%' },
                // { text: 'Tyre Wear Indicator twi', datafield: 'tireWearIndicator_twi',align: 'center',cellsalign: 'center', width: '5%' },
                // { text: 'Max Pressure Threshold', datafield: 'maxPressureThreshold',align: 'center', width: '5%' },
                // { text: 'Minpressure Threshold', datafield: 'minpressureThreshold',align: 'center', width: '5%' },
                // { text: 'Max Temperature Threshold', datafield: 'maxTemperatureThreshold',align: 'center', width: '5%' },
                { text: 'Tyre Cost', datafield: 'tireCost',align: 'center',cellsalign: 'center', width: '10%' },
                { text: 'Tyre Remould Cost', datafield: 'tireRemouldCost',hidden:true,align: 'center', width: '5%' },
                { text: 'ExpectedTyre Life km', datafield: 'expectedTireLife_km',hidden:true,align: 'center', width: '5%' },
                { text: 'Recommended Cold Inflation Pressure', datafield: 'recommendedcoldInflationPressure',hidden:true,align: 'center', width: '5%' },
                { text: 'Tyre Recommended Position', datafield: 'tireRecommendedPositionName',hidden:true,align: 'center', width: '15%' },
                {
                    text: 'Action', cellsAlign: 'center', align: "center",width: '15%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    // render custom column.
                    return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    }
                }
            ]
        });
        buttonsEvents();
        // Initlize Button Event
        $("#tirespecgrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
        }
        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
        // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#tirespecgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#tirespecgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            console.log("selectedRowData",selectedRowData);
            $scope.changeId(selectedRowData.tiremanufacturerId);
            $scope.vccUser = {
                _id : selectedRowData._id,
                tiremanufacturerId: selectedRowData.tiremanufacturerId,
                tireModelId: selectedRowData.tiremodelId,
                tireTypeId: selectedRowData.tireTypeId,
                tireThreadDepth_t1: selectedRowData.tireThreadDepth_t1,
                tireThreadDepth_t2: selectedRowData.tireThreadDepth_t2,
                tireThreadDepth_t3: selectedRowData.tireThreadDepth_t3,
                tireWearIndicator_twi: selectedRowData.tireWearIndicator_twi,
                maxPressureThreshold: selectedRowData.maxPressureThreshold,
                minpressureThreshold: selectedRowData.minpressureThreshold,
                maxTemperatureThreshold: selectedRowData.maxTemperatureThreshold,
                tireCost: selectedRowData.tireCost,
                tireRemouldCost: selectedRowData.tireRemouldCost,
                expectedTireLife_km: selectedRowData.expectedTireLife_km,
                recommendedcoldInflationPressure: selectedRowData.recommendedcoldInflationPressure,
                remouldCount: selectedRowData.remouldCount,
                recommendedPositionId: selectedRowData.recommendedPositionId
            }
            $scope.openModal();
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#tirespecgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#tirespecgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTireSpecDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
            
        }
        $scope.clearData = function(){ //window.location.reload();
            $scope.vccUser = {};
            $("#tirespecgrid").jqxGrid('clearselection');
        }

        $scope.openTireSpecModal = function () {
            vm.ModalinstanceTireSpecs=$uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTireSpecDetailBulkUpload.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.save = function (data) {
                        console.log(data);
                        $scope.currRowData(data);
                        if($scope.isValidate==true){
                            $uibModalInstance.close();
                        }                    
                    };

                    $scope.uploadTagDetails = function() {
                        //console.log("ssssss"); 
                    var returnVal =false;
                        var currData = {};
                        var fileInput = document.getElementById('the-file');
                        var file = fileInput.files[0];     
                        var fd = new FormData();
                        fd.append('file', file);
                        fd.append('data', 'string');
                        $http.post(config.apiUrl+'/api/upload', fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined }
                        })
                        .success(function(response){
                            console.log(response);
                            //$scope.isValidate==true;
                            var currTagType= '';
                            bulktags=[];
                            $scope.lineNo;
                            if(response.data){
                                console.log(response.data);
                                if(response.data.length ==0){
                                    alertify.alert("Please check uploaded file for valid data");
                                    return
                                }
                                if(Object.keys(response.data[0]).length>=16 || response.data.length !=0 ){
                                    if(response.data.length>0)
                                    {
                                        for(var i=0;i<response.data.length;i++){
                                            $scope.lineNo=i+1;
                                            console.log("sending it to other side response.data[i]");
                                            console.log(response.data[i]);
                                            returnVal =1;
                                            if(response.data[i]['tire manufacturer']!=undefined && 
                                            response.data[i]['tire model']!=undefined && 
                                            response.data[i]['tire recommended position']!=undefined && 
                                            response.data[i]['tire type']!=undefined && 
                                            response.data[i]['threaddepth t1']!=undefined && 
                                            response.data[i]['threaddepth t2']!=undefined && 
                                            response.data[i]['threaddepth t3']!=undefined && 
                                            response.data[i]['tire wear indicator']!=undefined && 
                                            response.data[i]['max pressure threshold']!=undefined && 
                                            response.data[i]['min pressure threshold']!=undefined && 
                                            response.data[i]['max temperature threshold']!=undefined && 
                                            response.data[i]['tire cost']!=undefined && 
                                            response.data[i]['tire remolud cost']!=undefined && 
                                            response.data[i]['expected tire life in km']!=undefined && 
                                            response.data[i]['remould count']!=undefined 
                                            // && 
                                            // response.data[i]['tire manufacturer']!='' && 
                                            // response.data[i]['tire model']!='' && 
                                            // response.data[i]['tire recommended position']!='' && 
                                            // response.data[i]['tire type']!='' && 
                                            // response.data[i]['threaddepth t1']!='' && 
                                            // response.data[i]['threaddepth t2']!='' && 
                                            // response.data[i]['threaddepth t3']!='' && 
                                            // response.data[i]['tire wear indicator']!='' && 
                                            // response.data[i]['max pressure threshold']!='' && 
                                            // response.data[i]['min pressure threshold']!='' && 
                                            // response.data[i]['max temperature threshold']!='' && 
                                            // response.data[i]['tire cost']!='' && 
                                            // response.data[i]['tire remolud cost']!='' && 
                                            // response.data[i]['expected tire life in km']!='' && 
                                            // response.data[i]['remould count']!=''
                                            )
                                            {
                                                returnVal=validatefilerows(
                                                    response.data[i]['tire manufacturer'],
                                                    response.data[i]['tire model'],
                                                    response.data[i]['tire recommended position'],
                                                    response.data[i]['tire type'],
                                                    response.data[i]['threaddepth t1'],
                                                    response.data[i]['threaddepth t2'],
                                                    response.data[i]['threaddepth t3'],
                                                    response.data[i]['tire wear indicator'],
                                                    response.data[i]['max pressure threshold'],
                                                    response.data[i]['min pressure threshold'],
                                                    response.data[i]['max temperature threshold'],
                                                    response.data[i]['tire cost'],
                                                    response.data[i]['tire remolud cost'],
                                                    response.data[i]['expected tire life in km'],
                                                    response.data[i]['recommended cold inflation pressure'],
                                                    response.data[i]['remould count']
                                                )
                                            }                                        
                                            console.log(returnVal);
                                            if(returnVal==10){
                                                continue;  
                                            }
    
                                            if(returnVal==0){
                                                currData = {
                                                    expectedtirelifeinkm:response.data[i]['expected tire life in km'],
                                                    maxpressurethreshold: response.data[i]['max pressure threshold'],
                                                    maxtemperaturethreshold:response.data[i]['max temperature threshold'],
                                                    minpressurethreshold: response.data[i]['min pressure threshold'],
                                                    recommendedcoldinflationpressure: response.data[i]['recommended cold inflation pressure'],
                                                    remouldcount: response.data[i]['remould count'],
                                                    threaddeptht1: response.data[i]['threaddepth t1'],
                                                    threaddeptht2: response.data[i]['threaddepth t2'],
                                                    threaddeptht3: response.data[i]['threaddepth t3'],
                                                    tirecost: response.data[i]['tire cost'],
                                                    tiremanufacturer: response.data[i]['tire manufacturer'],
                                                    tiremodel: response.data[i]['tire model'],
                                                    tirerecommendedposition: response.data[i]['tire recommended position'],
                                                    tireremoludcost: response.data[i]['tire remolud cost'],
                                                    tiretype: response.data[i]['tire type'],
                                                    tirewearindicator: response.data[i]['tire wear indicator']
                                                };
                                                bulktags.push(currData);
                                            }
                                            else{
                                                if(returnVal==1)
                                                {
                                                    alertify.alert("Tire Thread Depth T1 is invalid, pattern accepted (00:00) and should be less than 30 at line "+(i+1));
                                                    return
                                                }else if(returnVal==2)
                                                {
                                                    alertify.alert("Tire Thread Depth T2 is invalid, pattern accepted (00:00) and should be less than 30 at line "+(i+1));
                                                    return
                                                }else if(returnVal==3)
                                                {
                                                    alertify.alert("Tire Thread Depth T3 is invalid, pattern accepted (00:00) and should be less than 30 at line "+(i+1));
                                                    return
                                                }else if(returnVal==4)
                                                {
                                                    alertify.alert("tire wear indicator is invalid, pattern accepted (0:0) and should be less than 2 at line "+(i+1));
                                                    return
                                                }else if(returnVal==5)
                                                {
                                                    alertify.alert("Max Pressure Threshold is invalid, it should be less than 255, at line "+(i+1));
                                                    return
                                                }else if(returnVal==6)
                                                {
                                                    alertify.alert("Min Pressure Threshold is invalid, it should be less than 255 and less than max pressure ,at line "+(i+1));
                                                    return
                                                }else if(returnVal==7)
                                                {
                                                    alertify.alert("Max Temperature Threshold is invalid, it should be less than 100,at line "+(i+1));
                                                    return
                                                }else if(returnVal==8)
                                                {
                                                    alertify.alert("Tire Cost is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==9)
                                                {
                                                    alertify.alert("Tire Remould Cost is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==11)
                                                {
                                                    alertify.alert("Expected Tire Life km is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==12)
                                                {
                                                    alertify.alert("Recommended cold Inflation Pressure is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==13)
                                                {
                                                    alertify.alert("Remould Count is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==14)
                                                {
                                                    alertify.alert("Tire Manufacturer is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==15)
                                                {
                                                    alertify.alert("Tire Model is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==16)
                                                {
                                                    alertify.alert("Tire Recommended Position is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal==17)
                                                {
                                                    alertify.alert("Tire Type is invalid at line "+(i+1));
                                                    return
                                                }else if(returnVal=18){
                                                    alertify.alert("Please Check your file");
                                                    return
                                                }
                                            
                                                break;
                                            }
                                            console.log(bulktags);
    
                                            }  
                                            if(returnVal==0 || returnVal==10)
                                            {
                                                // alert('END')
                                                $scope.fileData= bulktags;
                                                //if(response.data.length== bulktags.length)
                                                //console.log(bulktags +" bulktags");
                                                    AdddepotinBulk(bulktags);
                                                    //$uibModalInstance.close();
                                            }else{
                                                console.log("invalid");
                                            }
                                    }else{
                                        console.log("file is empty");
                                    }
                                }else{
                                    alertify.alert("Please check uploaded file for valid data");
                                    return
                                }
                            }
                        })
                        .error(function(response){
                            $scope.isValidate==false;
                            console.log(response);
                        });
                    }

                        function validatefilerows(Tmf, Tmd,Trp,TT,TTD1,TTD2,TTD3,TWI,Mxpt,Mnpt,MxTT,TC,TRC,ETL,RCIP,RC)
                        {
                            var ErrorFlag=0;
                        if((Tmf=="")&&(Tmd=="")&&(Trp=="")&&(TT=="")&&(TTD1=="")&&(TTD2=="")&&(TTD3=="")
                        &&(TWI=="")&&(Mxpt=="")&&(Mnpt=="")&&(MxTT=="")&&(TC=="")&&(TRC=="")&&(ETL=="")&&(RCIP=="")&&(RC==""))
                        {
                        // console.log("ALL EMPTY IGNORE");
                            ErrorFlag=10;
                            return  ErrorFlag;
                        }
                        var intRegex = /^\d+(?:\.\d\d?)?$/;
                        var decRegex = /^\d+(?:\.\d{1,6})?$/;
                        var numbers = /^[0-9]+$/;
                        var req = /^(?=.*[a-zA-Z]).{2,100}$/;
                        var NameCode= /^[a-zA-Z][a-zA-Z\s]*$/
                                
                        if(!Tmf.match(req)) {
                            ErrorFlag=14;
                            console.log("Tmf invalid number "+Tmf);
                            return  ErrorFlag;
                        }else{
                            ErrorFlag =0;
                        }

                        if(!Tmd.match(req)) {
                            ErrorFlag=15;
                            console.log("Tmd invalid number "+Tmd);
                            return  ErrorFlag;
                        }else{
                            ErrorFlag =0;
                        }

                        if(!Trp.match(req)) {
                            ErrorFlag=16;
                            console.log("Tmd invalid number "+Trp);
                            return  ErrorFlag;
                        }else{
                            ErrorFlag =0;
                        }

                        if(!TT.match(NameCode)) {
                            ErrorFlag=17;
                            console.log("TT invalid number "+TT);
                            return  ErrorFlag;
                        }else{
                            ErrorFlag =0;
                        }


                        if (!TTD1.match(intRegex)) {
                
                        console.log("Please enter a valid number");
                        ErrorFlag=1;
                        return  ErrorFlag;
                        }else {
                            if(parseInt(TTD1)>30)
                            {
                                console.log("Please enter a valid number");
                                ErrorFlag=1;
                                return  ErrorFlag;
                            }else{
                                ErrorFlag =0;
                            }
                        
                        }

                        if (!TTD2.match(intRegex)) {
                
                            console.log("Please enter a valid number");
                                ErrorFlag=2;
                                return  ErrorFlag;
                        }else {
                                    if(parseInt(TTD2)>30)
                                    {
                                        console.log("Please enter a valid number");
                                        ErrorFlag=2;
                                        return  ErrorFlag;
                                    }else{
                                        ErrorFlag =0;
                                    }
                                
                        }

                        if (!TTD3.match(intRegex)) {
                            console.log("Please enter a valid number");
                            ErrorFlag=3;
                            return  ErrorFlag;
                        }else {
                            if(parseInt(TTD3)>30)
                            {
                                console.log("Please enter a valid number");
                                ErrorFlag=3;
                                return  ErrorFlag;
                            }else{
                                ErrorFlag =0;
                            }
                        }

                        if (!TWI.match(intRegex)) {
                            console.log("Please enter a valid number");
                            ErrorFlag=4;
                            return  ErrorFlag;
                        }else {
                            if(parseInt(TWI)>2)
                            {
                                console.log("Please enter a valid number");
                                ErrorFlag=4;
                                return  ErrorFlag;
                            }else{
                                ErrorFlag =0;
                            }
                        }

                        if (!Mxpt.match(decRegex) && (parseInt(Mxpt)<255)) {
                            ErrorFlag=5;
                            console.log("Mxpt invalid number "+Mxpt);
                            return  ErrorFlag;
                        }else {
                            ErrorFlag=5;
                            if(parseInt(Mxpt)>255)
                            {
                                console.log("Please enter a valid number");
                                ErrorFlag=5;
                                return  ErrorFlag;
                            }else{
                                ErrorFlag =0;
                            }
                        }

                        if (!Mnpt.match(decRegex) && (parseInt(Mnpt)<255)) {
                            console.log(!Mnpt.match(decRegex) && (parseInt(Mnpt)<255));
                            console.log(!Mnpt.match(decRegex), (parseInt(Mnpt)<255))
                            ErrorFlag=6;
                            return  ErrorFlag;
                                // if((parseInt(Mxpt)>parseInt(Mnpt))){
                                //     ErrorFlag =0;
                                // }else{
                                //     console.log("Please enter a valid number");
                                //     ErrorFlag=6;
                                //     return  ErrorFlag;
                                // }
                        }else {
                            ErrorFlag=6;
                            //if((parseInt(Mnpt)>255)){
                                if((parseInt(Mxpt)>parseInt(Mnpt))){
                                    ErrorFlag =0;
                                }else{
                                    console.log("Please enter a valid number");
                                    ErrorFlag=6;
                                    return  ErrorFlag;
                                }
                            //}
                        }

                        if (typeof MxTT===undefined || !MxTT.match(decRegex)) {
                            console.log("welcome to MxTT.match(decRegex)");
                            console.log(parseInt(MxTT));
                            console.log("MxTT.match(decRegex)");
                            console.log(MxTT.match(decRegex));
                            ErrorFlag=7;
                            console.log("MxTT invalid number "+MxTT);
                            return  ErrorFlag;
                        }else {
                            console.log("welcome to MxTT.match(decRegex)");
                            console.log(parseInt(MxTT));
                            console.log("MxTT.match(decRegex)");
                            console.log(MxTT.match(decRegex));
                            //if(parseInt(MxTT)>1 && parseInt(MxTT)<100 )
                            if(parseInt(MxTT)<100 )
                            {
                                ErrorFlag =0;
                            }else{                                
                                console.log("Please enter a valid number");
                                ErrorFlag=7;
                                return  ErrorFlag;
                            }
                        }

                        if (!TC.match(decRegex)) {
                            ErrorFlag=8;
                            console.log("TC invalid number "+TC);
                            return  ErrorFlag;
                        }else {
                            // if(parseInt(TC)>255)
                            // {
                            //     console.log("Please enter a valid number");
                            //     ErrorFlag=8;
                            //     return  ErrorFlag;
                            // }else{
                                ErrorFlag =0;
                            //}
                        }

                        if (!TRC.match(numbers)) {
                            ErrorFlag=9;
                            console.log("TRC invalid number "+TRC);
                            return  ErrorFlag;
                        }else {
                            // if(parseInt(TRC)>255)
                            // {
                            //     console.log("Please enter a valid number");
                            //     ErrorFlag=9;
                            //     return  ErrorFlag;
                            // }else{
                                ErrorFlag =0;
                            //}
                        }

                        if (!ETL.match(decRegex)) {
                            ErrorFlag=11;
                            console.log("ETL invalid number "+ETL);
                            return  ErrorFlag;
                        }else {
                            // if(parseInt(ETL)>255)
                            // {
                            //     console.log("Please enter a valid number");
                            //     ErrorFlag=11;
                            //     return  ErrorFlag;
                            // }else{
                                ErrorFlag =0;
                            //}
                        }

                        if (!RCIP.match(decRegex)) {
                            ErrorFlag=12;
                            console.log("RCIP invalid number "+RCIP);
                            return  ErrorFlag;
                        }else {
                            // if(parseInt(RCIP)>255)
                            // {
                            //     console.log("Please enter a valid number");
                            //     ErrorFlag=12;
                            //     return  ErrorFlag;
                            // }else{
                                ErrorFlag =0;
                            //}
                        }

                        if (!RC.match(numbers)) {
                            ErrorFlag=13;
                            console.log("RC invalid number "+RC);
                            return  ErrorFlag;
                        }else {
                            if(parseInt(RC)>255)
                            {
                                console.log("Please enter a valid number");
                                ErrorFlag=13;
                                return  ErrorFlag;
                            }else{
                                ErrorFlag =0;
                            }
                        }

                        return  ErrorFlag;

                    }

                     var bulktags = [];

                    function addBulkTags(data) {
                        console.log(data);
                        animation.myFunc('tirespecbulk');
                        depotService.AddBulkDepot(data,function (result) {
                            if (result.success === true) {
                                toastr.success(result.message);   
                                getAllCustomerDepotDetails();
                                $scope.clearData();
                                $("#tirespecbulk").hide();
                                // $scope.fileData= result.data;
                            }else{
                                toastr.error(result);                
                                $("#tirespecbulk").hide();
                            }
                        })
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.clearData();
                    };

                    function AdddepotinBulk(data){
                        animation.myFunc('tirespecbulk');
                        tireService.AddBulkTireSpecMaster(data, function (result) {
                            console.log(result.data +" :AdddepotinBulk "+ JSON.stringify(data));
                            if (result.success === true) {
                                vm.custTagAssignDetails = {};
                                $scope.vccUser={};
                                $scope.customerDepotData= [];
                                getAllDropdowns();
                                toastr.success(result.message);
                                $('#tirespecbulk').waitMe('hide');
                                vm.ModalinstanceTireSpecs.close();
                                $scope.clearData();
                            } else {
                                $('#tirespecbulk').waitMe('hide');
                                //vm.ModalinstanceTireSpecs.close();
                                // toastr.error(result.message); 
                                //console.log(result.data[0].errmsg);
                                if(result.errorCode==null)
                                {
                                    var message=result.message;  
                                    toastr.error(message.err+" at line "+message.line +" Ignored blank records from the file");
                                }else{
                                    toastr.error(showMissingValue(result.message[0].errmsg));
                                }
                                //vm.error = message;                    
                            }
                        })
                    }
                
                    var showMissingValue = function(errorMsg){
                        console.log(errorMsg);
                        var afterIndex = errorMsg.split('index:');
                        var value = afterIndex[1];
                        var field = value.split('_1');
                        var afterDupErr = errorMsg.split('dup key: { :');
                        var value2 = afterDupErr[1];
                        var field2= value2.split(', :');
                        //alert(field[0]);
                        //alert(field2[0]);
                        //duplicate key error
                        var str1 = errorMsg;
                        var str2 = "duplicate";
                        if(str1.indexOf(str2) != -1){
                            console.log(str2 + " found");
                            //alert(str2)
                            $('#tirespecbulk').waitMe('hide');
                            return field[0]+' is duplicate';
                        }else{
                            //animation
                            $('#tirespecbulk').waitMe('hide');
                            console.log(field[0]+field2[0]);
                            return field[0]+field2[0];
                        }
                    }
                },
                backdrop: 'static',
                scope: $scope,
                })
        };
    }
  
})();
  
  