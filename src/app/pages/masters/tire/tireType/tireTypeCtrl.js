(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireTypesCtrl', TireTypesCtrl);
  
    /** @ngInject */
    function TireTypesCtrl($uibModal,$scope,tireService,$filter, editableOptions, editableThemes, toastr) {
        var vm = this; 
        $scope.openTireTypeModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'tireTypePopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
        vm.getAllTireTypeDetails = getAllTireTypeDetails;
        vm.addTireType = addTireType;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            tireTypeName:"",
            tireTypeDesc:""
       }
        initController();
        $scope.tires = [];
        var data = [];

        function initController() {
            getAllTireTypeDetails();
            // For Open Popup for Add and Edit
           // $("#jqxwindowTType").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addTireType() {            
            tireService.AddTireType(vm.tireTypeName, vm.tireTypeDesc, function (result) {
                if (result.success === true) { 
                    vm.tireTypeName="";
                    vm.tireTypeDesc="";
                    $scope.tires=[];
                    data =[];    
                    $scope.vccUser= {};
                    getAllTireTypeDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowTType").jqxWindow('close');
                } else {
                    toastr.error(result.message);  
                    vm.error = result.message;                    
                }
            });
        };
 

        function getAllTireTypeDetails() {            
            tireService.GetAllTireType(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.tires.push(result.data[i]);  
                        data.push(result.data[i]);   
                    } 
                    myGrid(); 
                } else {
                    toastr.error(result.message);  
                    vm.error = result.message;                    
                }
            });
        };


        function deleteTireTypeDetails(_id) {            
            tireService.DeleteTireType(_id,function (result) {
                if (result.success === true) {  
                    $scope.tires=[];
                    data=[];
                    getAllTireTypeDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);  
                    vm.error = result.message;                    
                }
            });
        };
        function updateTireTypeDetails() {            
            tireService.UpdateTireType(vm._id,vm.tireTypeName,vm.tireTypeDesc,function (result) {
                if (result.success === true) {
                    vm.tireTypeName="";
                    vm.tireTypeDesc=""; 
                    $scope.tires=[];
                    data=[];
                    getAllTireTypeDetails();
                    toastr.success(result.message);
                   // $("#jqxwindowTType").jqxWindow('close');
                } else {
                    toastr.error(result.message);  
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
             //for validation if data is blank or not.
             $scope.isValidate= false;
             if((rowform.tireTypeName==undefined) || rowform.tireTypeName==''){
                toastr.error("Please Fill valid data in required fields");
                return;
            } else {
                $scope.isValidate= true;
                if(rowform._id == "" || rowform._id == undefined){
                    vm.tireTypeName=rowform.tireTypeName;
                    vm.tireTypeDesc= rowform.tireTypeDesc; 
                    addTireType();                
                    $scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.tireTypeName= rowform.tireTypeName; 
                    vm.tireTypeDesc= rowform.tireTypeDesc;
                    updateTireTypeDetails()
                    $scope.clearData();
                    rowform ={};
                }   
            }   
          
        }

    //  $("#popup").click(function () {
    //      $("#tireTypegrid").jqxGrid('clearselection');
    //        getselectedrowindexes =null;
    //        selectedRowData =null;
    //        $('#_id').val('');
    //        vm._id =''
    //      $("#jqxwindowTType").jqxWindow('open');
    //  });
    //  $("#jqxwindowTType").bind('close', function (event){
    //     $scope.clearData();
    // });
    //  $("#button_input").click(function () {
    //      var T = $("#text_input").val();
    //      $("#textbox").text(T);
    //      $("#jqxwindowTType").jqxWindow('close');
    //  });
    //  $("#button_no").click(function () {
    //      $("#jqxwindowTType").jqxWindow('close');
    //  });

     // For Loading the Grid Widgets

     function myGrid(){ 
        var source =
        {
            localdata: data,
            datatype: "json",
            datafields:
            [
                { name: '_id', type: 'string' },
                { name: 'tireTypeName', type: 'string' },
                { name: 'tireTypeDesc', type: 'string' }
            ]
        };

        var dataAdapter = new $.jqx.dataAdapter(source);         
        $("#tireTypegrid").jqxGrid(
        {
            width: '100%',
            theme: 'dark',
            source: dataAdapter,
            columnsresize: false,
            //selectionmode: 'checkbox',
            sortable: true,
            pageable: true,
            pagesize:5,
            autoheight: true,
            columnsheight: 60,
            rowsheight: 50,
            columns: [
                {
                    text: 'Sr No',sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                    cellsrenderer: function (row, column, value) {
                    return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                    }
                },
                { text: 'Name', datafield: 'tireTypeName',align: 'center',cellsalign: 'center', width: '33%' },
                { text: 'Description', datafield: 'tireTypeDesc',align: 'center', cellsalign: 'center', width: '33%' },
                {
                    text: 'Action', cellsAlign: 'center', align: "center", cellsalign: 'center', width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    // render custom column.
                    return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" disabled data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    }
                }
            ]
        });
        // Initlize Button Event
        buttonsEvents();
        $("#tireTypegrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
    }

    function buttonsEvents() 
    {
        // For Edit Details
        var editbuttons = document.getElementsByClassName('editButtons');
        for (var i = 0; i < editbuttons.length; i+=1) {
            editbuttons[i].addEventListener('click', editDetails);
        }
          // For Delete Details
          var deletebuttons = document.getElementsByClassName('deleteButtons');
          for (var i = 0; i < deletebuttons.length; i+=1) {
            deletebuttons[i].addEventListener('click', deleteDetails);
          }
    }

    function editDetails() { 
        var id = this.getAttribute("data-row"); 
        var getselectedrowindexes = $('#tireTypegrid').jqxGrid('getselectedrowindexes');
        if (getselectedrowindexes.length > 0)
        { 
            var selectedRowData = $('#tireTypegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
        } 
        $scope.vccUser = {
            _id : selectedRowData._id,
            tireTypeName:selectedRowData.tireTypeName,
            tireTypeDesc:selectedRowData.tireTypeDesc
        }

        $scope.openTireTypeModal();
        // $('#_id').val(selectedRowData._id);
        // $('#tireTypeName').val(selectedRowData.tireTypeName);
        // $("#tireTypeDesc").val(selectedRowData.tireTypeDesc); 
       // $("#jqxwindowTType").jqxWindow('open'); 
    }  
    function deleteDetails(){
        getselectedrowindexes = $('#tireTypegrid').jqxGrid('getselectedrowindexes');
        if (getselectedrowindexes.length > 0)
        {
            selectedRowData = $('#tireTypegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
        } 
        // confirm dialog
        alertify.confirm("Are you sure, you want to delete?", function () {
            // user clicked "ok".
            deleteTireTypeDetails(selectedRowData._id);
        }, function() {
            // user clicked "cancel"
        });
        
    }
        $scope.clearData = function(){ //window.location.reload();
            // $('#_id').val('');
            // $('#tireTypeName').val('');
            // $("#tireTypeDesc").val(''); 
            $scope.vccUser = {
                _id:"",
                tireTypeName:"",
                tireTypeDesc:""
            }
            $("#tireTypegrid").jqxGrid('clearselection');
        }
        
        // $("#button_no").click(function () {
        //     $("#jqxwindowTType").jqxWindow('close');
        // });

    }
  
})();
  
  