(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireManufacturerCtrl', TireManufacturerCtrl);
  
    /** @ngInject */
    function TireManufacturerCtrl($scope,tireService,$filter, editableOptions, editableThemes, toastr, $uibModal) {
        var vm = this; 

        $scope.openModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowTireManufacturer.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };


        vm.getAllTireManufacturerDetails = getAllTireManufacturerDetails;
        vm.addTireManufacturer = addTireManufacturer; 
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            tireManufacturerName:"",
            tireManufacturerPlace:""
       }
        initController();
        $scope.tireManufacturers = []; 
        var data = [];
        
        function initController() {
            getAllTireManufacturerDetails();
           // $("#jqxwindowTireManufacturer").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addTireManufacturer() {            
            tireService.AddTireManufacture(vm.tireManufacturerName, vm.tireManufacturerPlace, function (result) {
                if (result.success === true) { 
                    vm.tireManufacturerName="";
                    vm.tireManufacturerPlace="";
                    $scope.tireManufacturers=[];
                    data =[];    
                    $scope.vccUser= {};
                    getAllTireManufacturerDetails();
                    //$("#jqxwindowTireManufacturer").jqxWindow('close');
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };
 

        function getAllTireManufacturerDetails() {            
            tireService.GetallTireManufacture(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireManufacturers.push(result.data[i]);    
                        data.push(result.data[i]); 
                    }
                    myGrid(); 
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };


        function deleteTireManufacturerDetails(_id) {            
            tireService.DeleteTireManufacture(_id,function (result) {
                if (result.success === true) {  
                    $scope.tireManufacturers=[];
                    data=[];
                    getAllTireManufacturerDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); ;
                    vm.error = result.message;                    
                }
            });
        };
        function updateTireManufactureDetails() {            
            tireService.UpdateTireManufacture(vm._id,vm.tireManufacturerName,vm.tireManufacturerPlace,function (result) {
                if (result.success === true) {
                    vm.tireManufacturerName="";
                    vm.tireManufacturerPlace=""; 
                    $scope.tireManufacturers=[];
                    data=[];
                    getAllTireManufacturerDetails();
                   // $("#jqxwindowTireManufacturer").jqxWindow('close');
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        
        // Save or Update Data
        $scope.currRowData = function(rowform) {
             //for validation if data is blank or not.
             $scope.isValidate = false;
             if(rowform==undefined||(rowform.tireManufacturerName=="" || rowform.tireManufacturerName==undefined)||(rowform.tireManufacturerPlace=="" || rowform.tireManufacturerPlace==undefined)){ 
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate = false;
                return;
            } else {
                $scope.isValidate = true;
                if(rowform._id == "" || rowform._id == undefined){
                    vm.tireManufacturerName=rowform.tireManufacturerName;
                    vm.tireManufacturerPlace= rowform.tireManufacturerPlace; 
                    addTireManufacturer();                
                    $scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.tireManufacturerName= rowform.tireManufacturerName; 
                    vm.tireManufacturerPlace= rowform.tireManufacturerPlace;
                    updateTireManufactureDetails()
                    $scope.clearData();
                    rowform ={};
                }               
            }    

        }   

        // $("#popup").click(function () {
        //     $("#tireManufacturergrid").jqxGrid('clearselection');
        //     getselectedrowindexes =null;
        //     selectedRowData =null;
        //     $('#_id').val('');
        //     vm._id =''
        //     $("#jqxwindowTireManufacturer").jqxWindow('open');
        // })

        // $("#jqxwindowTireManufacturer").bind('close', function (event){
        //  $scope.clearData();
        // })

        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'tireManufacturerName', type: 'string' },
                    { name: 'tireManufacturerPlace', type: 'string' }
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#tireManufacturergrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Name', datafield: 'tireManufacturerName',align: 'center',cellsalign: 'center', width: '33%' },
                    { text: 'Place', datafield: 'tireManufacturerPlace',align: 'center',cellsalign: 'center', width: '33%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
            $("#tireManufacturergrid").on("pagechanged filter sort scroll pagesizechanged", function (event) {
                buttonsEvents();
            });
        }
      
        function buttonsEvents() {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
            deletebuttons[i].addEventListener('click', deleteDetails);
            }
         }

        function editDetails() { 
            var getselectedrowindexes = $('#tireManufacturergrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#tireManufacturergrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id : selectedRowData._id,
                tireManufacturerName:selectedRowData.tireManufacturerName,
                tireManufacturerPlace:selectedRowData.tireManufacturerPlace
            }
            $scope.openModal();
            // $('#_id').val(selectedRowData._id);
            // $('#tireManufacturerName').val(selectedRowData.tireManufacturerName);
            // $("#tireManufacturerPlace").val(selectedRowData.tireManufacturerPlace); 
            // $("#jqxwindowTireManufacturer").jqxWindow('open'); 
        }  

        function deleteDetails(){
            getselectedrowindexes = $('#tireManufacturergrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#tireManufacturergrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteTireManufacturerDetails(selectedRowData._id);
            }, function() {
            });

        }
        $scope.clearData = function(){ //window.location.reload();
            // $('#_id').val('');
            // $('#tireManufacturerName').val('');
            // $("#tireManufacturerPlace").val(''); 
            vm._id =''
            $scope.vccUser = {
                _id:"",
                tireManufacturerName:"",
                tireManufacturerPlace:""
            }
            $("#tireManufacturergrid").jqxGrid('clearselection');
        }
        // $("#button_no").click(function () {
        //     $("#jqxwindowTireManufacturer").jqxWindow('close');
        // });
    }
  
})();
  
  