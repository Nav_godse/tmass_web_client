(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireStatusCtrl', TireStatusCtrl);
  
    /** @ngInject */
    function TireStatusCtrl($uibModal,$scope,tireService,$filter, editableOptions, editableThemes, toastr) {
        var vm = this; 
        $scope.openTireStatusModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'tireStatusPopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
        vm.getAllTireStatusDetails = getAllTireStatusDetails;
        vm.addTireStatus = addTireStatus;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            tireStatusName:"",
            tireStatusDesc:""
       }
        initController();
        $scope.tirestatuses = []; 
        var data = [];
        
        function initController() {
            getAllTireStatusDetails();
            // For Open Popup for Add and Edit
            //$("#jqxwindowTStatus").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addTireStatus() {            
            tireService.AddTireStatus(vm.tireStatusName, vm.tireStatusDesc, function (result) {
                if (result.success === true) { 
                    vm.tireStatusName="";
                    vm.tireStatusDesc="";
                    $scope.tirestatuses=[];
                    data =[];    
                    $scope.vccUser= {};
                    getAllTireStatusDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowTStatus").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
 

        function getAllTireStatusDetails() {            
            tireService.GetallTireStatus(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                        $scope.tirestatuses.push(result.data[i]); 
                        data.push(result.data[i]);      
                    } 
                    myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };


        function deleteTireStatusDetails(_id) {            
            tireService.DeleteTireStatus(_id,function (result) {
                if (result.success === true) {  
                    $scope.tirestatuses=[];
                    data=[];
                    getAllTireStatusDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function updateTireStatusDetails() {            
            tireService.UpdateTireStatus(vm._id,vm.tireStatusName,vm.tireStatusDesc,function (result) {
                if (result.success === true) {
                    vm.tireStatusName="";
                    vm.tireStatusDesc=""; 
                    $scope.tirestatuses=[];
                    data=[];
                    getAllTireStatusDetails();
                    toastr.success(result.message);
                   // $("#jqxwindowTStatus").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        } 

        $scope.currRowData = function(rowform) {
             //for validation if data is blank or not.
             $scope.isValidate= false;
             if(rowform==undefined||(rowform.tireStatusName=="")|| rowform.tireStatusName==undefined){
                toastr.error("Please Fill valid data in required fields");
                return;
            } else {
                $scope.isValidate= true;
                if(rowform._id == "" || rowform._id == undefined){
                    vm.tireStatusName=rowform.tireStatusName;
                    vm.tireStatusDesc= rowform.tireStatusDesc; 
                    addTireStatus();                
                    $scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.tireStatusName= rowform.tireStatusName; 
                    vm.tireStatusDesc= rowform.tireStatusDesc;
                    updateTireStatusDetails()
                    $scope.clearData();
                    rowform ={};
                }
            }
                                  
        }
        

        // $("#popup").click(function () {
        //     $("#tirestatusgrid").jqxGrid('clearselection');
        //     getselectedrowindexes =null;
        //     selectedRowData =null;
        //     $('#_id').val('');
        //     vm._id =''
        //     $("#jqxwindowTStatus").jqxWindow('open');
        // });
        // $("#jqxwindowTStatus").bind('close', function (event){
        //     $scope.clearData();
        // });
        // $("#button_input").click(function () {
        //     var T = $("#text_input").val();
        //     $("#textbox").text(T);
        //     $("#jqxwindowTStatus").jqxWindow('close');
        // });
        // $("#button_no").click(function () {
        //     $("#jqxwindowTStatus").jqxWindow('close');
        // });

        // For Loading the Grid Widgets
        function myGrid(){ 
        var source =
        {
            localdata: data,
            datatype: "json",
            datafields:
            [
                { name: '_id', type: 'string' },
                { name: 'tireStatusName', type: 'string' },
                { name: 'tireStatusDesc', type: 'string' }
            ]
        };

        var dataAdapter = new $.jqx.dataAdapter(source);         
        $("#tirestatusgrid").jqxGrid(
        {
            width: '100%',
            theme: 'dark',
            source: dataAdapter,
            columnsresize: false,
            //selectionmode: 'checkbox',
            sortable: true,
            pageable: true,
            pagesize:5,
            autoheight: true,
            columnsheight: 60,
            rowsheight: 50,
            columns: [
                {
                    text: 'Sr No',sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                    cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                    }
                },
                { text: 'Name', datafield: 'tireStatusName',align: 'center',cellsalign: 'center', width: '33%' },
                { text: 'Description', datafield: 'tireStatusDesc',align: 'center', cellsalign: 'center', width: '33%' },
                {
                    text: 'Action', cellsAlign: 'center', align: "center", width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    // render custom column.
                    return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    }
                }
            ]
        });
        // Initlize Button Event
        buttonsEvents();
        $("#tirestatusgrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
        }
        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
        }
        // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#tirestatusgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#tirestatusgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id : selectedRowData._id,
                tireStatusName:selectedRowData.tireStatusName,
                tireStatusDesc:selectedRowData.tireStatusDesc
            }
            $scope.openTireStatusModal();
            // $('#_id').val(selectedRowData._id);
            // $('#tireStatusName').val(selectedRowData.tireStatusName);
            // $("#tireStatusDesc").val(selectedRowData.tireStatusDesc); 
           // $("#jqxwindowTStatus").jqxWindow('open'); 
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#tirestatusgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#tirestatusgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTireStatusDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
        }
    

        $scope.clearData = function(){ //window.location.reload();
            // $('#_id').val('');
            // $('#tireStatusName').val('');
            // $("#tireStatusDesc").val(''); 
            // $("#tirestatusgrid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                tireStatusName:"",
                tireStatusDesc:""
            }
        }
        // $("#button_no").click(function () {
        //     $("#jqxwindowTStatus").jqxWindow('close');
        // });
    }
  
})();
  
  