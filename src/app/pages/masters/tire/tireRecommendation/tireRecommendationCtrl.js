(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireRecommendationCtrl', TireRecommendationCtrl);
  
    /** @ngInject */
    function TireRecommendationCtrl($scope,tireService,$filter, editableOptions, editableThemes, toastr, $uibModal) {
        var vm = this; 

        $scope.openModal = function () {
            vm.modalInstance= $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowTRec.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.getAllTireRecommendationDetails = getAllTireRecommendationDetails;
        vm.addTireRecommendation = addTireRecommendation;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            tireRecommendedPositionName:"",
            tireRecommendedPositionDescription:""
       }
        initController();
        $scope.tirerecommendations = [];
        var data = [];

        function initController() {
            getAllTireRecommendationDetails();
            // For Open Popup for Add and Edit
            //$("#jqxwindowTRec").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addTireRecommendation() {            
            tireService.AddTireRecommendedPosition(vm.tireRecommendedPositionName, vm.tireRecommendedPositionDescription, function (result) {
                if (result.success === true) {
                    vm.tireRecommendedPositionName="";
                    vm.tireRecommendedPositionDescription="";
                    $scope.tirerecommendations=[];
                    data =[];    
                    $scope.vccUser= {};
                    //$("#jqxwindowTRec").jqxWindow('close'); 
                    getAllTireRecommendationDetails();
                    toastr.success(result.message);
                    $scope.isValidate==true;
                    vm.modalInstance.close()
                   // $scope.clearData();
                } else {
                    $scope.isValidate==false;
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }
 
        function getAllTireRecommendationDetails() {            
            tireService.GetallTireRecommendedPosition(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.tirerecommendations.push(result.data[i]); 
                        data.push(result.data[i]);   
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        function deleteTireRecommendationDetails(_id) {            
            tireService.DeleteTireRecommendedPosition(_id,function (result) {
                if (result.success === true) {  
                    $scope.tirerecommendations=[];
                    data=[];
                    getAllTireRecommendationDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        function updateTireRecommendationDetails() {            
            tireService.UpdateTireRecommendedPosition(vm._id,vm.tireRecommendedPositionName,vm.tireRecommendedPositionDescription,function (result) {
                if (result.success === true) {
                    vm.tireRecommendedPositionName="";
                    vm.tireRecommendedPositionDescription=""; 
                    $scope.tirerecommendations=[];
                    data=[];
                    //$("#jqxwindowTRec").jqxWindow('close');
                    getAllTireRecommendationDetails();
                   // $scope.clearData();
                   $scope.isValidate==true;
                   vm.modalInstance.close()
                    toastr.success(result.message);
                } else {
                    $scope.isValidate==false;
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            $scope.isValidate = false;
            if(rowform==undefined || (rowform.tireRecommendedPositionName=="")){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate = false;
                return;
            } else {
                //$scope.isValidate = true;
                if(rowform._id == "" || rowform._id == undefined){
                    vm.tireRecommendedPositionName=rowform.tireRecommendedPositionName;
                    vm.tireRecommendedPositionDescription= rowform.tireRecommendedPositionDescription; 
                    addTireRecommendation();           
                    $scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.tireRecommendedPositionName= rowform.tireRecommendedPositionName; 
                    vm.tireRecommendedPositionDescription= rowform.tireRecommendedPositionDescription;
                    updateTireRecommendationDetails()
                    $scope.clearData();
                    rowform ={};
                }                 
            }

        }

        //  $("#popup").click(function () {
        //      $("#tirereccomgrid").jqxGrid('clearselection');
        //      getselectedrowindexes =null;
        //      selectedRowData =null;
        //      $('#_id').val('');
        //      vm._id ='';
        //      $("#jqxwindowTRec").jqxWindow('open');
        //  });
        // $("#jqxwindowTRec").bind('close', function (event){
        //     $scope.clearData();
        // });
          // For Loading the Grid Widgets
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'tireRecommendedPositionName', type: 'string' },
                    { name: 'tireRecommendedPositionDescription', type: 'string' }
                ]
            };
    
            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#tirereccomgrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Name', datafield: 'tireRecommendedPositionName',align: 'center',cellsalign: 'center', width: '33%' },
                    { text: 'Description', datafield: 'tireRecommendedPositionDescription',align: 'center', cellsalign: 'center', width: '33%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();            
            $("#tirereccomgrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
            }

            function buttonsEvents() 
            {
                // For Edit Details
                var editbuttons = document.getElementsByClassName('editButtons');
                for (var i = 0; i < editbuttons.length; i+=1) {
                    editbuttons[i].addEventListener('click', editDetails);
                }
                  // For Delete Details
                  var deletebuttons = document.getElementsByClassName('deleteButtons');
                  for (var i = 0; i < deletebuttons.length; i+=1) {
                    deletebuttons[i].addEventListener('click', deleteDetails);
                  }
            }
            // For Edit Details
            function editDetails() { 
                var id = this.getAttribute("data-row"); 
                var getselectedrowindexes = $('#tirereccomgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                { 
                    var selectedRowData = $('#tirereccomgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                $scope.vccUser = {
                    _id : selectedRowData._id,
                    tireRecommendedPositionName: selectedRowData.tireRecommendedPositionName,
                    tireRecommendedPositionDescription: selectedRowData.tireRecommendedPositionDescription,
                    tireStatusDesc: selectedRowData.tireStatusDesc
                }
                $scope.openModal();
                // $('#_id').val(selectedRowData._id);
                // $('#tireRecommendedPositionName').val(selectedRowData.tireRecommendedPositionName);
                // $("#tireRecommendedPositionDescription").val(selectedRowData.tireRecommendedPositionDescription); 
                // $("#jqxwindowTRec").jqxWindow('open'); 
            }

              // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#tirereccomgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#tirereccomgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTireRecommendationDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
        }
    
        $scope.clearData = function(){ //window.location.reload();
            // $('#_id').val('');
            // $('#tireRecommendedPositionName').val('');
            // $("#tireRecommendedPositionDescription").val(''); 
            $scope.vccUser = {
                _id:"",
                tireRecommendedPositionName:"",
                tireRecommendedPositionDescription:""
            }
            vm._id ='';
            $("#tirereccomgrid").jqxGrid('clearselection');
        }
        // $("#button_no").click(function () {
        //     $("#jqxwindowTRec").jqxWindow('close');
        // });
    }
  
})();
  
  