(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireDetailsCtrl', TireDetailsCtrl);
  
    /** @ngInject */
    function TireDetailsCtrl($uibModal,$scope,tireService,tagService,depotService,toastr,localStorage,allDashboardService,customerService,animation) {
        var vm = this;
        $scope.disabled = false;
        vm.curruserData = localStorage.getObject('dataUser');
        console.log(vm.curruserData.otherData.roleName);
        $scope.roleName=vm.curruserData.otherData.roleName;
        $scope.allOemAdminsId = [];
        $scope.openTireDetailModal = function () {
            vm.modalInstance =$uibModal.open({
                template: "<div ng-include src=\"'tireDetailsPopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
        vm.getAllTireDetails = getAllTireDetails;
        vm.addTireDetails = addTireDetails;
        vm.TireSpeclId="";
        var getselectedrowindexes =null;
        var selectedRowData =null;
        // $scope.vccUser = {
        //     _id: "",
        //     tireManufacturerId: "", //edit
        //     tagId: "", 
        //     tireSerialNumber: "",   //edit
        //     isTireModelNew: "", //false
        //     tireModelNo: "",    //edit
        //     tireThreadDepth_t1: "",
        //     tireThreadDepth_t2: "",
        //     tireThreadDepth_t3: "",
        //     tireWearIndicator_twi: "",
        //     maxPressureThreshold: "",
        //     minPressureThreshold: "",
        //     maxTempratureThreshold: "",
        //     tireCost_inr: "",
        //     tireRemouldCost: "",
        //     expectedTireLife_km: "",
        //     recommendedcoldInflationPressure: "",
        //     remouldCount: "",
        //     recommendedPositionName: "", //dropdown
        //     cusotmerAssignedTagId: "", //blank
        //     tireStatusName: "", //dropdown
        //     tireTypeName: "", //dropdown
        //     kmTravelled: "",        //edit
        //     depotId: "", //dropdown     //edit
        // }
        initController();
        $scope.tireData = [];
        $scope.tireRecommendedPositions = [];
        $scope.tireManufacturers = [];
        $scope.allTagId = [];
        $scope.allTireStatus = [];
        $scope.allDepot = [];
        $scope.tireModels = [];
        $scope.tiretypes = [];
        function initController() {
            getAllDropdowns()            
        }
        
        function getOEMAdmins() {
            customerService.ShowadminsOfOEM(function(result){
                if (result.success === true) {
                    console.log(result.data);
                    for(var i=0;i<result.data.length;i++){
                        $scope.allOemAdminsId.push(result.data[i]._id);
                    }                    
                }else{
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }

        function getAllDropdowns(){
            if(vm.curruserData.otherData.roleName=="OEM Admin"){
                getOEMAdmins();
            }
            validateFeilds();
            getAllTireType();
            getAllTireManufacturerDetails();
            //getAllTagDetails();
            getAllTireStatus();
            getAllTireModelDetails();
            getAllTireRecommendedPositionDetails();
            getAllTireDetails();
            getAllDepot();
        }

        function addTireDetails(data) {  
            $scope.tireData = [];          
            animation.myFunc('tireDiv');
            tireService.AddTireDetails(data, function (result) {
                if (result.success === true) {
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.clearData();
                    $scope.isValidate=true;
                    $('#tireDiv').waitMe('hide');
                    vm.modalInstance.close();
                } else {
                    $('#tireDiv').waitMe('hide');
                    toastr.error(result.message); 
                }
            })
            tireService.AddTireSpecification($scope.specdata2, function (result) {
                if (result.success === true) {
                } else {        
                }
            });
        }

        function getAllTireDetails() {   
            $scope.tireData = [];         
            //GetTireReportofSystem
            if(vm.curruserData.otherData.roleName=="System Admin" || vm.curruserData.otherData.roleName=="System Super Admin"){
                tireService.GetTireReportofSystem(function (result) {
                    console.log(result);
                    if (result.success === true) {                    
                        for(var i=0;i<result.data.length;i++){
                            
                            for(var j=0;j<$scope.tireModels.length;j++){
                                if(result.data[i].tireModelId===$scope.tireModels[j].value){
                                    result.data[i].tireModelNo = $scope.tireModels[j].text;
                                    break;
                                }else{
                                    result.data[i].tireModelNo = "None";
                                }
                            }
                            for(var j=0;j<$scope.tireManufacturers.length;j++){
                                if(result.data[i].tireManufacturerId===$scope.tireManufacturers[j].value){
                                    result.data[i].tireManufacturerName = $scope.tireManufacturers[j].text;
                                    break;
                                }else{
                                    result.data[i].tireManufacturerName = "None";
                                }
                            }
                            $scope.tireData.push(result.data[i]);
                               
                        }
                        console.log("tireDetails", $scope.tireData) ;
                        myGrid();
                    } else  {
                        toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                })
            }else if(vm.curruserData.otherData.roleName=="OEM Admin"){
                customerService.ShowadminsOfOEM(function(result){
                    $scope.allOemAdminsId =[];
                    if (result.success === true) {
                        console.log(result.data);
                        for(var i=0;i<result.data.length;i++){
                            $scope.allOemAdminsId.push(result.data[i]._id);
                        }               
                        tireService.GetTireReportofSystem(function (result) {
                            console.log('result.data');
                            console.log(result.data);
                            if (result.success === true) {     
                                for(var j=0;j<$scope.allOemAdminsId.length;j++){               
                                    for(var i=0;i<result.data.length;i++){
                                        console.log('result.data[i].customerId, $scope.allOemAdminsId[j]');
                                        console.log(result.data[i].customerId, $scope.allOemAdminsId[j]);
                                        if(result.data[i].customerId == $scope.allOemAdminsId[j]){
                                            for(var j=0;j<$scope.tireModels.length;j++){
                                                if(result.data[i].tireModelId===$scope.tireModels[j].value){
                                                    result.data[i].tireModelNo = $scope.tireModels[j].text;
                                                    break;
                                                }else{
                                                    result.data[i].tireModelNo = "None";
                                                }
                                            }
                                            for(var j=0;j<$scope.tireManufacturers.length;j++){
                                                if(result.data[i].tireManufacturerId===$scope.tireManufacturers[j].value){
                                                    result.data[i].tireManufacturerName = $scope.tireManufacturers[j].text;
                                                    break;
                                                }else{
                                                    result.data[i].tireManufacturerName = "None";
                                                }
                                            }                                    
                                        }
                                        
                                    }
                                    $scope.tireData.push(result.data[i]);
                                }
                                console.log("tireDetails", $scope.tireData) ;
                                myGrid();
                            } else  {
                                toastr.error(result.message);
                                vm.error = result.message;                    
                            }
                        })     
                    }else{
                        toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                })                
            } else {
                tireService.GetAllTireData(function (result) {
                    console.log(result);
                    if (result.success === true) {                    
                        for(var i=0;i<result.data.length;i++){                            
                            for(var j=0;j<$scope.tireModels.length;j++){
                                if(result.data[i].tireModelId===$scope.tireModels[j].value){
                                    result.data[i].tireModelNo = $scope.tireModels[j].text;
                                    break;
                                }else{
                                    result.data[i].tireModelNo = "None";
                                }
                            }
                            for(var j=0;j<$scope.tireManufacturers.length;j++){
                                if(result.data[i].tireManufacturerId===$scope.tireManufacturers[j].value){
                                    result.data[i].tireManufacturerName = $scope.tireManufacturers[j].text;
                                    break;
                                }else{
                                    result.data[i].tireManufacturerName = "None";
                                }
                            }
                            $scope.tireData.push(result.data[i]);                               
                        }
                        console.log("tireDetails", $scope.tireData) ;
                        //myGrid();
                    } else {
                        $scope.tireData=[];
                        toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                    myGrid();
                })
            }
        }
        $scope.callspecData= function(){
            var selectedText = $("#tireStatusName option:selected").text();
            //var tiremodelNo =JSON.stringify($("#tireModelNo option:selected").text());
            var tiremodelNo =$("#tireModelNo").val();
            var tireManufcaturerId=$("#tireManufacturerId").val();
            GetTireSpecificationbyManufacurerandModeldata(tiremodelNo,tireManufcaturerId);
            if(selectedText=='New' && $scope.specDataTrue==true){
                $(".disableIfNew").prop('disabled', true);
                GetTireSpecificationbyManufacurerandModeldata(tiremodelNo,tireManufcaturerId);
            }else{
                $(".disableIfNew").prop('disabled', false);
            }
        }
        $scope.addNewModel= function(){
            alert("test here");  
        }
        function validateFeilds(){
            console.log("inside validate feilds");
            $("#remouldCount #expectedTireLife_km #recommendedcoldInflationPressure").keypress(function (e) {     
                var maxlengthNumber = parseInt($('#remouldCount #expectedTireLife_km #recommendedcoldInflationPressure').attr('maxlength'));
                var inputValueLength = $('#remouldCount #expectedTireLife_km #recommendedcoldInflationPressure').val().length + 1;
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {                   
                          return false;
               }
               if(maxlengthNumber < inputValueLength) {
                   return false;
               }
              });
              
        }
        $scope.specDataTrue= false;
        function GetTireSpecificationbyManufacurerandModeldata(tiremodelNo,tireManufcaturerId){
            tireService.GetTireSpecificationbyTireManufacurerandModel(tireManufcaturerId,tiremodelNo, function (result) {
                animation.myFunc('tireDiv');
                if (result.success === true) {
                    $scope.specDataTrue= true;
                    //getAllDropdowns();
                    console.log('result.data result.data result.data');
                    console.log(result.data);
                    //toastr.success(result.message);
                    //$scope.clearData();
                    $("#tireThreadDepth_t1").val(result.data.tireThreadDepth_t1);
                    $("#tireThreadDepth_t2").val(result.data.tireThreadDepth_t2);
                    $("#tireThreadDepth_t3").val(result.data.tireThreadDepth_t3);
                    $("#tireWearIndicator_twi").val(result.data.tireWearIndicator_twi);
                    $("#maxPressureThreshold").val(result.data.maxPressureThreshold);
                    $("#minPressureThreshold").val(result.data.minpressureThreshold);
                    $("#maxTempratureThreshold").val(result.data.maxTemperatureThreshold);
                    $("#tireCost_inr").val(result.data.tireCost);
                    $("#tireRemouldCost").val(result.data.tireRemouldCost);
                    $("#expectedTireLife_km").val(result.data.expectedTireLife_km);
                    $("#recommendedcoldInflationPressure").val(result.data.recommendedcoldInflationPressure);
                    $("#remouldCount").val(result.data.remouldCount);
                    $("#recommendedPositionName").val(result.data.recommendedPositionId); //dropdown
                    $("#tireTypeName").val(result.data.tireTypeId); 
                    $("#tireDiv").waitMe('hide');
                } else {
                    toastr.error('No Specifications found for Model and Manufacturer'); 
                    $("#tireDiv").waitMe('hide');
                }
            })
        }

        //getting dropdown values
        //to get all tire manufacurer details.
        function getAllTireManufacturerDetails() {
            $scope.tireManufacturers = [];            
            tireService.GetallTireManufacture(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireManufacturers.push({value: result.data[i]._id, text: result.data[i].tireManufacturerName});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };

        function getAllTagDetails() {
            $scope.allTagId = [];
            tagService.GetAllTagMaster(function (result) {
                ////console.log("tag", result);
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.allTagId.push({value: result.data[i]._id, text: result.data[i].tagId});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };

        function getAllTireStatus () {
            $scope.allTireStatus = [];
            tireService.GetallTireStatus ( function (result) {
                ////console.log("status", result);
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.allTireStatus.push({value: result.data[i]._id, text: result.data[i].tireStatusName});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            })
        }

        function getAllDepot() {
            $scope.allDepot = [];
            if(vm.curruserData.otherData.roleName=="Client Manager"){
                depotService.GetAlldepotByCustomer( function(result) {
                    ////console.log("depot", result);
                    if (result.success === true) {
                        for(var i=0;i<result.data.length;i++){
                            if(vm.curruserData.otherData.depotId==result.data[i]._id){
                                $scope.allDepot.push({value: result.data[i]._id, text: result.data[i].depotName});   
                            }                                 
                        }
                    } else {
                        toastr.error(result.message);
                        //console.log(result);       
                    }
                })
            }else{
                depotService.GetAlldepotByCustomer( function(result) {
                    ////console.log("depot", result);
                    if (result.success === true) {
                        for(var i=0;i<result.data.length;i++){
                            $scope.allDepot.push({value: result.data[i]._id, text: result.data[i].depotName});    
                        }
                    } else {
                        toastr.error(result.message);
                        //console.log(result);       
                    }
                })
            }
        }

        function getAllTireModelDetails() {
            $scope.tireModels = [];            
            tireService.GetallTireModel(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireModels.push({value: result.data[i]._id, text: result.data[i].tireModelNo, tireManufacturerId : result.data[i].tireManufacturerId});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };
        function getAllTireRecommendedPositionDetails() { 
            $scope.tireRecommendedPositions = [];           
            tireService.GetallTireRecommendedPosition(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireRecommendedPositions.push({value: result.data[i]._id, text: result.data[i].tireRecommendedPositionName});    
                    }
                } else {
                    toastr.error(result.message);
                    ////console.log(result);       
                }
            });
        };
        function getAllTireType() { 
            $scope.tiretypes= [];           
            tireService.GetAllTireType(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.tiretypes.push({value: result.data[i]._id, text: result.data[i].tireTypeName});    
                    }
                } else {
                    toastr.error(result.message);
                    ////console.log(result);       
                }
            });
        };
        //end of dropdown values

        function deleteTireDetails(id) {            
            tireService.DeleteTireDetails(id,function (result) {
                if (result.success === true) {
                    $scope.tireData=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.clearData();
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };

        function updateTireSpecDetails(data) {            
            animation.myFunc('tireDiv');
            console.log("in update function",data)
            tireService.UpdateTireDetails(data,function (result) {
                if (result.success === true) {
                    $scope.vccUser = {};
                    $scope.tireData=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.isValidate=true;
                    vm.modalInstance.close();
                    $('#tireDiv').waitMe('hide');
                   // $("#jqxwindowTiredet").jqxWindow('close');
                    $scope.clearData();
                } else {
                    $scope.isValidate=false;
                    $('#tireDiv').waitMe('hide');
                    toastr.error(result.message);
                    //console.log(result);   
                }
            })
        }

        $scope.currRowData = function(rowform) {
            console.log(rowform);
            var constVal =30;
            var x3=parseFloat($("#tireThreadDepth_t3").val());
            var x2=parseFloat($("#tireThreadDepth_t2").val());
            var x1=parseFloat($("#tireThreadDepth_t1").val());
            var twival = parseFloat($('#tireWearIndicator_twi').val());
            console.log(x3,x2,x1,twival)
            console.log(x3,x2,x1);
            if(x1 > 30.00 || x1 <= 0){
                alertify.alert("Thread Depth 1 cannot be greater than 30.00 or 0");
                $scope.isValidate = false;
                return;
            }else if(x2 > 30.00 || x2 <= 0){
                alertify.alert("Thread Depth 2 cannot be greater than 30.00 or 0");
                $scope.isValidate = false;
                return;
            }else if(x3 > 30.00 || x3 <= 0 ){
                alertify.alert("Thread Depth 3 cannot be greater than 30.00 or 0");
                $scope.isValidate = false;
                return;
            }else if(twival > 2 | twival <= 0){
                alertify.alert("TWI cannot be greater than 2 or 0");
                $scope.isValidate = false;
                return;
            }else if(parseInt($("#minpressureThreshold").val()) >= parseInt($("#maxPressureThreshold").val())){
                alertify.alert("Minimum pressure Value should be less than the Max Pressure");
                $scope.isValidate = false;
                return;
            }
            //for validation if data is blank or not.
            $scope.isValidate= false;
            var count=0;
            if( rowform.tireSerialNumber == "" || rowform.tireSerialNumber == undefined ){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return false; 
            }
            else if(rowform) {
                $(".data_field").each(function() {
                    ////console.log($(this));
                    var dataValue= $(this).val(); 
                    ////console.log(dataValue);
                    count++;
                    if(dataValue == "" || dataValue == undefined){
                        toastr.error("Please Fill valid data in required fields");
                        $scope.isValidate= false;
                        return false;
                    }
                })
            }

            var data = {};
            if(count>=20){
                //$scope.isValidate= true;
                if(rowform._id == "" || rowform._id == undefined){
                    data={
                        // $("#tireWearIndicator_twi").val(result.data.tireWearIndicator_twi);
                        // $("#maxPressureThreshold").val(result.data.maxPressureThreshold);
                        // $("#minPressureThreshold").val(result.data.minpressureThreshold);
                        // $("#maxTempratureThreshold").val(result.data.maxTemperatureThreshold);
                        // $("#tireCost_inr").val(result.data.tireCost);
                        // $("#tireRemouldCost").val(result.data.tireRemouldCost);
                        // $("#expectedTireLife_km").val(result.data.expectedTireLife_km);
                        // $("#recommendedcoldInflationPressure").val(result.data.recommendedcoldInflationPressure);
                        // $("#remouldCount").val(result.data.remouldCount);
                        // $("#recommendedPositionName").val(result.data.recommendedPositionId); //dropdown
                        // $("#tireTypeName").val(result.data.tireTypeId); 
                        tireManufacturerId:rowform.tireManufacturerId, //edit
                        tagId:rowform.tagId,//$('#tagId :selected').text(),
                        tireSerialNumber:rowform.tireSerialNumber, //edit
                        isTireModelNew:'false',
                        tireModelNo:$('#tireModelNo :selected').text(), //edit
                        tireThreadDepth_t1: $("#tireThreadDepth_t1").val(),
                        tireThreadDepth_t2: $("#tireThreadDepth_t2").val(),
                        tireThreadDepth_t3: $("#tireThreadDepth_t3").val(),
                        tireWearIndicator_twi:$("#tireWearIndicator_twi").val(),
                        maxPressureThreshold:$("#maxPressureThreshold").val(),
                        minPressureThreshold:$("#minPressureThreshold").val(),
                        maxTempratureThreshold:$("#maxTempratureThreshold").val(),
                        tireCost_inr:$("#tireCost_inr").val(),
                        tireRemouldCost:$("#tireRemouldCost").val(),
                        expectedTireLife_km:$("#expectedTireLife_km").val(),
                        recommendedcoldInflationPressure:$("#recommendedcoldInflationPressure").val(),
                        remouldCount:$("#remouldCount").val(),
                        recommendedPositionName:$('#recommendedPositionName :selected').text(),
                        cusotmerAssignedTagId:"",
                        tireStatusName:$('#tireStatusName :selected').text(),//rowform.tireStatusName,
                        tireTypeName:$('#tireTypeName :selected').text(),//rowform.tireTypeName,
                        kmTravelled:rowform.kmTravelled, //edit
                        depotId:rowform.depotId, //edit
                    }
                    $scope.specdata2={
                        tireManufacturerName:$("#tireManufacturerId option:selected").text(),
                        tireModelNo:$("#tireModelNo option:selected").text(),
                        tireThreadDepth_t1:$("#tireThreadDepth_t1").val(),
                        tireThreadDepth_t2:$("#tireThreadDepth_t2").val(),
                        tireThreadDepth_t3:$("#tireThreadDepth_t3").val(),
                        tireWearIndicator_twi:$("#tireWearIndicator_twi").val(),
                        maxPressureThreshold:$("#maxPressureThreshold").val(),
                        minpressureThreshold:$("#minPressureThreshold").val(),
                        maxTemperatureThreshold:$("#maxTempratureThreshold").val(),
                        tireCost:$("#tireCost_inr").val(),
                        tireRemouldCost:$("#tireRemouldCost").val(),
                        expectedTireLife_km:$("#expectedTireLife_km").val(),
                        recommendedcoldInflationPressure:$("#recommendedcoldInflationPressure").val(),
                        remouldCount:$("#remouldCount").val(),
                        recommendedPositionName:$('#recommendedPositionName :selected').text(),
                        tireTypeName:$('#tireTypeName :selected').text() 
                    }
                    console.log("add data", data);
                    addTireDetails(data);
                    //$scope.isValidate= true;
                } else {
                    data={
                        _id:rowform._id,
                        tireManufacturerId:rowform.tireManufacturerId,
                        tagId:rowform.tagId,
                        tireSerialNumber:rowform.tireSerialNumber,
                        isTireModelNew:'false',
                        tireModelNo:rowform.tireModelNo, //edit
                        tireThreadDepth_t1: $("#tireThreadDepth_t1").val(),
                        tireThreadDepth_t2: $("#tireThreadDepth_t2").val(),
                        tireThreadDepth_t3: $("#tireThreadDepth_t3").val(),
                        tireWearIndicator_twi:$("#tireWearIndicator_twi").val(),                        
                        maxPressureThreshold:$("#maxPressureThreshold").val(),
                        minPressureThreshold:$("#minPressureThreshold").val(),
                        maxTempratureThreshold:$("#maxTempratureThreshold").val(),
                        tireCost_inr:$("#tireCost_inr").val(),
                        tireRemouldCost:$("#tireRemouldCost").val(),
                        expectedTireLife_km:$("#expectedTireLife_km").val(),
                        recommendedcoldInflationPressure:$("#recommendedcoldInflationPressure").val(),
                        remouldCount:$("#remouldCount").val(),
                        recommendedPositionName:$('#recommendedPositionName :selected').text(),
                        cusotmerAssignedTagId:"",
                        tireStatusName:$('#tireStatusName :selected').text(),//rowform.tireStatusName,
                        tireTypeId:$('#tireTypeName :selected').val(),
                        tireStatusId:$('#tireStatusName :selected').val(),
                        tireTypeName:$('#tireTypeName :selected').text(),//rowform.tireTypeName,
                        kmTravelled:rowform.kmTravelled,
                        depotId:rowform.depotId,
                    }
                    console.log("data", data);
                    updateTireSpecDetails(data)
                }
            } 
        }

        // For Loading the Grid Widgets
        function myGrid(){ 
        var source =
        {
            localdata: $scope.tireData,
            datatype: "array",
            datafields:
            [   
                { name: '_id', type: 'string' },
                { name: 'tireManufacturerId', type: 'string' },
                {name:'tireManufacturerName', type: 'string'},
                { name: 'tagId', type: 'string' },
                { name: 'tireSerialNumber', type: 'string' },
                { name: 'isTireModelNew', type: 'string' },
                { name: 'tireModelId', type: 'string' },
                { name: 'tireModelNo', type: 'string' },
                { name: 'TireThreadDepthDetails.tireThreadDepth_t1', type: 'number' },
                { name: 'TireThreadDepthDetails.tireThreadDepth_t2', type: 'number' },
                { name: 'TireThreadDepthDetails.tireThreadDepth_t3', type: 'number' },
                { name: 'tireWearIndicator_twi', type: 'number' },
                { name: 'maxPressureThreshold', type: 'number' },
                { name: 'minPressureThreshold', type: 'number' },
                { name: 'maxTempratureThreshold', type: 'number' },
                { name: 'tireCost_inr', type: 'number' },
                { name: 'tireRemouldCost', type: 'number' },
                { name: 'expectedTireLife_km', type: 'number' },
                { name: 'recommendedcoldInflationPressure', type: 'number' },
                { name: 'remouldCount', type: 'number' },
                { name: 'recommendedPositionName', type: 'string' },
                { name: 'cusotmerAssignedTagId', type: 'string' },
                { name: 'tireStatusName', type: 'string' },
                { name: 'tireTypeName', type: 'string' },
                { name: 'kmTravelled', type: 'number' },
                { name: 'depotId', type: 'string' },
                { name: 'customerId', type: 'string' }
            ]
        };

        var dataAdapter = new $.jqx.dataAdapter(source);         
        $("#tireDetailsgrid").jqxGrid(
        {
            width: '100%',
            theme: 'dark',
            source: dataAdapter,
            columnsresize: false,
            sortable: true,
            pageable: true,
            pagesize:5,
            autoheight: true,
            columnsheight: 60,
            rowsheight: 50,
            columns: [
                {
                    text: 'Sr No',sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                    cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                    }
                },
                { text: 'Tire Serial Number', datafield: 'tireSerialNumber',hidden:false,align: 'center',cellsalign: 'center', width: '20%' },
                { text: 'Km Travelled', datafield: 'kmTravelled',cellsalign: 'center', width: '20%' },
                { text: 'tiremodelId', datafield: 'tireModelId',hidden:true,align: 'center',cellsalign: 'center', width: '15%' },
                { text: 'Tire Model No', datafield: 'tireModelNo',align: 'center',cellsalign: 'center', width: '20%' },
                { text: 'tireManufacturerId', datafield: 'tireManufacturerId',hidden:true,align: 'center', width: '5%' },
                { text: 'Tire Manufacturer Name', datafield: 'tireManufacturerName',align: 'center',cellsalign: 'center', width: '20%' },
                { text: 'Tire ThreadDepth t1', datafield: 'tireThreadDepth_t1',hidden:true,align: 'center',cellsalign: 'center', width: '5%' },
                { text: 'TireThread Depth t2', datafield: 'tireThreadDepth_t2',hidden:true,align: 'center', width: '5%' },
                { text: 'Tire Thread Depth t3', datafield: 'tireThreadDepth_t3',hidden:true,align: 'center',cellsalign: 'center', width: '5%' },
                { text: 'Tire Wear Indicator twi', datafield: 'tireWearIndicator_twi',hidden:true,align: 'center',cellsalign: 'center', width: '5%' },
                { text: 'Max Pressure Threshold', datafield: 'maxPressureThreshold',hidden:true,align: 'center', width: '5%' },
                { text: 'Min pressure Threshold', datafield: 'minPressureThreshold',hidden:true,align: 'center', width: '5%' },
                { text: 'Max Temperature Threshold', datafield: 'maxTempratureThreshold',hidden:true,align: 'center', width: '5%' },
                { text: 'Tire Cost', datafield: 'tireCost_inr',hidden:true,align: 'center', width: '5%' },
                { text: 'Tire Remould Cost', datafield: 'tireRemouldCost',hidden:true,align: 'center', width: '5%' },
                { text: 'expectedTireLife_km', datafield: 'expectedTireLife_km',hidden:true,align: 'center', width: '5%' },
                { text: 'recommendedcoldInflationPressure', datafield: 'recommendedcoldInflationPressure',hidden:true,align: 'center', width: '5%' },
                { text: 'TireRecommendedPositionName', datafield: 'recommendedPositionName',hidden:true,align: 'center', width: '15%' },
                {
                    text: 'Action', cellsAlign: 'center', align: "center",width: '15%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    // render custom column.
                    if(vm.curruserData.otherData.roleName=="System Super Admin" || vm.curruserData.otherData.roleName=="System Admin"){
                        return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    }else{
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    }
                    
                    }
                }
            ]
        });
        buttonsEvents();

        }
        
        // Initlize Button Event
        $("#tireDetailsgrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });

        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
        // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#tireDetailsgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#tireDetailsgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            var tireSerialNumber = selectedRowData.tireSerialNumber;
            //console.log("tireSerialNumber", tireSerialNumber);
            $scope.openTireDetailModal();
            tireService.GetTireDetails(tireSerialNumber,function(result){
                animation.myFunc('tireDiv');
                console.log("result",result.data);
                if (result.success === true) {
                    $scope.disabled = true;                   
                    
                    var TireModelno = JSON.stringify(result.data[result.data.length-1].tireModelId);
                    console.log("#tireModelNo====================",TireModelno);   

                    // setTimeout(function(){
                    //     $('#tireModelNo').val(TireModelno); 
                    // }, 3000);
                             
                    $scope.vccUser = {
                        _id : result.data[result.data.length-1]._id,
                        tireManufacturerId: result.data[result.data.length-1].tireManufacturerId,
                        tireSerialNumber: result.data[result.data.length-1].tireSerialNumber,
                        tireModelNo: result.data[result.data.length-1].tireModelId,
                        tireTypeName: result.data[result.data.length-1].TireLifeDetails.tireTypeId,
                        tireStatusName: result.data[result.data.length-1].TireLifeDetails.tireStatusId,
                        tireThreadDepth_t1: result.data[result.data.length-1].TireThreadDepthDetails.tireThreadDepth_t1,
                        tireThreadDepth_t2: result.data[result.data.length-1].TireThreadDepthDetails.tireThreadDepth_t2,
                        tireThreadDepth_t3: result.data[result.data.length-1].TireThreadDepthDetails.tireThreadDepth_t3,
                        tireWearIndicator_twi: result.data[result.data.length-1].TireLifeDetails.tireWearIndicator,
                        maxPressureThreshold: result.data[result.data.length-1].TireExtraDetails.maxPressureThreshold,
                        minPressureThreshold: result.data[result.data.length-1].TireExtraDetails.minpressureThreshold,
                        maxTempratureThreshold: result.data[result.data.length-1].TireExtraDetails.maxTemperatureThreshold,
                        tireCost_inr: result.data[result.data.length-1].tireCost_inr,
                        tireRemouldCost: result.data[result.data.length-1].TireLifeDetails.tireRemouldCost,
                        expectedTireLife_km: result.data[result.data.length-1].TireLifeDetails.expectedTireLife_km,
                        recommendedcoldInflationPressure: result.data[result.data.length-1].TireExtraDetails.recommendedcoldInflationPressure,
                        remouldCount: result.data[result.data.length-1].TireLifeDetails.remoldCount,
                        recommendedPositionName: result.data[result.data.length-1].TireExtraDetails.recommendedPositionId,
                        kmTravelled: result.data[result.data.length-1].kmTravelled,
                        depotId: result.data[result.data.length-1].depotId
                    }
                    
                    $('#tireDiv').waitMe('hide');
                }else{
                    $('#tireDiv').waitMe('hide');
                }
            });
            
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#tireDetailsgrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#tireDetailsgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                console.log(selectedRowData );
                deleteTireDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
            
        }
        $scope.clearData = function(){ //window.location.reload();
            $scope.disabled = false;
            $scope.vccUser = {};
            $("#tireDetailsgrid").jqxGrid('clearselection');
        }

        $scope.setTwoNumberDecimal=function (event) {
            this.value = parseFloat(this.value).toFixed(2);
        }
    }
  
})();
  
  