(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TireModelCtrl', TireModelCtrl);
  
    /** @ngInject */
    function TireModelCtrl($uibModal,$scope,tireService,$filter, editableOptions, editableThemes,toastr) {
        var vm = this;

        $scope.openTiresModelModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'tiresModelPopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();    
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };

        vm.getAllTireModelDetails = getAllTireModelDetails;
        vm.addTireModel = addTireModel;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            tireManufacturerId:"",
            tireModelNo:"",
            tireModelDescription:""
       }
        initController();
        $scope.tireModels = [];
        var data = [];
        $scope.tireManufacturers = [];
        $scope.currTireManufacturerId="0";

        function initController() {
            getAllTireManufacturerDetails(); 
        }

        function addTireModel() {            
            tireService.AddTireModel(vm.tireManufacturerId, vm.tireModelNo, vm.tireModelDescription, function (result) {
                if (result.success === true) {
                    //console.log(result);
                    vm.tireManufacturerId= "";
                    vm.tireModelNo= "";
                    vm.tireModelDescription= "";
                    $scope.tireModels= [];
                    data=[];
                    getAllTireModelDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };

        function getAllTireModelDetails() {            
            tireService.GetallTireModel(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        //$scope.tireModels.push(result.data[i]);   
                        for(var j=0;j<$scope.tireManufacturers.length;j++){                        
                            if(result.data[i].tireManufacturerId==$scope.tireManufacturers[j].value){
                                result.data[i].tireManufacturerName= $scope.tireManufacturers[j].text
                            }
                        } 
                        $scope.tireModels.push(result.data[i]);    
                        data.push(result.data[i]);
                    } 
                    myGrid();
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };
        

        function deleteTireModelDetails(_id) {     
              // confirm dialog
              alertify.confirm("Are you sure want to delete", function () {
                // user clicked "ok"
                tireService.DeleteTireModel(_id,function (result) {
                    if (result.success === true) {
                        $scope.tireModels=[];
                        data=[];
                        getAllTireModelDetails();
                        toastr.success(result.message);
                    } else {
                        toastr.error(result.message); 
                        vm.error = result.message;                    
                    }
                });
                }, function() {
                // user clicked "cancel"
                });             
        };


        function updateTireModelDetails() {            
            tireService.UpdateTireModel(vm._id, vm.tireManufacturerId, vm.tireModelNo, vm.tireModelDescription,function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.tireManufacturerId= "";
                    vm.tireModelNo= "";
                    vm.tireModelDescription= "";
                    $scope.tireModels=[];
                    data=[];
                    getAllTireModelDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            //console.log(rowform);
            //for validation if data is blank or not.
            $scope.isValidate= false;
            if(rowform==undefined || (rowform.tireManufacturerId=="" || rowform.tireManufacturerId==undefined)||(rowform.tireModelNo=="" || rowform.tireModelNo==undefined)){ 
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            } else {
                $scope.isValidate= true;
                if(rowform._id == "" || rowform._id == undefined){
                    vm._id = rowform._id; 
                    vm.tireManufacturerId= rowform.tireManufacturerId;
                    vm.tireModelNo= rowform.tireModelNo;
                    vm.tireModelDescription= rowform.tireModelDescription;  
                    addTireModel();
                    //$scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.tireManufacturerId= rowform.tireManufacturerId;
                    vm.tireModelNo= rowform.tireModelNo;
                    vm.tireModelDescription= rowform.tireModelDescription;
                    updateTireModelDetails();
                    //$scope.clearData();
                    rowform ={};
                }
            }
                           
        }

        //to get all tire manufacurer details.
        function getAllTireManufacturerDetails() {            
            tireService.GetallTireManufacture(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.tireManufacturers.push({value: result.data[i]._id, text: result.data[i].tireManufacturerName});    
                    }         
                    getAllTireModelDetails();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.showManufacturerName = function(user) {
            var selected = [];
            if(user.tireManufacturerId) {
              selected = $filter('filter')($scope.tireManufacturers, {value: user.tireManufacturerId});
            }
            return selected.length ? selected[0].text : 'Not set';
        };
    
       
        // $("#popup").click(function () {
        //     $("#tiremodelgrid").jqxGrid('clearselection');
        //       getselectedrowindexes =null;
        //       selectedRowData =null;
        //       $('#_id').val('');
        //       vm._id ='';
        //       $scope.currTireManufacturerId="0";
        //       $('#tireManufacturerType').val(0);
        //       $("#jqxwindowTireModel").jqxWindow('open');
        // });
        // $("#jqxwindowTireModel").bind('close', function (event){
        //     $scope.clearData();
        // });
        /////////////////////////////////////////////
          function myGrid(){ 
              var source =
              {
                  localdata: data,
                  datatype: "json",
                  datafields:
                  [
                      { name: '_id', type: 'string' },
                      { name: 'tireManufacturerId', type: 'string' },
                      { name: 'tireManufacturerName', type: 'string' },
                      { name: 'tireModelNo', type: 'string' },
                      { name: 'tireModelDescription', type: 'string' },
                  ]
              };
  
              var dataAdapter = new $.jqx.dataAdapter(source);         
              $("#tiremodelgrid").jqxGrid(
              {
                  width: '100%',
                  theme: 'dark',
                  source: dataAdapter,
                  columnsresize: false,
                  //selectionmode: 'checkbox',
                  sortable: true,
                  pageable: true,
                  pagesize:5,
                  autoheight: true,
                  columnsheight: 60,
                  rowsheight: 50,
                  columns: [
                      {
                          text: 'Sr No',sortable: false, filterable: false, editable: false,
                          groupable: false, draggable: false, resizable: false,
                          datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                          cellsrenderer: function (row, column, value) {
                              return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                          }
                      },
                      { text: 'Manufacturer Id', datafield: 'tireManufacturerId',align: 'center', cellsalign: 'center', width: '20%', hidden:true },
                      { text: 'Manufacturer Name', datafield: 'tireManufacturerName',align: 'center', cellsalign: 'center', width: '20%' },
                      { text: 'Model No', datafield: 'tireModelNo',align: 'center', cellsalign: 'center', width: '18%' },
                      { text: 'Model Description', datafield: 'tireModelDescription',align: 'center', cellsalign: 'center', width: '28%' },
                      {
                          text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                          // render custom column.
                          return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button><input disabled type="button" data-row="' + row + '" class="deleteButtons btn btn-danger" value="Delete" /> </div>';
                          }
                      }
                  ]
              });
              // Initlize Button Event
              buttonsEvents();
          }
               
          
              function buttonsEvents() {
                  // For Edit Details
                  var editbuttons = document.getElementsByClassName('editButtons');
                  for (var i = 0; i < editbuttons.length; i+=1) {
                      editbuttons[i].addEventListener('click', editDetails);
                  }
                    // For Delete Details
                    var deletebuttons = document.getElementsByClassName('deleteButtons');
                    for (var i = 0; i < deletebuttons.length; i+=1) {
                      deletebuttons[i].addEventListener('click', deleteDetails);
                    }
               }
  
  
               function editDetails() { 
                  var getselectedrowindexes = $('#tiremodelgrid').jqxGrid('getselectedrowindexes');
                  if (getselectedrowindexes.length > 0)
                  { 
                      var selectedRowData = $('#tiremodelgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                  } 
                  $scope.vccUser = {
                      _id : selectedRowData._id,
                      tireManufacturerId:selectedRowData.tireManufacturerId,
                      tireModelNo:selectedRowData.tireModelNo,
                      tireModelDescription: selectedRowData.tireModelDescription
                  }
                  $scope.currTireManufacturerId = selectedRowData.tireManufacturerId;
                  $scope.openTiresModelModal();
                //   $('#tireManufacturerType').val($scope.currTireManufacturerId);
                //   $('#_id').val(selectedRowData._id);
                //   $('#tireManufacturerId').val(selectedRowData.tireManufacturerId);
                //   $("#tireModelNo").val(selectedRowData.tireModelNo);
                //   $("#tireModelDescription").val(selectedRowData.tireModelDescription);
                  //$("#jqxwindowTireModel").jqxWindow('open'); 
              }  
      
              function deleteDetails(){ 
                  getselectedrowindexes = $('#tiremodelgrid').jqxGrid('getselectedrowindexes');
                  if (getselectedrowindexes.length > 0)
                  { 
                      selectedRowData = $('#tiremodelgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                  } 
                  deleteTireModelDetails(selectedRowData._id);
              }
      
              $("#tiremodelgrid").on("pagechanged", function (event) { 
                  buttonsEvents();
              });
  
          $scope.clearData = function(){ //window.location.reload();
            //   $('#_id').val('');
            //   $('#tireManufacturerId').val('');
            //   $("#tireModelNo").val('');
            //   $("#tireModelDescription").val('');
            $("#tiremodelgrid").jqxGrid('clearselection');
              $scope.currTireManufacturerId="0";
              $scope.vccUser = {
                  _id:"",
                  tireManufacturerId:"",
                  tireModelNo:"",
                  tireModelDescription:""
             }
          }
        //   $("#button_no").click(function () {
        //     $("#jqxwindowTireModel").jqxWindow('close');
        // });
    }
  
})();
  
  