(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TubeQtyAssignmentCtrl', TubeQtyAssignmentCtrl);
  
    /** @ngInject */
    function TubeQtyAssignmentCtrl($scope,tubeService,tagService,toastr, $uibModal) {
        var vm = this;
        vm.getAllTubeQtyAssignment = getAllTubeQtyAssignment;
        vm.addTubeQtyAssignment = addTubeQtyAssignment;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.TubeQtyAssignmentId='';
        $scope.date = new Date();
        initController();
        $scope.tubeQtyAssignment = [];
        vm.datatableData = [];
        $scope.customers = [];
        
        $scope.maxDate = new Date().toDateString();
        //to set todays date in the datepicker
        $scope.todaysDate= new Date().toDateString();

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllCustomerDetails();
            setTimeout(function(){
                getAllTubeQtyAssignment();
            }, 1000);           
        }

        function addTubeQtyAssignment() {            
            tubeService.AddTubeQtyAssignment(vm.assignmentDate, vm.customerId, vm.tubeQty, function (result) {
                if (result.success === true) {
                    vm.assignmentDate= "";
                    vm.customerId= "";
                    vm.tubeQty = "";
                    $scope.tubeQtyAssignment= [];
                    getAllTubeQtyAssignment();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        function getAllTubeQtyAssignment() {            
            tubeService.GetAllTubeQtyAssignment(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        // for(var j=0;j<$scope.tubeTypes.length;j++){
                        //     if(result.data[i].customerId===$scope.tubeTypes[j].value){
                        //         result.data[i].tubeTypeName = $scope.tubeTypes[j].text;
                        //         break;
                        //     }else{
                        //         result.data[i].tubeTypeName = "None";
                        //     }
                        // }
                        for(var j=0;j<$scope.customers.length;j++){                        
                            if(result.data[i].customerId==$scope.customers[j].value){
                                result.data[i].customerName= $scope.customers[j].text;
                                break;
                            } else 
                            result.data[i].customerName = "None";
                        } 
                        $scope.tubeQtyAssignment.push(result.data[i]);    
                    }
                    myGrid();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteTubeQtyAssignment(TubeQtyAssignmentId) {            
            tubeService.DeleteTubeQtyAssignment(TubeQtyAssignmentId,function (result) {
                if (result.success === true) {
                    $scope.tubeQtyAssignment=[];
                    getAllTubeQtyAssignment();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);               
                }
            });
        };

        function updateTubeQtyAssignment() {            
            tubeService.UpdateTubeQtyAssignment(vm.TubeQtyAssignmentId, vm.assignmentDate, vm.customerId, vm.tubeQty, function (result) {
                if (result.success === true) {
                    vm.TubeQtyAssignmentId= "";
                    vm.assignmentDate= "";
                    vm.customerId= "";
                    vm.tubeQty = "";
                    $scope.tubeQtyAssignment=[];
                    getAllTubeQtyAssignment();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);            
                }
            });
        };

        //to get all Customer Details details.
        function getAllCustomerDetails() {      
            $scope.customers=[];      
            tagService.GetAllCustomerDetails(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.customers.length;i++){
                        $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].customerName});    
                    }       
                    //getAllTagType();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            $scope.isValidate= false;
            console.log("rowform", rowform);
            //return ;

            if(rowform== undefined || rowform.assignmentDate==undefined || rowform.assignmentDate == '' ||rowform.customerId==undefined||rowform.customerId=='' || rowform.tubeQty == undefined || rowform.tubeQty == ''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }

            // var checkmanufacturingDate = Date.parse($('#manufacturingDate').val());

            // if (isNaN(checkmanufacturingDate) == true) { 
            //     $scope.isValidate= false;
            //     alertify.alert("Invalid date");
            //     return;
            // }
            
            if(rowform._id != undefined){
                vm.TubeQtyAssignmentId = rowform._id; 
                vm.assignmentDate= rowform.assignmentDate;
                vm.customerId= rowform.customerId;
                vm.tubeQty = rowform.tubeQty;
                //vm.manufacturingDate= $("#manufacturingDate").val();
                updateTubeQtyAssignment()
            }else{
                vm.assignmentDate= rowform.assignmentDate;
                vm.customerId= rowform.customerId;
                vm.tubeQty = rowform.tubeQty;
                //vm.manufacturingDate= $("#manufacturingDate").val();
                addTubeQtyAssignment();
            }               
        }

        //to get all Tube Types details.
        // function getAllTubeTypesDetails() {            
        //     tubeService.GetAllTubeType(function (result) {
        //         if (result.success === true) { 
        //             for(var i=0;i<result.data.length;i++){
        //               $scope.tubeTypes.push({value: result.data[i]._id, text: result.data[i].tubeTypeName});    
        //             } 
        //         } else {
        //             toastr.error(result.message); 
        //             //console.log(result);             
        //         }
        //     });
        // };

        //to get all Tube Manufacturer details.
        // function getAllTubeManufacturerDetails() {            
        //     tubeService.GetAllTubeManufacturer(function (result) {
        //         if (result.success === true) { 
        //             for(var i=0;i<result.data.length;i++){
        //                 $scope.tubeManufacturers.push({value: result.data[i]._id, text: result.data[i].tubeManufacturerName});    
        //             } 
        //         } else {
        //             toastr.error(result.message);
        //             //console.log(result);            
        //         }
        //     });
        // };

        //to get all Tube Size details.
        // function getAllTubeSizeDetails() {            
        //     tubeService.GetAllTubeSize(function (result) {
        //         if (result.success === true) {  
        //         for(var i=0;i<result.data.length;i++){
        //           $scope.tubeSizes.push({value: result.data[i]._id, text: result.data[i].tubeSizeName});    
        //         } 
        //     } else {
        //         toastr.error(result.message); 
        //         //console.log(result);              
        //     }
        //     });
        // }

         // For Loading the Grid Widgets
        function myGrid(){ 
            var source =
            {
                localdata: $scope.tubeQtyAssignment,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'assignmentDate', type: 'string' },
                    { name: 'customerName', type: 'string' },
                    { name: 'tubeQty', type: 'string' }, 
                    { name: 'customerId', type: 'string' }, 
                ]
            };
 
            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#grid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Assignment Date', datafield: 'assignmentDate',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Customer Name', datafield: 'customerName',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Tube Quanitity', datafield: 'tubeQty',align: 'center',cellsalign: 'center', width: '25%' },
                    
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '20%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
            $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }

         function buttonsEvents() 
         {            
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Edit Details
         function editDetails() { 
             //var id = this.getAttribute("data-row"); 
             var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             $scope.todaysDate = selectedRowData.assignmentDate;
             $scope.vccUser = {
                _id:selectedRowData._id,
                assignmentDate:selectedRowData.assignmentDate,
                customerId:selectedRowData.customerId,
                tubeQty: selectedRowData.tubeQty

             };
             vm.assignmentDate= selectedRowData.assignmentDate;
             vm.customerId= selectedRowData.customerId;
             vm.tubeQty = selectedRowData.tubeQty;
             $scope.openModal();
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTubeQtyAssignment(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }     

         $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.todaysDate= new Date().toDateString();
            $scope.vccUser = {}
         }

        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTubeQtyAssignments.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
    }
  
})();
  
  