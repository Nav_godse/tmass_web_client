(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TubeDetailsCtrl', TubeDetailsCtrl);
  
    /** @ngInject */
    function TubeDetailsCtrl($scope,tubeService,toastr, $uibModal) {
        var vm = this;
        vm.getAllTubeMasterDetails = getAllTubeMasterDetails;
        vm.addTubeMaster = addTubeMaster;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.TubeMasterId='';
        $scope.date = new Date();
        initController();
        $scope.tubedetails = [];
        vm.datatableData = [];
        $scope.tubeTypes = [];
        $scope.tubeSizes = [];
        $scope.tubeManufacturers = [];     
        
        $scope.maxDate = new Date().toDateString();
        //to set todays date in the datepicker
        $scope.todaysDate= new Date().toDateString();

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllTubeSizeDetails();
            getAllTubeTypesDetails();
            getAllTubeManufacturerDetails();
            setTimeout(function(){
                getAllTubeMasterDetails();
            }, 1000);
            // setTimeout(() => {
            //     getAllTubeMasterDetails();
            // }, 1000);            
        }

        function addTubeMaster() {            
            tubeService.AddTubeDetails(vm.tubeQr, vm.tubeTypeId, vm.tubeSizeId,
                vm.tubeManufacturerId,vm.manufacturingDate,vm.tubeModel, function (result) {
                if (result.success === true) {
                    vm.tubeQr= "";
                    vm.tubeTypeId= "";
                    vm.tubeSizeId= "";
                    vm.tubeManufacturerId="";
                    vm.manufacturingDate="";
                    vm.tubeModel=""; 
                    $scope.tubedetails= [];
                    getAllTubeMasterDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowTubeDetails").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }

        function getAllTubeMasterDetails() {            
            tubeService.GetAllTubeDetails(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.tubeTypes.length;j++){
                            if(result.data[i].tubeTypeId===$scope.tubeTypes[j].value){
                                result.data[i].tubeTypeName = $scope.tubeTypes[j].text;
                                break;
                            }else{
                                result.data[i].tubeTypeName = "None";
                            }
                        }
                        for(var k=0;k<$scope.tubeManufacturers.length;k++){
                            if(result.data[i].tubeManufacturerId===$scope.tubeManufacturers[k].value){
                                result.data[i].tubeManufacturerName = $scope.tubeManufacturers[k].text;
                                break;
                            }else{
                                result.data[i].tubeManufacturerName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.tubeSizes.length;l++){
                            if(result.data[i].tubeSizeId===$scope.tubeSizes[l].value){
                                result.data[i].tubeSizeName = $scope.tubeSizes[l].text;
                                break;
                            }else{
                                result.data[i].tubeSizeName = "None";
                            } 
                        }
                        $scope.tubedetails.push(result.data[i]);    
                    }
                    myGrid();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteTubeMasterDetails(TubeMasterId) {            
            tubeService.DeleteTubeDetails(TubeMasterId,function (result) {
                if (result.success === true) {
                    $scope.tubedetails=[];
                    getAllTubeMasterDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateTubeMasterDetails() {            
            tubeService.UpdateTubeDetails(vm.TubeMasterId, vm.tubeQr, vm.tubeTypeId, vm.tubeSizeId,
                vm.tubeManufacturerId,vm.manufacturingDate,vm.tubeModel,function (result) {
                if (result.success === true) {
                    vm.TubeMasterId= "";
                    vm.tubeQr= "";
                    vm.tubeTypeId= "";
                    vm.tubeSizeId= "";
                    vm.tubeManufacturerId="";
                    vm.manufacturingDate="";
                    vm.tubeModel="";
                    $scope.tubedetails=[];
                    getAllTubeMasterDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowTubeDetails").jqxWindow('close'); 
                } else {
                    toastr.error(result.message);
                    //console.log(result);              
                }
            });
        };

        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            // Validation 
            $scope.isValidate= false;
            if(rowform== undefined || rowform.tubeQr==undefined || rowform.tubeQr == '' ||rowform.tubeTypeId==undefined||rowform.tubeSizeId==undefined
            ||rowform.tubeManufacturerId==undefined || rowform.tubeModel ==''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }

            // var checkmanufacturingDate = Date.parse($('#manufacturingDate').val());
            // //console.log(checkmanufacturingDate);
            // //check if the date is valid or not.
            // if (isNaN(checkmanufacturingDate) == true) { 
            //     $scope.isValidate= false;
            //     alertify.alert("Invalid date");
            //     return;
            // }
            
            if(rowform._id != undefined){
                vm.TubeMasterId = rowform._id; 
                vm.tubeQr= rowform.tubeQr;
                vm.tubeTypeId= rowform.tubeTypeId;
                vm.tubeSizeId= rowform.tubeSizeId;
                vm.tubeManufacturerId=  rowform.tubeManufacturerId;
                vm.manufacturingDate= rowform.manufacturingDate; //$("#manufacturingDate").val();
                vm.tubeModel= rowform.tubeModel;
                updateTubeMasterDetails()
            }else{
                vm.tubeQr= rowform.tubeQr;
                vm.tubeTypeId= rowform.tubeTypeId;
                vm.tubeSizeId= rowform.tubeSizeId;
                vm.tubeManufacturerId= rowform.tubeManufacturerId;
                vm.manufacturingDate= rowform.manufacturingDate; //$("#manufacturingDate").val();
                vm.tubeModel= rowform.tubeModel;
                addTubeMaster();
            }               
        }

        //to get all Tube Types details.
        function getAllTubeTypesDetails() {            
            tubeService.GetAllTubeType(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                      $scope.tubeTypes.push({value: result.data[i]._id, text: result.data[i].tubeTypeName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            });
        };

        //to get all Tube Manufacturer details.
        function getAllTubeManufacturerDetails() {            
            tubeService.GetAllTubeManufacturer(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.tubeManufacturers.push({value: result.data[i]._id, text: result.data[i].tubeManufacturerName});    
                    } 
                } else {
                    toastr.error(result.message);
                    //console.log(result);            
                }
            });
        };

        //to get all Tube Size details.
        function getAllTubeSizeDetails() {            
            tubeService.GetAllTubeSize(function (result) {
                if (result.success === true) {  
                for(var i=0;i<result.data.length;i++){
                  $scope.tubeSizes.push({value: result.data[i]._id, text: result.data[i].tubeSizeName});    
                } 
            } else {
                toastr.error(result.message); 
                //console.log(result);              
            }
            });
        }
        
         // For Open Popup for Add and Edit
         
        //  $("#popup").click(function () {
        //      $("#grid").jqxGrid('clearselection');
        //      getselectedrowindexes =null;
        //      selectedRowData =null;
        //      $('#_id').val('');
        //      vm._id =''
        //      //$("#jqxwindowTubeDetails").jqxWindow('open');
        //      //$("#jqxwindowTubeDetails").jqxWindow('focus');
        //  });
        //  $('#jqxwindowTubeDetails').on('open', function (event) {
        //     buttonsEvents();
        //  })
        //  //$("#jqxwindowTubeDetails").bind('close', function (event){
        //     e.preventDefault();
        //  });
         // For Loading the Grid Widgets
         function myGrid(){ 
         var source =
         {
             localdata: $scope.tubedetails,
             datatype: "json",
             datafields:
             [
                { name: '_id', type: 'string' },
                { name: 'tubeQr', type: 'string' },
                { name: 'tubeTypeId', type: 'string' },
                { name: 'tubeTypeName', type: 'string' },
                { name: 'tubeSizeId', type: 'string' },
                { name: 'tubeSizeName', type: 'string' },
                { name: 'tubeManufacturerId', type: 'string' },
                { name: 'tubeManufacturerName', type: 'string' },
                { name: 'manufacturingDate', type: 'string' },
                { name: 'tubeModel', type: 'string' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#grid").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Tube Qr', datafield: 'tubeQr',align: 'center',cellsalign: 'center', width: '12%' },
                 { text: 'Tube Type Id',hidden:true, datafield: 'tubeTypeId',align: 'center',cellsalign: 'center', width: '10%' },
                 { text: 'Tube Size Id',hidden:true, datafield: 'tubeSizeId',align: 'center',cellsalign: 'center', width: '10%' },
                 { text: 'Tube Manufacturer Id',hidden:true, datafield: 'tubeManufacturerId',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Tube Type Name', datafield: 'tubeTypeName',align: 'center', width: '12%',cellsalign: 'center' },
                 { text: 'Tube Size Name', datafield: 'tubeSizeName',align: 'center',cellsalign: 'center', width: '12%' },
                 { text: 'Tube Manufacturer Name',datafield: 'tubeManufacturerName',align: 'center',cellsalign: 'center', width: '19%' },
                 { text: 'Manufacturing Date',datafield: 'manufacturingDate',align: 'center',cellsalign: 'center', width: '15%'}, //,cellsformat: 'dd-MMM-yyyy'
                 { text: 'Tube Model', datafield: 'tubeModel',align: 'center',cellsalign: 'center', width: '10%' },
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '15%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Edit Details
         function editDetails() { 
             //var id = this.getAttribute("data-row"); 
             var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             $scope.todaysDate = selectedRowData.manufacturingDate;
             $scope.vccUser = {
                _id:selectedRowData._id,
                tubeQr:selectedRowData.tubeQr,
                tubeTypeId:selectedRowData.tubeTypeId,
                tubeSizeId:selectedRowData.tubeSizeId,
                tubeManufacturerId:selectedRowData.tubeManufacturerId,
                tubeTypeName:selectedRowData.tubeTypeName,
                tubeSizeName:selectedRowData.tubeSizeName,
                tubeManufacturerName:selectedRowData.tubeManufacturerName,
                manufacturingDate:selectedRowData.manufacturingDate,
                tubeModel:selectedRowData.tubeModel
             };
             vm.tubeQr= selectedRowData.tubeQr;
             vm.tubeTypeId= selectedRowData.tubeTypeId;
             vm.tubeSizeId= selectedRowData.tubeSizeId;
             vm.tubeManufacturerId= selectedRowData.tubeManufacturerId;
             vm.tubeTypeName= selectedRowData.tubeTypeName;
             vm.tubeSizeName= selectedRowData.tubeSizeName;
             vm.tubeManufacturerName= selectedRowData.tubeManufacturerName;
             vm.manufacturingDate= selectedRowData.manufacturingDate;
             vm.tubeModel= selectedRowData.tubeModel;
             $scope.openModal();
             //$("#jqxwindowTubeDetails").jqxWindow('open'); 
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTubeMasterDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }     

         $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.todaysDate= new Date().toDateString();
            $scope.vccUser = {}
         }

        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTubeDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
    }
  
})();
  
  