(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TubeSizeCtrl', TubeSizeCtrl);
  
    /** @ngInject */
    function TubeSizeCtrl($scope,tubeService,$uibModal, editableOptions, editableThemes, toastr) {
        var vm = this; 
        vm.getAllTubeSizeDetails = getAllTubeSizeDetails;
        vm.addTubeSize = addTubeSize;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            tubeSizeName:"",
            tubeSizeDesc:""
       }
        initController();
        $scope.tubes = []; 
        var data = [];
        
        function initController() {
            getAllTubeSizeDetails();
        }

        function addTubeSize() {            
            tubeService.AddTubeSize(vm.tubeSizeName, vm.tubeSizeDesc, function (result) {
                if (result.success === true) {
                    //console.log(result);
                    vm.tubeSizeName="";
                    vm.tubeSizeDesc="";
                    $scope.tubes=[];
                    data =[];    
                    $scope.vccUser= {};
                    getAllTubeSizeDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
 

        function getAllTubeSizeDetails() {            
            tubeService.GetAllTubeSize(function (result) {
                if (result.success === true) {
                    //console.log(result.data.length);
                    vm.userTubeSize=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.tubes.push(result.data[i]);
                        data.push(result.data[i]);    
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };


        function deleteTubeSizeDetails(_id) {            
            tubeService.DeleteTubeSize(_id,function (result) {
                if (result.success === true) {  
                    $scope.tubes=[];
                    data=[];
                    getAllTubeSizeDetails();
                    toastr.success(result.message);
                    $scope.clearData();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function updateTubeSizeDetails() {            
            tubeService.UpdateTubeSize(vm._id,vm.tubeSizeName,vm.tubeSizeDesc,function (result) {
                if (result.success === true) {
                    vm.tubeSizeName="";
                    vm.tubeSizeDesc="";
                    vm.userTubeSize=result.data;
                    $scope.tubes=[];
                    data=[];
                    getAllTubeSizeDetails();
                    toastr.success(result.message);
                    $scope.clearData();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            $scope.isValidate= false;
            if(rowform== undefined || rowform.tubeSizeName==undefined || rowform.tubeSizeName==''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }

            if(rowform._id == "" || rowform._id == undefined){
                vm.tubeSizeName=rowform.tubeSizeName;
                vm.tubeSizeDesc= rowform.tubeSizeDesc; 
                addTubeSize();                
            }else{
                vm._id = rowform._id; 
                vm.tubeSizeName= rowform.tubeSizeName; 
                vm.tubeSizeDesc= rowform.tubeSizeDesc;
                updateTubeSizeDetails()
            }                
        }  

         // For Loading the Grid Widgets
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'tubeSizeName', type: 'string' },
                    { name: 'tubeSizeDesc', type: 'string' }
                ]
            };
    
            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#grid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Name', datafield: 'tubeSizeName',align: 'center',cellsalign: 'center', width: '33%' },
                    { text: 'Description', datafield: 'tubeSizeDesc',align: 'center', width: '33%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
        }

        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
        }
        // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id : selectedRowData._id,
                tubeSizeName:selectedRowData.tubeSizeName,
                tubeSizeDesc:selectedRowData.tubeSizeDesc
            }
            $scope.openModal();
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
            // user clicked "ok".
                deleteTubeSizeDetails(selectedRowData._id);
            }, function() {
            // user clicked "cancel"
            });
            
        }
    
        $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
        $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.vccUser = {};
        }
        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTubeSize.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
    }
  
})();
  
  