(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('CustomerTubeTagAllocationCtrl', CustomerTubeTagAllocationCtrl);
  
    /** @ngInject */
    function CustomerTubeTagAllocationCtrl($scope,customerService,tubeService,depotService,toastr, $uibModal) {
        var vm = this;

        $scope.openModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowCustomerTubeTagAllocateDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.addCustTubeTagAllocatesToDepot = addCustTubeTagAllocatesToDepot;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        $scope.vccUser = {}   
        initController();
        $scope.customerTubeTagDetails = [];
        $scope.customers= [];
        $scope.customerTubeTagAllcatestoDepot= [];

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllDepotDetails();
            getAllTubeSizeDetails();
            getAllCustomers();
            setTimeout(function(){
                getAllCustTubeTagAllocatesToDepot();
            }, 1000);          
        }


        //all loops
        //to get all Tag QtyAssignment details.
        function getAllCustomers() {            
            customerService.GetAllCustomers(function (result) {
                $scope.customers=[];
                if (result.success === true) { 
                    for(var i=0;i<result.customers.length;i++){
                      $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].customerName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            })
        }

        function getAllTubeSizeDetails() {
            tubeService.GetAllTubeSize (function (result) {
                console.log(result);
                $scope.tubeSizeDetails = [];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.tubeSizeDetails.push({value: result.data[i]._id, text: result.data[i].tubeSizeName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            });
        }
        //to get all Depot details.
        function getAllDepotDetails() {            
            depotService.GetAlldepotByCustomer(function (result) {
                $scope.customerDepot=[];    
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.customerDepot.push({value: result.data[i]._id, text: result.data[i].depotName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }
        //end all loops
        function getAllCustTubeTagAllocatesToDepot() {            
            tubeService.GetallCustTubeTagAllocatesToDepot(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.tagTypes.length;j++){
                            if(result.data[i].tubeSizeId===$scope.tagTypes[j].value){
                                result.data[i].tubeSizeName = $scope.tagTypes[j].text;
                                break;
                            }else{
                                result.data[i].tubeSizeName = "None";
                            }
                        }
                        for(var k=0;k<$scope.customers.length;k++){
                            if(result.data[i].customerId===$scope.customers[k].value){
                                result.data[i].customerName = $scope.customers[k].text;
                                break;
                            }else{
                                result.data[i].customerName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.customerDepot.length;l++){
                            if(result.data[i].depotId===$scope.customerDepot[l].value){
                                result.data[i].depotName = $scope.customerDepot[l].text;
                                break;
                            }else{
                                result.data[i].depotName = "None";
                            } 
                        }
                        $scope.customerTubeTagDetails.push(result.data[i]);    
                    }
                    mygrid();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteCustTubeTagAllocatesToDepot(_id) {            
            customerService.DeleteCustTubeTagAllocatesToDepot(_id,function (result) {
                if (result.success === true) {
                    $scope.customerTubeTagDetails=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateCustTubeTagAllocatesToDepot(data) {            
            customerService.UpdateCustTubeTagAllocatesToDepot(data,function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.customerId= "";
                    vm.depotId= "";
                    vm.tagQty= "";
                    vm.tubeSizeId= "";
                    $scope.customerTubeTagDetails=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);              
                }
            });
        };

        function addCustTubeTagAllocatesToDepot(data) {
            console.log(data);            
            customerService.AddCustTubeTagAllocatesToDepot(data, function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.customerId= "";
                    vm.depotId= "";
                    vm.tagQty= "";
                    vm.tubeSizeId= "";
                    $scope.vccUser={};
                    $scope.customerTubeTagDetails= [];
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }
        
        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            console.log("rowform",rowform);
            $scope.isValidate = false;
            // if(isNaN(rowform.tagQty))
            //     alert("hi");
            if (rowform == undefined || ( rowform.depotId == undefined || rowform.depotId == "" || rowform.customerId == "" || rowform.customerId == undefined || parseInt(rowform.tagQty) < 0 || rowform.tagQty == null || isNaN(rowform.tagQty) || rowform.tubeSizeId == "" || rowform.tubeSizeId == undefined )){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return ;
            }
            
            if(rowform._id != undefined){
                vm.custTagAssignDetails ={    
                    _id : rowform._id, 
                    customerId: rowform.customerId,
                    depotId: rowform.depotId,
                    tagQty: rowform.tagQty,
                    tubeSizeId: rowform.tubeSizeId
                }
                updateCustTubeTagAllocatesToDepot(vm.custTagAssignDetails);
                $scope.isValidate = true;
            } else {
                vm.custTagAssignDetails ={    
                    customerId: rowform.customerId,
                    depotId: rowform.depotId,
                    tagQty: rowform.tagQty,
                    tubeSizeId: rowform.tubeSizeId
                }
                addCustTubeTagAllocatesToDepot(vm.custTagAssignDetails);
                $scope.isValidate = true;
            }
            
                           
        }
        
         // For Loading the gridcustTubeTagAllocate Widgets
        function mygrid() { 
            var source =
            {
                localdata: $scope.customerTubeTagDetails,
                datatype: "json",
                datafields:
                [
                { name: '_id', type: 'string' },
                { name: 'customerId', type: 'string' },
                { name: 'depotId', type: 'string' },
                { name: 'tagQty', type: 'number' },
                { name: 'tubeSizeId', type: 'string' },
                { name: 'customerName', type: 'string' },
                { name: 'depotName', type: 'string' },
                { name: 'tubeSizeName', type: 'string' },
                ]
            };
 
            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#gridcustTubeTagAllocate").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Customer Name', datafield: 'customerName',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'Tag Qty', datafield: 'tagQty',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Depot Name', datafield: 'depotName',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Tube Size', datafield: 'tubeSizeName',align: 'center',cellsalign: 'center', width: '20%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '20%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
        // Initlize Button Event
            buttonsEvents();
            $("#gridcustTubeTagAllocate").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }

        function buttonsEvents() {            
                // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
                for (var i = 0; i < editbuttons.length; i+=1) {
                    editbuttons[i].addEventListener('click', editDetails);
                }
                // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
                for (var i = 0; i < deletebuttons.length; i+=1) {
                    deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
         // For Edit Details
        function editDetails() { 
            var getselectedrowindexes = $('#gridcustTubeTagAllocate').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            var selectedRowData = $('#gridcustTubeTagAllocate').jqxGrid('getrowdata', getselectedrowindexes[0]);
            $scope.vccUser = {
                _id:selectedRowData._id,
                customerId:selectedRowData.customerId,
                depotId:selectedRowData.depotId,
                tagQty:selectedRowData.tagQty,
                tubeSizeId:selectedRowData.tubeSizeId
            }
            $scope.openModal();
        }  
         // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#gridcustTubeTagAllocate').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
                selectedRowData = $('#gridcustTubeTagAllocate').jqxGrid('getrowdata', getselectedrowindexes[0]);
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteCustTubeTagAllocatesToDepot(selectedRowData._id);
            }, function() {});
        }     

        $scope.clearData = function() {
            $("#gridcustTubeTagAllocate").jqxGrid('clearselection');
            $scope.vccUser = {}            
        }
    }
  
})();
  
  