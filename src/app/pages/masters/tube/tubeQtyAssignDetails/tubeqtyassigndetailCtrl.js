(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TubeQtyAssignDetailsCtrl', TubeQtyAssignDetailsCtrl);
  
    /** @ngInject */
    function TubeQtyAssignDetailsCtrl($scope,tubeService,toastr, $uibModal) {
        var vm = this;
        vm.getAllTubeQtyAssignDetail = getAllTubeQtyAssignDetail;
        vm.addTubeQtyAssignDetail = addTubeQtyAssignDetail;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.TubeQtyAssignmentDetailsId='';
        $scope.date = new Date();
        initController();
        $scope.tubeQtyAssigndetail = [];
        vm.datatableData = [];
        $scope.tubeQtyAssign = [];
        $scope.tubeSizes = [];
        $scope.tubeManufacturers = [];     
        
        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllTubeSizeDetails();
            getAllTubeQtyAssignment();
            getAllTubeManufacturerDetails();
            setTimeout(function(){
                getAllTubeQtyAssignDetail();
            }, 1000);
        }

        function addTubeQtyAssignDetail() {            
            tubeService.AddTubeQtyAssignDetail(vm.tubeQtyAssignmentId, vm.tagQty, vm.tubeSizeId,
                vm.tubeManufacturer, vm.tubeModel, function (result) {
                if (result.success === true) {
                    vm.tubeQtyAssignmentId= "";
                    vm.tagQty= "";
                    vm.tubeSizeId= "";
                    vm.tubeManufacturer="";
                    vm.tubeModel=""; 
                    $scope.tubeQtyAssigndetail= [];
                    getAllTubeQtyAssignDetail();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }

        function getAllTubeQtyAssignDetail() {            
            tubeService.GetAllTubeQtyAssignDetail(function (result) {
                console.log(result);
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.tubeQtyAssign.length;j++){
                            if(result.data[i].tubeQtyAssignmentId===$scope.tubeQtyAssign[j].value){
                                result.data[i].tubeQty = $scope.tubeQtyAssign[j].text;
                                break;
                            }else{
                                result.data[i].tubeQty = "None";
                            }
                        }
                        // for(var k=0;k<$scope.tubeManufacturers.length;k++){
                        //     if(result.data[i].tubeManufacturer===$scope.tubeManufacturers[k].value){
                        //         result.data[i].tubeManufacturerName = $scope.tubeManufacturers[k].text;
                        //         break;
                        //     }else{
                        //         result.data[i].tubeManufacturerName = "None";
                        //     } 
                        // }
                        for(var l=0;l<$scope.tubeSizes.length;l++){
                            if(result.data[i].tubeSizeId===$scope.tubeSizes[l].value){
                                result.data[i].tubeSizeName = $scope.tubeSizes[l].text;
                                break;
                            }else{
                                result.data[i].tubeSizeName = "None";
                            } 
                        }
                        $scope.tubeQtyAssigndetail.push(result.data[i]);    
                    }
                    myGrid();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteTubeQtyAssignDetail(TubeQtyAssignmentDetailsId) {            
            tubeService.DeleteTubeQtyAssignDetail(TubeQtyAssignmentDetailsId,function (result) {
                if (result.success === true) {
                    $scope.tubeQtyAssigndetail=[];
                    getAllTubeQtyAssignDetail();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateTubeQtyAssignDetail() {            
            tubeService.UpdateTubeQtyAssignDetail(vm.TubeQtyAssignmentDetailsId, vm.tubeQtyAssignmentId, vm.tagQty, vm.tubeSizeId,
                vm.tubeManufacturer, vm.tubeModel,function (result) {
                if (result.success === true) {
                    vm.TubeQtyAssignmentDetailsId= "";
                    vm.tubeQtyAssignmentId= "";
                    vm.tagQty= "";
                    vm.tubeSizeId= "";
                    vm.tubeManufacturer="";
                    vm.tubeModel="";
                    $scope.tubeQtyAssigndetail=[];
                    getAllTubeQtyAssignDetail();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);              
                }
            });
        };

        $scope.currRowData = function(rowform) {
            console.log(rowform);
            $scope.isValidate= false;
            if(rowform== undefined || rowform.tubeQtyAssignmentId==undefined || rowform.tubeQtyAssignmentId == '' ||rowform.tagQty==undefined||rowform.tubeSizeId==undefined
            ||rowform.tubeManufacturer==undefined || rowform.tubeModel ==''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }
            
            if(rowform._id != undefined){
                vm.TubeQtyAssignmentDetailsId = rowform._id; 
                vm.tubeQtyAssignmentId= rowform.tubeQtyAssignmentId;
                vm.tagQty= rowform.tagQty;
                vm.tubeSizeId= rowform.tubeSizeId;
                vm.tubeManufacturer=  rowform.tubeManufacturer;
                vm.tubeModel= rowform.tubeModel;
                updateTubeQtyAssignDetail()
            }else{
                vm.tubeQtyAssignmentId= rowform.tubeQtyAssignmentId;
                vm.tagQty= rowform.tagQty;
                vm.tubeSizeId= rowform.tubeSizeId;
                vm.tubeManufacturer= rowform.tubeManufacturer;
                //vm.manufacturingDate= $("#manufacturingDate").val();
                vm.tubeModel= rowform.tubeModel;
                addTubeQtyAssignDetail();
            }               
        }

        //to get all Tube Types details.
        function getAllTubeQtyAssignment() {            
            tubeService.GetAllTubeQtyAssignment(function (result) {
                console.log(result);
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                      $scope.tubeQtyAssign.push({value: result.data[i]._id, text: result.data[i].tubeQty});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            });
        };

        //to get all Tube Manufacturer details.
        function getAllTubeManufacturerDetails() {            
            tubeService.GetAllTubeManufacturer(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.tubeManufacturers.push({value: result.data[i]._id, text: result.data[i].tubeManufacturerName});    
                    } 
                } else {
                    toastr.error(result.message);
                    //console.log(result);            
                }
            });
        };

        //to get all Tube Size details.
        function getAllTubeSizeDetails() {            
            tubeService.GetAllTubeSize(function (result) {
                if (result.success === true) {  
                for(var i=0;i<result.data.length;i++){
                  $scope.tubeSizes.push({value: result.data[i]._id, text: result.data[i].tubeSizeName});    
                } 
            } else {
                toastr.error(result.message); 
                //console.log(result);              
            }
            });
        }
        
         // For Loading the Grid Widgets
         function myGrid(){ 
         var source =
         {
             localdata: $scope.tubeQtyAssigndetail,
             datatype: "json",
             datafields:
             [
                { name: '_id', type: 'string' },
                { name: 'tubeQtyAssignmentId', type: 'string' },
                { name: 'tubeQty', type: 'string' },
                { name: 'tagQty', type: 'string' },
                { name: 'tubeSizeId', type: 'string' },
                { name: 'tubeSizeName', type: 'string' },
                { name: 'tubeManufacturer', type: 'string' },
                //{ name: 'tubeManufacturerName', type: 'string' },
                { name: 'tubeModel', type: 'string' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#grid").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Tube Qty Assignment', hidden:true, datafield: 'tubeQtyAssignmentId',align: 'center',cellsalign: 'center', width: '15%' },
                 { text: 'Tube Quantity', datafield: 'tubeQty',align: 'center',cellsalign: 'center', width: '10%' },
                 { text: 'Tube Manufacturer', datafield: 'tubeManufacturer',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Tag Quantity', datafield: 'tagQty',align: 'center',cellsalign: 'center', width: '15%' },
                 { text: 'Tube Size Name', datafield: 'tubeSizeName',align: 'center',cellsalign: 'center', width: '15%' },
                 { text: 'Tube Model', datafield: 'tubeModel',align: 'center',cellsalign: 'center', width: '20%' },
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '15%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Edit Details
         function editDetails() { 
             //var id = this.getAttribute("data-row"); 
             var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 

             $scope.vccUser = {
                _id:selectedRowData._id,
                tubeQtyAssignmentId:selectedRowData.tubeQtyAssignmentId,
                tagQty:selectedRowData.tagQty,
                tubeSizeId:selectedRowData.tubeSizeId,
                tubeManufacturer:selectedRowData.tubeManufacturer,
                tubeSizeName:selectedRowData.tubeSizeName,
                tubeModel:selectedRowData.tubeModel
             };
             vm.tubeQtyAssignmentId= selectedRowData.tubeQtyAssignmentId;
             vm.tagQty= selectedRowData.tagQty;
             vm.tubeSizeId= selectedRowData.tubeSizeId;
             vm.tubeManufacturer= selectedRowData.tubeManufacturer;
             vm.tubeSizeName= selectedRowData.tubeSizeName;
             vm.tubeModel= selectedRowData.tubeModel;
             $scope.openModal();
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTubeQtyAssignDetail(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }     

         $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');

            $scope.vccUser = {}
         }

         $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTubeQtyAssignDetail.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
    }
  
})();
  
  