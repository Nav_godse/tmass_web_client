(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('CustAssignedTubeTagIdDetailCtrl', CustAssignedTubeTagIdDetailCtrl);
  
    /** @ngInject */
    function CustAssignedTubeTagIdDetailCtrl($scope,tubeService,toastr, $uibModal) {
        var vm = this;
        vm.getAllAssignedTubeTagIdDetail = getAllAssignedTubeTagIdDetail;
        vm.addAssignedTubeTagIdDetails = addAssignedTubeTagIdDetails;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.TubeTagAssignedId='';
        $scope.date = new Date();
        initController();
        $scope.depoDetails = [];
        vm.datatableData = [];
        $scope.tubeTypes = [];
        $scope.tubeSizes = [];
        $scope.tubeQtyAssignmentDetail = [];     
        
        $scope.maxDate = new Date().toDateString();
        //to set todays date in the datepicker
        $scope.todaysDate= new Date().toDateString();

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllTubeSizeDetails();
            getAllTubeTypesDetails();
            getAllTubeQtyAssignDetail();
            setTimeout(function(){
                getAllAssignedTubeTagIdDetail();
            }, 1000);         
        }

        function addAssignedTubeTagIdDetails() {            
            tubeService.AddTubeDetails(vm.tubeTagQR, vm.tubeQtyAssignmentDetailsId, vm.depotId, vm.depotAdditionDate, function (result) {
                if (result.success === true) {
                    vm.tubeTagQR= "";
                    vm.tubeQtyAssignmentDetailsId= "";
                    vm.depotId= "";
                    vm.depotAdditionDate="";
                    $scope.depoDetails= [];
                    getAllAssignedTubeTagIdDetail();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        function getAllAssignedTubeTagIdDetail() {            
            tubeService.GetAllTubeDetails(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.tubeTypes.length;j++){
                            if(result.data[i].tubeQtyAssignmentDetailsId===$scope.tubeTypes[j].value){
                                result.data[i].tubeTypeName = $scope.tubeTypes[j].text;
                                break;
                            }else{
                                result.data[i].tubeTypeName = "None";
                            }
                        }
                        for(var k=0;k<$scope.tubeQtyAssignmentDetail.length;k++){
                            if(result.data[i].tubeQtyAssignmentDetailsId===$scope.tubeQtyAssignmentDetail[k].value){
                                result.data[i].tubeQtyAssignmentDetailsName = $scope.tubeQtyAssignmentDetail[k].text;
                                break;
                            }else{
                                result.data[i].tubeQtyAssignmentDetailsName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.tubeSizes.length;l++){
                            if(result.data[i].depotId===$scope.tubeSizes[l].value){
                                result.data[i].tubeSizeName = $scope.tubeSizes[l].text;
                                break;
                            }else{
                                result.data[i].tubeSizeName = "None";
                            } 
                        }
                        $scope.depoDetails.push(result.data[i]);    
                    }
                    myGrid();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteCustAssignedTubeTagIdDetail(TubeTagAssignedId) {            
            tubeService.DeleteCustAssignedTubeTagIdDetail(TubeTagAssignedId,function (result) {
                if (result.success === true) {
                    $scope.depoDetails=[];
                    getAllAssignedTubeTagIdDetail();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateCustAssignedTubeTagIdDetail() {            
            tubeService.UpdateCustAssignedTubeTagIdDetail(vm.TubeTagAssignedId, vm.tubeTagQR, vm.tubeQtyAssignmentDetailsId, vm.depotId,vm.depotAdditionDate,function (result) {
                if (result.success === true) {
                    vm.TubeTagAssignedId= "";
                    vm.tubeTagQR= "";
                    vm.tubeQtyAssignmentDetailsId= "";
                    vm.depotId= "";
                    vm.depotAdditionDate="";
                    $scope.depoDetails=[];
                    getAllAssignedTubeTagIdDetail();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                }
            });
        };

        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            // Validation 
            console.log(rowform);
            
            $scope.isValidate= false;
            if(rowform== undefined || rowform.tubeTagQR==undefined || rowform.tubeTagQR == '' ||rowform.tubeQtyAssignmentDetailsId==undefined||rowform.depotId==undefined
            || rowform.depotAdditionDate ==''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }

            // var checkdepotAdditionDate = Date.parse($('#depotAdditionDate').val());
            // //console.log(checkdepotAdditionDate);
            // //check if the date is valid or not.
            // if (isNaN(checkdepotAdditionDate) == true) { 
            //     $scope.isValidate= false;
            //     alertify.alert("Invalid date");
            //     return;
            // }
            
            if(rowform._id != undefined){
                vm.TubeTagAssignedId = rowform._id; 
                vm.tubeTagQR= rowform.tubeTagQR;
                vm.tubeQtyAssignmentDetailsId= rowform.tubeQtyAssignmentDetailsId;
                vm.depotId= rowform.depotId;
                vm.depotAdditionDate= rowform.depotAdditionDate; //$("#depotAdditionDate").val();
                updateCustAssignedTubeTagIdDetail()
            }else{
                vm.tubeTagQR= rowform.tubeTagQR;
                vm.tubeQtyAssignmentDetailsId= rowform.tubeQtyAssignmentDetailsId;
                vm.depotId= rowform.depotId;
                vm.depotAdditionDate= rowform.depotAdditionDate; //$("#depotAdditionDate").val();
                addAssignedTubeTagIdDetails();
            }               
        }

        //to get all Tube Types details.
        function getAllTubeTypesDetails() {            
            tubeService.GetAllTubeType(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                      $scope.tubeTypes.push({value: result.data[i]._id, text: result.data[i].tubeTypeName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            });
        };

        //to get all Tube Manufacturer details.
        function getAllTubeQtyAssignDetail() {            
            tubeService.GetAllTubeQtyAssignDetail(function (result) {
                if (result.success === true) { 
                    console.log(result);
                    for(var i=0;i<result.data.length;i++){
                        $scope.tubeQtyAssignmentDetail.push({value: result.data[i]._id, text: result.data[i].tubeManufacturerName});    
                    } 
                } else {
                    toastr.error(result.message);
                    //console.log(result);            
                }
            });
        };

        //to get all Tube Size details.
        function getAllTubeSizeDetails() {            
            tubeService.GetAllTubeSize(function (result) {
                if (result.success === true) {  
                for(var i=0;i<result.data.length;i++){
                  $scope.tubeSizes.push({value: result.data[i]._id, text: result.data[i].tubeSizeName});    
                } 
            } else {
                toastr.error(result.message); 
                //console.log(result);              
            }
            });
        }
        
        // For Loading the Grid Widgets
         function myGrid(){ 
         var source =
         {
             localdata: $scope.depoDetails,
             datatype: "json",
             datafields:
             [
                { name: '_id', type: 'string' },
                { name: 'tubeTagQR', type: 'string' },
                { name: 'tubeQtyAssignmentDetailsId', type: 'string' },
                { name: 'tubeQtyAssignmentDetailsName', type: 'string' },                
                { name: 'depotId', type: 'string' },
                { name: 'depotName', type: 'string' },
                { name: 'depotAdditionDate', type: 'string' },
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#grid").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Tube Qr', datafield: 'tubeTagQR',align: 'center',cellsalign: 'center', width: '12%' },
                 { text: 'Tube Type Id',hidden:true, datafield: 'tubeQtyAssignmentDetailsId',align: 'center',cellsalign: 'center', width: '10%' },
                 { text: 'Tube Size Id',hidden:true, datafield: 'depotId',align: 'center',cellsalign: 'center', width: '10%' },
                 { text: 'Tube Size Name', datafield: 'tubeSizeName',align: 'center',cellsalign: 'center', width: '12%' },
                 { text: 'Manufacturing Date',datafield: 'depotAdditionDate',align: 'center',cellsalign: 'center', width: '15%'}, //,cellsformat: 'dd-MMM-yyyy'
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '15%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Edit Details
         function editDetails() { 
             //var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.todaysDate = selectedRowData.depotAdditionDate;
            $scope.vccUser = {
                _id:selectedRowData._id,
                tubeTagQR:selectedRowData.tubeTagQR,
                tubeQtyAssignmentDetailsId:selectedRowData.tubeQtyAssignmentDetailsId,
                depotId:selectedRowData.depotId,
                depotAdditionDate:selectedRowData.depotAdditionDate,
            };
             vm.tubeTagQR= selectedRowData.tubeTagQR;
             vm.tubeQtyAssignmentDetailsId= selectedRowData.tubeQtyAssignmentDetailsId;
             vm.depotId= selectedRowData.depotId;
             vm.depotAdditionDate= selectedRowData.depotAdditionDate;
             $scope.openModal();
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteCustAssignedTubeTagIdDetail(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }     

         $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.todaysDate= new Date().toDateString();
            $scope.vccUser = {}
         }

        $scope.openModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'customerAssignedTubeTagIdDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.done = function (data) {
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
    }
  
})();
  
  