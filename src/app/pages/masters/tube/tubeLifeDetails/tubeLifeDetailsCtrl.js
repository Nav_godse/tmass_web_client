(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TubeLifeDetailsCtrl', TubeLifeDetailsCtrl);
  
    /** @ngInject */
    function TubeLifeDetailsCtrl($scope,tubeService,toastr, $uibModal) {
        var vm = this;
        vm.getAllTubeLifeDetails = getAllTubeLifeDetails;
        vm.addTubeLife = addTubeLife;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.tubeLifeId='';
        $scope.date = new Date();
        initController();
        $scope.tubedetails = [];
        vm.datatableData = [];
        $scope.tubeTypes = [];
        // $scope.tubeSizes = [];
        // $scope.tubeManufacturers = [];     
        
        $scope.maxDate = new Date().toDateString();
        //to set todays date in the datepicker
        $scope.todaysDate= new Date().toDateString();

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            // getAllTubeSizeDetails();
            getAllTubeTypesDetails();
            // getAllTubeManufacturerDetails();
            setTimeout(function(){
                getAllTubeLifeDetails();
            }, 1000);
            // setTimeout(() => {
            //     getAllTubeLifeDetails();
            // }, 1000);            
        }

        function addTubeLife() {            
            tubeService.AddTubeDetails(vm.tubeTypeId, vm.vehicleTubeTagIdAssigned, vm.purchaseDate, function (result) {
                if (result.success === true) {
                    vm.tubeTypeId= "";
                    vm.vehicleTubeTagIdAssigned= "";
                    vm.purchaseDate="";
                    $scope.tubedetails= [];
                    getAllTubeLifeDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        function getAllTubeLifeDetails() {            
            tubeService.GetAllTubeDetails(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.tubeTypes.length;j++){
                            if(result.data[i].tubeTypeId===$scope.tubeTypes[j].value){
                                result.data[i].tubeTypeName = $scope.tubeTypes[j].text;
                                break;
                            }else{
                                result.data[i].tubeTypeName = "None";
                            }
                        }
                        for(var k=0;k<$scope.tubeManufacturers.length;k++){
                            if(result.data[i].tubeManufacturerId===$scope.tubeManufacturers[k].value){
                                result.data[i].tubeManufacturerName = $scope.tubeManufacturers[k].text;
                                break;
                            }else{
                                result.data[i].tubeManufacturerName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.tubeSizes.length;l++){
                            if(result.data[i].vehicleTubeTagIdAssigned===$scope.tubeSizes[l].value){
                                result.data[i].tubeSizeName = $scope.tubeSizes[l].text;
                                break;
                            }else{
                                result.data[i].tubeSizeName = "None";
                            } 
                        }
                        $scope.tubedetails.push(result.data[i]);    
                    }
                    myGrid();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteTubeMasterDetails(tubeLifeId) {            
            tubeService.DeleteTubeDetails(tubeLifeId,function (result) {
                if (result.success === true) {
                    $scope.tubedetails=[];
                    getAllTubeLifeDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateTubeMasterDetails() {            
            tubeService.UpdateTubeDetails(vm.tubeLifeId, vm.tubeTypeId, vm.vehicleTubeTagIdAssigned,
                vm.purchaseDate, function (result) {
                if (result.success === true) {
                    vm.tubeLifeId= "";
                    vm.tubeTypeId= "";
                    vm.vehicleTubeTagIdAssigned= "";
                    vm.purchaseDate="";
                    $scope.tubedetails=[];
                    getAllTubeLifeDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                }
            });
        };

        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            // Validation 
            $scope.isValidate= false;
            if(rowform== undefined || rowform.tubeTypeId==undefined||rowform.vehicleTubeTagIdAssigned==undefined
            ||rowform.purchaseDate==undefined || rowform.purchaseDate ==''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }

            // var checkpurchaseDate = Date.parse($('#purchaseDate').val());
            // //console.log(checkpurchaseDate);
            // //check if the date is valid or not.
            // if (isNaN(checkpurchaseDate) == true) { 
            //     $scope.isValidate= false;
            //     alertify.alert("Invalid date");
            //     return;
            // }
            
            if(rowform._id != undefined){
                vm.tubeLifeId = rowform._id; 
                vm.tubeTypeId= rowform.tubeTypeId;
                vm.vehicleTubeTagIdAssigned= rowform.vehicleTubeTagIdAssigned;
                vm.purchaseDate= rowform.purchaseDate; //$("#purchaseDate").val();
                updateTubeMasterDetails()
            }else{
                vm.tubeTypeId= rowform.tubeTypeId;
                vm.vehicleTubeTagIdAssigned= rowform.vehicleTubeTagIdAssigned;
                vm.purchaseDate= rowform.purchaseDate; //$("#purchaseDate").val();
                addTubeLife();
            }               
        }

        //to get all Tube Types details.
        function getAllTubeTypesDetails() {            
            tubeService.GetAllTubeType(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                      $scope.tubeTypes.push({value: result.data[i]._id, text: result.data[i].tubeTypeName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            });
        };

        //to get all Tube Size details.
        function getAllTubeSizeDetails() {            
            tubeService.GetAllTubeSize(function (result) {
                if (result.success === true) {  
                for(var i=0;i<result.data.length;i++){
                  $scope.tubeSizes.push({value: result.data[i]._id, text: result.data[i].tubeSizeName});    
                } 
            } else {
                toastr.error(result.message); 
                //console.log(result);              
            }
            });
        }
        
         // For Loading the Grid Widgets
         function myGrid(){ 
         var source =
         {
             localdata: $scope.tubedetails,
             datatype: "json",
             datafields:
             [
                { name: '_id', type: 'string' },
                { name: 'tubeTypeId', type: 'string' },
                { name: 'tubeTypeName', type: 'string' },
                { name: 'vehicleTubeTagIdAssigned', type: 'string' },
                { name: 'tubeSizeName', type: 'string' },
                { name: 'purchaseDate', type: 'string' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#grid").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Tube Type Id',hidden:true, datafield: 'tubeTypeId',align: 'center',cellsalign: 'center', width: '10%' },
                 { text: 'Tube Type Name', datafield: 'tubeTypeName',align: 'center', width: '12%',cellsalign: 'center' },
                 { text: 'Manufacturing Date',datafield: 'purchaseDate',align: 'center',cellsalign: 'center', width: '15%'}, //,cellsformat: 'dd-MMM-yyyy'
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '15%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Edit Details
         function editDetails() { 
             //var id = this.getAttribute("data-row"); 
             var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             $scope.todaysDate = selectedRowData.purchaseDate;
             $scope.vccUser = {
                _id:selectedRowData._id,
                tubeTypeId:selectedRowData.tubeTypeId,
                vehicleTubeTagIdAssigned:selectedRowData.vehicleTubeTagIdAssigned,
                tubeTypeName:selectedRowData.tubeTypeName,
                purchaseDate:selectedRowData.purchaseDate,
             };
             vm.tubeTypeId= selectedRowData.tubeTypeId;
             vm.vehicleTubeTagIdAssigned= selectedRowData.vehicleTubeTagIdAssigned;
             vm.tubeTypeName= selectedRowData.tubeTypeName;
             vm.purchaseDate= selectedRowData.purchaseDate;
             $scope.openModal();
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTubeMasterDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }     

         $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.todaysDate= new Date().toDateString();
            $scope.vccUser = {}
         }

        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTubeLifeDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
    }
  
})();
  
  