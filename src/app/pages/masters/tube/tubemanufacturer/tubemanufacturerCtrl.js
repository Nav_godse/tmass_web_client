(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TubeManufacturerCtrl', TubeManufacturerCtrl);
  
    /** @ngInject */
    function TubeManufacturerCtrl($scope,tubeService,$filter, editableOptions, editableThemes, toastr, $uibModal) {
        var vm = this; 
        vm.getAllTubeManufacturerDetails = getAllTubeManufacturerDetails;
        vm.addTubeManufacturer = addTubeManufacturer;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            tubeManufacturerName:""
       }
        initController();
        $scope.tubes = [];
        var data = [];
        function initController() {
            getAllTubeManufacturerDetails();
            // For Open Popup for Add and Edit
            //$("#jqxwindowTubeManctr").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addTubeManufacturer() {            
            tubeService.AddTubeManufacturer(vm.tubeManufacturerName, function (result) {
                if (result.success === true) {
                    vm.tubeManufacturerName=""; 
                    $scope.tubes=[];
                    data=[];
                    getAllTubeManufacturerDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowTubeManctr").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
 
        function getAllTubeManufacturerDetails() {            
            tubeService.GetAllTubeManufacturer(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        data.push(result.data[i]);
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function deleteTubeManufacturerDetails(_id) {            
            tubeService.DeleteTubeManufacturer(_id,function (result) {
                if (result.success === true) {  
                    $scope.tubes=[];
                    data=[];
                    getAllTubeManufacturerDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function updateTubeManufacturerDetails() {            
            tubeService.UpdateTubeManufacturer(vm._id,vm.tubeManufacturerName,function (result) {
                if (result.success === true) {
                    vm.tubeManufacturerName=""; 
                    $scope.vccUser={};
                    $("#tubeManufacturerName").val('');
                    data=[];
                    getAllTubeManufacturerDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowTubeManctr").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            // Validation 
            $scope.isValidate= false;
            if(rowform==undefined ||rowform.tubeManufacturerName=='' || (rowform.tubeManufacturerName==undefined)){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }

            if( rowform== undefined || rowform._id == "" || rowform._id == undefined){
                vm.tubeManufacturerName=rowform.tubeManufacturerName; 
                addTubeManufacturer();                
                $scope.clearData();
                rowform ={};
            }else{
                vm._id = rowform._id; 
                vm.tubeManufacturerName= rowform.tubeManufacturerName;  
                updateTubeManufacturerDetails()
                $scope.clearData();
                rowform ={};
            }                
        }
        
        //  $("#popup").click(function () {
        //      $("#grid").jqxGrid('clearselection');
        //      getselectedrowindexes =null;
        //      selectedRowData =null;
        //      $('#_id').val('');
        //      $scope.vccUser._id ='';
        //      //$("#jqxwindowTubeManctr").jqxWindow('open');
        //  });

         // For Loading the Grid Widgets
         function myGrid(){ 
         var source =
         {
             localdata: data,
             datatype: "json",
             datafields:
             [
                 { name: '_id', type: 'string' },
                 { name: 'tubeManufacturerName', type: 'string' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#grid").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Name', datafield: 'tubeManufacturerName',align: 'center',cellsalign: 'center', width: '66%' },
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
         // Initlize Button Event
         buttonsEvents();
         }
         function buttonsEvents() 
         {
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
         }

         // For Edit Details
         function editDetails() { 
             var id = this.getAttribute("data-row"); 
             var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             $scope.vccUser._id=selectedRowData._id;
             $scope.vccUser.tubeManufacturerName= selectedRowData.tubeManufacturerName;
             vm._id=selectedRowData._id;
             //$("#jqxwindowTubeManctr").jqxWindow('open'); 
             $scope.openModal();
         }  

         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are You Sure You Want To Delete This?", function () {
                // user clicked "ok"
                deleteTubeManufacturerDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }
     
         $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
             buttonsEvents();
         });

        $scope.clearData = function(){ //window.location.reload();

            $scope.vccUser = {};
            $("#grid").jqxGrid('clearselection');
        }
        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowTubeManctr.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
    }
  
})();
  
  