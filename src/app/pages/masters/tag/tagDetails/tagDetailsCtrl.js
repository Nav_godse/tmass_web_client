(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TagDetailsCtrl', TagDetailsCtrl);
  
    /** @ngInject */
    function TagDetailsCtrl($scope,tagService,$filter, editableOptions, editableThemes,toastr, $uibModal,$http,config,localStorage,$log,animation) {
        var vm = this;
        $scope.viewTagTypes = [];
        $scope.currUser = localStorage.getObject('dataUser');
        $scope.currRole = $scope.currUser.otherData.roleName;
        console.log("$scope.currUser");
        console.log($scope.currUser);
        $scope.viewTagsize = 5;
        $scope.viewTagbyPage = 1;
        $scope.currentPage = 1;
        $scope.maxSize = 10;
        $scope.openModal = function () {
            vm.ModalInstance1=$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowTagDetail.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        //$uibModalInstance.close();
                    }                    
                };

                $scope.uploadTagDetails = function() {  
                    run_waitMe_body();
                    var currData = {};
                    var fileInput = document.getElementById('the-file');
                    var file = fileInput.files[0];   
                    if(file===undefined){
                        $('#waitMe_ex3').waitMe('hide'); 
                        return
                    }  
                    var fd = new FormData();
                    var test=0;
                    var test2=0;
                    fd.append('file', file);
                    fd.append('data', 'string');
                    $http.post(config.apiUrl+'/api/upload', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    })
                    .success(function(response){
                        //$('#waitMe_ex3').waitMe('hide'); 
                        var currTagType= '';
                        bulktags=[];
                        if(response.data.length ==0){
                            $('#waitMe_ex3').waitMe('hide'); 
                            alertify.alert("Please check uploaded file for valid data");
                            
                            return
                        }
                        if(response.data){
                            run_waitMe_body();
                            for(var i=0;i<response.data.length;i++){
                                console.log("response data from file");
                                console.log(response.data[i]);
                                if(response.data[i]['tag id']!="" && response.data[i]['tag id']!=null && response.data[i]['manufacturing date']!="" && response.data[i]['manufacturing date']!=null && response.data[i]['tag id']!=null && response.data[i]['tag id']!=""){
                                    test+=1;
                                    //console.log(test);.
                                    console.log(response.data[i]['tag id']);
                                        if(response.data[i]['tag type']== 'TR0001TG0101'){
                                            currTagType= 'ID'
                                        }else if(response.data[i]['tag type']== 'TR0001TG0201'){
                                            currTagType= 'TPMS + ID'
                                        }else if(response.data[i]['tag type']== 'TR0001TB0201'){
                                            currTagType= 'TPMS Tube'
                                        }else if(response.data[i]['tag type']== 'TR0001TP0201'){
                                            currTagType= 'TPMS RIM'
                                        }
                                        currData = {
                                            "tagPacket":0,
                                            "tagId":response.data[i]['tag id'],
                                            "tagType":currTagType,
                                            "manufacturingDate":response.data[i]['manufacturing date'],
                                            "isAssigned":false
                                        };
                                        if(currData!={}){
                                            bulktags.push(currData);
                                        }
                                }else{
                                    test2+=1;
                                    //console.log(test2);
                                }


                                // if(response.data[i]['tag type']!=""){
                                //     if(response.data[i]['tag type']== 'TR0001TG0101'){
                                //         currTagType= 'ID'
                                //     }else if(response.data[i]['tag type']== 'TR0001TG0201'){
                                //         currTagType= 'TPMS + ID'
                                //     }else if(response.data[i]['tag type']== 'TR0001TB0201'){
                                //         currTagType= 'TPMS Tube'
                                //     }else if(response.data[i]['tag type']== 'TR0001TP0201'){
                                //         currTagType= 'TPMS RIM'
                                //     }
                                // }
                                // if(response.data[i]['tag id']!=""){

                                // }
                                // currData = {
                                //     "tagPacket":0,
                                //     "tagId":response.data[i]['tag id'],
                                //     "tagType":currTagType,
                                //     "manufacturingDate":response.data[i]['manufacturing date'],
                                //     "isAssigned":false
                                // };
                                // bulktags.push(currData);
                            }    
                            console.log("data with error ",test2);
                            console.log("data presrent ",test);
                            console.log(bulktags);
                            $scope.fileData= bulktags;
                            //if(response.data.length== bulktags.length)
                                //run_waitMe_body();
                                addBulkTags(bulktags);
                                //$uibModalInstance.close();
                        }
                    })
                    .error(function(response){
                        $('#waitMe_ex4').waitMe('hide'); 
                        console.log(response);
                    });
                }
              
                $scope.cancel = function () {
                    console.log("clicked here 11101010");
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                    //vm.ModalInstance1.close();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        $scope.openModalForviewDetails = function () {
            vm.ModalInstance2=$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowviewDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    
                    $scope.save = function (data) {
                        console.log(data);
                        $scope.currRowData(data);
                        if($scope.isValidate==true){
                            $uibModalInstance.close();
                        }                    
                    };              
                    $scope.viewDetails = function() {  
                        var currData = {};
                        run_waitMe_tagdatabody();
                        //GetallTagbyTagType();
                    }

                    $scope.cancel = function () {
                        console.log("clicked here 0000000000");
                        //vm.ModalInstance2.close();
                        $uibModalInstance.dismiss('cancel');
                        //$scope.clearData();
                    };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.getAllTagMasterDetails = getAllTagMasterDetails;
        vm.addTagDetails = addTagDetails;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.TagDetailsId="";
        $scope.resultTrue=false;
        $scope.date = new Date();
        initController();
        $scope.tagdetails = [];
        vm.datatableData = []; 
        var data = [];
        $scope.tagTypes = []; 

        // if ( $('#manufacturingDate')[0].type != 'date' ) $('#manufacturingDate').datepicker();

        $scope.maxDate = new Date().toDateString();
        //to set todays date in the datepicker
        $scope.todaysDate= new Date().toDateString();

        $scope.vccUser = {
            _id:"",
            tagPacket:"",
            tagId:"",
            tagTypeId:"",
            manufacturingDate:""
        }
        
        function initController() {
            getAllTagTypeDetails();
            setTimeout(function(){
                getAllTagMasterDetails();
            }, 500);
            // $('.input-daterange input').each(function() {
            //     $(this).datepicker('clearDates');
            // });

           // $("#jqxwindowTagDetail").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addTagDetails() {            
            tagService.AddTagMaster(vm.tagPacket, vm.tagId, vm.tagType,
                vm.manufacturingDate, function (result) {
                if (result.success === true) {
                    vm.tagPacket= "";
                    vm.tagId= "";
                    vm.tagType= "";
                    vm.manufacturingDate="";
                    $scope.tagdetails= [];
                    getAllTagMasterDetails();
                    toastr.success(result.message);
                    $scope.isValidate = true;
                    vm.ModalInstance.close();
                    $('#waitMe_ex4').waitMe('hide'); 
                } else {
                    $('#waitMe_ex4').waitMe('hide'); 
                    $scope.isValidate = false;
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
            
        }

        $scope.GetallTagbyTagType = function(viewTagbyPage,viewTagsize){
            //console.log('test1111',viewTagbyPage,viewTagsize);
            run_waitMe_tagdatabody();     
            tagService.GetallTagMasterbyTagTypeId(vm.currTagTypeId,viewTagsize,10, function (result) {
                //run_waitMe_tagdatabody();
                if (result.success === true) {     
                    $scope.resultTrue=true;
                    $('#waitMe_ex4').waitMe('hide');               
                    $scope.viewTagTypes= result.data;
                    console.log($scope.viewTagTypes);
                    $scope.viewTagsize = result.currentQueryResult;
                    $scope.viewTagbyPage = result.pages;
                    $scope.maxSize = 10;
                    $scope.totalItems=parseInt(result.pages);                    
                    $scope.bigTotalItems = parseInt(result.pages);
                    $scope.viewTagbyPage = parseInt(result.pages);
                    console.log($scope.viewTagsize,$scope.viewTagbyPage);
                    console.log("inside the getallmasterstag",$scope.bigTotalItems);
                    //toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;        
                    $('#waitMe_ex4').waitMe('hide');            
                }
            })
        }

        var bulktags = [];
        //wait me js
        function run_waitMe_body(){
            animation.myFunc('waitMe_ex3');
        }
        function run_waitMe_tagdatabody(){
            animation.myFunc('waitMe_ex4');
        }
        //end of wait mejs
        function addBulkTags(data) {
            console.log(data);            
            tagService.AddBulkTagMaster(data,function (result) {
                run_waitMe_body();
                if (result.success === true) {
                    toastr.success(result.message);   
                    getAllTagMasterDetails();
                    $('#waitMe_ex3').waitMe('hide');
                    $scope.clearData();
                    vm.ModalInstance1.close();
                    // $scope.fileData= result.data;
                }else{
                    $('#waitMe_ex3').waitMe('hide');
                    toastr.error(result);                
                }
            })
        }

        function getAllTagMasterDetails() {            
            tagService.GetAllTagCount(function (result) {
                console.log(result);
                data= [];
                $scope.tagdetails=[];
                if (result.success === true) {                    
                    // for(var i=0;i<result.data.length;i++){
                    //     for(var j = 0; j < $scope.tagTypes.length; j++) {
                    //         if(result.data[i].tagTypeId===$scope.tagTypes[j].value){
                    //             result.data[i].tagTypeName = $scope.tagTypes[j].text;
                    //             break;
                    //         }else{
                    //             result.data[i].tagTypeName = "None";
                    //         }
                    //     }
                    //     $scope.tagdetails.push(result.data[i]);
                    //    // data.push(result.data[i]);    
                    // }
                    $scope.tagdetails =result.data ;
                    myGrid();
                    //toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };

        function deleteTagMasterDetails (TagDetailsId) {            
            tagService.DeleteTagMaster(TagDetailsId,function (result) {
                if (result.success === true) {
                    $scope.tagdetails=[];
                    getAllTagMasterDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        function updateTagMasterDetails() {
            console.log(vm.tagType);            
            tagService.UpdatetagMaster(vm.TagDetailsId, vm.tagPacket, vm.tagId, vm.tagType,
                vm.manufacturingDate,function (result) {
                if (result.success === true) {
                    vm.TagDetailsId= "";
                    vm.tagPacket= "";
                    vm.tagId= "";
                    vm.tagType= "";
                    vm.manufacturingDate="";
                    $scope.tagdetails=[];
                    getAllTagMasterDetails();
                    toastr.success(result.message);
                    $scope.clearData();
                    vm.ModalInstance.close()
                    $scope.isValidate = true;
                    //$("#jqxwindowTagDetail").jqxWindow('close');
                } else {
                    toastr.error(result.message);
                    $scope.isValidate = false;
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };


        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            //console.log(rowform);
            $scope.isValidate = false;
            if((rowform.tagPacket=="")||(rowform.tagId=="")||(rowform.tagType=="")
            ||(rowform.manufacturingDate=="")){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate = false;
                return;
            }
            // var checkmanufacturingDate = Date.parse($('#manufacturingDate').val());
            // //console.log(checkmanufacturingDate);
            // //check if the date is valid or not.
            // if (isNaN(checkmanufacturingDate) == true) { 
            //     alertify.alert("Invalid date");
            //     $scope.isValidate = false;
            //     return;
            // } 
            else {
                //$scope.isValidate = true;
                if(rowform._id == undefined || rowform._id == ""){
                    vm.tagPacket= rowform.tagPacket;
                    vm.tagId= rowform.tagId ;
                    vm.tagType= $('#tagTypeId :selected').text();
                    //vm.tagType= rowform.tagTypeId; // $("#tagTypeId option:selected").text() ;
                    vm.manufacturingDate= rowform.manufacturingDate;// $("#manufacturingDate").val();
                    //console.log(vm.manufacturingDate);
                    addTagDetails();
                    $scope.clearData();
                }else{
                    vm.TagDetailsId= rowform._id; 
                    vm.tagPacket= rowform.tagPacket;
                    vm.tagId= rowform.tagId ;
                    vm.tagType= $('#tagTypeId :selected').text();
                   // vm.tagType= $("#tagTypeId option:selected").text() ;
                   vm.manufacturingDate= rowform.manufacturingDate; //$("#manufacturingDate").val();
                    updateTagMasterDetails();
                    $scope.clearData();
                    //console.log(vm);
                }
            }               
        } 

        //to get all Tag Types details.
        function getAllTagTypeDetails() {            
            tagService.GetAllTagType(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.tagTypes.length;i++){
                        $scope.tagTypes.push({value: result.tagTypes[i]._id, text: result.tagTypes[i].TagType});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };      
    
        $scope.showTagType = function(user) {
            var selected = [];
            if(user.tagTypeId) {
              selected = $filter('filter')($scope.tagTypes, {value: user.tagTypeId});
            }
            return selected.length ? selected[0].text : 'Not set';
        }
        
        // For Loading the Grid Widgets
        function myGrid(){ 
        var source =
        {
            localdata: $scope.tagdetails,
            datatype: "json",
            datafields:
            [
                { name: '_id', type: 'string' },
                { name: 'count', type: 'number' },
                { name: 'TagType', type: 'string' }
            ]
        };

        var dataAdapter = new $.jqx.dataAdapter(source);         
        $("#taggrid").jqxGrid(
        {
            width: '100%',
            theme: 'dark',
            source: dataAdapter,
            columnsresize: false,
            sortable: true,
            pageable: true,
            pagesize:5,
            autoheight: true,
            columnsheight: 60,
            rowsheight: 50,
            columns: [
                {
                    text: 'Sr No',sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '10%' ,
                    cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                    }
                },
                { text: 'Tag Type', datafield: 'TagType',align: 'center', cellsalign: 'center', width: '35%' }, 
                { text: 'Total Count', datafield: 'count',align: 'center', cellsalign: 'center', width: '35%' },
                {
                    text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"> View Details </button>';
                    }
                }
            ]
        });
        // Initlize Button Event
        buttonsEvents();
        }
        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
        }
        // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#taggrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#taggrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            console.log(selectedRowData," selectedRowData")
            $scope.selectedTagType = selectedRowData.TagType;
            vm.currTagTypeId = selectedRowData._id;
            $scope.currentPage = 1;
            $scope.GetallTagbyTagType($scope.viewTagbyPage,$scope.currentPage);
            //run_waitMe_tagdatabody();
            //pagination
            // $scope.totalItems = 64;
            //timeoute
            setTimeout(function(){
                if($scope.resultTrue==true)
                    console.log('$scope.bigTotalItems',$scope.bigTotalItems);
                    $scope.openModalForviewDetails();
            }, 500);
          
            // $scope.setPage = function (pageNo) {
            //   $scope.currentPage = 1;
            // };
            $scope.pageChanged = function(tagtype,pageno) {
                //alert(tagtype,pageno)
              $log.log('Page changed to: ' + $scope.currentPage);
              //for pagination page no and other details
              $scope.GetallTagbyTagType(tagtype,pageno);
            };
          
            // $scope.maxSize = 5;

            // $scope.bigCurrentPage = 1;
            //pagination

            
            // vm.ModalInstance2.rendered.then(function(){
            //     //alert('hi');
            //     $scope.clearData()
            //     run_waitMe_tagdatabody();
            // });
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#taggrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#taggrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
           // confirm dialog
           alertify.confirm("Are you sure, you want to delete?", function () {
               // user clicked "ok".
               deleteTagMasterDetails(selectedRowData._id);//deleteTagMasterDetails
           }, function() {
               // user clicked "cancel"
           });
           
        }
    
        $("#taggrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
        
        $scope.clearData = function(){ //window.location.reload();
            //ssetTimeout(function(){
                // $('#_id').val('');
                // $('#tagPacket').val('');
                // $("#tagId").val('');
                // $("#tagTypeId").val('');
                // $("#manufacturingDate").val(''); 
                $("#taggrid").jqxGrid('clearselection');
                $scope.viewTagTypes=[];
                //vm.ModalInstance1.close();
                //vm.ModalInstance2.close();
                //$scope.selectedTagType='';
                // $scope.todaysDate= new Date().toDateString();
                // $scope.vccUser = {
                //     _id:"",
                //     tagPacket:"",
                //     tagId:"",
                //     tagTypeId:"",
                //     manufacturingDate:""
                // }
                $scope.fileData = [];
            //}, 200);
        }
        // $("#button_no").click(function () {
        //     $("#jqxwindowTagDetail").jqxWindow('close');
        // });
    
    }
  
})();
  
