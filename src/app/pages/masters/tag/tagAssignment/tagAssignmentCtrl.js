(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TagAssignmentCtrl', TagAssignmentCtrl);
  
    /** @ngInject */
    function TagAssignmentCtrl($scope,tagService,customerService,toastr, $uibModal,localStorage,animation) {
        var vm = this;
        $scope.tagsdata= false;
        $scope.test= [];
        var currloggedinUser = localStorage.getObject('dataUser');
        $scope.currRole = currloggedinUser.otherData.roleName;
        $scope.assignTags = false;
        $scope.openModal = function () {
            vm.ModalInstance=$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowTagAssign.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data,$scope.test);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.getAllTagQtyAssignment = getAllTagQtyAssignment;
        vm.addTagQtyAssignment = addTagQtyAssignment;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            customerId:"",
            tagTypeId:"",
            tagQty:""
       }
        initController();
        $scope.tagassignments = [];
        var data = [];
        $scope.customers = [];
        $scope.tagtypes=[];
        $scope.currCustomerId="0";
        $scope.currTagTypeId="0";

        function initController() {
            getAllCustomerDetails();
        }

        function addTagQtyAssignment(assignedTagData) {            
            tagService.AddTagQtyAssignment(vm.customerId, vm.tagTypeId, vm.tagQty, function (result) {
                if (result.success === true) { 
                    getAllTagQtyAssignment();
                    console.log("result.data,assignedTagDataresult.data,assignedTagDataresult.data,assignedTagData");
                    console.log(result.data,assignedTagData);
                    addCustomerAssignedTag(result.data,assignedTagData);
                    // $("#jqxwindowTagAssign").jqxWindow('close');
                    vm.customerId= "";
                    vm.tagTypeId= "";
                    vm.tagQty= "";
                    $scope.tagassignments= [];
                    data=[];
                    $scope.isValidate = true;
                    vm.ModalInstance.close();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function getAllTagQtyAssignment() {            
            tagService.GetAllTagQtyAssignment(function (result) {
                console.log(result);
                if (result.success === true) {
                    
                    // if(currloggedinUser.otherData.roleName === 'System Super Admin' || currloggedinUser.otherData.roleName === 'System Admin') {
                    //     $scope.isDisable = false;
                    //     for(var i=0;i<result.customers.length;i++) {
                    //         if(result.customers[i]._id !== currloggedinUser.otherData._id ) // if customer is not same as logged in user
                    //             if(result.customers[i]._id !== currloggedinUser.otherData.createdByCustomerId)  // if customer is not parent
                    //                if(result.customers[i].createdByCustomerId == currloggedinUser.otherData._id )
                    //                     $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].customerName});
                    //     }
                    // } else {
                    //     for(var i=0;i<result.customers.length;i++){
                    //         if(result.customers[i]._id == currloggedinUser.otherData._id ){
                    //             $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].customerName});
                    //             $scope.vccUser.customerId = currloggedinUser.otherData._id;
                    //             $scope.isDisable = true;
                    //         }
                    //     }
                    // }

                    for(var i=0;i<result.data.length;i++){
                        //$scope.tagassignments.push(result.data[i]);   
                        for(var j=0;j<$scope.customers.length;j++){                        
                            if(result.data[i].customerId==$scope.customers[j].value){
                                result.data[i].customerName= $scope.customers[j].text
                            }
                        } 
                        for(var k=0;k<$scope.tagtypes.length;k++){                        
                            if(result.data[i].tagTypeId==$scope.tagtypes[k].value){
                                result.data[i].TagType= $scope.tagtypes[k].text
                            }
                        } 
                    $scope.tagassignments.push(result.data[i]);    
                    data.push(result.data[i]); 
                    }
                    myGrid();  
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };


        function deleteTagAssignmentDetails(TagAssignmentId) {    
            alertify.confirm("Are you sure want to delete", function () {        
                tagService.DeleteTagQtyAssignment(TagAssignmentId,function (result) {
                    if (result.success === true) {
                        $scope.tagassignments=[];
                        data=[];
                        getAllTagQtyAssignment();
                        toastr.success(result.message);
                    } else {
                        toastr.error(result.message); 
                        vm.error = result.message;                    
                    }
                });
            }, function() {
            // user clicked "cancel"
            });   
        };


        function updateTagAssignmentDetails() {            
            tagService.UpdatetagQtyAssignment(vm._id, vm.customerId, vm.tagTypeId, vm.tagQty,function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.customerId= "";
                    vm.tagTypeId= "";
                    vm.tagQty= "";
                    $scope.tagassignments=[];
                    data=[];
                    getAllTagQtyAssignment();
                    toastr.success(result.message);
                    vm.ModalInstance.close();
                    $scope.isValidate = true;
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };
       
        $scope.currRowData = function(rowform,assignedTagData) {
             //for validation if data is blank or not.
             $scope.isValidate = false;
            if(rowform=={}|| rowform==undefined||rowform.customerId=="" || rowform.customerId==undefined ||rowform.tagTypeId==undefined || rowform.tagTypeId=="" || isNaN(rowform.tagQty) || parseInt(rowform.tagQty) < 0 || rowform.tagQty == null){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate = false;
                return;
            }
            else {
                //$scope.isValidate = true;
                
                if(rowform._id == "" || rowform._id == undefined){
                    vm.customerId= rowform.customerId;
                    vm.assignedCustomerId = rowform.customerId;
                    vm.tagTypeId= rowform.tagTypeId;
                    vm.tagQty= rowform.tagQty;
                    addTagQtyAssignment(assignedTagData);
                    $scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.customerId= rowform.customerId;
                    vm.tagTypeId= rowform.tagTypeId;
                    vm.tagQty= rowform.tagQty;
                    updateTagAssignmentDetails();
                    $scope.clearData();
                    rowform ={};
                }              
            }
 
        }

        //to get all Customer Details details.
        function getAllCustomerDetails() {      
            $scope.customers=[];      
            customerService.GetAllCustomers(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.customers.length;i++){      
                        $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].email}); 
                    }
                    console.log($scope.customers, "$scope.customers")
                    getAllTagType();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        //to get all Tag types details.
        function getAllTagType() {            
            $scope.tagtypes=[];
            tagService.GetAllTagType(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.tagTypes.length;i++){
                        $scope.tagtypes.push({value: result.tagTypes[i]._id, text: result.tagTypes[i].TagType});    
                    }
                    getAllTagQtyAssignment();       
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'customerId', type: 'string' },
                    { name: 'customerName', type: 'string' },
                    { name: 'tagTypeId', type: 'string' },
                    { name: 'TagType', type: 'string' }, 
                    { name: 'tagQty', type: 'string' },
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#taggrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Customer Id', datafield: 'customerId',cellsalign: 'center', width: '20%', hidden:true },
                    { text: 'Customer', datafield: 'customerName',cellsalign: 'center', width: '34%' },
                    { text: 'TagType Id', datafield: 'tagTypeId',cellsalign: 'center', width: '20%', hidden:true },
                    { text: 'Tag Type', datafield: 'TagType',cellsalign: 'center', width: '24%' }, 
                    { text: 'Tag Quantity', datafield: 'tagQty',align: 'center',cellsalign: 'center', width: '10%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >View Details </button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
        }               
          
        function buttonsEvents() {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
        } 

        function editDetails() { 
            var getselectedrowindexes = $('#taggrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#taggrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id : selectedRowData._id,
                customerId:selectedRowData.customerId,
                tagTypeId:selectedRowData.tagTypeId,
                tagQty: selectedRowData.tagQty
            }
            $scope.currCustomerId = selectedRowData.customerId;
            $scope.currTagTypeId = selectedRowData.tagTypeId;
            //$scope.openModal();
            GetallCustomerAssignedTagIdDetailsbytagQtyAssignmentId(selectedRowData._id);
            
        }  
        $scope.allocatedtags=[];
        function GetallCustomerAssignedTagIdDetailsbytagQtyAssignmentId(tagQtyAssignmentId){
        tagService.GetallCustomerAssignedTagIdDetailsbytagQtyAssignmentId(tagQtyAssignmentId,function (result) {
            if (result.success === true) {  
                console.log(result);
                $scope.allocatedtags = result.data;
                $scope.openModalForviewDetails();
            } else {
                toastr.error(result.message); 
                vm.error = result.message;                    
            }
        });
        }

        $scope.openModalForviewDetails = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowViewTagAssign.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.viewDetails = function() {  
                        var currData = {};
                        //GetallTagbyTagType();
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.clearData();
                    };
                },
                backdrop: 'static',
                scope: $scope,
            })
        };
        function deleteDetails(){ 
            getselectedrowindexes = $('#taggrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                selectedRowData = $('#taggrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            deleteTagAssignmentDetails(selectedRowData._id);
        }      
        $("#taggrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
        $("#jqxwindowTagAssign").bind('close', function (event){
            $scope.clearData();
        });
          $scope.clearData = function(){ //window.location.reload();
              $scope.currCustomerId="0";
              $scope.currTagTypeId="0";
              $scope.test= [];
              $scope.vccUser = {
                  _id:"",
                  customerId:"",
                  tagTypeId:"",
                  tagQty:""
             }
             $("#taggrid").jqxGrid('clearselection');
          }
        //get allocated specifc qty to depot 
        $scope.getSpecificTags =function(data){
            //alert('test');
            animation.myFunc('tagAssign');
            console.log(data);
            if(data.tagTypeId=="" && data.tagQty==""){
                alertify.alert("Please provide all the values to assign the tag quantity");
                return
            }            
            $scope.test= [];
            vm.allTagList = [];   
            vm.customerId=currloggedinUser.otherData._id;
            //alert(vm.customerId)
            vm.customerName=$('#customerName :selected').text();
            vm.tagTypeName=$('#tagTypeName :selected').text();
            vm.tagQty=$('#tagQty').val();
            setTimeout(function(){ 
                $('#tagList li').each(function (i) {
                    var index = $(this).index();
                    var text = $(this).text();
                    var value = $(this).attr('value');
                    console.log('Index is: ' + index + ' and text is ' + text + ' and Value ' + value);
                    vm.allTagList.push(text);
                });            
                $('#tagAssign').waitMe('hide');
            }, 500);

            console.log(vm.allTagList);
            console.log('customerName, tagTypeName,tagQty');
            console.log( vm.customerName, vm.tagTypeName, vm.tagQty, vm.allTagList);
            console.log(data);
            var size =data.tagQty;
            var tagTypeId = data.tagTypeId;
            if(size!=''){
                tagService.GetSpecificTagMaster(size,tagTypeId,vm.customerId,function (result) {
                    if (result.success === true) {  
                        console.log(result);
                        if(result.data.length!=size){
                            alertify.alert("Insufficient tag for selected Tag Type, cannot assign any tags Buy More Tags");
                            $scope.assignTags=false;
                            $scope.vccUser.tagQty='';
                            return  
                        }else{
                            $scope.assignTags=true;
                        }
                        $scope.test=result.data;
                        if(result.data.tagmasterId!=undefined){
                            vm.custTagAssignDetails ={    
                                tagQtyAssignmentId : result.data.tagQtyAssignmentId, 
                                tagmasterId : result.data.tagmasterId, 
                                depotId : result.data.depotId, 
                                tagTypeId : result.data.tagTypeId
                            }
                        }else{
                            vm.custTagAssignDetails ={    
                                tagQtyAssignmentId : result.data.tagQtyAssignmentId, 
                                tagmasterId : result.data.tagId, 
                                depotId : result.data.depotId, 
                                tagTypeId : result.data.tagTypeId
                            }
                        }

                    } else {
                        toastr.error(result.message); 
                        vm.error = result.message;                    
                    }
                });
                $scope.tagsdata= true;
            }
        }
        //create table 
        function buildTableBody(data, columns) {
            var body = [];
        
            body.push(columns);
        
            data.forEach(function(row) {
                var dataRow = [];
        
                columns.forEach(function(column) {
                    dataRow.push(row[column].toString());
                })
        
                body.push(dataRow);
            });
        
            return body;
        }
        //get table body
        function tablehlt(data, columns) {
            return {
                table: {
                    headerRows: 1,
                    widths:['50%','50%'],
                    body: buildTableBody(data, columns)
                },
                layout: {
                    fillColor: function (node) {
                        return (1 % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            };
        }
        $scope.printDiv = function(assignedTagData, data,customerName,tagTypeName,tagQty, allTagList) {

            //alertify("please enable your window popup setting in browser");
            var newArr=[];            
            $('#tagAssign').hide(); 
            var externalDataRetrievedFromServer = [];
            var headerArr =[];
            var dashheader='';
            // get a new date (locale machine date time)
            var date = new Date();
            // get the date as a string
            var n = moment().format("YYYY-MM-DD");
            dashheader='Allocated Tag Details \n';
            headerArr=['Sno', 'Tags'];
            for(var i=0;i<assignedTagData.length;i++){
                var a={ 
                    'Sno': i+1, 
                    'Tags':assignedTagData[i].tagId
                }
                externalDataRetrievedFromServer.push(a);
            }
            var dd = {
                content: [
                    { text: dashheader,bold: true,fontSize: 14, alignment:'center' },
                    { text: 'Date: '+n,bold: true, alignment:'left' },
                    { text: '\n'},
                    { text:'Customer Name:'+vm.customerName,bold: true},
                    { text: '\n'},
                    { text:'Tag Type: '+vm.tagTypeName,bold: true},
                    { text: '\n'},
                    { text:'Tag Quantity: '+assignedTagData.length,bold: true},
                    { text: '\n'},
                    { text:'Assignment Date:' +moment().format("YYYY-MM-DD")},
                    { text: '\n'},
                    tablehlt(externalDataRetrievedFromServer, headerArr)                        
                ]
            }    
            pdfMake.createPdf(dd).download()
            pdfMake.createPdf(dd).print()

        }


        function addCustomerAssignedTag(data,assignedTagData) {  
            //assign tags to customer here
            var bulkData = [];
            var currCustomerId = localStorage.getObject('dataUser');
            for(var i=0;i<assignedTagData.length;i++){
                if(assignedTagData[i].tagId!=undefined){
                    vm.custTagAssignDetails ={    
                        tagQtyAssignmentId : data._id, 
                        tagmasterId : assignedTagData[i].tagId, 
                        depotId : '', 
                        tagTypeId : assignedTagData[i].tagTypeId,
                        customerId: vm.assignedCustomerId
                    }  
                }else{
                    vm.custTagAssignDetails ={    
                        tagQtyAssignmentId : data._id, 
                        tagmasterId : assignedTagData[i].tagmasterId, 
                        depotId : '', 
                        tagTypeId : assignedTagData[i].tagTypeId,
                        customerId: vm.assignedCustomerId
                    }  
                }

                bulkData.push(vm.custTagAssignDetails);
            }
            console.log("bulkDatabulkDatabulkData",bulkData);
            customerService.AddCustomerAssignedTagDetailsBulk(bulkData, function (result) {
                if (result.success === true) {
                    //toastr.success(result.message);
                    animation.myFunc('tagAssign');
                    vm.assignedCustomerId='';
                    //setTimeout(function(){ 
                        $scope.printDiv(assignedTagData, data,vm.customerName,vm.tagTypeName,vm.tagQty, vm.allTagList); 
                        $scope.isValidate = true;      
                    //}, 3000);
                } else {
                    //deletetagmaster data._id
                    toastr.error(result.message); 
                    vm.error = result.message;  
                    window.location.reload();                  
                }
            })
        }
    }
  
})();
  
  