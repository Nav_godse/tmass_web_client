(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('TagTypeCtrl', TagTypeCtrl);
  
    /** @ngInject */
    function TagTypeCtrl($scope,tagService,$filter, editableOptions, editableThemes,toastr,$uibModal) {
        var vm = this;

        $scope.openModal = function () {
            vm.ModalInstance=$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowTagType.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.getAllTagTypeDetails = getAllTagTypeDetails;
        vm.addTagType = addTagType; 
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            TagType:"",
            TagTypeName:"",
            TagTypeDescription:""
       }
        initController();
        $scope.tagTypes = [];
        vm.datatableData = [];
        var data = [];
        function initController() {
            getAllTagTypeDetails();
        }

        function addTagType() {            
            tagService.AddTagType(vm.TagType, vm.TagTypeName, vm.TagTypeDescription, function (result) {
                if (result.success === true) {
                    //console.log(result);
                    vm.TagType= "";
                    vm.TagTypeName= "";
                    vm.TagTypeDescription= "";
                    $scope.tagTypes= [];
                    data =[];    
                    $scope.vccUser= {};
                    vm.ModalInstance.close()
                    $scope.isValidate=true;
                    getAllTagTypeDetails();
                    toastr.success(result.message);
                    //$("#jqxwindow").jqxWindow('close');
                } else {
                    toastr.error(result.message);
                    $scope.isValidate=false;
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
            
        };

        function getAllTagTypeDetails() {                 
            tagService.GetAllTagType(function (result) {
                data= [];
                if (result.success === true) {
                    for(var i=0;i<result.tagTypes.length;i++){
                        $scope.tagTypes.push(result.tagTypes[i]);    
                        data.push(result.tagTypes[i]);
                        vm.datatableData.push(result.tagTypes[i]);
                    }
                    myGrid();
                    //toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        function deleteTagTypeDetails(_id) {            
            tagService.DeleteTagType(_id,function (result) {
                if (result.success === true) {
                    $scope.tagTypes=[];
                    getAllTagTypeDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        function updateTagTypeDetails() {            
            tagService.UpdatetagType(vm._id, vm.TagType, vm.TagTypeName, vm.TagTypeDescription,function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.TagType= "";
                    vm.TagTypeName= "";
                    vm.TagTypeDescription= "";
                    $scope.tagTypes=[];
                    getAllTagTypeDetails();
                    toastr.success(result.message);
                    vm.ModalInstance.close();
                    $scope.isValidate=true;
                   // $("#jqxwindow").jqxWindow('close');
                    $("#tagTypegrid").jqxGrid('clearselection');
                } else {
                    toastr.error(result.message);
                    $scope.isValidate=false;
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) { 
             //for validation if data is blank or not.
             $scope.isValidate = false;
            if(rowform.TagType=="" || rowform.TagType==undefined||rowform.TagTypeName=="" || rowform.TagTypeName==undefined || rowform=={}){ 
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate = false;
                return;
            } else {
                $scope.isValidate = true;
                if(rowform._id == "" || rowform._id == undefined){
                    vm.TagType=rowform.TagType;
                    vm.TagTypeName= rowform.TagTypeName;
                    vm.TagTypeDescription= rowform.TagTypeDescription;
                    addTagType();                
                    $scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.TagType= rowform.TagType;
                    vm.TagTypeName= rowform.TagTypeName;
                    vm.TagTypeDescription= rowform.TagTypeDescription;
                    updateTagTypeDetails();
                    $scope.clearData();
                    rowform ={};
                }               

            }
        }
        
      ///////////////////////////////////////
    //   $("#jqxwindow").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
    //   //$('.jqx-window-header').css("height", "40px");
    //   $("#popup").click(function () {
    //       $("#tagTypegrid").jqxGrid('clearselection');
    //         getselectedrowindexes =null;
    //         selectedRowData =null;
    //         $('#_id').val('');
    //         vm._id =''
    //       $("#jqxwindow").jqxWindow('open');
    //   });

      /////////////////////////////////////////////
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'TagType', type: 'string' },
                    { name: 'TagTypeName', type: 'string' },
                    { name: 'TagTypeDescription', type: 'string' },
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#tagTypegrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                        { text: 'Type', datafield: 'TagType',align: 'center', width: '22%' },
                        { text: 'Name', datafield: 'TagTypeName',align: 'center', width: '22%' },
                        { text: 'Description', datafield: 'TagTypeDescription',align: 'center', width: '22%' },
                        {
                            text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                            // render custom column.
                            return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary" disabled >Edit </button><input disabled type="button" data-row="' + row + '" class="deleteButtons btn btn-danger" value="Delete" /> </div>';
                            }
                        }
                ]
            });

            // Initlize Button Event
            buttonsEvents();
        }
             
        
            function buttonsEvents() {
                // For Edit Details
                var editbuttons = document.getElementsByClassName('editButtons');
                for (var i = 0; i < editbuttons.length; i+=1) {
                    editbuttons[i].addEventListener('click', editDetails);
                }
                  // For Delete Details
                  var deletebuttons = document.getElementsByClassName('deleteButtons');
                  for (var i = 0; i < deletebuttons.length; i+=1) {
                    deletebuttons[i].addEventListener('click', deleteDetails);
                  }
             }

        
             function editDetails() { 
                var getselectedrowindexes = $('#tagTypegrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                { 
                    var selectedRowData = $('#tagTypegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                $scope.vccUser = {
                    _id : selectedRowData._id,
                    TagType:selectedRowData.TagType,
                    TagTypeName:selectedRowData.TagTypeName,
                    TagTypeDescription: selectedRowData.TagTypeDescription
                }
                $scope.openModal();
                // $('#_id').val(selectedRowData._id);
                // $('#TagType').val(selectedRowData.TagType);
                // $("#TagTypeName").val(selectedRowData.TagTypeName);
                // $("#TagTypeDescription").val(selectedRowData.TagTypeDescription);
                // $("#jqxwindow").jqxWindow('open'); 
            }  
    
            function deleteDetails(){ 
                getselectedrowindexes = $('#tagTypegrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                {
                    // returns the selected row's data.
                    selectedRowData = $('#tagTypegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                alertify.confirm("Are you sure, you want to delete?", function () {
                    // user clicked "ok".
                    deleteTagTypeDetails(selectedRowData._id);
                }, function() {
                    // user clicked "cancel"
                });
                
            }
    
            $("#tagTypegrid").on("pagechanged", function (event) {
                // page number.
                var pagenum = event.args.pagenum;
                // page size.
                var pagesize = args.pagesize;
                buttonsEvents();
            });

        $scope.clearData = function(){ //window.location.reload();
            // $('#_id').val('');
            // $('#TagType').val('');
            // $("#TagTypeName").val('');
            // $("#TagTypeDescription").val('');
            $("#tagTypegrid").jqxGrid('clearselection');
           // $("#jqxwindow").jqxWindow('close'); 
            $scope.vccUser = {
                _id:"",
                TagType:"",
                TagTypeName:"",
                TagTypeDescription:""
           }
        }
        // $("#jqxwindow").bind('close', function (event){
        //     $scope.clearData();
        // });

        // $("#button_no").click(function () {
        //     $("#jqxwindow").jqxWindow('close');
        // });
    }    
})();
  
  