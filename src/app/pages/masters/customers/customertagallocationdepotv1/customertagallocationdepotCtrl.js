(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('CustomerTagAllocationCtrlv1', CustomerTagAllocationCtrlv1);
  
    /** @ngInject */
    function CustomerTagAllocationCtrlv1($scope,customerService,tagService,depotService,toastr, $uibModal,localStorage) {
        var vm = this;
        $scope.allocatedTagCount=0;
        $scope.remainAllocatedTag=0;
        vm.currCustomerDetails = localStorage.getObject('dataUser');
        $scope.openModal = function () {
            $scope.tagFromCust=[];
            vm.ModalInstance=$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowCustomerTagAllocateDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);                         
                    if($scope.remainAllocatedTag==0 || $scope.remainAllocatedTag < $("#tagQty").val()){
                        alertify.alert("No Tags assigned of particular type. Buy more tags");   
                        $scope.isValidate==false; 
                     }
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                        $scope.remainAllocatedTag=0;
                        $scope.tagFromCust=[];
                        $scope.vccUser=[];
                    }                    
                };
              
                $scope.cancel = function () {
                    $scope.remainAllocatedTag="";
                    //$scope.remainAllocatedTag=0;
                    $scope.tagFromCust=[];
                    $scope.vccUser=[]
                    $uibModalInstance.close();
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.addCustomerAssignedTag = addCustomerAssignedTag;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        $scope.vccUser = {}   
        initController();
        $scope.customerTagDetails = [];
        $scope.customers= [];
        $scope.tagTypes= [];
        $scope.tagTypes= [];
        $scope.customerDepot= []; 
        //$scope.remainAllocatedTag=0;
        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllCustomerDepotDetails();
            getAllTagTypeDetails();
            getAllTagQtyAssignmentDetails();
            setTimeout(function(){
                getAllCustomerTagMasterDetails();
            }, 1000);          
        }

        $scope.AllocatedTagstome = [];
        $scope.tagFromCust = [];
        $scope.totalAllocatedTags = 0;
        $scope.emptytagFromCust= function(){
            $scope.tagFromCust = [];
            $scope.remainAllocatedTag=0;
        }
        $scope.findTagAllocatedTocustomer = function(){     
            //alert("herer"); 
            if(Object.entries($scope.vccUser).length>=3)
            {
                $scope.tagFromCust=[];
                //$scope.remainAllocatedTag=0;
                customerService.GetTagAllocatedTocustomer($scope.vccUser.customerId,$scope.vccUser.tagTypeId, function (result) {
                    if (result.success === true) { 
                        console.log("result.data",result.data);
                        //alert(result.data)
                        $scope.allocatedTagCount=0;
                        //$scope.AllocatedTagstome = result.data.Tags;
                        //$scope.totalAllocatedTags = result.data.TotalTags;
                        // for(var i=0;i<$scope.customerTagDetails.length;i++){
                        //     if($scope.customerTagDetails[i].tagTypeId == $scope.vccUser.tagTypeId){
                        //         $scope.allocatedTagCount = $scope.allocatedTagCount + $scope.customerTagDetails[i].tagQty;
                        //     }    
                        // }
                        if( result.data !=0){
                            console.log("result.data",result.data);
                            for(var x in result.data){
                                $scope.tagFromCust.push({tagTypeId: result.data[x].tagTypeId,tagmasterId:result.data[x].tagmasterId});
                            }        
                            console.log("$scope.tagFromCust",$scope.tagFromCust);      
                            $scope.remainAllocatedTag= $scope.tagFromCust.length;              
                        }else{
                            alertify.alert("No Tags assigned of particular type. Buy more tags");   
                            $scope.isValidate==false 
                        }            
                        // if($scope.remainAllocatedTag==0 ){
                        //     alertify.alert("No Tags assigned of particular type. Buy more tags");   
                        //     $scope.isValidate==false 
                        // }                             
                    } else {
                        $scope.tagFromCust=[];
                        toastr.error(result.message); 
                        vm.error = result.message;                    
                    }
                });
            }
            // else{
            //     alertify.alert("Please Fill all fields");      
            // }
        }
        //all loops
        //to get all Tag QtyAssignment details.
        function getAllTagQtyAssignmentDetails() {            
            customerService.GetAllCustomers(function (result) {
                $scope.customers=[];
             
                if (result.success === true) { 
                    if(result.customers.length==0 && vm.currCustomerDetails.otherData.roleName=="Client Admin"){
                        $scope.customers.push({value: vm.currCustomerDetails.otherData._id, text: vm.currCustomerDetails.otherData.email});      
                    }else{
                        for(var i=0;i<result.customers.length;i++){
                            $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].email});    
                          } 
                    }
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            })
        }

        //tag types:
        function getAllTagTypeDetails() {            
            tagService.GetAllTagType(function (result) {
                $scope.tagTypes=[]
                if (result.success === true) { 
                    for(var i=0;i<result.tagTypes.length;i++){
                      $scope.tagTypes.push({value: result.tagTypes[i]._id, text: result.tagTypes[i].TagType});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            })
        }

        //to get all Depot details.
        function getAllCustomerDepotDetails() {            
            depotService.GetAlldepotByCustomer(function (result) {
                $scope.customerDepot=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.customerDepot.push({value: result.data[i]._id, text: result.data[i].depotName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }
        //end all loops
        function getAllCustomerTagMasterDetails() {            
            customerService.GetallCustomerTagAllocatesToDepot(function (result) {
                 
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.tagTypes.length;j++){
                            if(result.data[i].tagTypeId===$scope.tagTypes[j].value){
                                result.data[i].tagTypeName = $scope.tagTypes[j].text;
                                break;
                            }else{
                                result.data[i].tagTypeName = "None";
                            }
                        }
                        for(var k=0;k<$scope.customers.length;k++){
                            if(result.data[i].customerId===$scope.customers[k].value){
                                result.data[i].customerName = $scope.customers[k].text;
                                break;
                            }else{
                                result.data[i].customerName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.customerDepot.length;l++){
                            if(result.data[i].depotId===$scope.customerDepot[l].value){
                                result.data[i].depotName = $scope.customerDepot[l].text;
                                break;
                            }else{
                                result.data[i].depotName = "None";
                            } 
                        } 
                        $scope.customerTagDetails.push(result.data[i]);    
                    }                  
                    mygridcustTagAllocate();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteTagMasterDetails(_id) {            
            customerService.DeleteCustomerTagAllocatesToDepot(_id,function (result) {
                if (result.success === true) {
                    $scope.customerTagDetails=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateTagMasterDetails(data) {            
            customerService.UpdateCustomerTagAllocatesToDepot(data,function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.customerId= "";
                    vm.depotId= "";
                    vm.tagQty= "";
                    vm.tagTypeId= "";
                    $scope.customerTagDetails=[];
                    vm.custTagAssignDetails={};
                    getAllDropdowns();
                    $scope.isValidate=true;
                    vm.ModalInstance.close();
                    toastr.success(result.message);
                } else {
                    $scope.isValidate=false;
                    toastr.error(result.message);
                    //console.log(result);              
                }
            });
        };

        function addCustomerAssignedTag(data) {
            console.log(data);            
            customerService.AddCustomerTagAllocatesToDepot(data, function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.customerId= "";
                    vm.depotId= "";
                    vm.tagQty= "";
                    vm.tagTypeId= "";
                    $scope.vccUser={};
                    $scope.customerTagDetails= [];
                    $scope.AllocatedTagstome = [];
                    $scope.totalAllocatedTags = 0;
                    $scope.remainAllocatedTag=0;
                    vm.ModalInstance.close();
                    $scope.isValidate=true;
                    vm.custTagAssignDetails={};
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }
        
        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            console.log("rowform",rowform);
           
            // if(isNaN(rowform.tagQty))
            //     alert("hi");

            if(rowform.tagQty==0 || $scope.remainAllocatedTag < rowform.tagQty){
                alertify.alert("Enter valid tag count or buy more tags");    
             }else{
                $scope.isValidate = false;
            if (rowform == undefined || ( rowform.depotId == undefined || rowform.depotId == "" || rowform.customerId == "" || rowform.customerId == undefined || parseInt(rowform.tagQty) < 0 || rowform.tagQty == null || isNaN(rowform.tagQty) || rowform.tagTypeId == "" || rowform.tagTypeId == undefined )){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return ;
            }
            
            if(rowform._id != undefined){
                vm.custTagAssignDetails ={    
                    _id : rowform._id, 
                    customerId: rowform.customerId,
                    depotId: rowform.depotId,
                    tagQty: rowform.tagQty,
                    tagTypeId: rowform.tagTypeId
                }
                console.log(vm.custTagAssignDetails);
                updateTagMasterDetails(vm.custTagAssignDetails);
                $scope.isValidate = true;
            } else {
                vm.custTagAssignDetails ={    
                    customerId: rowform.customerId,
                    depotId: rowform.depotId,
                    tagQty: rowform.tagQty,
                    tagTypeId: rowform.tagTypeId
                }
                addCustomerAssignedTag(vm.custTagAssignDetails);
                $scope.isValidate = true;
            }
        }
                           
        }
        
         // For Loading the gridcustTagAllocate Widgets
        function mygridcustTagAllocate() { 
            var source =
            {
                localdata: $scope.customerTagDetails,
                datatype: "json",
                datafields:
                [
                { name: '_id', type: 'string' },
                { name: 'customerId', type: 'string' },
                { name: 'depotId', type: 'string' },
                { name: 'tagQty', type: 'number' },
                { name: 'tagTypeId', type: 'string' },
                { name: 'customerName', type: 'string' },
                { name: 'depotName', type: 'string' },
                { name: 'tagTypeName', type: 'string' },
                ]
            };
 
            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#gridcustTagAllocate").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '10%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Customer', datafield: 'customerName',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Tag Qty',hidden:false, datafield: 'tagQty',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Depot Name',hidden:false, datafield: 'depotName',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Tag Type Name',hidden:false, datafield: 'tagTypeName',align: 'center',cellsalign: 'center', width: '20%' },
                    // {
                    //     text: 'Action', cellsAlign: 'center', align: "center",width: '20%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    //     // render custom column.
                    //     return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    //     }
                    // }
                ]
            });
        // Initlize Button Event
            buttonsEvents();
            $("#gridcustTagAllocate").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }

        function buttonsEvents() {            
                // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
                for (var i = 0; i < editbuttons.length; i+=1) {
                    editbuttons[i].addEventListener('click', editDetails);
                }
                // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
                for (var i = 0; i < deletebuttons.length; i+=1) {
                    deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
         // For Edit Details
        function editDetails() { 
            var getselectedrowindexes = $('#gridcustTagAllocate').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            var selectedRowData = $('#gridcustTagAllocate').jqxGrid('getrowdata', getselectedrowindexes[0]);
            $scope.vccUser._id=selectedRowData._id;
            $scope.vccUser.customerId=selectedRowData.customerId;
            $scope.vccUser.depotId=selectedRowData.depotId;
            $scope.vccUser.tagQty=selectedRowData.tagQty;
            $scope.vccUser.tagTypeId=selectedRowData.tagTypeId;
            $scope.openModal();
        }  
         // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#gridcustTagAllocate').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
                selectedRowData = $('#gridcustTagAllocate').jqxGrid('getrowdata', getselectedrowindexes[0]);
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteTagMasterDetails(selectedRowData._id);
            }, function() {});
        }     

        $scope.clearData = function() {
            $("#gridcustTagAllocate").jqxGrid('clearselection');
            $scope.remainAllocatedTag=0;
            $scope.vccUser = {}            
        }
    }
  
})();
  
  