(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('CustomerDetailsCtrl', CustomerDetailsCtrl);
  
    /** @ngInject */
    function CustomerDetailsCtrl($uibModal,$scope,localStorage,customerService,roleService,$filter, editableOptions, editableThemes,toastr,animation) {
        var vm = this;
        $scope.pinMsg= '';
        vm.currloggedinUser = localStorage.getObject('dataUser');
        // $( "#code" ).on('shown', function(){
        //     alert("Shown!");
        // });

        $scope.openModal = function () {
            vm.modalInstance =$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowCustomer.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.save = function (data) {
                        console.log(data);
                        $scope.currRowData(data);
                        if($scope.isValidate==true){
                            $uibModalInstance.close();
                            $scope.clearData();
                        }                    
                    };
              
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.clearData();
                        $scope.vccUser.isEnable = true;
                    };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.personalInfo = {};
        $scope.users = [];
        $scope.custTypes= [];
        $scope.licenseTypes= [];
        $scope.states= [];
        $scope.cities= [];
        $scope.customers= [];
        var getselectedrowindexes =null;
        var selectedRowData =null;
        //$scope.orgEmail='';
        initController();

        //from state,city and country
        $scope.vccUser = {};
        $scope.vccUser.isEnable = true;

        function initController() {
            getAlldropdowns();
        }

        function getAlldropdowns(){        
            //getAllCustomerTypeDetails();
            GetroleLevelforAddCustomer();
            getAllCustomerDetails();            
        }
        $scope.onesystemAdmin=false;
        //to get customer GetAllCustomers
        function getAllCustomerDetails() {          
            customerService.GetAllCustomers(function (result) {
                $scope.customers = [];
                if (result.success === true) {
                    $scope.onesystemAdmin=false;
                    for(var i=0;i<result.customers.length;i++){                        
                        $scope.customers.push(result.customers[i]);
                        if(result.customers[i].roleName=="System Admin"){
                            $scope.onesystemAdmin=true;
                        }
                    }
                    //myfunct($scope.customers,)
                    myGrid();
                    //console.log($scope.customers);
                } else {
                    vm.error = result.message;
                }
            })
        }
        //To Add Edit and delete customer
        function addCustomer() { 
            console.log($scope.vccUser);       
            showAnimation('custform');
            customerService.AddCustomer($scope.vccUser, function (result) {                
                if (result.success === true) { 
                    getAlldropdowns(); 
                    addDefaultLicense(result.data._id);        
                    toastr.success(result.message);
                    $scope.selectedcustomerTypeId = '';
                    $scope.vccUser = {};
                    $scope.custTypes = [];
                    $scope.licenseTypes = [];
                    $scope.customers = [];
                    $scope.isValidate = true;
                    vm.modalInstance.close()
                    $('#custform').waitMe('hide');
                } else {
                    $('#custform').waitMe('hide');
                    if(result.message=='Customer Added, Verification link sending failed!'){
                        $scope.isValidate = true;
                        getAlldropdowns(); 
                        addDefaultLicense(result.data._id);        
                        vm.modalInstance.close()
                    }else{
                        $scope.isValidate = false;
                    }
                    toastr.error(result.message); 
                    //vm.error = result.message;                    
                }
            })
        }

        function addDefaultLicense(selectedCustomerId){
            customerService.GetAllLicenseType(function (result) {
                if (result.success === true) { 
                    console.log(result.licenseTypes);
                    for(var i=0;i<result.licenseTypes.length;i++){
                        if(result.licenseTypes[i].licenseTypeName=="Connected"){
                            $scope.DefaultLicenseData ={
                                customerId:selectedCustomerId,
                                licenseTypeId:result.licenseTypes[i]._id,
                                tireQty:0,
                                vehicleQty:0,
                                licenseDate:Date.now(),
                            }
                        }
                    }
                    if(result.licenseTypes.length>0){
                        customerService.AddCustomerLicenseDetail($scope.DefaultLicenseData, function (result) {
                            if (result.success === true) { 
                                $scope.DefaultLicenseData={};
                                toastr.success(result.message);
                            } else {
                                toastr.error(result.message); 
                                vm.error = result.message;                    
                            }
                        });
                    }else{
                        alertify.alert("Please add some license type");
                    }
                } else {
                    toastr.error(result.message); 
                    console.log(result);
                    vm.error = result.message;                    
                }
            })
        }

        function addCustomerLicenseDetail() { 
            console.log($scope.vccUser);          
            customerService.AddCustomerLicenseDetail($scope.vccUser, function (result) {
                if (result.success === true) { 
                    $scope.vccUser={};
                    $scope.custTypes= [];
                    $scope.licenseTypes= [];
                    $scope.customers= [];
                    $scope.customerLicense = [];
                    getAlldropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };


        function deleteCustomerById(_id) {            
            customerService.DeleteCustomer(_id,function (result) {
                if (result.success === true) {
                    $scope.custTypes= [];
                    $scope.licenseTypes= [];
                    $scope.states= [];
                    $scope.cities= [];
                    $scope.customers= [];
                    toastr.success(result.message);
                    getAlldropdowns();
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                
                }
            });
        };
        function showAnimation(id){
            animation.myFunc(id); 
        }
        function updateCustomerById(data) { 
            //console.log("data",data)       
            showAnimation('custform');
            customerService.updateCustomer(data,function (result) {
                if (result.success === true) {
                    $scope.custTypes= [];
                    $scope.licenseTypes= [];
                    $scope.states= [];
                    $scope.vccUser = {};
                    $scope.cities= [];
                    $scope.customers= [];
                    getAlldropdowns(); 
                    if(vm.isnewMail==true){
                        addDefaultLicense(result.data._id); 
                    }
                    vm.isnewMail=false;
                    toastr.success(result.message);
                    $scope.isValidate = true;
                    vm.modalInstance.close()
                    $('#custform').hide();
                } else {
                    if(result.message=='Customer Added, Verification link sending failed!'){
                        $scope.isValidate = true;
                        vm.modalInstance.close()
                    }else{
                        $scope.isValidate = false;
                    }
                    toastr.error(result.message); 
                    $('#custform').hide();
                    vm.error = result.message;                    
                }
            });
        };

        //End of Add ,Edit and delete
        $scope.getauthrolestoadduser = [];
        //custTypes
        function GetroleLevelforAddCustomer(){
            roleService.GetroleLevelforAddCustomer(function(result){
                if (result.success === true) { 
                    console.log(result.data[0].roleLevelCustomerAddition);
                    $scope.getauthrolestoadduser=result.data[0].roleLevelCustomerAddition;
                    getAllCustomerTypeDetails();
                    // for(var i=0; i<result.data.length; i++){
                    // $scope.allDepo.push({value: result.data[i]._id, text: result.data[i].depotName});    
                    // }
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            // //console.log($scope.allDepo);
            });
        }
        //to get customer types
        function getAllCustomerTypeDetails() {
            customerService.GetAllCustomerType(function (result) {
                if (result.success === true) {
                    console.log(result);
                    vm.customertypes=result.customerTypes;

                    for(var i=0;i<result.customerTypes.length;i++){
                        // if(vm.currloggedinUser.otherData.roleName=="System Super Admin")
                        // {
                        //     $scope.custTypes.push({value: result.customerTypes[i]._id, text: result.customerTypes[i].customerTypeName}); 
                        //     return;
                        // }
                        for(var j=0;j<$scope.getauthrolestoadduser.length;j++){
                            if(result.customerTypes[i].customerLevel==$scope.getauthrolestoadduser[j])
                                $scope.custTypes.push({value: result.customerTypes[i]._id, text: result.customerTypes[i].customerTypeName}); 
                        }
                        
                    }
                } else {
                    vm.error = result.message;
                }
            })
        }

        //to get license types
        function getAllLicenseTypeDetails() {
            customerService.GetAllLicenseType(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.licenseTypes.length;i++){
                        $scope.licenseTypes.push({value: result.licenseTypes[i]._id, text: result.licenseTypes[i].licenseTypeName});
                    }
                } else {
                    vm.error = result.message;
                }
            })
        }
        $scope.currRowData = function(rowform) {
            console.log(rowform);
            $scope.isValidate = false;
            if(rowform.pincode){
                var validData= valid();
                if(validData == false){
                    $scope.isValidate= false;
                    toastr.error("Please Enter valid Pincode");
                    return;
                }
            }            
            if (rowform == undefined || ( rowform.customerName == undefined ||  rowform.customerName == "" || rowform.stateId == "" || rowform.stateId == undefined || rowform.cityId == "" || rowform.cityId == undefined || rowform.customerTypeId == "" || rowform.customerTypeId == undefined)) {
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return ;
            }
            if(rowform._id == "" || rowform._id == undefined){
                //$scope.isValidate = true;
                addCustomer();                
            }else{
                //$scope.isValidate = true;
                vm.isnewMail=false;
                if($scope.orgEmail!=rowform.email){
                    vm.isnewMail=true;
                }
                rowform['isnewEmail']=vm.isnewMail;
                // var updateparms={
                //     "addressLine1": ""
                //     "addressLine2": ""
                //     "cityId": "Pune"
                //     "country": "India"
                //     "customerName": "sege"
                //     "customerTypeId": "5a9130acef8d0302586ec39a"
                //     "email": "sege@nada.ltd"
                //     "isEnable": true
                //     "isnewEmail": false
                //     pincode: null
                //     stateId: "Maharastra"
                //     _id: "5b8fa5ad2d9e4d1e58b8b383"
                // }
                console.log(rowform);
                updateCustomerById(rowform)
            }                               
        }
        //to validate the feilds
        function valid()
        {
            var pin_code=document.getElementById("pincode");
            var pat1=/^\d{6}$/;
            if(!pat1.test(pin_code.value))
            {
               // $scope.pinMsg = "Pin code should be 6 digits";
                //alertify.alert("Pin code should be 6 digits ");
                pin_code.focus();
                return false;
            }else{
                return true;
            }
        }

        /////////////////////////////////////////////
        function myGrid(){ 
            var source =
            {
                localdata: $scope.customers,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'licenseDate', type: 'date' },
                    { name: 'customerName', type: 'string' },
                    //createdByCustomerId
                    { name: 'createdByCustomerId', type: 'string' },
                    { name: 'email', type: 'string'},
                    { name: 'addressLine1', type:'string'},
                    { name: 'addressLine2', type:'string'},
                    { name: 'pincode', type:'number'},
                    { name: 'cityId', type:'string'},
                    { name: 'stateId', type:'string'},
                    { name: 'country', type:'string'},
                    { name: 'customerTypeId', type:'string'},
                    { name: 'customerTypeName', type:'string'},
                    { name: 'isEnable', type:'string'},
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#customergrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'License Date', hidden: true, datafield: 'licenseDate',align: 'center',cellsalign: 'center', width: '15%',cellsformat: 'dd-MMM-yyyy' },
                    { text: 'Customer', hidden:false, datafield: 'email',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Customer Name', hidden:true, datafield: 'customerName',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Address', datafield: 'addressLine1',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Address', hidden:true, datafield: 'addressLine2',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'Pincode', hidden:true, datafield: 'pincode',align: 'center',cellsalign: 'center', width: '15%' },
                    //createdByCustomerId
                    { text: 'createdByCustomerId', hidden:true, datafield: 'createdByCustomerId',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'State Id', hidden:true, datafield: 'stateId',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'State Name', hidden:true, datafield: 'stateName',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'City Id', hidden:true, datafield: 'cityId',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'City Name', hidden:true, datafield: 'cityName',align: 'center',cellsalign: 'center', width: '15%' },
                    { text: 'Customer Type', datafield: 'customerTypeName',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Customer Id', hidden:true, datafield: 'customerTypeId',align: 'center',cellsalign: 'center', width: '13%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });

            $("#customergrid").on("pagechanged filter sort scroll pagesizechanged", function (event) {
                buttonsEvents();
            });
            // Initlize Button Event
            buttonsEvents();
        }
      
        function buttonsEvents() {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
         }
        function myfunct(arrayA,arrayB){
            console.log(arrayA,arrayB)
            var arrayA=[];
            var arrayB=[]
            for(var x=0;x<arrayA.length;x++){
                arrayA.push(arrayA[x].depotId);
            }
            for(var y=0;y<arrayB.length;y++){
                arrayB.push(arrayB[y].value);
            }
            arrayB = arrayB.filter(function(val) {
                return arrayA.indexOf(val) == -1;
            });
            //arrayB = arrayB.filter(val => !arrayA.includes(val));
            console.log(arrayB);
            $scope.dropDowndownDepo=[];
            for(var w=0;w<arrayB.length;w++){
                for(var e=0;e<arrayB.length;e++){
                    if(arrayB[w]===arrayB[e].value){
                        $scope.dropDowndownDepo.push({value: arrayB[e].value, text: arrayB[e].text});  
                    }
                }
            }
            console.log($scope.dropDowndownDepo);
        }
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            //$scope.globalStateId = $scope.vccUser.stateId;
            var getselectedrowindexes = $('#customergrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#customergrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            console.log(selectedRowData);
            $scope.orgEmail=selectedRowData.email;
            $scope.vccUser = {
                _id: selectedRowData._id,
                customerName: selectedRowData.customerName,
                email: selectedRowData.email,
                addressLine1: selectedRowData.addressLine1,
                addressLine2: selectedRowData.addressLine2,
                pincode: selectedRowData.pincode,
                country: selectedRowData.country,
                stateId: selectedRowData.stateId,
                cityId: selectedRowData.cityId,
                isEnable: selectedRowData.isEnable,
                createdByCustomerId: selectedRowData.createdByCustomerId,
                customerTypeId: selectedRowData.customerTypeId
            }       

            $scope.openModal();
        }  

        function deleteDetails(){
            getselectedrowindexes = $('#customergrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {   selectedRowData = $('#customergrid').jqxGrid('getrowdata', getselectedrowindexes[0]);}
            
            if(selectedRowData.email == vm.currloggedinUser.otherData.email){
                alertify.alert("You cannot delete your own user.");
            }else{
                alertify.confirm("Are you sure, you want to delete?", function () {
                    deleteCustomerById(selectedRowData._id);
                }, function() {});
            }
        }

        $scope.clearData = function() {
            $("#customergrid").jqxGrid('clearselection');
            $scope.pinMsg= '';
            $scope.vccUser = {};
        }   
        //-------------------------------END OF SELECT CASCADING-------------------------//
    }
  
})();
  
  