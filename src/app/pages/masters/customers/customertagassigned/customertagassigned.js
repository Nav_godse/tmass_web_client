(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('CustomerTagAssignedCtrl', CustomerTagAssignedCtrl);
  
    /** @ngInject */
    function CustomerTagAssignedCtrl($scope,customerService,tagService,depotService,toastr, $uibModal) {
        var vm = this;
        vm.getAllTagMasterDetails = getAllTagMasterDetails;
        vm.addCustomerAssignedTag = addCustomerAssignedTag;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        $scope.vccUser = {}   
        initController();
        $scope.customerTagDetails = [];
        $scope.tagTypesAssigned= [];
        $scope.tagTypes= [];
        $scope.tagMasters= [];
        $scope.customerDepot= [];

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllCustomerDepotDetails();
            getAllTagMasterDetails();
            getAllTagTypeDetails();
            getAllTagQtyAssignmentDetails();
            setTimeout(function(){
                getAllCustomerTagMasterDetails();
            }, 1000);          
        }


        //all loops
        //to get all Tag QtyAssignment details.
        function getAllTagQtyAssignmentDetails() {            
            tagService.GetAllTagQtyAssignment(function (result) {
                $scope.tagTypesAssigned=[];
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                      $scope.tagTypesAssigned.push({value: result.data[i]._id, text: result.data[i].tagQty});    
                    } 
                } else {
                    toastr.error(result.message);             
                }
            })
        }

        //tag types:
        function getAllTagTypeDetails() {            
            tagService.GetAllTagType(function (result) {
                $scope.tagTypes=[]
                if (result.success === true) { 
                    for(var i=0;i<result.tagTypes.length;i++){
                      $scope.tagTypes.push({value: result.tagTypes[i]._id, text: result.tagTypes[i].TagType});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);             
                }
            })
        }

        //to get all TagMaster
        function getAllTagMasterDetails() {            
            tagService.GetAllTagMaster(function (result) {
                $scope.tagMasters=[];
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.tagMasters.push({value: result.data[i]._id, text: result.data[i].tagId});    
                    } 
                } else {
                    toastr.error(result.message);
                    //console.log(result);            
                }
            })
        }

        //to get all Depot details.
        function getAllCustomerDepotDetails() {            
            depotService.GetAlldepotByCustomer(function (result) {
                $scope.customerDepot=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.customerDepot.push({value: result.data[i]._id, text: result.data[i].depotName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }
        //end all loops
        function getAllCustomerTagMasterDetails() {            
            customerService.GetAllCustomerAssignedTagId(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.tagTypes.length;j++){
                            if(result.data[i].tagTypeId===$scope.tagTypes[j].value){
                                result.data[i].tagTypeName = $scope.tagTypes[j].text;
                                break;
                            }else{
                                result.data[i].tagTypeName = "None";
                            }
                        }
                        for(var k=0;k<$scope.tagTypesAssigned.length;k++){
                            if(result.data[i].tagQtyAssignmentId===$scope.tagTypesAssigned[k].value){
                                result.data[i].tagQtyAssignmentName = $scope.tagTypesAssigned[k].text;
                                break;
                            }else{
                                result.data[i].tagQtyAssignmentName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.tagMasters.length;l++){
                            if(result.data[i].tagmasterId===$scope.tagMasters[l].value){
                                result.data[i].tagmasterName = $scope.tagMasters[l].text;
                                break;
                            }else{
                                result.data[i].tagmasterName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.customerDepot.length;l++){
                            if(result.data[i].depotId===$scope.customerDepot[l].value){
                                result.data[i].depotName = $scope.customerDepot[l].text;
                                break;
                            }else{
                                result.data[i].depotName = "None";
                            } 
                        }
                        $scope.customerTagDetails.push(result.data[i]);    
                    }
                    mygridcustTagAssign();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteTagMasterDetails(_id) {            
            customerService.DeleteCustomerAssignedTagIdById(_id,function (result) {
                if (result.success === true) {
                    $scope.customerTagDetails=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateTagMasterDetails(data) {            
            customerService.UpdateCustomerAssignedTagIdById(data,function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.addedByUserId= "";
                    vm.depotAdditionDate= "";
                    vm.depotId= "";
                    vm.depotName= "";
                    vm.tagQtyAssignmentId= "";
                    vm.tagQtyAssignmentName= "";
                    vm.tagTypeId= "";
                    vm.tagTypeName= "";
                    vm.tagmasterId= "";
                    vm.tagmasterName= "";
                    $scope.customerTagDetails=[];
                    getAllDropdowns();
                    vm.ModalInstance.close();
                    toastr.success(result.message);
                    $scope.isValidate = true;
                } else {
                    toastr.error(result.message);
                    //console.log(result);              
                }
            });
        };

        function addCustomerAssignedTag(data) {            
            customerService.AddCustomerAssignedTagDetails(data, function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.addedByUserId= "";
                    vm.depotAdditionDate= "";
                    vm.depotId= "";
                    vm.depotName= "";
                    vm.tagQtyAssignmentId= "";
                    vm.tagQtyAssignmentName= "";
                    vm.tagTypeId= "";
                    vm.tagTypeName= "";
                    vm.tagmasterId= "";
                    vm.tagmasterName= "";
                    $scope.vccUser={};
                    $scope.customerTagDetails= [];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.isValidate = true;
                    vm.ModalInstance.close();
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }
        
        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            $scope.isValidate = false;
            if(rowform=={} || rowform==undefined || rowform.depotId == undefined || rowform.tagQtyAssignmentId == undefined || rowform.tagmasterId == undefined || rowform.tagTypeId == undefined ){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate = false;
                return;
            } 
            if(rowform._id != undefined){
                vm.custTagAssignDetails ={    
                    _id : rowform._id, 
                    tagQtyAssignmentId : rowform.tagQtyAssignmentId, 
                    tagmasterId : rowform.tagmasterId, 
                    depotId : rowform.depotId, 
                    tagTypeId : rowform.tagTypeId
                }
                updateTagMasterDetails(vm.custTagAssignDetails);
                //$scope.isValidate = true;
            }else{
                vm.custTagAssignDetails ={    
                    tagQtyAssignmentId : rowform.tagQtyAssignmentId, 
                    tagmasterId : rowform.tagmasterId, 
                    depotId : rowform.depotId, 
                    tagTypeId : rowform.tagTypeId
                }
                addCustomerAssignedTag(vm.custTagAssignDetails);
                //$scope.isValidate = true;
            }            
                           
        }
        
         // For Loading the gridcustTagAssign Widgets
         function mygridcustTagAssign(){ 
         var source =
         {
             localdata: $scope.customerTagDetails,
             datatype: "json",
             datafields:
             [

                { name: '_id', type: 'string' },
                { name: 'tagmasterId', type: 'string' },
                { name: 'tagmasterName', type: 'string' },
                { name: 'depotId', type: 'string' },
                { name: 'depotName', type: 'string' },
                { name: 'tagQtyAssignmentId', type: 'string' },
                { name: 'tagQtyAssignmentName', type: 'string' },
                { name: 'tagTypeId', type: 'string' },
                { name: 'tagTypeName', type: 'string' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#gridcustTagAssign").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Depot Name', datafield: 'depotName',align: 'center',cellsalign: 'center', width: '15%' },
                 { text: 'Tag Qty Assignment',hidden:false, datafield: 'tagQtyAssignmentName',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Tag Type Name',hidden:false, datafield: 'tagTypeName',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Tag Name',hidden:false, datafield: 'tagmasterName',align: 'center',cellsalign: 'center', width: '20%' },
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '20%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }
                 }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#gridcustTagAssign").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
            }
         }
         // For Edit Details
         function editDetails() { 
             var getselectedrowindexes = $('#gridcustTagAssign').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#gridcustTagAssign').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 

             $scope.vccUser={
                _id : selectedRowData._id,
                tagQtyAssignmentId : selectedRowData.tagQtyAssignmentId, 
                tagmasterId : selectedRowData.tagmasterId, 
                depotId : selectedRowData.depotId, 
                tagTypeId : selectedRowData.tagTypeId
             }
             $scope.openModal();
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#gridcustTagAssign').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#gridcustTagAssign').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTagMasterDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }     

         $scope.clearData = function() {
            $("#gridcustTagAssign").jqxGrid('clearselection');
            $scope.vccUser = {}            
         }

        $scope.openModal = function () {
            vm.ModalInstance=$uibModal.open({
                template: "<div ng-include src=\"'jqxwindowCustomerTagAssignedDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };
    }
  
})();
  
  