(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('CustomerTypesCtrl', CustomerTypesCtrl);
  
    /** @ngInject */
    function CustomerTypesCtrl($scope,customerService,$filter, editableOptions, editableThemes, toastr, $uibModal) {
        var vm = this;

        $scope.openModal = function () {
            vm.ModalInstance = $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowCustomerType.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };
        //jqxwindowViewCustomerType
        $scope.openModalView = function () {
            vm.ModalInstance = $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowViewCustomerType.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
            
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
            },
            backdrop: 'static',
            scope: $scope,
            })
        };
        vm.getAllCustomerTypeDetails = getAllCustomerTypeDetails;
        vm.addCustomerType = addCustomerType;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            customerTypeName:"",
            customerTypeDesc:""
       }
        initController();
        $scope.customers = [];
       var data=[];

        function initController() {
            getAllCustomerTypeDetails();
        }

        function addCustomerType() {            
            customerService.AddCustomerType(vm.customerTypeName, vm.customerTypeDesc, function (result) {
                if (result.success === true) { 
                    vm.customerTypeName="";
                    vm.customerTypeDesc="";
                    $scope.customers=[];
                    data=[];
                    getAllCustomerTypeDetails();
                    $scope.isValidate=true;
                    vm.ModalInstance.close();
                    toastr.success(result.message);
                } else {
                    $scope.isValidate =false;
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function deleteCustomerTypeDetails(_id) {            
            customerService.DeleteCustomerType(_id,function (result) {
                if (result.success === true) {
                    $scope.customers=[];
                    data=[];
                    toastr.success(result.message);
                    getAllCustomerTypeDetails();
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                
                }
            });
        };

        function updateCustomerTypeDetails() {            
            customerService.UpdateCustomerType(vm._id,vm.customerTypeName,vm.customerTypeDesc,function (result) {
                if (result.success === true) {
                    vm.customerTypeName="";
                    vm.customerTypeDesc=""; 
                    $scope.customers=[];
                    data=[];
                    getAllCustomerTypeDetails();
                    vm.ModalInstance.close();
                    $scope.isValidate = true;
                    toastr.success(result.message);
                } else {
                    $scope.isValidate =false
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function getAllCustomerTypeDetails() {            
            customerService.GetAllCustomerType(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.customerTypes.length;i++){
                        $scope.customers.push(result.customerTypes[i]);  
                        data.push(result.customerTypes[i]);
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        
        $scope.currRowData = function(rowform) {
             //for validation if data is blank or not.
             $scope.isValidate = false;
            if((rowform.customerTypeName=="")) {
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate = false;
                return;
            } else {
                //$scope.isValidate = true;
            }

            if(rowform._id == "" || rowform._id == undefined) {
                vm.customerTypeName = rowform.customerTypeName; 
                vm.customerTypeDesc = rowform.customerTypeDesc;
                addCustomerType();
                //$scope.isValidate = true;
                rowform ={};
            } else {
                vm._id = rowform._id; 
                vm.customerTypeName = rowform.customerTypeName; 
                vm.customerTypeDesc = rowform.customerTypeDesc; 
                updateCustomerTypeDetails();
                //$scope.isValidate = true;
                rowform ={};
            }               
        }
 
         // For Loading the Grid Widgets
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'customerTypeName', type: 'string' },
                    { name: 'customerTypeDesc', type: 'string' }
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#cusotmerTypegrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Customer Type Name', datafield: 'customerTypeName',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Customer Type Description', datafield: 'customerTypeDesc',align: 'center', width: '60%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '15%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        //return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary" disabled >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" disabled >Delete</button> </div>';
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary" >View </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" disabled >Delete</button> </div>';
                        }
                    }
                ]
            });
        // Initlize Button Event
            buttonsEvents();
            $("#cusotmerTypegrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }
         function buttonsEvents() 
         {
             // For Edit Details
             var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
               var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
               }
         }
         // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#cusotmerTypegrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            var selectedRowData = $('#cusotmerTypegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            $scope.vccUser = {
                _id : selectedRowData._id,
                customerTypeName:selectedRowData.customerTypeName,
                customerTypeDesc:selectedRowData.customerTypeDesc
            }
            $scope.openModalView();
        }  
         // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#cusotmerTypegrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
                selectedRowData = $('#cusotmerTypegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            
            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteCustomerTypeDetails(selectedRowData._id);
            }, function() {});
        }
     
        $("#cusotmerTypegrid").on("pagechanged", function (event) { 
            buttonsEvents();
        });
        $scope.clearData = function() {
            $("#cusotmerTypegrid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                customerTypeName:"",
                customerTypeDesc:""
            }
        }
    }
})();
  
  