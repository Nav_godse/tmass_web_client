(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('OprMasterCtrl', OprMasterCtrl);
  
    /** @ngInject */
    function OprMasterCtrl($uibModal,$scope,userService,$filter, editableOptions, editableThemes,toastr) {
        var vm = this;
        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'myModalContent.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
        
        vm.getAllOperationsMaster = getAllOperationsMaster;
        vm.addOperationMaster = addOperationMaster;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            operationName:"",
            operationNameToDisplay:"",
            isMenu:"",
            isScreen:""
       }
        initController();
        $scope.operationmasters = [];
        var data=[];
        
        function initController() { 
            getAllOperationsMaster(); 
        }

        function addOperationMaster() {            
            userService.AddOperationMaster(vm.operationName, vm.operationNameToDisplay, vm.isMenu,
                vm.isScreen, function (result) {
                if (result.success === true) {
                    vm.operationName= "";
                    vm.operationNameToDisplay= "";
                    vm.isMenu= "";
                    vm.isScreen="";
                    $scope.operationmasters= [];
                    data=[];
                    getAllOperationsMaster();
                    toastr.success(result.message);
                    //$("#jqxwindowOperations").jqxWindow('close');
                } else {
                    toastr.error(result.message);  
                    vm.error = result.message;                    
                }
            });
        };

        function getAllOperationsMaster() {            
            userService.GetAllOperationMaster(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        $scope.operationmasters.push(result.data[i]);    
                        data.push(result.data[i]);
                    }
                myGrid();
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };

        function deleteOperationMasterDetails(_id) {            
            userService.DeleteOperationMaster(_id,function (result) {
                if (result.success === true) {
                    $scope.operationmasters=[];
                    data=[];
                    getAllOperationsMaster();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); ;
                    vm.error = result.message;                    
                }
            });
        };

        function updateOperationMaster() {            
            userService.UpdateOperationMaster(vm._id, vm.operationName, vm.operationNameToDisplay, vm.isMenu,
                vm.isScreen,function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.operationName= "";
                    vm.operationNameToDisplay= "";
                    vm.isMenu= "";
                    vm.isScreen="";
                    $scope.operationmasters=[];
                    data=[];
                    getAllOperationsMaster();
                    toastr.success(result.message);
                    //$("#jqxwindowOperations").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            $scope.isValidate= false;
            if(rowform == undefined || rowform.operationName == '' || rowform.isScreen.toString() == "" ||rowform.operationNameToDisplay == ''||rowform.isMenu.toString() == ''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }
           
            if(rowform._id == "" || rowform._id == undefined){
                vm.operationName= rowform.operationName;
                vm.operationNameToDisplay= rowform.operationNameToDisplay;
                vm.isMenu= rowform.isMenu;
                vm.isScreen= rowform.isScreen; 
                addOperationMaster();
                $scope.clearData();
                rowform ={};
            }else{
                vm._id = rowform._id; 
                vm.operationName= rowform.operationName;
                vm.operationNameToDisplay= rowform.operationNameToDisplay;
                vm.isMenu= rowform.isMenu;
                vm.isScreen= rowform.isScreen; 
                updateOperationMaster();
                $scope.clearData();
                rowform ={};
            }               
        }
       // JQX Popup
    //   $("#jqxwindowOperations").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
    //   //$('.jqx-window-header').css("height", "40px");
    //   $("#popup").click(function () {
    //       $("#operationgrid").jqxGrid('clearselection');
    //         getselectedrowindexes =null;
    //         selectedRowData =null;
    //         $('#_id').val('');
    //         vm._id =''
    //       $("#jqxwindowOperations").jqxWindow('open');
    //   });
     
      
        // Display Data on Grid
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'operationName', type: 'string' },
                    { name: 'operationNameToDisplay', type: 'string' },
                    { name: 'isMenu', type: 'string' }, 
                    { name: 'isScreen', type: 'string' }
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#operationgrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                        { text: 'Name', datafield: 'operationName',align: 'center',cellsalign: 'center', width: '19%' },
                        { text: 'Name To Display', datafield: 'operationNameToDisplay',align: 'center',cellsalign: 'center', width: '19%' },
                        { text: 'Is Menu', datafield: 'isMenu',align: 'center',cellsalign: 'center', width: '15%' },
                        { text: 'Is Screen', datafield: 'isScreen',align: 'center',cellsalign: 'center', width: '15%' },
                        {
                            text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                            // render custom column.
                            return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button><input type="button" data-row="' + row + '" class="deleteButtons btn btn-danger" value="Delete" /> </div>';
                            }
                        }
                ]
            });

            // Initlize Button Event
            buttonsEvents();
        }
             
        
            function buttonsEvents() {
                // For Edit Details
                var editbuttons = document.getElementsByClassName('editButtons');
                for (var i = 0; i < editbuttons.length; i+=1) {
                    editbuttons[i].addEventListener('click', editDetails);
                }
                  // For Delete Details
                  var deletebuttons = document.getElementsByClassName('deleteButtons');
                  for (var i = 0; i < deletebuttons.length; i+=1) {
                    deletebuttons[i].addEventListener('click', deleteDetails);
                  }
             }

        
             function editDetails() { 
                var getselectedrowindexes = $('#operationgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                { 
                    var selectedRowData = $('#operationgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                //console.log("selectedRowData", selectedRowData);
                $scope.vccUser = {
                    _id : selectedRowData._id,
                    operationName:selectedRowData.operationName,
                    operationNameToDisplay:selectedRowData.operationNameToDisplay,
                    isMenu: selectedRowData.isMenu,
                    isScreen: selectedRowData.isScreen
                }
                $scope.openModal();
            }  
    
            function deleteDetails(){ 
                getselectedrowindexes = $('#operationgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                {
                    // returns the selected row's data.
                    selectedRowData = $('#operationgrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                
                alertify.confirm("Are you sure, you want to delete?", function () {
                    // user clicked "ok".
                    deleteOperationMasterDetails(selectedRowData._id);
                }, function() {
                    // user clicked "cancel"
                });
            }
    
            $("#operationgrid").on("pagechanged", function (event) { 
                buttonsEvents();
            });

        $scope.clearData = function(){ //window.location.reload();
            //console.log("clear");

            $("#operationgrid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                operationName:"",
                operationNameToDisplay:"",
                isMenu:"",
                isScreen:""
           }
        }
    }
  
})();
  
  