(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('UserDetailsCtrl', UserDetailsCtrl);
  
    /** @ngInject */
    function UserDetailsCtrl($uibModal,$scope,userService,roleService,customerService,depotService,toastr,localStorage,animation) {
        var vm = this;
        vm.isEdit=false;
        $scope.currloggedinUserroleName ='';

        var currloggedinUser = localStorage.getObject('dataUser');
        $scope.currloggedinUserroleName = currloggedinUser.otherData.roleName;
        console.log($scope.currloggedinUserroleName);
        $scope.orgEmail='';
        $scope.vccUser = {
            _id:'',
            customerId:'',
            customerName:'',
            username:'',
            password:'',
            roleId:'',
            roleName:'',
            email:'',
            alternativeEmail:'',
            mobile:'',
            depotId:'',
            depotName:'',
        };

        $scope.ph_numbr = /^\+?\d{10}$/;
        $scope.disabled = false;
        $scope.openUserDetailsModal = function () {
            console.log(vm.isEdit);
            if(vm.isEdit!=true){
                //alert("1");
                $scope.dropDowndownDepo=[];   
                getAllUserDetails();
            }
            //getAllUserDetails();

            vm.modalInstance =$uibModal.open({
                template: "<div ng-include src=\"'userDetailsPopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {

                    $uibModalInstance.rendered.then(function () {

                        
                    });
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    // if($scope.isValidate==true){
                    //     $uibModalInstance.close();
                    // }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                    $scope.dropDowndownDepo=[];
                    vm.isEdit=false;
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };
        
        function phoneno(){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }

        vm.getAllUserDetails = getAllUserDetails;
        vm.addUser = addUser;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.UserDetailsId='';
        initController();
        $scope.allUsers = [];
        $scope.allRoles = [];
        $scope.allCustomers = [];
        $scope.allDepo = [];  
        $scope.dropDowndownDepo=[];
        function initController() {
            getAllDepo();
            phoneno();
            GetroleLevelforAddingUser();            
            getAllCustomers();

            setTimeout(function(){
                getAllUserDetails();
            }, 1000);    
        }
        //wait me js
        function run_waitMe_body(){
            animation.myFunc('waitMe_ex3');
        }
        //end of wait mejs
        function addUser() {    
            vm.userPushToken = null;      
            run_waitMe_body();
            userService.AddUser(vm.name, vm.username, vm.roleId, vm.password, vm.email, vm.alternativeEmail, vm.mobile, vm.depotName, vm.userPushToken, function (result) {
                if (result.success === true) {
                    vm.name = "";
                    vm.username = "";
                    vm.roleId = "";
                    vm.password = "";
                    vm.email = "";
                    vm.alternativeEmail = "";
                    vm.mobile = "";
                    vm.depotName = "";
                    $scope.allUsers= [];
                    $('#waitMe_ex3').waitMe('hide');
                    $scope.clearData();
                    getAllUserDetails();
                    $scope.isValidate= true;
                    vm.modalInstance.close()
                    toastr.success(result.message);
                } else {
                    $('#waitMe_ex3').waitMe('hide');
                    if(result.message=="Verification link sending failed!"){
                        $scope.isValidate= true;
                        vm.modalInstance.close()
                    }else{
                        $scope.isValidate= false;
                    }
                    toastr.error(result.message); 
                    //$scope.isValidate= false;
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }
        $scope.resultTogrid=[];
        function getAllUserDetails() {     
            $scope.dropDowndownDepo=[];   
            userService.GetAllUser(function(result){
                $scope.resultTogrid= result.data;
                $scope.allUsers =[];
                if (result.success == true ) { 
                    if (result.data.length>0) {                  
                        console.log("result:",result);
                        for(var i=0;i<result.data.length;i++){

                            for(var j = 0; j < $scope.allRoles.length; j++) {
                                if(result.data[i].roleId===$scope.allRoles[j].value){
                                    result.data[i].roleName = $scope.allRoles[j].text;
                                    break;
                                }else{
                                    result.data[i].roleName = "None";
                                }
                            }
                            for(var k = 0; k < $scope.allCustomers.length; k++) {
                                if(result.data[i].customerId===$scope.allCustomers[k].value){
                                    result.data[i].customerName = $scope.allCustomers[k].text;
                                    break;
                                }else{
                                    result.data[i].customerName = "None";
                                }   
                            }
                            for(var d = 0; d < $scope.allDepo.length; d++) {
                                if(result.data[i].depotId===$scope.allDepo[d].value){
                                    result.data[i].depotName = $scope.allDepo[d].text;
                                    break;
                                }else{
                                    result.data[i].depotName = "None";
                                }   
                            }
                            if(result.data[i].roleName!="None"){
                                $scope.allUsers.push(result.data[i]);
                            }                                
                            console.log($scope.allUsers);
                        }
                        if($scope.resultTogrid.length!=0){
                            myfunct($scope.allDepo,$scope.allUsers)
                        }
                       // myGrid();
                    }else{
                        $scope.allUsers.length=0;
                    }
                    console.log($scope.allUsers, "Myusers data here");
                   // myGrid();
                } else {
                    toastr.error(result.message);
                    //console.log(result);
                    vm.error = result.message;                    
                }
                myGrid();
                //console.log("User", $scope.allUsers);
            })
        };
        function myfunct(arrayA,arrayB){
            console.log($scope.allUsers,$scope.allDepo)
            var arrayA=[];
            var arrayB=[]
            for(var x=0;x<$scope.allUsers.length;x++){
                arrayA.push($scope.allUsers[x].depotId);
            }
            for(var y=0;y<$scope.allDepo.length;y++){
                arrayB.push($scope.allDepo[y].value);
            }
            arrayB = arrayB.filter(function(val) {
                return arrayA.indexOf(val) == -1;
              });
            //arrayB = arrayB.filter(val => !arrayA.includes(val));
            console.log(arrayB);
            $scope.dropDowndownDepo=[];
            for(var w=0;w<arrayB.length;w++){
                for(var e=0;e<$scope.allDepo.length;e++){
                    if(arrayB[w]===$scope.allDepo[e].value){
                        $scope.dropDowndownDepo.push({value: $scope.allDepo[e].value, text: $scope.allDepo[e].text});  
                    }
                }
            }
            console.log($scope.dropDowndownDepo);
            if(currloggedinUser.otherData.roleName=="Client Admin" && vm.isEdit==false && $scope.dropDowndownDepo.length==0){
                alertify.alert("All Depots are Assigned, Please add more depots.");
                return;
            }
        }

        function deleteUserDetails(_id) {            
            run_waitMe_body();
            userService.DeleteUser(_id,function (result) {
                if (result.success === true) {
                    $('#waitMe_ex3').waitMe('hide');
                    $scope.allUsers=[];
                    getAllUserDetails();
                    $scope.clearData();
                    toastr.success(result.message);
                } else {
                    $('#waitMe_ex3').waitMe('hide');
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateUserDetails() {
            vm.userPushToken = null;       
            run_waitMe_body();
            vm.newEmail=false;
            if($scope.orgEmail!=vm.email){
                vm.newEmail== true;
            }
            userService.UpdateUser(vm.UserDetailsId, vm.name, vm.username, vm.roleId, vm.password, vm.email, vm.alternativeEmail, vm.mobile, vm.depotName, vm.userPushToken,vm.newEmail, function (result) {
                if (result.success === true) {
                    vm.name = "";
                    vm.username = "";
                    vm.roleId = "";
                    vm.password = "";
                    vm.email = "";
                    vm.alternativeEmail = "";
                    vm.mobile = "";
                    vm.depotName = "";
                    $scope.allUsers= [];
                    getAllUserDetails();
                    //vm.modalInstance.close()
                    $scope.clearData();
                    toastr.success(result.message);
                    $scope.isValidate= true; 
                    $('#waitMe_ex3').waitMe('hide');
                    vm.modalInstance.close()
                } else {
                    $('#waitMe_ex3').waitMe('hide');
                    toastr.error(result.message);
                    $scope.isValidate= false;
                    //console.log(result);              
                }
            });
        };

        function validateEmail(email){
            var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
            return re.test(email);
        }
        

        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            if(rowform.depotName=='' && currloggedinUser.otherData.roleName=="Client Admin"){
                alertify.alert("Please Add depot");
                //$scope.disableAddbutton=true;
                return;
            }
            var myMobileNo = $('#mobileNumber').val();
            console.log(rowform, myMobileNo);
            $scope.isValidate= false;
            if(rowform == undefined || rowform.name==''|| rowform.name==undefined ||rowform.email==undefined ||rowform.email==''
            ||rowform.roleId==undefined ||rowform.roleId=='' || rowform.mobile ==undefined || rowform.mobile ==''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            } 

            // if(rowform.email) {
            //     var set = validateEmail(rowform.email);
            //     if(set == true){
            //         return true;
            //     }
            //     else {
            //         toastr.error("Please Fill valid data in required fields");
            //         return;
            //     };
            // }

            else {
                
                if(rowform._id == "" || rowform._id == undefined) {
                    if(rowform.password == "" || rowform.password == undefined){
                        toastr.error("Please enter valid password");
                        $scope.isValidate= false;
                        return;
                    }else {
                        vm.customerId = rowform.customerId;
                        vm.name = rowform.name;
                        vm.username = rowform.email;
                        vm.roleId = rowform.roleId;
                        vm.password = rowform.password;
                        vm.email = rowform.email;
                        vm.alternativeEmail = rowform.email;
                        vm.mobile = rowform.mobile;
                        vm.depotName = rowform.depotName;
                        //$scope.isValidate= true;
                        addUser();
                        //$scope.clearData();
                    }
    
                } else {
                    vm.UserDetailsId = rowform._id;
                    vm.customerId = rowform.customerId;
                    vm.name = rowform.name;
                    vm.username = rowform.email;
                    vm.password = rowform.password;
                    vm.roleId = rowform.roleId;
                    vm.email = rowform.email;
                    vm.alternativeEmail = rowform.email;
                    vm.mobile = rowform.mobile;
                    vm.depotName = rowform.depotName;
                    //$scope.isValidate= true;
                    updateUserDetails();
                    //$scope.clearData();
                }  
            }         
        }
        $scope.disableAddbutton = false;

        function getAllSystemDepot(){
            depotService.GetAlldepot(function(result){
                if (result.success === true) { 
                    console.log(result.data);
                    for(var i=0; i<result.data.length; i++){
                        $scope.allDepo.push({value: result.data[i]._id, text: result.data[i].depotName});    
                    }
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            })
        }

        function getAllDepo () {
            if(currloggedinUser.otherData.roleName=="System Admin" || currloggedinUser.otherData.roleName=="OEM Admin" || currloggedinUser.otherData.roleName=="OEM Manager" || currloggedinUser.otherData.roleName=="System User" || currloggedinUser.otherData.roleName=="System Manager")
            { 
                $scope.allDepo=[];
                return;
            }
            if(currloggedinUser.otherData.roleName=="Client Manager"){
                depotService.GetAlldepotByCustomer(function(result){
                    if (result.success === true) { 
                        // if(result.data.length==0 && currloggedinUser.otherData.roleName=="Client Admin" ){
                        //     alertify.alert("Please Add depot before adding user");
                        //     $scope.disableAddbutton=true;
                        //     return;
                        // }    
                        console.log('result.data11111');
                        console.log(result.data);
                        for(var i=0; i<result.data.length; i++){
                         $scope.allDepo.push({value: result.data[i]._id, text: result.data[i].depotName});    
                        }
                    } else {
                        toastr.error(result.message);
                        vm.error = result.message;                    
                    }
                   // //console.log($scope.allDepo);
                });
            }else{
                depotService.GetAlldepotByCustomer(function(result){
                    $scope.dropDowndownDepo=[];
                    if (result.success === true) { 
                        if(result.data.length==0 && currloggedinUser.otherData.roleName=="Client Admin" ){
                            alertify.alert("Please Add depot before adding user");
                            $scope.disableAddbutton=true;
                            return;
                        }                        
                        console.log('result.data222222');
                        console.log(result.data);
                        for(var i=0; i<result.data.length; i++){
                            //$scope.allUsers
                            //for checking if users is already assigned depot
                            //for(var j=0; j<$scope.allUsers.length; j++){
                                //if($scope.allUsers[j].depotId==result.data.depotId)
                                //{
                                    $scope.allDepo.push({value: result.data[i]._id, text: result.data[i].depotName});   
                                    $scope.dropDowndownDepo.push({value: result.data[i]._id, text: result.data[i].depotName});   
                                //}
                            //}                            
                        }
                    } else {
                        toastr.error(result.message);
                        vm.error = result.message;                    
                    }
    
                   // //console.log($scope.allDepo);
                });
            }
        }

        $scope.getauthrolestoadduser = [];
        function GetroleLevelforAddingUser(){
            roleService.GetroleLevelforAddUser(function(result){
                if (result.success === true) { 
                    console.log(result.data[0].roleLevelUserAddition);
                    $scope.getauthrolestoadduser=result.data[0].roleLevelUserAddition;
                    getAllRoles();
                    // for(var i=0; i<result.data.length; i++){
                    // $scope.allDepo.push({value: result.data[i]._id, text: result.data[i].depotName});    
                    // }
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            // //console.log($scope.allDepo);
            });
        }

        function getAllRoles(){         
            roleService.GetAllRoles(function(result){
                console.log("roles", result);
                if (result.success === true) { 
                    for(var i=0;i<result.roleMasters.length;i++){
                        for(var j=0;j<$scope.getauthrolestoadduser.length;j++){
                            console.log(result.roleMasters[i].roleLevel, $scope.getauthrolestoadduser[j]);
                            if(result.roleMasters[i].roleLevel==$scope.getauthrolestoadduser[j])
                                $scope.allRoles.push({value: result.roleMasters[i]._id, text: result.roleMasters[i].roleName, roleType: result.roleMasters.roleType });
                        }                      
                    }
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };

        function getAllSystemRoles(){         
            roleService.GetAllRoles(function(result){
                console.log("roles", result);
                if (result.success === true) { 
                    for(var i=0;i<result.roleMasters.length;i++){
                        for(var j=0;j<$scope.getauthrolestoadduser.length;j++){
                                $scope.allRoles.push({value: result.roleMasters[i]._id, text: result.roleMasters[i].roleName, roleType: result.roleMasters.roleType });
                        }                      
                    }
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };

        function getAllCustomers () {
            customerService.GetAllCustomers (function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.customers.length;i++){
                        $scope.allCustomers.push({value: result.customers[i]._id, text: result.customers[i].customerName});    
                    }
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        }

        function myGrid(){ 
            var source = {
                localdata: $scope.allUsers,
                datatype: "array",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name:'customerId',type:'string' },
                    { name:'customerName',type:'string' },
                    { name: 'name', type: 'string' },
                    { name: 'username', type: 'string' },
                    { name: 'roleId', type: 'string' },
                    { name: 'roleName', type: 'string' },
                    { name: 'email', type: 'string' },
                    { name: 'alternativeEmail', type: 'string' },
                    { name: 'mobile', type: 'string' },
                    { name: 'depotId', type: 'string' },
                    { name: 'depotName', type: 'string' },
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);  
            if(currloggedinUser.otherData.roleName=="Client Admin" ||  currloggedinUser.otherData.roleName=="Client Manager")  {
                $("#usergrid").jqxGrid(
                    {
                        width: '100%',
                        theme: 'dark',
                        source: dataAdapter,
                        columnsresize: false,
                        //selectionmode: 'checkbox',
                        sortable: true,
                        pageable: true,
                        pagesize:5,
                        autoheight: true,
                        columnsheight: 60,
                        rowsheight: 50,
                     
                        columns: [
                            {
                                text: 'Sr No',sortable: false, filterable: false, editable: false,
                                groupable: false, draggable: false, resizable: false,
                                datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                                cellsrenderer: function (row, column, value) {
                                    return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                                }
                            },
                            { text: 'Name', datafield: 'name',align: 'center',cellsalign: 'center', width: '12%' },
                            { text: 'Userame',hidden:true, datafield: 'username',align: 'center',cellsalign: 'center', width: '10%' },
                            { text: 'roleId', hidden:true, datafield: 'roleId',align: 'center',cellsalign: 'center', width: '10%' },
                            { text: 'Role Name', datafield: 'roleName',align: 'center',cellsalign: 'center', width: '15%' },                 
                            { text: 'Customer Name', hidden:true, datafield: 'customerName',align: 'center',cellsalign: 'center', width: '10%' },
                            { text: 'Customer Id', hidden:true, datafield: 'customerId',align: 'center',cellsalign: 'center', width: '10%' },                 
                            { text: 'E-mail', datafield: 'email',align: 'center',cellsalign: 'center', width: '25%' },
                            { text: 'Alternative Email', hidden:true, datafield: 'alternativeEmail',align: 'center', width: '10%',cellsalign: 'center' },
                            { text: 'Mobile', datafield: 'mobile',align: 'center',cellsalign: 'center', width: '11%' },
                      
                            { text: 'depotId', hidden: true, datafield: 'depotId',align: 'center',cellsalign: 'center', width: '19%' },
                            { text: 'Depot Name',datafield: 'depotName',align: 'center',cellsalign: 'center', width: '12%' },                 
                         
                            {
                            text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                            // render custom column.
                            return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button><input type="button" data-row="' + row + '" class="deleteButtons btn btn-danger" value="Delete" /> </div>';
                            }
                        }
                        ]
                    });
            } else {
            $("#usergrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
             
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Name', datafield: 'name',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Userame',hidden:true, datafield: 'username',align: 'center',cellsalign: 'center', width: '10%' },
                    { text: 'roleId', hidden:true, datafield: 'roleId',align: 'center',cellsalign: 'center', width: '10%' },
                    { text: 'Role Name', datafield: 'roleName',align: 'center',cellsalign: 'center', width: '15%' },                 
                    { text: 'Customer Name', hidden:true, datafield: 'customerName',align: 'center',cellsalign: 'center', width: '10%' },
                    { text: 'Customer Id', hidden:true, datafield: 'customerId',align: 'center',cellsalign: 'center', width: '10%' },                 
                    { text: 'E-mail', datafield: 'email',align: 'center',cellsalign: 'center', width: '27%' },
                    { text: 'Alternative Email', hidden:true, datafield: 'alternativeEmail',align: 'center', width: '10%',cellsalign: 'center' },
                    { text: 'Mobile', datafield: 'mobile',align: 'center',cellsalign: 'center', width: '13%' },
  
                    { text: 'depotId', hidden: true, datafield: 'depotId',align: 'center',cellsalign: 'center', width: '19%' },
                    { text: 'Depot Name',hidden: true, datafield: 'depotName',align: 'center',cellsalign: 'center', width: '12%' },                 
                 
                    {
                    text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    // render custom column.
                    return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button><input type="button" data-row="' + row + '" class="deleteButtons btn btn-danger" value="Delete" /> </div>';
                    }
                }
                ]
            });
        }
            // Initlize Button Event
            buttonsEvents();
            $("#usergrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }

        function buttonsEvents() {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
        
         // For Edit Details
        function editDetails() { 
            $scope.dropDowndownDepo=[];
            vm.isEdit=true;
            var getselectedrowindexes = $('#usergrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
                var selectedRowData = $('#usergrid').jqxGrid('getrowdata', getselectedrowindexes[0]);

            $scope.disabled = true;
            $scope.orgEmail=selectedRowData.email;
            $scope.vccUser = {
                _id:selectedRowData._id,
                customerId:selectedRowData.customerId,
                customerName:selectedRowData.customerName,
                name:selectedRowData.name,
                username:selectedRowData.username,
                roleId:selectedRowData.roleId,
                roleName:selectedRowData.roleName,
                email:selectedRowData.email,
                alternativeEmail:selectedRowData.alternativeEmail,
                mobile:selectedRowData.mobile,
                depotId:selectedRowData.depotId,
                depotName:selectedRowData.depotName,
            };
            //$scope.dropDowndownDepo.push({value: selectedRowData.depotId, text: selectedRowData.depotName});  
            console.log($scope.vccUser);
            myfunct($scope.allDepo,$scope.allUsers);
           
            //getAllUserDetails();
            $scope.dropDowndownDepo.push({value: selectedRowData.depotId, text: selectedRowData.depotName});  
            $scope.openUserDetailsModal();
        }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#usergrid').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
                selectedRowData = $('#usergrid').jqxGrid('getrowdata', getselectedrowindexes[0]);

            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteUserDetails(selectedRowData._id);
            }, function() {});
             
         }     

        $scope.clearData = function() {
            $scope.disabled = false;
            $("#usergrid").jqxGrid('clearselection');
            $scope.vccUser = {}
        }
    }
  
})();
  
  