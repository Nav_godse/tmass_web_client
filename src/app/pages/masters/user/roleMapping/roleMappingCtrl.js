(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('RoleMapCtrl', RoleMapCtrl);
  
    /** @ngInject */
    function RoleMapCtrl($uibModal,$scope,roleService,userService,toastr) {
        var vm = this;

        $scope.openRoleMappingModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'roleMappingPopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',scope: $scope, 
            })
        };

        vm.getAllOperationsRoleMapping = getAllOperationsRoleMapping;
        vm.addOperationRoleMapping = addOperationRoleMapping;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        initController();
    
        $scope.operationRoleMap = [];
        $scope.allRoles = [];
        $scope.allUsers = [];
        $scope.allOperations = [];        

        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllRoles();
            getAllUsers();
            getAllOperations();
            setTimeout(function(){
                getAllOperationsRoleMapping();
            }, 500);            
        }

        function addOperationRoleMapping() {            
            userService.AddOperationRole(vm.userId, vm.roleId, vm.operationId, vm.viewFlag, vm.editFlag, vm.addFlag, 
                vm.deleteFlag, function (result) {
                if (result.success === true) {
                    vm.userId= "";
                    vm.roleId= "";
                    vm.operationId= "";
                    vm.viewFlag="";
                    vm.editFlag="";
                    vm.addFlag=""; 
                    vm.deleteFlag="";
                    $scope.operationRoleMap= [];
                    getAllOperationsRoleMapping();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            })
        }

        function getAllOperationsRoleMapping() {            
            userService.GetallOperationRole(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++) {
                        for(var j = 0; j < $scope.allRoles.length; j++) {
                            if(result.data[i].roleId===$scope.allRoles[j].value){
                                result.data[i].roleName = $scope.allRoles[j].text;
                                break;
                            }else{
                                result.data[i].roleName = "None";
                            }
                        }
                        for(var k = 0; k < $scope.allUsers.length; k++) {
                            if(result.data[i].userId===$scope.allUsers[k].value){
                                result.data[i].userName = $scope.allUsers[k].text;
                                break;
                            }else{
                                result.data[i].userName = "None";
                            }
                        }
                        for(var l = 0; l < $scope.allOperations.length; l++) {
                            if(result.data[i].operationId===$scope.allOperations[l].value){
                                result.data[i].operationName = $scope.allOperations[l].text;
                                break;
                            }else{
                                result.data[i].operationName = "None";
                            }
                        }
                        $scope.operationRoleMap.push(result.data[i]);    
                       // data.push(result.data[i]);
                    }
                    myGrid();                    
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteOperationMasterDetails(_id) {            
            userService.DeleteOperationRole(_id ,function (result) {
                if (result.success === true) {
                    $scope.operationRoleMap=[];
                    getAllOperationsRoleMapping();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); ;
                    vm.error = result.message;                    
                }
            });
        };

        function updateOperationRoleMaster() {            
            userService.UpdateOperationRole(vm._id, vm.userId, vm.roleId, vm.operationId, vm.viewFlag, vm.editFlag, vm.addFlag, 
                vm.deleteFlag, function (result) {
                if (result.success === true) {
                    vm._id= "";
                    vm.userId= "";
                    vm.roleId= "";
                    vm.operationId= "";
                    vm.viewFlag="";
                    vm.editFlag="";
                    vm.addFlag="";
                    vm.deleteFlag="";
                    $scope.operationRoleMap=[];
                    getAllOperationsRoleMapping();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);              
                }
            });
        };

        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.
            $scope.isValidate= false;
            if(rowform == undefined || rowform.userId == undefined || rowform.userId == '' ||rowform.roleId == undefined||rowform.operationId == undefined
            ||rowform.viewFlag == undefined || rowform.editFlag == undefined || rowform.addFlag == undefined || rowform.deleteFlag == undefined){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else {
                $scope.isValidate= true;
            }     
            //console.log(rowform);                

            if(rowform._id == "" || rowform._id == undefined){
                vm.userId= rowform.userId,
                vm.roleId= rowform.roleId,
                vm.operationId= rowform.operationId,
                vm.viewFlag= rowform.viewFlag,
                vm.editFlag= rowform.editFlag,
                vm.addFlag= rowform.addFlag,
                vm.deleteFlag= rowform.deleteFlag
                addOperationRoleMapping();
                $scope.clearData();
                rowform ={};
            }else{
                vm._id = rowform._id; 
                vm.userId= rowform.userId,
                vm.roleId= rowform.roleId,
                vm.operationId= rowform.operationId,
                vm.viewFlag= rowform.viewFlag,
                vm.editFlag= rowform.editFlag,
                vm.addFlag= rowform.addFlag,
                vm.deleteFlag= rowform.deleteFlag
                updateOperationRoleMaster();
                $scope.clearData();
                rowform ={};
            }
        }

        function getAllRoles(){
            roleService.GetAllRoles(function(result){
                ////console.log(result);
                if (result.success === true) { 
                    for(var i=0;i<result.roleMasters.length;i++){
                        $scope.allRoles.push({value: result.roleMasters[i]._id, text: result.roleMasters[i].roleName});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);
                    vm.error = result.message;                    
                }
                //console.log($scope.allRoles);
            });
        };

        function getAllUsers(){
            userService.GetAllUser(function(result){        
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.allUsers.push({value: result.data[i]._id, text: result.data[i].name});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }

        function getAllOperations () {
            userService.GetAllOperationMaster(function(result){
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.allOperations.push({value: result.data[i]._id, text: result.data[i].operationNameToDisplay});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);
                    vm.error = result.message;                    
                }
                //console.log("operation", $scope.allOperations);
            });
        }   
        
         // For Open Popup for Add and Edit
         
        //  $("#popup").click(function () {
        //      $("#roleMappinggrid").jqxGrid('clearselection');
        //      getselectedrowindexes =null;
        //      selectedRowData =null;
        //      $('#_id').val('');
        //      vm._id ='';
        //      //$('#viewFlag select:first').focus();
        //     //  $("#jqxwindowRoleMapping").jqxWindow('focus');
        //     //  $("#jqxwindowRoleMapping").jqxWindow('open');

        //      //$("#jqxwindowRoleMapping").jqxWindow('focus');
        //  });
        //  $("#jqxwindowRoleMapping").bind('close', function (event){
        //     $scope.clearData();
        // });
         // For Loading the Grid Widgets
        function myGrid() { 
            var source = {
                localdata: $scope.operationRoleMap,
                datatype: "json",
                datafields:
                [
                { name: '_id', type: 'string' },
                { name: 'userId', type: 'string' },
                { name: 'userName', type: 'string' },
                { name: 'roleId', type: 'string' },
                { name: 'roleName', type: 'string' },
                { name: 'operationId', type: 'string' },
                { name: 'operationName', type: 'string' },
                { name: 'viewFlag', type: 'string' },
                { name: 'editFlag', type: 'string' },
                { name: 'addFlag', type: 'string' },
                { name: 'deleteFlag', type: 'string' }
                ]
            };
            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#roleMappinggrid").jqxGrid({
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '10%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'User Name', datafield: 'userName',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Role Name', datafield: 'roleName',align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Operation Name', datafield: 'operationName',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'viewFlag', hidden:true, datafield: 'viewFlag',align: 'center',cellsalign: 'center', width: '5%' },
                    { text: 'editFlag', hidden:true, datafield: 'editFlag',align: 'center', width: '5%',cellsalign: 'center' },
                    { text: 'addFlag', hidden:true, datafield: 'addFlag',align: 'center',cellsalign: 'center', width: '5%' },
                    { text: 'deleteFlag', hidden:true, datafield: 'deleteFlag',align: 'center',cellsalign: 'center', width: '5%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '20%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
            $("#roleMappinggrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }

        function buttonsEvents() {            
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
         // For Edit Details
        function editDetails() { 
            var getselectedrowindexes = $('#roleMappinggrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
                var selectedRowData = $('#roleMappinggrid').jqxGrid('getrowdata', getselectedrowindexes[0]); 
            $scope.vccUser = {
            _id : selectedRowData._id,
            userId:selectedRowData.userId,
            roleId:selectedRowData.roleId,
            operationId:selectedRowData.operationId,
            viewFlag:selectedRowData.viewFlag,
            editFlag:selectedRowData.editFlag,
            addFlag:selectedRowData.addFlag,
            deleteFlag:selectedRowData.deleteFlag
            };
            $scope.openRoleMappingModal(); 
        }  
         // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#roleMappinggrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
                selectedRowData = $('#roleMappinggrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteOperationMasterDetails(selectedRowData._id);
            }, function() {});
        }     

        $scope.clearData = function(){
            $scope.vccUser = {}
        }
    }
  
})();
  