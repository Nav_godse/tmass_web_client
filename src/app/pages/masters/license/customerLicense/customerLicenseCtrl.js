(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('CusotmerLicenseCtrl', CusotmerLicenseCtrl);
  
        
    /** @ngInject */
    function CusotmerLicenseCtrl($uibModal,$scope,customerService,$filter, localStorage, editableOptions, editableThemes, toastr) {
        var vm = this;
        $scope.pinMsg= '';
        $scope.gists = ['System Admin', 'System Super Admin','OEM Admin'];

        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.custTypes= [];
        $scope.licenseTypes= [];
        $scope.customers= [];
        $scope.customerLicense = [];
        initController();
        //from state,city and country
        $scope.vccUser = {};

        function initController() {
            getAlldropdowns();
        }

        function getAlldropdowns() {            
            getAllLicenseTypeDetails();
            getAllCustomerDetails();
            setTimeout(function(){
                getallCustomerLicenseDetail();
            }, 1000);

        }

        //config details for the page
        //$scope.minDate = new Date().toString(); // If you want to set current date
        $scope.maxDate = new Date().toDateString();
        //to set todays date in the datepicker
        $scope.todaysDate= new Date().toDateString();

        var user = localStorage.getObject('dataUser');        
        //to get customer GetAllCustomers
        function getAllCustomerDetails() {
            customerService.GetAllCustomers(function (result) {
                console.log(result);
                if (result.success === true) {
                    for(var i=0;i<result.customers.length;i++){
                        if(result.customers[i]._id != user.otherData._id)
                            if(result.customers[i].roleLevel != user.otherData.roleLevel)
                                $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].email});
                    }
                }else{
                    vm.error = result.message;
                }
            })
        }

        function getallCustomerLicenseDetail() {
            customerService.GetallCustomerLicenseDetail(function (result) {
                $scope.customerLicense=[];
                if (result.success === true) {
                    if(result.data.length>0){
                        for(var i=0;i<result.data.length;i++) {
                            for(var j=0;j<$scope.customers.length;j++){
                                if(result.data[i].customerId===$scope.customers[j].value){
                                    result.data[i].customerName = $scope.customers[j].text;
                                    break;
                                }else{
                                    result.data[i].customerName = "None";
                                }
                            }
                            for(var j=0;j<$scope.licenseTypes.length;j++){
                                if(result.data[i].licenseTypeId===$scope.licenseTypes[j].value){
                                    result.data[i].licenseTypeName = $scope.licenseTypes[j].text;
                                    break;
                                }else{
                                    result.data[i].licenseTypeName = "None";
                                }
                            }
                            $scope.customerLicense.push(result.data[i]);
                        }
                    }else{
                        $scope.customerLicense.length = 0;
                    }
                    
                    myCustLicesnseGrid();
                } else {
                    vm.error = result.message;
                }
            })
        }


        //To Add Edit and delete customer
        function addCustomerLicenseDetail() { 
            console.log($scope.vccUser);          
            customerService.AddCustomerLicenseDetail($scope.vccUser, function (result) {
                if (result.success === true) { 
                    $scope.vccUser={};
                    $scope.custTypes= [];
                    $scope.licenseTypes= [];
                    $scope.customers= [];
                    $scope.customerLicense = [];
                    getAlldropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function deleteCustomerLicense(_id) {            
            customerService.DeleteCustomerLicense(_id,function (result) {
                if (result.success === true) {
                    $scope.custTypes= [];
                    $scope.licenseTypes= [];
                    $scope.customers= [];
                    $scope.customerLicense = [];
                    toastr.success(result.message);
                    getAlldropdowns();
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                
                }
            });
        };

        function updateCustomerLicenseDetail(data) { 
            //console.log("data",data)           
            customerService.UpdateCustomerLicenseDetail(data,function (result) {
                if (result.success === true) {
                    $scope.custTypes= [];
                    $scope.licenseTypes= [];
                    $scope.customers= [];
                    $scope.customerLicense = [];
                    getAlldropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        //End of Add ,Edit and delete

        //to get license types
        function getAllLicenseTypeDetails() {
            customerService.GetAllLicenseType(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.licenseTypes.length;i++){
                        $scope.licenseTypes.push({value: result.licenseTypes[i]._id, text: result.licenseTypes[i].licenseTypeName});
                    }
                } else {
                    vm.error = result.message;
                }
            })
        }

        $scope.currRowData = function(rowform) {
            console.log(rowform);
            $scope.isValidate = false;            
            if (rowform == undefined || rowform.customerId == "" || rowform.customerId == undefined || rowform.licenseTypeId == undefined || 
            rowform.licenseTypeId == "" ||isNaN(rowform.tireQty) || parseInt(rowform.tireQty) < 0 || rowform.tireQty == null  || 
            parseInt(rowform.vehicleQty) < 0 || isNaN(rowform.vehicleQty) || rowform.vehicleQty == null || rowform.licenseDate == undefined || rowform.licenseDate == "" ) {
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return ;
            }
            if(rowform._id == "" || rowform._id == undefined){
                $scope.isValidate = true;
                addCustomerLicenseDetail();                
            }else{
                $scope.isValidate = true;
                updateCustomerLicenseDetail(rowform)
            }                               
        }
        /////////////////////////////////////////////
        function myCustLicesnseGrid(){ 
            var source =
            {
                localdata: $scope.customerLicense,
                datatype: "array",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'licenseDate', type: 'date' },
                    { name: 'customerName', type: 'string' },
                    { name: 'customerId', type:'string'},
                    { name: 'licenseTypeId', type:'string'},
                    { name: 'licenseTypeName', type:'string'},
                    { name: 'tireQty', type:'number'},
                    { name: 'vehicleQty', type:'number'}
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#custlicensegrid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'License Date', hidden: false, datafield: 'licenseDate',align: 'center',cellsalign: 'center', width: '25%',cellsformat: 'dd-MMM-yyyy' },
                    { text: 'Customer', datafield: 'customerName',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Customer Id', hidden:true, datafield: 'customerId',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'License Type', datafield: 'licenseTypeName',align: 'center',cellsalign: 'center', width: '10%' },
                    { text: 'Tire Qty', datafield: 'tireQty',align: 'center',cellsalign: 'center', width: '10%' },
                    { text: 'Vehicle Qty', datafield: 'vehicleQty',align: 'center',cellsalign: 'center', width: '10%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"   >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });

            $("#custlicensegrid").on("pagechanged filter sort scroll pagesizechanged", function (event) {
                buttonsEvents();
            });
            // Initlize Button Event
            buttonsEvents();
        }
      
        function buttonsEvents() {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
         }

        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            //$scope.globalStateId = $scope.vccUser.stateId;
            var getselectedrowindexes = $('#custlicensegrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#custlicensegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id: selectedRowData._id,
                customerName: selectedRowData.customerName,
                customerId: selectedRowData.customerId,
                licenseTypeId: selectedRowData.licenseTypeId,
                tireQty: selectedRowData.tireQty,
                vehicleQty: selectedRowData.vehicleQty,
            }
            $scope.openModal();
        }  

        function deleteDetails(){
            getselectedrowindexes = $('#custlicensegrid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
                selectedRowData = $('#custlicensegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteCustomerLicense(selectedRowData._id);
            }, function() {});
        }

        $scope.clearData = function() {
            $("#custlicensegrid").jqxGrid('clearselection');
            $scope.pinMsg= '';
            $scope.vccUser = {};
        }

        //for popup modal form
        $scope.openModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowCustomerlicense.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };
    }
  
})();