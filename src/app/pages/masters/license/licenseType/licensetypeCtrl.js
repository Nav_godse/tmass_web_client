(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('LicenseTypesCtrl', LicenseTypesCtrl);
  
        
    /** @ngInject */
    function LicenseTypesCtrl($uibModal,$scope,customerService,$filter, editableOptions, editableThemes, toastr) {
        var vm = this;

        $scope.openLicenseTypeModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'licenceTypePopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope, 
            })
        };

        vm.getAllLicenseTypeDetails = getAllLicenseTypeDetails;
        vm.addLicenseType = addLicenseType;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            licenseTypeName:"",
            licenseTypeDesc:""
       }
        initController();
        $scope.licenses = [];
        var data = [];

        function initController() {
            getAllLicenseTypeDetails();
        }

        function addLicenseType() {            
            customerService.AddLicenseType(vm.licenseTypeName, vm.licenseTypeDesc, function (result) {
                if (result.success === true) { 
                    vm.licenseTypeName="";
                    vm.licenseTypeDesc="";
                    $scope.licenses=[];
                    data=[];
                    getAllLicenseTypeDetails();
                    toastr.success(result.message);
                   // $("#jqxwindowLicenseType").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function getAllLicenseTypeDetails() {            
            customerService.GetAllLicenseType(function (result) {
                data = [];
                $scope.licenses = [];
                if (result.success === true) { 
                    if(result.licenseTypes.length>0){
                        for(var i=0;i<result.licenseTypes.length;i++){
                            $scope.licenses.push(result.licenseTypes[i]);  
                            // data.push(result.licenseTypes[i]);  
                        }
                    }else{
                        $scope.licenses.length = 0;
                    }
                     
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function deleteLicenseTypeDetails(_id) {            
            customerService.DeleteLicenseType(_id,function (result) {
                if (result.success === true) { 
                    console.log( vm.userLicenseType);
                    $scope.licenses=[];
                    data=[];
                    getAllLicenseTypeDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function updateLicenseTypeDetails() {            
            customerService.UpdateLicenseType(vm._id,vm.licenseTypeName,vm.licenseTypeDesc,function (result) {
                if (result.success === true) {
                    vm.licenseTypeName="";
                    vm.licenseTypeDesc=""; 
                    $scope.licenses=[];
                    data=[];
                    getAllLicenseTypeDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowLicenseType").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
             //for validation if data is blank or not.
             $scope.isValidate= false;
             if(rowform==undefined || rowform=={} ||(rowform.licenseTypeName=="" || rowform.licenseTypeName==undefined)){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            } else {
                $scope.isValidate= true;
                if(rowform._id == ""||rowform._id == undefined){
                    vm.licenseTypeName = rowform.licenseTypeName; 
                    vm.licenseTypeDesc = rowform.licenseTypeDesc;
                    addLicenseType();
                    $scope.clearData();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.licenseTypeName = rowform.licenseTypeName; 
                    vm.licenseTypeDesc = rowform.licenseTypeDesc;
                    updateLicenseTypeDetails()
                    $scope.clearData();
                    rowform ={};
                }               
            }

        }
           // For Open Popup for Add and Edit
        //    $("#jqxwindowLicenseType").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        //    $("#popup").click(function () {
        //        $("#licensegrid").jqxGrid('clearselection');
        //        getselectedrowindexes =null;
        //        selectedRowData =null;
        //        $('#_id').val('');
        //        vm._id =''
        //        $("#jqxwindowLicenseType").jqxWindow('open');
        //    });
        //    $("#jqxwindowLicenseType").bind('close', function (event){
        //         $scope.clearData();
        //     });
        //    $("#button_input").click(function () {
        //        var T = $("#text_input").val();
        //        $("#textbox").text(T);
        //    });
        //    $("#button_no").click(function () {
        //        $("#jqxwindowLicenseType").jqxWindow('close');
        //    });
   
           // For Loading the Grid Widgets
           function myGrid(){ 
           var source =
           {
               localdata: $scope.licenses,
               datatype: "json",
               datafields:
               [
                   { name: '_id', type: 'string' },
                   { name: 'licenseTypeName', type: 'string' },
                   { name: 'licenseTypeDesc', type: 'string' }
               ]
           };
   
           var dataAdapter = new $.jqx.dataAdapter(source);         
           $("#licensegrid").jqxGrid(
           {
               width: '100%',
               theme: 'dark',
               source: dataAdapter,
               columnsresize: false,
               //selectionmode: 'checkbox',
               sortable: true,
               pageable: true,
               pagesize:5,
               autoheight: true,
               columnsheight: 60,
               rowsheight: 50,
               columns: [
                   {
                       text: 'Sr No',sortable: false, filterable: false, editable: false,
                       groupable: false, draggable: false, resizable: false,
                       datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '10%' ,
                       cellsrenderer: function (row, column, value) {
                           return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                       }
                   },
                   { text: 'License Type Name', datafield: 'licenseTypeName',align: 'center',cellsalign: 'center', width: '25%' },
                   { text: 'License Type Description', datafield: 'licenseTypeDesc',align: 'center', cellsalign: 'center', width: '45%' },
                   {
                       text: 'Action', cellsAlign: 'center', align: "center",width: '20%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                       // render custom column.
                       return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary" disabled >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" disabled >Delete</button> </div>';
                       }
                   }
               ]
           });
           // Initlize Button Event
           buttonsEvents();
           }
           function buttonsEvents() 
           {
               // For Edit Details
               var editbuttons = document.getElementsByClassName('editButtons');
               for (var i = 0; i < editbuttons.length; i+=1) {
                   editbuttons[i].addEventListener('click', editDetails);
               }
                 // For Delete Details
                 var deletebuttons = document.getElementsByClassName('deleteButtons');
                 for (var i = 0; i < deletebuttons.length; i+=1) {
                   deletebuttons[i].addEventListener('click', deleteDetails);
                 }
           }
           // For Edit Details
           function editDetails() { 
               var id = this.getAttribute("data-row"); 
               var getselectedrowindexes = $('#licensegrid').jqxGrid('getselectedrowindexes');
               if (getselectedrowindexes.length > 0)
               { 
                   var selectedRowData = $('#licensegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
               } 
               $scope.vccUser = {
                   _id : selectedRowData._id,
                   licenseTypeName:selectedRowData.licenseTypeName,
                   licenseTypeDesc:selectedRowData.licenseTypeDesc
               }
               $scope.openLicenseTypeModal();
           }  
           // For Delete Details
           function deleteDetails(){
               getselectedrowindexes = $('#licensegrid').jqxGrid('getselectedrowindexes');
               if (getselectedrowindexes.length > 0)
               {
                   selectedRowData = $('#licensegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
               } 
                // confirm dialog
                alertify.confirm("Are you sure, you want to delete?", function () {
                    // user clicked "ok".
                    deleteLicenseTypeDetails(selectedRowData._id);
                }, function() {
                // user clicked "cancel"
                });
               
           }
       
           $("#licensegrid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
               buttonsEvents();
           });
           $scope.clearData = function(){ //window.location.reload();
               console.log("hello");
               $scope.vccUser = {
                   _id:"",
                   licenseTypeName:"",
                   licenseTypeDesc:""
               }
           }
      }
})();
  
  