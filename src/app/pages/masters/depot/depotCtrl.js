(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('DepotCtrl', DepotCtrl);
  
    /** @ngInject */
    function DepotCtrl($scope,customerService,depotService,toastr,localStorage,$uibModal,$http,config,animation) {
        var vm = this;
        //vm.getAllTagMasterDetails = getAllTagMasterDetails;
        var currloggedinUser = localStorage.getObject('dataUser');
        //console.log("currloggedinUser", currloggedinUser);
        vm.currloggedinUserRole = currloggedinUser.otherData.roleName;
        $scope.showCustIdFeild = false;
        if(vm.currloggedinUserRole == 'System Super Admin' || vm.currloggedinUserRole == 'System Admin' ){
            //$scope.vccUser.customerId = currloggedinUser.otherData._id;
            $scope.showCustIdFeild = true;
        } 
        vm.addCustomerAssignedTag = addCustomerAssignedTag;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm._id='';
        $scope.vccUser = {}   
        initController();
        $scope.customerDepotData = [];
        $scope.customers= [];
        $scope.tagTypes= [];
        $scope.tagTypes= [];
        $scope.customerDepot= [];
        $scope.cities=[];
        $scope.states=[];
        function initController() {
           // getAllCountryStateandCity();
            getAllDropdowns();
        }

        function getAllDropdowns() {
            // getAllCities();
            // getAllStates();
            getAllCustomerDetails();
            setTimeout(function(){
                getAllCustomerDepotDetails();
            }, 1000);          
        }

        //to get all Depot details.
        $scope.isDisable = false;
        function getAllCustomerDetails() { 
            $scope.customers = [];           
            // if(currloggedinUser.otherData.roleName === 'System Manager' || currloggedinUser.otherData.roleName === 'OEM Manager' || currloggedinUser.otherData.roleName === 'Client Manager'){
            //     //
            //     $scope.customers.push({value: (currloggedinUser.otherData._id).toString(), text: (currloggedinUser.otherData.name).toString()});
            //     $scope.vccUser.customerId = currloggedinUser.otherData._id;
            //     $scope.isDisable = true;
            //     return
            // }else{
                customerService.GetallTransporters(function (result) {
                    console.log(result);
                    if (result.success === true) {
                        if(currloggedinUser.otherData.roleName === 'System Super Admin' || currloggedinUser.otherData.roleName === 'System Admin') {
                            $scope.isDisable = false;
                            for(var i=0;i<result.customers.length;i++) {
                                //if(result.customers[i]._id !== currloggedinUser.otherData._id ) // if customer is not same as logged in user
                                    //if(result.customers[i]._id !== currloggedinUser.otherData.createdByCustomerId)  // if customer is not parent
                                       //if(result.customers[i].createdByCustomerId == currloggedinUser.otherData._id )
                                            $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].customerName, text2: result.customers[i].email});
                            }
                        } else if(currloggedinUser.otherData.roleName === 'System Manager' || currloggedinUser.otherData.roleName === 'OEM Manager' || currloggedinUser.otherData.roleName === 'Client Manager'){
                            $scope.customers.push({value: (currloggedinUser.otherData._id).toString(), text: (currloggedinUser.otherData.name).toString()});
                            $scope.vccUser.customerId = currloggedinUser.otherData._id;
                            $scope.isDisable = true;
                            return
                        } else {
                            for(var i=0;i<result.customers.length;i++){
                                if(result.customers[i]._id == currloggedinUser.otherData._id ){
                                    $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].customerName});
                                    $scope.vccUser.customerId = currloggedinUser.otherData._id;
                                    $scope.isDisable = true;
                                }
                            }
                        }
    
                        console.log($scope.customers);
                    } else {
                        vm.error = result.message;
                    }
                })
            //}
        }

        function getAllCustomerDepotDetails() {            
            depotService.GetAlldepotByCustomer(function (result) {
                $scope.customerDepot=[];
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                     $scope.customerDepot.push({value: result.data[i]._id, text: result.data[i].depotName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);              
                }
            })
        }
        
        //end all loops
        function getAllCustomerDepotDetails() {            
            depotService.GetAlldepotByCustomer(function (result) {
                console.log(result);
                $scope.customerDepotData=[];
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.customers.length;j++){
                            if(result.data[i].customerId===$scope.customers[j].value){
                                result.data[i].customerName = $scope.customers[j].text;
                                break;
                            }else{
                                result.data[i].customerName = "None";
                            }
                        }
                        // for(var k=0;k<$scope.states.length;k++){
                        //     if(result.data[i].stateId===$scope.states[k].value){
                        //         result.data[i].stateName = $scope.states[k].text;
                        //         break;
                        //     }else{
                        //         result.data[i].stateName = "None";
                        //     } 
                        // }
                        $scope.customerDepotData.push(result.data[i]);    
                    }
                    mygridDepot();
                } else {
                    toastr.error(result.message);       
                }
            });
        };

        function deleteTagMasterDetails(_id) {            
            depotService.Deletedepot(_id,function (result) {
                if (result.success === true) {
                    $scope.customerDepotData=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //console.log(result);                
                }
            });
        };

        function updateTagMasterDetails(data) {            
            depotService.Updatedepot(data,function (result) {
                if (result.success === true) {
                    vm.custTagAssignDetails = {};
                    // vm._id= "";
                    // vm.depotName="";
                    // vm.addressLine1="";
                    // vm.addressLine2="";
                    // vm.pincode="";
                    // vm.cityId="";
                    // vm.stateId="";
                    // vm.countryName="";
                    // vm.latitude="";
                    // vm.logitude="";
                    $scope.customerDepotData=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.clearData();
                    $scope.isValidate= true;
                    vm.modalInstance.close()
                    //$("#jqxwindowDepot").jqxWindow('close'); 
                } else {
                    $scope.isValidate= false;
                    toastr.error(result.message);
                    //console.log(result);              
                }
            });
        };

        function addCustomerAssignedTag(data) {            
            depotService.Adddepot(data, function (result) {
                if (result.success === true) {
  
                    vm.custTagAssignDetails = {};
                    $scope.vccUser={};
                    $scope.customerDepotData= [];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.clearData();
                    $scope.isValidate= true;
                    vm.modalInstance.close()
                    //$("#jqxwindowDepot").jqxWindow('close');
                } else {
                    $scope.isValidate= false;
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            })
        }
        //adddepot in bulk
        function AdddepotinBulk(data){
            animation.myFunc('depotBulkUpload');
            depotService.AdddepotinBulk(data, function (result) {
                if (result.success === true) {
                    vm.custTagAssignDetails = {};
                    $scope.vccUser={};
                    $scope.customerDepotData= [];
                    getAllDropdowns();
                    toastr.success(result.message);
                    $scope.clearData();
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    getAllDropdowns();
                    vm.error = result.message;                    
                }
            })
        }
        
        $scope.currRowData = function(rowform) {
            console.log(rowform);
            //return;
            $scope.currCustId = currloggedinUser.otherData._id;
             $scope.isValidate= false;
             if(vm.currloggedinUserRole == 'System Super Admin' || vm.currloggedinUserRole == 'System Admin' ){
                //$scope.vccUser.customerId = currloggedinUser.otherData._id;
                if(rowform.customerId==undefined){
                    alertify.alert("Please select the Customer name to assign the depot.");
                    return
                }
            } 

            if(rowform.pincode){
                var validData= valid();
                if(validData == false){
                    $scope.isValidate= false;
                    toastr.error("Please Enter valid Pincode");
                    return;
                }
            }
            if(rowform.latitude){
                var isValid = validateLatitude();
                if(isValid == false){
                    $scope.isValidate = false;
                    toastr.error("Please Enter valid Latitude");
                    return;
                }
            }
            if(rowform.logitude){
                var isValid = validateLongitude();
                if(isValid == false){
                    $scope.isValidate = false;
                    toastr.error("Please Enter Valid Longitude");
                    return;
                }
            }  
            if(rowform=={} || rowform==undefined || rowform.depotName == undefined || rowform.depotName == "" || rowform.countryName == undefined || rowform.countryName == "" || rowform.stateId == undefined || rowform.stateId == "" || rowform.cityId == undefined || rowform.cityId == ""){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            } else {
                //$scope.isValidate= true;
                
                if(rowform.customerId==undefined){
                    $scope.currCustId=currloggedinUser.otherData._id;
                }else{
                    $scope.currCustId=rowform.customerId[0].value;
                }
                if(rowform._id == "" || rowform._id == undefined) {
                    vm.custTagAssignDetails ={
                        customerId: $scope.currCustId,  
                        depotName: rowform.depotName, 
                        addressLine1: rowform.addressLine1, 
                        addressLine2: rowform.addressLine2, 
                        pincode: $('#pincode').val(), 
                        countryName: rowform.countryName,
                        cityId: rowform.cityId, 
                        stateId: rowform.stateId, 
                        latitude: rowform.latitude, 
                        logitude: rowform.logitude
                    }
                    addCustomerAssignedTag(vm.custTagAssignDetails);
                } else {
                    vm.custTagAssignDetails ={
                        _id : rowform._id,
                        customerId: $scope.currCustId,  
                        depotName: rowform.depotName, 
                        addressLine1: rowform.addressLine1, 
                        addressLine2: rowform.addressLine2, 
                        pincode: $('#pincode').val(),
                        cityId: rowform.cityId, 
                        stateId: rowform.stateId, 
                        countryName: rowform.countryName, 
                        latitude: rowform.latitude, 
                        logitude: rowform.logitude
                    }
                    updateTagMasterDetails(vm.custTagAssignDetails);
                }
            }
        }
        
        function valid() {
            var pin_code=document.getElementById("pincode");
            var pat1=/^\d{6}$/;
            if(!pat1.test(pin_code.value)) {
                $scope.pinMsg = "Pin code should be 6 digits";
                pin_code.focus();
                return false;
            } else {
                return true;
            }
        }

        function validateLatitude() {
            var latitude=document.getElementById("latitude");
            var pat1=/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
            if(!pat1.test(latitude.value)) {   
                console.log("wrong latitude");
                latitude.focus();
                return false;
            } else {
                return true;
            }
        }

        function validateLongitude() {
            var logitude=document.getElementById("logitude");
            var pat1=/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;
            if(!pat1.test(logitude.value)) {   
                console.log("wrong Longitude");
                logitude.focus();
                return false;
            } else {
                return true;
            }
        }
         // For Open Popup for Add and Edit
         // For Loading the gridDepot Widgets
         function mygridDepot(){ 
         var source =
         {
             localdata: $scope.customerDepotData,
             datatype: "json",
             datafields:
             [
                { name: '_id', type: 'string' },
                { name: 'customerId', type: 'string'},
                { name: 'depotName', type: 'string' },
                { name: 'addressLine1', type: 'string' },
                { name: 'addressLine2', type: 'string' },
                { name: 'pincode', type: 'number' },
                { name: 'country', type: 'string' },
                { name: 'cityId', type: 'string' },
                { name: 'stateId', type: 'string' },
                { name: 'countryName', type: 'string' },
                { name: 'latitude', type: 'number' },
                { name: 'logitude', type: 'number' }
             ]
         };
 
         var dataAdapter = new $.jqx.dataAdapter(source);         
         $("#gridDepot").jqxGrid(
         {
             width: '100%',
             theme: 'dark',
             source: dataAdapter,
             columnsresize: false,
             //selectionmode: 'checkbox',
             sortable: true,
             pageable: true,
             pagesize:5,
             autoheight: true,
             columnsheight: 60,
             rowsheight: 50,
             columns: [
                 {
                     text: 'Sr No',sortable: false, filterable: false, editable: false,
                     groupable: false, draggable: false, resizable: false,
                     datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                     cellsrenderer: function (row, column, value) {
                         return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                     }
                 },
                 { text: 'Depot Name', datafield: 'depotName',align: 'center',cellsalign: 'center', width: '15%' },
                 { text: 'Pincode',hidden:false, datafield: 'pincode',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Address Line 1',hidden:false, datafield: 'addressLine1',align: 'center',cellsalign: 'center', width: '20%' },
                 { text: 'Address Line 2',hidden:false, datafield: 'addressLine2',align: 'center',cellsalign: 'center', width: '20%' },
                 {
                     text: 'Action', cellsAlign: 'center', align: "center",width: '20%', filterable: false, editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                     // render custom column.
                     //check the role here.
                     if(vm.currloggedinUserRole == 'System Super Admin' || vm.currloggedinUserRole == 'System Admin' ){
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary" disabled >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     }else{
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                     } 
                        
                     }
                 }
             ]
         });
            // Initlize Button Event
            buttonsEvents();
            $("#gridDepot").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
         }

         function buttonsEvents() 
         {            
             // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
             for (var i = 0; i < editbuttons.length; i+=1) {
                 editbuttons[i].addEventListener('click', editDetails);
             }
               // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', deleteDetails);
            }
         }
         // For Edit Details
         function editDetails() { 
             var getselectedrowindexes = $('#gridDepot').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             { 
                 var selectedRowData = $('#gridDepot').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             console.log(selectedRowData);
             $scope.vccUser._id=selectedRowData._id;
             $scope.vccUser.customerId = selectedRowData.customerId;
             $scope.vccUser.depotName=selectedRowData.depotName; 
             $scope.vccUser.addressLine1=selectedRowData.addressLine1; 
             $scope.vccUser.addressLine2=selectedRowData.addressLine2; 
             $scope.vccUser.pincode=selectedRowData.pincode; 
             $scope.vccUser.cityId=selectedRowData.cityId; 
             $scope.vccUser.stateId=selectedRowData.stateId; 
             $scope.vccUser.countryName='India';
             $scope.vccUser.latitude=selectedRowData.latitude; 
             $scope.vccUser.logitude=selectedRowData.logitude;
             console.log($scope.vccUser);
             $scope.openModal();
         }  
         // For Delete Details
         function deleteDetails(){
             getselectedrowindexes = $('#gridDepot').jqxGrid('getselectedrowindexes');
             if (getselectedrowindexes.length > 0)
             {
                 selectedRowData = $('#gridDepot').jqxGrid('getrowdata', getselectedrowindexes[0]);
             } 
             // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteTagMasterDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
             
         }     

        $scope.clearData = function(){ //window.location.reload();
            $("#gridDepot").jqxGrid('clearselection');
            $scope.vccUser = {}            
        }

        $scope.openModal = function () {
            vm.modalInstance= $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowDepot.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    if(currloggedinUser.otherData.roleName == 'System Super Admin' || currloggedinUser.otherData.roleName == 'System Admin'){
                        console.log("system");
                    }
                    else {
                        console.log("here");
                        $scope.vccUser.customerId = currloggedinUser.otherData._id;
                    }

                $scope.done = function (data) {
                    
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };
        var myarr=[]
        //for bulk upload
        $scope.opendepotBulkUploadModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'jqxwindowdepotBulkUpload.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.save = function (data) {
                        console.log(data);
                        $scope.currRowData(data);
                        if($scope.isValidate==true){
                            $uibModalInstance.close();
                        }                    
                    };

                    $scope.uploadTagDetails = function() {
                        //console.log("ssssss"); 
                        var returnVal =false;
                        var currData = {};
                        var fileInput = document.getElementById('the-file');
                        var file = fileInput.files[0];     
                        var fd = new FormData();
                        fd.append('file', file);
                        fd.append('data', 'string');
                        $http.post(config.apiUrl+'/api/upload', fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        })
                        .success(function(response){
                            console.log(response);
                            var currTagType= '';
                            myarr=[];
                            bulktags=[];
                            if(response.data){
                                if(response.data.length ==0){
                                    alertify.alert("Please check uploaded file for valid data");
                                    return
                                }
                                console.log(response.data);
                                var filejson={
                                    "": "",
                                    "address 1": "",
                                    "address 2": "",
                                    "city": "",
                                    "country": "",
                                    "depot name": "",
                                    "lattitude": "",
                                    "longitude": "",
                                    "pincode": "",
                                    "state": ""
                                }
                                if((Object.keys(response.data[0]).length)<=4){
                                    alertify.alert("Please verify data in file and upload again");
                                    return    
                                }
                                for(var x=0;x<response.data.length;x++){

                                    if((response.data[x]['depot name']!="")||(response.data[x]['pincode']!="")&&(response.data[x]['city']!="")&&(response.data[x]['country']!="")&&(response.data[x]['state']!="")){
                                        myarr.push(response.data[x]);
                                    }
                                }
                                console.log(myarr , "myarr");
                                //alert(response.data.length);
                                if(myarr.length>0)
                                {
                                    for(var i=0;i<myarr.length;i++){
                                    //  console.log(myarr[i]);
                                        returnVal =false;
                                        $scope.line=(i+1);
                                        //console.log(myarr[i]['depot name']);
                                        //console.log(myarr[i]['pincode']);
                                        if((myarr[i]['depot name']=="")&&(myarr[i]['depot name']=="")&&(myarr[i]['depot name']=="")&&(myarr[i]['pincode']=="")&&(myarr[i]['lattitude']=="")&&(myarr[i]['longitude']==""))
                                        {
                                            continue;
                                        }
                                        if((myarr[i]['depot name']!="")||(myarr[i]['pincode']!="")&&(myarr[i]['city']!="")&&(myarr[i]['country']!="")&&(myarr[i]['state']!=""))
                                        {
                                            var DepotCode = /^[a-zA-Z0-9_]*$/;
                                            var EmptyCode = /^[A-Za-z0-9_-\s]*$/;
                                            console.log("myarr[i] "+ JSON.stringify(myarr[i]))
                                            if(myarr[i]['depot name']!='' && myarr[i]['city']!='' && myarr[i]['country']!='' && myarr[i]['state']!=''){
                                                if(myarr[i]['pincode']!=''){
                                                    var CheckZipCode = /(^\d{6}$)/;
                                                    if(CheckZipCode.test(myarr[i]['pincode'])==false){
                                                        alertify.alert("Error at line "+$scope.line+" please check Pincode, blank records are ignored");
                                                        return
                                                    }
                                                }else if(myarr[i]['lattitude']!=''){
                                                    var lat=/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
                                                    
                                                    if(lat.test(myarr[i]['lattitude'])==false){
                                                        alertify.alert("Error at line "+$scope.line+" please check lattitude, blank records are ignored");
                                                        return
                                                    }
                                                }else if(myarr[i]['longitude']!=''){
                                                    var lon=/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;
                                                    if(lon.test(myarr[i]['longitude'])==false){
                                                        alertify.alert("Error at line "+$scope.line+" please check longitude, blank records are ignored");
                                                        return
                                                    }
                                                }else if(myarr[i]['country']!=''){
                                                    if(EmptyCode.test(myarr[i]['country'])==false){
                                                        alertify.alert("Error at line "+$scope.line+" please check country, blank records are ignored");
                                                        return
                                                    }
                                                }else if(myarr[i]['depot name']!=''){
                                                   
                                                    if(DepotCode.test(myarr[i]['depot name'])==false){
                                                        alertify.alert("Error at line "+$scope.line+" please check depot name, blank records are ignored");
                                                        return
                                                    }
                                                }else if(myarr[i]['state']!=''){
                                                    
                                                    if(EmptyCode.test(myarr[i]['state'])==false){
                                                        alertify.alert("Error at line "+$scope.line+" please check state, blank records are ignored");
                                                        return
                                                    }
                                                }else if(myarr[i]['city']!=''){
                                                    
                                                    if(EmptyCode.test(myarr[i]['city'])==false){
                                                        alertify.alert("Error at line "+$scope.line+" please check city, blank records are ignored");
                                                        return
                                                    }
                                                }
                                                returnVal= validatefilerows(myarr[i]['depot name'],
                                                myarr[i]['pincode'],
                                                myarr[i]['lattitude'],
                                                myarr[i]['longitude'],
                                                myarr[i]['city'],
                                                myarr[i]['country'],
                                                myarr[i]['state']);
                                            }else{
                                                $scope.errorNo=0
                                            }
                                        }else{
                                            returnVal =false;
                                        }

                                        $.each(bulktags, function (index, value) {
                                        
                                            if(returnVal)
                                            {
                                                if(value.depotName.toLowerCase()==myarr[i]['depot name'].toLowerCase())
                                                {
                                                    returnVal= false;
                                                    //break;
                                                }else{
                                                //  console.log("Unique "+myarr[i]['depot name']);
                                                    returnVal= true;
                                                }
                                            }
                                        
                                        });

                                    if(returnVal)
                                    {
                                        currData = {
                                            depotName:myarr[i]['depot name'],
                                            addressLine1: myarr[i]['address 1'],
                                            addressLine2:myarr[i]['address 2'],
                                            pincode: myarr[i]['pincode'],
                                            cityId: myarr[i]['city'],
                                            stateId :myarr[i]['state'],
                                            countryName :myarr[i]['country'],
                                            latitude :myarr[i]['lattitude'],
                                            logitude :myarr[i]['longitude']
                                        };
                                        bulktags.push(currData);
                                    }else{
                                        if($scope.errorNo==1){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Error at line "+$scope.line+" please check Depotname, blank records are ignored");
                                            break;
                                        }else if($scope.errorNo==2){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Error at line "+$scope.line+" please check city, blank records are ignored");
                                            break;
                                        }else if($scope.errorNo==3){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Error at line "+$scope.line+" please check state, blank records are ignored");
                                            break;
                                        }else if($scope.errorNo==4){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Error at line "+$scope.line+" please check country, blank records are ignored");
                                            break;
                                        }else if($scope.errorNo==5){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Error at line "+$scope.line+" please check lattitude, blank records are ignored");
                                            break;
                                        }else if($scope.errorNo==6){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Error at line "+$scope.line+" please check longitude, blank records are ignored");
                                            break;
                                        }else if($scope.errorNo==7){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Error at line "+$scope.line+" please check Pincode, blank records are ignored");
                                            break;
                                        }else if($scope.errorNo==0){
                                            console.log("$scope.errorNo"+ $scope.errorNo);
                                            alertify.alert("Invalid File or data not according to sample file and please fill all required fields Depot Name, City, Country and State");
                                            break;
                                        }
                                    }
                                    }  
                                    if(returnVal){
                                        $scope.fileData= bulktags;
                                        //if(myarr.length== bulktags.length)
                                        console.log(bulktags +" bulktags");
                                        myarr=[];
                                        AdddepotinBulk(bulktags);
                                        $uibModalInstance.close();
                                    }else{
                                        console.log("invalid");
                                    }
                                }else{
                                    console.log("file is empty");
                                }
                            }
                        })
                        .error(function(response){
                            console.log(response);
                        });
                    }
                    $scope.line;
                    $scope.errorNo;
                    function validatefilerows(depotname, pincode,latitude,logitude,city,country,state)
                    {
                        var DepotCode = /^[a-zA-Z0-9_]*$/;
                        var EmptyCode = /^[A-Za-z0-9_-\s]*$/;
                        var CheckZipCode = /(^\d{6}$)/;
                        var lat=/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
                        var lon=/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;
                        var ErrFlag=true;

                        // console.log("insidede validaterows+++++++++++++++++++++++++++++++++++++++++++++++++++");
                        // console.log(depotname, pincode,latitude,logitude,city,country,state);
                        // console.log(bulktags);
                        // if(depotname==undefined){
                        //     alertify.alert("Please check file for proper data ");
                        //     return
                        // }

                        // if(DepotCode.test(depotname)){
                        //     ErrFlag= false;
                        //     //return ErrFlag;
                        // }else{

                        //     if(depotname==""){
                        //         $scope.errorNo=1;
                        //         alertify.alert("Error at line "+$scope.line+" please check Depot Name, blank records are ignored");
                        //         ErrFlag= true;
                        //         return ErrFlag;
                        //     }
                        // }

                        // if(EmptyCode.test(city))
                        // {
                        //     console.log(EmptyCode.test(city) +" eeeeeeee "+city);
                        //     ErrFlag= false;
                        // }else{
                        //     console.log(EmptyCode.test(city) +" eeeeeeee "+city);

                        //     if(city==""){
                        //         alertify.alert("Error at line "+$scope.line+" please check city, blank records are ignored");
                        //         $scope.errorNo=2;
                        //         ErrFlag= true;
                        //         return ErrFlag;
                        //     }
                        // }

                        // if(EmptyCode.test(country)){

                        //     ErrFlag= false;
                        // }else{
                        //     console.log("EmptyCode.test(country)")
                        //     console.log(EmptyCode.test(country))
                        //     if(country==""){
                        //         alertify.alert("Error at line "+$scope.line+" please check country, blank records are ignored");
                        //         $scope.errorNo=3;
                        //         ErrFlag= true;
                        //         return ErrFlag;
                        //     }
                        // }

                        // if(EmptyCode.test(state)){
                        //     ErrFlag= false;
                              
                        // }else{
                        //     if(state==""){
                        //         $scope.errorNo=4;
                        //         alertify.alert("Error at line "+$scope.line+" please check state, blank records are ignored");
                        //         ErrFlag= true;
                        //         return ErrFlag;
                        //     }

                        // }

                        // if((pincode!="")) {
                        //     var CheckZipCode = /(^\d{6}$)/;
                        //     if(CheckZipCode.test(pincode)==false){
                        //         alertify.alert("Error at line "+$scope.line+" please check Pincode, blank records are ignored");
                        //         //break;
                        //         $scope.errorNo=7;
                        //         ErrFlag= true;
                        //         return ErrFlag;
                        //     } else {
                        //         console.log("pincode")
                        //         console.log(pincode)
                        //         console.log("CheckZipCode.test(pincode)");
                        //         console.log(CheckZipCode.test(pincode));
                        //         ErrFlag= false;
                        //     }
                        // }    

                        // if((!lat.test(latitude))&&(latitude!="")) {   
                        //     $scope.errorNo=5;
                        //     alertify.alert("Error at line "+$scope.line+" please check lattitude, blank records are ignored");
                        //     ErrFlag= true;
                        //     return ErrFlag;
                        // } else {
                        //     ErrFlag= false;
                        // }

                        // if((!lon.test(logitude))&&(logitude!="")){   
                        //     $scope.errorNo=6;
                        //     alertify.alert("Error at line "+$scope.line+" please check longitude, blank records are ignored");
                        //     ErrFlag= true;
                        //     return ErrFlag;
                        // } else {
                        //     ErrFlag= false;
                        //     //return ErrFlag;
                        // }
                            return  ErrFlag;
                        }

                        var bulktags = [];

                        function addBulkTags(data) {
                        console.log(data);
                        depotService.AddBulkDepot(data,function (result) {
                        if (result.success === true) {
                            console.log(result);
                            toastr.success(result.message);   
                            getAllCustomerDepotDetails();
                            $scope.clearData();
                            myarr=[];
                            bulktags=[];
                            $("#depotBulkUpload").hide();
                            // $scope.fileData= result.data;
                        }else{
                            $("#depotBulkUpload").hide();
                            toastr.error(result);                
                        }
                        })
                        }

                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                            $scope.clearData();
                        };

                },
                    backdrop: 'static',
                    scope: $scope,
                })
        };
    }
  
})();
  
  