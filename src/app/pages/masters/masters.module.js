(function() {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters', ['ui.select', 'ui.bootstrap', 'ngSanitize'])
    .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider,$httpProvider) {
      $httpProvider.interceptors.push('authInterceptor');
      $stateProvider
        .state('main.master', {
          url: '/master',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'Masters',
          sidebarMeta: {
            icon: 'icoMasters',
            order: 2,
          },
          authenticate: true,
          params: {                // <-- focusing this one
            authRoles:['System Admin', 'System Super Admin', 'System Manager', 'System User', 'OEM Admin', 'Client Admin','System Manager', 'Client Manager','OEM Manager','OEM User']   // <-- roles allowed for this module
          }
        })
        //podmaster
        // .state('main.master.pod', {
        //   url: '/podbulkUpload',
        //   templateUrl: 'app/pages/masters/pod/pod.html',
        //   //template: 'app/pages/masters/pod/pod.html',
        //   controller: "podCtrl",
        //   title: 'Pod Master',
        //   sidebarMeta: {
        //     order: 2,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin']
        //   }
        // })

        // .state('main.master.license', {
        //   url: '/license',
        //   template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        //   title: 'Customer License',
        //   sidebarMeta: {
        //     order: 0,
        //   },
        //   authenticate: true,
        //   params: {                // <-- focusing this one
        //     authRoles: ['System Admin', 'System Super Admin', 'OEM Admin']   // <-- roles allowed for this module
        //   }
        // })
        
        //customer master
        .state('main.master.customer', {
          url: '/customer',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
         //  controller: "CustomerDetailsCtrl",
          title: 'Customer',
          sidebarMeta: {
          order: 1,
          },
          authenticate: true,
          params: {                // <-- focusing this one
            authRoles: ['System Admin', 'System Super Admin', 'OEM Admin']   // <-- roles allowed for this module
          }
        })        
        .state('main.master.customer.licensetype', {
          url: '/license_type',
          templateUrl: 'app/pages/masters/license/licenseType/licensetype.html',
          controller: "LicenseTypesCtrl",
          title: 'License Type',
          sidebarMeta: {
            order: 0,
          },
          authenticate: true,
          params: {                // <-- focusing this one
            authRoles: ['System Admin', 'System Super Admin']   // <-- roles allowed for this module
          }
        })
        .state('main.master.customer.customer', {
          url: '/Customerlicense',
          templateUrl: 'app/pages/masters/license/customerLicense/customerLicense.html',
          controller: "CusotmerLicenseCtrl",
          title: 'Customer License',
          sidebarMeta: {
            order: 1,
          },
          authenticate: true,
          params: {                // <-- focusing this one
            authRoles: ['System Admin', 'System Super Admin']   // <-- roles allowed for this module
          }
        })
        .state('main.master.customer.customerDetails', {
          url: '/Customer_Details',
          templateUrl: 'app/pages/masters/customers/customerdetails/customerdetails.html',
          controller: "CustomerDetailsCtrl",
          title: 'Customer Master',
          sidebarMeta: {
            order: 3,
          },          
          authenticate: true,
          params: {                // <-- focusing this one
            authRoles: ['System Admin', 'System Super Admin', 'OEM Admin']   // <-- roles allowed for this module
          }
        })

        .state('main.master.customer.customertype', {
          url: '/customer_type',
          controller: "CustomerTypesCtrl",
          templateUrl: 'app/pages/masters/customers/customertypes/customertypes.html',
          title: 'Customer Type',
          sidebarMeta: {
            order: 2,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin']
          }
        })
        // .state('main.master.vehicletpms', {
        //   url: '/vehicle_tpms',
        //   templateUrl: 'app/pages/masters/vehicle/vehicletpms/vehicleTpms.html',
        //   controller: "VehicleTpmsCtrl",
        //   title: 'Vehicle TPMS',
        //   sidebarMeta: {
        //     order: 3,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin', 'OEM Admin', 'Client Manager']
        //   }
        // })
        .state('main.master.depot', {
          url: '/depot',
          templateUrl: 'app/pages/masters/depot/depot.html',
          controller: "DepotCtrl",
          title: 'Depot Master',
          sidebarMeta: {
            order: 3,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'Client Admin']
          }
        })
          //User master
          .state('main.master.usermaster', {
            url: '/usermaster',
            template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
           //  controller: "CustomerDetailsCtrl",
            title: 'User',
            sidebarMeta: {
            order: 4,
            },
            authenticate: true,
            params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'OEM Manager', 'Client Admin', 'Client Manager']   // <-- roles allowed for this module
            }
          })       
          // .state('main.master.usermaster.rolemapping', {
          //   url: '/role_mapping',
          //  templateUrl: 'app/pages/masters/user/roleMapping/roleMapping.html',
          //  controller: "RoleMapCtrl",
          //   title: 'Operation Role Mapping',
          //   sidebarMeta: {
          //     order: 0,
          //   },
          //   authenticate: true,
          //   params: {
          //     authRoles: ['System Admin', 'System Super Admin']   // <-- roles allowed for this module
          //   }
          // }) 
          //rolemaster
          .state('main.master.usermaster.role', {
            url: '/role',
            templateUrl: 'app/pages/masters/role/role.html',
            //template: 'app/pages/masters/role/role.html',
            controller: "RolesCtrl",
            title: 'Role Master',
            sidebarMeta: {
              order: 2,
            },
            authenticate: true,
            params: {
              authRoles: ['System Admin', 'System Super Admin']
            }
          })

          // .state('main.master.usermaster.operationmaster', {
          //   url: '/operation_master',
          //   templateUrl: 'app/pages/masters/user/operationMaster/operationMaster.html',
          //   controller: "OprMasterCtrl",
          //   title: 'Operation Master',
          //   sidebarMeta: {
          //     order: 1,
          //   },
          //   authenticate: true,
          //   params: {
          //     authRoles: ['System Admin', 'System Super Admin']   // <-- roles allowed for this module
          //   }
          // })
          
          .state('main.master.usermaster.userdetails', {
            url: '/user_details',
            templateUrl: 'app/pages/masters/user/userDetails/userDetails.html',
            controller: "UserDetailsCtrl",
            title: 'User Master',
            sidebarMeta: {
              order: 2,
            },
            authenticate: true,
            params: {
              authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'OEM Manager', 'Client Admin', 'Client Manager','System User']   // <-- roles allowed for this module
            }
          })
          
        //tag master
        .state('main.master.tagmaster', {
          url: '/tag_master',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          //controller: "MailTabCtrl",
          title: 'Tag',
          sidebarMeta: {
            order: 5,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'Client Admin','System User']
          }
        })
        .state('main.master.tagmaster.tagtype', {
          url: '/tag_type',
          templateUrl: 'app/pages/masters/tag/tagType/tagType.html',
          controller: "TagTypeCtrl",
          title: 'Tag Type',
          sidebarMeta: {
            order: 0,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager','System User']
          }
        })
        .state('main.master.tagmaster.tagdetails', {
          url: '/tag_details',
          templateUrl: 'app/pages/masters/tag/tagDetails/tagDetails.html',
          controller: "TagDetailsCtrl",
          title: 'Manufactured Tags',
          sidebarMeta: {
            order: 1,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        .state('main.master.tagmaster.tagAssignment', {
          url: '/tag_assignement',
          templateUrl: 'app/pages/masters/tag/tagAssignment/tagAssignment.html',
          controller: "TagAssignmentCtrl",
          title: 'Tag Quantity Assignment to Customer',
          sidebarMeta: {
            order: 2,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin','System User']
          }
        })
        .state('main.master.tagmaster.tagAllocation', {
          url: '/tag_allocation_depot',
          controller: "CustomerTagAllocationCtrl",
          templateUrl: 'app/pages/masters/customers/customertagallocationdepot/customertagallocationdepot.html',
          title: 'Customer Tag Allocation to Depot',
          sidebarMeta: {
            order: 3,
          },
          authenticate: true,
          params: {                // <-- focusing this one
            //authRoles: ['System Admin', 'System Super Admin', 'Client Admin']   // <-- roles allowed for this module
            authRoles: ['Client Admin']   // <-- roles allowed for this module
          }
        })
        // .state('main.master.tagmaster.tagAssigned', {
        //   url: '/customer_tagAssigned',
        //   controller: "CustomerTagAssignedCtrl",
        //   templateUrl: 'app/pages/masters/customers/customertagassigned/customertagassigned.html',
        //   title: 'Customer Assigned Tag ID Details',
        //   sidebarMeta: {
        //     order: 4,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'Client Admin']
        //   }
        // })
        //tire master
        .state('main.master.tiremaster', {
          url: '/Tyre_master',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          //controller: "MailTabCtrl",
          title: 'Tyre',
          sidebarMeta: {
            order: 6,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'OEM Admin', 'Client Admin', 'Client Manager','OEM User']
          }
        })
        .state('main.master.tiremaster.tiremanufacturer', {
          url: '/Tyre_manufacturer',
          templateUrl: 'app/pages/masters/tire/tireManufacturer/tireManufacturer.html',
           controller: "TireManufacturerCtrl",
          title: 'Tyre Manufacturer',
          sidebarMeta: {
            order: 0,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        .state('main.master.tiremaster.tiretype', {
          url: '/tyre_type',
          templateUrl: 'app/pages/masters/tire/tireType/tireType.html',
          controller: "TireTypesCtrl",
          title: 'Tyre Type',
          sidebarMeta: {
            order: 1,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        .state('main.master.tiremaster.tirestatus', {
          url: '/tyre_status',
          templateUrl: 'app/pages/masters/tire/tireStatus/tireStatus.html',
          controller: "TireStatusCtrl",
          title: 'Tyre Status',
          sidebarMeta: {
            order: 2,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        .state('main.master.tiremaster.tiremodel', {
          url: '/tyre_model',
          templateUrl: 'app/pages/masters/tire/tireModel/tireModel.html',
          controller: "TireModelCtrl",
          title: 'Tyre Model',
          sidebarMeta: {
            order: 3,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        .state('main.master.tiremaster.tiredetails', {
          url: '/tyre_details',
          templateUrl: 'app/pages/masters/tire/tireDetails/tireDetails.html',
          controller: "TireDetailsCtrl",
          title: 'Tyre Master',
          sidebarMeta: {
            order: 6,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin','Client Admin', 'Client Manager','OEM User']
          }
        })
        .state('main.master.tiremaster.tirerec', {
          url: '/tyre_rec',
          templateUrl: 'app/pages/masters/tire/tireRecommendation/tireRecommendation.html',
           controller: "TireRecommendationCtrl",
          title: 'Tyre Recommended Position',
          sidebarMeta: {
            order: 5,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        .state('main.master.tiremaster.tirespec', {
          url: '/tyre_spec',
          templateUrl: 'app/pages/masters/tire/tireSpecs/tireSpecs.html',
          controller: "TireSpecsCtrl",
          title: 'Tyre Specification',
          sidebarMeta: {
            order: 4,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager','OEM Admin']
          }
        })
        //vehicle master
        .state('main.master.vehiclemaster', {
          url: '/vehicle_master',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          //controller: "MailTabCtrl",
          title: 'Vehicle',
          sidebarMeta: {
            order: 7,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin', 'OEM Admin', 'Client Manager']
          }
        })
        .state('main.master.vehiclemaster.vehiclemodel', {
          url: '/vehicle_model',
          templateUrl: 'app/pages/masters/vehicle/vehicleModel/vehicleModel.html',
          controller: "VehicleModelCtrl",
          title: 'Vehicle Model',
          sidebarMeta: {
            order: 0,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })

        .state('main.master.vehiclemaster.vehiclemanufacturer', {
          url: '/vehicle_manufacturer',
          templateUrl: 'app/pages/masters/vehicle/vehiclemanufacturer/vehiclemanufacturer.html',
          controller: "VehicleManufacturerCtrl",
          title: 'Vehicle Manufacturer',
          sidebarMeta: {
            order: 1,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
       
        .state('main.master.vehiclemaster.vehiclegroup', {
          url: '/vehicle_group',
          templateUrl: 'app/pages/masters/vehicle/vehicleGroup/vehicleGroup.html',
           controller: "VehicleGroupsCtrl",
          title: 'Vehicle Group',
          sidebarMeta: {
            order: 2,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        .state('main.master.vehiclemaster.vehiclespec', {
          url: '/vehicle_spec',
          templateUrl: 'app/pages/masters/vehicle/vehicleSpec/vehicleSpec.html',
          controller: "VehicleSpecCtrl",
          title: 'Vehicle Specification',
          sidebarMeta: {
            order: 3,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager','OEM Admin']
          }
        })
        .state('main.master.vehiclemaster.vehicledetails', {
          url: '/vehicle_details',
          templateUrl: 'app/pages/masters/vehicle/vehicleDetails/vehicleDetails.html',
          controller: "VehicleDetailsCtrl",
          title: 'Vehicle Master',
          sidebarMeta: {
            order: 5,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin', 'Client Manager']
          }
        })
        .state('main.master.vehiclemaster.vehicleaxelconfig', {
          url: '/vehicleaxelconfig',
          templateUrl: 'app/pages/masters/vehicle/vehicleaxelconfig/vehicleaxelconfig.html',
          controller: "VehicleAxelConfigCtrl",
          title: 'Vehicle Axel Config',
          sidebarMeta: {
            order: 4,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'System Manager']
          }
        })
        // //vehicle tag details
        // .state('main.master.vehiclemaster.vehicleTagdetails', {
        //   url: '/vehicleTagdetails',
        //   templateUrl: 'app/pages/masters/vehicle/vehicleTagdetails/vehicleTagdetails.html',
        //   controller: "VehicleTagCtrl",
        //   title: 'Vehicle Tag',
        //   sidebarMeta: {
        //     order: 6,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin']
        //   }
        // })
        // //vehicle tube tag details
        // .state('main.master.vehiclemaster.vehicleTubeTagdetails', {
        //   url: '/vehicleTubeTagdetails',
        //   templateUrl: 'app/pages/masters/vehicle/vehicleTubeTagdetails/vehicleTubeTagdetails.html',
        //   controller: "vehicleTubeTagdetailsCtrl",
        //   title: 'Vehicle Tube Tag',
        //   sidebarMeta: {
        //     order: 7,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin']
        //   }
        // })
        // //vehicle tires dedtails
        // .state('main.master.vehiclemaster.vehicletire', {
        //   url: '/vehicleTire',
        //   templateUrl: 'app/pages/masters/vehicle/vehicletire/vehicletire.html',
        //   controller: "VehicleTireCtrl",
        //   title: 'Vehicle Tire',
        //   sidebarMeta: {
        //     order: 8,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin']
        //   }
        // })
        // //vehicle alert details
        // .state('main.master.vehiclemaster.vehicleAlert', {
        //   url: '/vehicleAlert',
        //   templateUrl: 'app/pages/masters/vehicle/vehicleAlert/vehicleAlert.html',
        //   controller: "vehicleAlertCtrl",
        //   title: 'Vehicle Alert',
        //   sidebarMeta: {
        //     order: 9,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin']
        //   }
        // })
        // //vehicle alert ack details
        // .state('main.master.vehiclemaster.vehicleAlertAck', {
        //   url: '/vehicleAlertAck',
        //   templateUrl: 'app/pages/masters/vehicle/vehicleAlertAck/vehicleAlertAck.html',
        //   controller: "vehicleAlertAckCtrl",
        //   title: 'Vehicle Alert Ack',
        //   sidebarMeta: {
        //     order: 10,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'System Manager', 'Client Admin']
        //   }
        // })
        //tube master
        .state('main.master.tubemaster', {
          url: '/tube_master',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          //controller: "MailTabCtrl",
          title: 'Tube',
          sidebarMeta: {
          order: 8,
          },
          authenticate: true,
          params: {
            //authRoles: ['System Admin', 'System Super Admin', 'Client Admin', 'OEM Admin']
          }
        })
        // .state('main.master.tubemaster.tubeqtyassignment', {
        //   url: '/tube_qty_assignment',
        //   templateUrl: 'app/pages/masters/tube/tubeQtyAssignmentMaster/tubeQtyAssignment.html',
        //   controller: "TubeQtyAssignmentCtrl",
        //   title: 'Tube Qty Assignment',
        //   sidebarMeta: {
        //     order: 0,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'Client Admin', 'OEM Admin']
        //   }
        // })
        // .state('main.master.tubemaster.tubeqtyassigndetail', {
        //   url: '/tube_qty_assign_detail',
        //   templateUrl: 'app/pages/masters/tube/tubeQtyAssignDetails/tubeqtyassigndetail.html',
        //   controller: "TubeQtyAssignDetailsCtrl",
        //   title: 'Tube Qty Assign Detail',
        //   sidebarMeta: {
        //     order: 1,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'Client Admin', 'OEM Admin']
        //   }
        // })
        // .state('main.master.tubemaster.custAssignedTubeTagDetail', {
        //   url: '/cust_Assigned_Tube_TagId_Details',
        //   templateUrl: 'app/pages/masters/tube/customerAssignedTubeTagDetail/custAssignedTubeTagDetail.html',
        //   controller: "CustAssignedTubeTagIdDetailCtrl",
        //   title: 'Customer Assigned Tube TagId Details',
        //   sidebarMeta: {
        //     order: 6,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'Client Admin', 'OEM Admin']
        //   }
        // })

        //Customer Tube Tags Allocation to Depot
        .state('main.master.tubemaster.customerTubeTagAllocatesToDepot', {
          url: '/customer_Tube_Tag_Allocates_To_Depot',
          templateUrl: 'app/pages/masters/tube/customerTubeTagAllocatesToDepot/custTubeTagAllocatesToDepot.html',
          controller: "CustomerTubeTagAllocationCtrl",
          title: 'Customer Tube Tags Allocation to Depot',
          sidebarMeta: {
            order: 6,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin', 'Client Admin', 'OEM Admin']
          }
        })

        .state('main.master.tubemaster.tubemanufacturer', {
          url: '/tube_manufacturer',
          templateUrl: 'app/pages/masters/tube/tubemanufacturer/tubemanufacturer.html',
          controller: "TubeManufacturerCtrl",
          title: 'Tube Manufacturer',
          sidebarMeta: {
            order: 2,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin']
          }
        })
        .state('main.master.tubemaster.tubetype', {
          url: '/tube_type',
          templateUrl: 'app/pages/masters/tube/tubetype/tubetype.html',
          controller: "TubeTypesCtrl",
          title: 'Tube Type',
          sidebarMeta: {
            order: 3,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin']
          }
        })
        .state('main.master.tubemaster.tubesize', {
          url: '/tube_size',
          templateUrl: 'app/pages/masters/tube/tubesize/tubesize.html',
          controller: "TubeSizeCtrl",
          title: 'Tube Size',
          sidebarMeta: {
            order: 4,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin']
          }
        })
        .state('main.master.tubemaster.tubedetails', {
          url: '/tube_details',
          templateUrl: 'app/pages/masters/tube/tubedetails/tubedetails.html',
          controller: "TubeDetailsCtrl",
          title: 'Tube Master',
          sidebarMeta: {
            order: 5,
          },
          authenticate: true,
          params: {
            authRoles: ['System Admin', 'System Super Admin']
          }
        });
        // .state('main.master.tubemaster.tubelifedetails', {
        //   url: '/tube_life_details',
        //   templateUrl: 'app/pages/masters/tube/tubeLifeDetails/tubeLifeDetails.html',
        //   controller: "TubeLifeDetailsCtrl",
        //   title: 'Tube Life Details',
        //   sidebarMeta: {
        //     order: 7,
        //   },
        //   authenticate: true,
        //   params: {
        //     authRoles: ['System Admin', 'System Super Admin', 'Client Admin', 'OEM Admin']
        //   }
        // });
    }
  })();