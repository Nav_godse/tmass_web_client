(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleManufacturerCtrl', VehicleManufacturerCtrl);
  
    /** @ngInject */
    function VehicleManufacturerCtrl($scope,vehicleService,$filter, editableOptions, editableThemes, toastr, $uibModal) {
        var vm = this; 
        vm.getAllVehicleManufacturerDetails = getAllVehicleManufacturerDetails;
        vm.addVehicleManufacturer = addVehicleManufacturer;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            vehicleManufacturerName:"",
            vehicleManufacturerPlace:""
       }
        initController();
        $scope.vehicles = [];
        var data = [];

        
        function initController() {
            getAllVehicleManufacturerDetails();
        }

        function addVehicleManufacturer() {            
            vehicleService.AddVehicleManufacturer(vm.vehicleManufacturerName, vm.vehicleManufacturerPlace, function (result) {
                if (result.success === true) { 
                    vm.vehicleManufacturerName="";
                    vm.vehicleManufacturerPlace="";
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleManufacturerDetails();
                    toastr.success(result.message);
                    $scope.isValidate= true;
                    vm.ModalInstance.close();
                    //$("#jqxwindowVehicleMnctr").jqxWindow('close');
                } else {
                    $scope.isValidate= false;
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
 

        function getAllVehicleManufacturerDetails() {            
            vehicleService.GetAllVehicleManufacturer(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicles.push(result.data[i]);  
                        data.push(result.data[i]);  
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };


        function deleteVehicleManufacturerDetails(_id) {            
            vehicleService.DeleteVehicleManufacturer(_id,function (result) {
                if (result.success === true) {   
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleManufacturerDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function updateVehicleManufacturerDetails() {            
            vehicleService.UpdateVehicleManufacturer(vm._id,vm.vehicleManufacturerName,vm.vehicleManufacturerPlace,function (result) {
                if (result.success === true) {
                    vm.vehicleManufacturerName="";
                    vm.vehicleManufacturerPlace=""; 
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleManufacturerDetails();
                    toastr.success(result.message);
                    $scope.isValidate= true;
                    vm.ModalInstance.close();
                    //$("#jqxwindowVehicleMnctr").jqxWindow('close');
                } else {
                    $scope.isValidate= false;
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            // Validation 
            $scope.isValidate= false;
            if(rowform==undefined || rowform.vehicleManufacturerName== '' || (rowform.vehicleManufacturerName==undefined)){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                //$scope.isValidate= true;
            }

            if(rowform._id == "" || rowform._id == undefined){
                vm.vehicleManufacturerName = rowform.vehicleManufacturerName; 
                vm.vehicleManufacturerPlace = rowform.vehicleManufacturerPlace;
                addVehicleManufacturer();
                $scope.clearData();
                rowform ={};
            }else{
                vm._id = rowform._id; 
                vm.vehicleManufacturerName = rowform.vehicleManufacturerName; 
                vm.vehicleManufacturerPlace = rowform.vehicleManufacturerPlace;
                updateVehicleManufacturerDetails();
                $scope.clearData();
                rowform ={};
            }               
        }
            ///////////////////////////////////////
     
    //   $("#popup").click(function () {
    //       $("#grid").jqxGrid('clearselection');
    //         getselectedrowindexes =null;
    //         selectedRowData =null;
    //         $('#_id').val('');
    //       //$("#jqxwindowVehicleMnctr").jqxWindow('open');
    //   });
    //   //$("#jqxwindowVehicleMnctr").bind('close', function (event){
    //     $scope.clearData();
    // });
    //   $("#button_input").click(function () {
    //       var T = $("#text_input").val();
    //       $("#textbox").text(T);
    //       //$("#jqxwindowVehicleMnctr").jqxWindow('close');
    //   });
    //   $("#button_no").click(function () {
    //       //$("#jqxwindowVehicleMnctr").jqxWindow('close');
    //   });

        /////////////////////////////////////////////
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'vehicleManufacturerName', type: 'string' },
                    { name: 'vehicleManufacturerPlace', type: 'string' }
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#grid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Name', datafield: 'vehicleManufacturerName',align: 'center',cellsalign: 'center', width: '33%' },
                    { text: 'Place', datafield: 'vehicleManufacturerPlace',align: 'center',cellsalign: 'center', width: '33%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
        }
      
        function buttonsEvents() {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
         }

        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id : selectedRowData._id,
                vehicleManufacturerName:selectedRowData.vehicleManufacturerName,
                vehicleManufacturerPlace:selectedRowData.vehicleManufacturerPlace
            }
            $('#_id').val(selectedRowData._id);
            $('#vehicleManufacturerName').val(selectedRowData.vehicleManufacturerName);
            $("#vehicleManufacturerPlace").val(selectedRowData.vehicleManufacturerPlace); 
            $scope.openModal();
            //$("#jqxwindowVehicleMnctr").jqxWindow('open'); 
        }  

        function deleteDetails(){
            getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete this record?", function () {
                // user clicked "ok"
                deleteVehicleManufacturerDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            })  
            
        }

        $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) {
            buttonsEvents();
        });

        $scope.clearData = function(){ //window.location.reload();

            $("#grid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                vehicleManufacturerName:"",
                vehicleManufacturerPlace:""
            }
        }
        $scope.openModal = function () {
            vm.ModalInstance=$uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowVehicleMnctr.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              scope: $scope,
              backdrop:'static'
            })
        };
    }
  
})();
  
  