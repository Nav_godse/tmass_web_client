(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleTpmsCtrl', VehicleTpmsCtrl);
  
    /** @ngInject */
    function VehicleTpmsCtrl($scope,vehicleService,toastr, $uibModal) {
        var vm = this; 
        
        vm.VehicleSpeclId=""; 
        $scope.currentMgr="";
        $scope.startDayMgr=0;
        $scope.startMonthMgr=0;
        $scope.MgrValue="0";
        
        $scope.vehicleAxelConfigs = [];
        $scope.selectedAxelConfig = "2x4x4";
        initController(); 
        function initController() {
            getAllDropdowns()            
            // For Open Popup for Add and Edit
            //$("#jqxwindowVehicleSpec").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function getAllDropdowns(){  
            getAllVehicleAxelConfigDetails(); 
            GetAllTPMSViewDetails();
        }
        //http://demo.tmaas.in/treel_data_lg/device_details/get_device_info_deviceid.php?DeviceID=865794033919954&deviceType=treel&MgrValue=0
          //to get all Vehicle  Axel Config details.
        function getAllVehicleAxelConfigDetails() {            
            vehicleService.GetAllVehicleAxelConfiguration(function (result) {
                if (result.success === true) {
                    vm.userVehicleAxelConfig=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleAxelConfigs.push({value: result.data[i]._id, text: result.data[i].vehicleAxelConfiguration});    
                    }
                    //console.log($scope.vehicleAxelConfigs);      
                } else {
                    toastr.error(result.message);
                    //console.log(result);         
                }
            });
        };
  
        function GetAllTPMSViewDetails() {            
            vehicleService.GetAllTPMSViewDetails("865794033871189","treel",function (result) {
                $scope.currentMgr="";
                $scope.startDayMgr=0;
                $scope.startMonthMgr=0;
    
                $scope.selectedVehicleNo = "Mh45";
                $scope.selectedAxelConfig = "2x4x4";
                $scope.deviceType = "treel";
                $scope.MgrValue = 0;

                $scope.vehicleDetails=result.data.data;
                console.log("response.data.data");
                console.log(result.data);
                if( result.data.data[0].currentMgrValue.length>=4){
                    $scope.currentMgr =Math.round(result.data.data[0].currentMgrValue/1000);
                } 
                if( result.data.data[0].todayStartMgr >=1){
                    $scope.startDayMgr =Math.round(result.data.data[0].todayStartMgr/1000) ;
                } 
                if( result.data.data[0].monthStartMgr >=1){
                    $scope.startMonthMgr =Math.round(result.data.data[0].monthStartMgr/1000);
                } 
                
             
                $scope.locationUrl="https://www.google.com/maps?q="+ result.data.data[0].Latitude+","+result.data.data[0].Longitude+" &t=m&z=16";
               

                // if (result.success === true) {
                //     vm.userVehicleAxelConfig=result.data;
                //     for(var i=0;i<result.data.length;i++){
                //         $scope.vehicleAxelConfigs.push({value: result.data[i]._id, text: result.data[i].vehicleAxelConfiguration});    
                //     }
                //     //console.log($scope.vehicleAxelConfigs);      
                // } else {
                //     toastr.error(result.message);
                //     //console.log(result);         
                // }
            });
        };

    }
  
})();
  
  