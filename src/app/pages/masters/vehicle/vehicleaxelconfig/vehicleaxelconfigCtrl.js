(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleAxelConfigCtrl', VehicleAxelConfigCtrl);
  
    /** @ngInject */
    function VehicleAxelConfigCtrl($scope,vehicleService,$filter, editableOptions, editableThemes, toastr,$uibModal,$rootScope ) {
        var vm = this; 
        vm.getAllVehicleAxelConfigDetails = getAllVehicleAxelConfigDetails;
        vm.addVehicleAxelConfig = addVehicleAxelConfig;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        initController();
        $scope.vehicles = [];
        var data = [];
         $scope.vccUser = {
            _id:"",
            vehicleAxelConfiguration:""  
        }
        function initController() {
            getAllVehicleAxelConfigDetails();
            // For Open Popup for Add and Edit
            //$("#jqxwindowVehicleAxel").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addVehicleAxelConfig() {         
               
            vehicleService.AddVehicleAxelConfiguration(vm.vehicleAxelConfiguration, function (result) {
                if (result.success === true) { 
                    vm.vehicleAxelConfiguration="";
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleAxelConfigDetails();
                    toastr.success(result.message);        
                    //$("#jqxwindowVehicleAxel").jqxWindow('close');            
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function getAllVehicleAxelConfigDetails() {            
            vehicleService.GetAllVehicleAxelConfiguration(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicles.push(result.data[i]);  
                        data.push(result.data[i]);  
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function deleteVehicleAxelConfigDetails(_id) {            
            vehicleService.DeleteVehicleAxelConfiguration(_id,function (result) {
                if (result.success === true) {  
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleAxelConfigDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function updateVehicleAxelConfigDetails() {            
            vehicleService.UpdateVehicleAxelConfiguration(vm._id,vm.vehicleAxelConfiguration,function (result) {
                if (result.success === true) {
                    vm.vehicleAxelConfiguration="";
                    vm.userVehicleAxelConfig=result.data;
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleAxelConfigDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowVehicleAxel").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
               // Validation 
            $scope.isValidate= false;
            if(rowform== undefined ||(rowform.vehicleAxelConfiguration=="" ||  rowform.vehicleAxelConfiguration == undefined)){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }

            if(rowform.vehicleAxelConfiguration.length>11){
                toastr.error("Maximum character allowed are 11");
                $scope.isValidate= false;
                return;
            }

            if( rowform.vehicleAxelConfiguration!=undefined && rowform.vehicleAxelConfiguration.length>0 ){
                for(var i=0;i<rowform.vehicleAxelConfiguration.length;i++)
                {
                    console.log("rowform.vehicleAxelConfiguration[i]",rowform.vehicleAxelConfiguration[i]);
                    //start axel 
                    if((i==0)&&((rowform.vehicleAxelConfiguration[i]=='0') || (rowform.vehicleAxelConfiguration[i]=='2') || (rowform.vehicleAxelConfiguration[i]=='1') || (rowform.vehicleAxelConfiguration[i]=='4')))
                    {
                        $scope.isValidate= true;
                    }
                    if(i!=1){
                        if((i+1==rowform.vehicleAxelConfiguration.length))
                        {
                            if((rowform.vehicleAxelConfiguration[i].toLowerCase()!='x'))
                            {
                                $scope.isValidate= true;
                            }else
                            {
                                toastr.error("Please Fill valid data in Vehicle Axel Configuration field");
                                $scope.isValidate= false;
                                return
                            }
                        }
    
                        if(i%2==0)
                        {
                            console.log("inside axel config03",rowform.vehicleAxelConfiguration[i]);
                            if((rowform.vehicleAxelConfiguration[i]=='2') || (rowform.vehicleAxelConfiguration[i]=='4') || (rowform.vehicleAxelConfiguration[i]=='1') || (rowform.vehicleAxelConfiguration[i]=='0'))
                            {
                                $scope.isValidate= true;
                            } else{
                                toastr.error("Please Fill valid data in Vehicle Axel Configuration field");
                                $scope.isValidate= false;
                                return
                            }
                        }else{
                            if((rowform.vehicleAxelConfiguration[i].toLowerCase()=='x'))
                            {
                                $scope.isValidate= true;
                            } else{
                                toastr.error("Please Fill valid data in Vehicle Axel Configuration field");
                                $scope.isValidate= false;
                                return
                            }
                        }
                    }

                }
            }

            if(rowform._id == "" || rowform._id == undefined){
                vm.vehicleAxelConfiguration = rowform.vehicleAxelConfiguration; 
                addVehicleAxelConfig();
                $scope.clearData();
                rowform ={};
            }else{             
                vm._id = rowform._id; 
                vm.vehicleAxelConfiguration = rowform.vehicleAxelConfiguration; 
                updateVehicleAxelConfigDetails()
                $scope.clearData();
                rowform ={};
            }               
        }    

        // $("#popup").click(function () {
        //     $("#grid").jqxGrid('clearselection');
        //     getselectedrowindexes =null;
        //     selectedRowData =null;
        //     $('#_id').val('');
        //     vm._id =''
        //     //$("#jqxwindowVehicleAxel").jqxWindow('open');
        // });
        // //$("#jqxwindowVehicleAxel").bind('close', function (event){
        //     $scope.clearData();
        // });
        // For Loading the Grid Widgets
        function myGrid(){ 
        var source =
        {
            localdata: data,
            datatype: "json",
            datafields:
            [
                { name: '_id', type: 'string' },
                { name: 'vehicleAxelConfiguration', type: 'string' }
            ]
        };

        var dataAdapter = new $.jqx.dataAdapter(source);         
        $("#grid").jqxGrid(
        {
            width: '100%',
            theme: 'dark',
            source: dataAdapter,
            columnsresize: false,
            //selectionmode: 'checkbox',
            sortable: true,
            pageable: true,
            pagesize:5,
            autoheight: true,
            columnsheight: 60,
            rowsheight: 50,
            columns: [
                {
                    text: 'Sr No',sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                    cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                    }
                },
                { text: 'Name', datafield: 'vehicleAxelConfiguration',align: 'center',cellsalign: 'center', width: '66%' },
                {
                    text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    // render custom column.
                    return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    }
                }
            ]
        });
        // Initlize Button Event
        buttonsEvents();
        }
        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
        }
        // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id : selectedRowData._id,
                vehicleAxelConfiguration:selectedRowData.vehicleAxelConfiguration
            }
            $scope.openModal();
            //$("#jqxwindowVehicleAxel").jqxWindow('open'); 
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete this record?", function () {
                // user clicked "ok"
                deleteVehicleAxelConfigDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });            
        }
    
        $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
        $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                vehicleAxelConfiguration:""
            }
        }

        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowVehicleAxel.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope
            })
        };
    }
  
})();
  
  