(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleDetailsCtrl', VehicleDetailsCtrl);
  
    /** @ngInject */
    function VehicleDetailsCtrl($scope,vehicleService,toastr,localStorage,$timeout, $uibModal) {
        var vm = this;

        vm.curruserData = localStorage.getObject('dataUser');
        console.log(vm.curruserData.otherData.roleName);
        $scope.roleName=vm.curruserData.otherData.roleName;
        $scope.editFlag=false;
        //vm.getAllVehicleDetails = getAllVehicleDetails;
        //vm.addVehicleDetails = addVehicleDetails;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        vm.VehicleId="";
        initController();
        $scope.vehicleDetails = [];
        vm.datatableData = [];
        $scope.customers = [];
        $scope.vehiclegroups = [];
        $scope.vehicleManufacturers = [];
        $scope.vehicleModels = [];
        $scope.vehicleAxelConfigs = [];        
        $scope.currentOdoMeterValue = 0 ;
        $scope.currentDistanceTravelledValue = 0 ;
        $scope.minDate = new Date().toDateString();
        //to set todays date in the datepicker
        $scope.subscriptionStartDate= new Date().toDateString();
        var tt = $scope.subscriptionStartDate;

        var date = new Date(tt);
        var newdate = new Date(date);
    
        newdate.setDate(newdate.getDate());
        var dd = newdate.getDate();
        var mm = newdate.getMonth();
        var y = newdate.getFullYear()+1;
    
        var someFormattedDate = mm + '/' + dd + '/' + y;
        console.log(someFormattedDate);
        $scope.subscriptionEndDate= someFormattedDate;
        //$scope.subscriptionEndDate= $scope.subscriptionStartDate.setFullYear(new Date().getFullYear() + 1).toDateString();

        $scope.getEndDate = function (dt){
            var tt =dt;

            var date = new Date(tt);
            var newdate = new Date(date);
        
            newdate.setDate(newdate.getDate()-1);
            var dd = newdate.getDate();
            var mm = newdate.getMonth();
            var y = newdate.getFullYear()+1;
        
            var someFormattedDate = mm + '/' + dd + '/' + y;
            console.log(someFormattedDate);
            $scope.subscriptionEndDate= someFormattedDate;
        }
        function initController() {
            getAllDropdowns();
        }

        function getAllDropdowns() {
            getAllCustomerDetails();
            getAllVehicleGroupDetails();
            getAllVehicleModelDetails();
            getAllVehicleAxelConfigDetails();
            getAllVehicleManufacturerDetails();
            getAllVehicleDetails();            
        }

        $scope.getVehicleSepcs=function(userdata){
            //get vehicle specs
            //alert("here");
            var vehicleModel=$("#vehicleModelId option:selected").html();
            console.log(userdata,userdata.vehicleManufacturerId,vehicleModel);
            //userdata.vehicleManufacturerId,vehicleModel
            vehicleService.GetVehicleSepcs(userdata.vehicleManufacturerId,vehicleModel,function (result) {
                if (result.success === true) {
                    console.log(result.data);
                    // $scope.vccUser={
                    //     vehicleManufacturerId:result.data.vehicleManufacturerId,
                    //     vehicleModelId:result.data.vehicleModelId,
                    //      vehicleAxelConfigurationId:result.data.vehicleAxelConfigurationId,
                    //      rotationThreshold:result.data.rotationThreshold,
                    //     loadCapacity:result.data.loadCapacity
                    // }
                    //$scope.vccUser = {
                        $('#vehicleAxelConfigurationId').val(result.data.vehicleAxelConfigurationId);
                        $('#rotationThreshold').val(result.data.rotationThreshold);
                        if(result.data.loadCapacity){
                            $('#vehicleAxelConfigurationId').attr('disabled','disabled');
                            $('#loadCapacity').on('focus', function(e) { 
                                e.preventDefault();
                                $(this).blur();
                            })
                        }else{
                            $('#loadCapacity').removeAttr('disabled');
                            $('#vehicleAxelConfigurationId').removeAttr('disabled');
                        }
                        $('#loadCapacity').val(result.data.loadCapacity);
                        $('#vehicleManufacturerId').val(result.data.vehicleManufacturerId);
                        $('#vehicleModelId').val(result.data.vehicleModelId);
                    //}
                    
                } else {
                    $('#loadCapacity').off('focus'); 
                    $('#loadCapacity').removeAttr('disabled');
                    $('#vehicleAxelConfigurationId').removeAttr('disabled');
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        }

        function addVehicleDetails() {        
            console.log('inside add')    
            var paramsObj={
                vehicleGroupId:vm.vehicleGroupId, 
                vehicleRegistrationNumber:vm.vehicleRegistrationNumber,
                vehicleAxelConfigurationId:$('#vehicleAxelConfigurationId').val(),
                rotationThreshold:$('#rotationThreshold').val(),
                loadCapacity:$('#loadCapacity').val(),
                vehicleManufacturerId:vm.vehicleManufacturerId,
                vehicleModelNo:vm.vehicleModelNo,
                odometerReading:vm.odometerReading,
                distanceTravelled:vm.distanceTravelled,
                subscriptionStartDate:vm.subscriptionStartDate,
                subscriptionEndDate:vm.subscriptionEndDate,
                isvehicleModelNew:vm.isvehicleModelNew
                // $('#vehicleAxelConfigurationId').val(result.data.vehicleAxelConfigurationId);
                // $('#rotationThreshold').val(result.data.rotationThreshold);
                // $('#loadCapacity').val(result.data.loadCapacity);
                // $('#vehicleManufacturerId').val(result.data.vehicleManufacturerId);
                // $('#vehicleModelId').val(result.data.vehicleModelId);
            }
            vehicleService.AddVehicleSpec(vm.vehicleManufacturerId, vm.vehicleModelNo, $('#vehicleAxelConfigurationId').val(),$('#rotationThreshold').val(),$('#loadCapacity').val(), function (result) {
                if (result.success === true) {
                } else {        
                }
            });
            vehicleService.AddVehicleMaster(paramsObj,function (result) {
                if (result.success === true) {
                    vm.vehicleGroupId= "";
                    vm.vehicleRegistrationNumber= "";
                    vm.vehicleAxelConfigurationId= "";
                    vm.rotationThreshold="";
                    vm.loadCapacity="";
                    vm.vehicleManufacturerId="";
                    vm.vehicleModelNo="";
                    vm.distanceTravelled = "";
                    vm.odometerReading = "";
                    vm.subscriptionStartDate="";
                    vm.subscriptionEndDate="";
                    vm.isvehicleModelNew="";
                    $scope.vehicleDetails= [];
                    getAllDropdowns();
                    $scope.isValidate=true;
                    toastr.success(result.message);
                    vm.ModalInstance.close();
                    $scope.clearData();
                    //location.reload();
                    //$("#jqxwindowVehicleDetails").jqxWindow('close'); 
                } else {
                    $scope.isValidate=false;
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });

        };

        function getAllVehicleDetails() {            
            vehicleService.GetAllVehicleMaster(function (result) {
                console.log(result);
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.customers.length;j++){
                            if(result.data[i].customerId===$scope.customers[j].value){
                                result.data[i].customerName = $scope.customers[j].text;
                                break;
                            }else{
                                result.data[i].customerName = "None";
                            }
                        }
                        for(var k=0;k<$scope.vehicleManufacturers.length;k++){
                            if(result.data[i].vehicleManufacturerId===$scope.vehicleManufacturers[k].value){
                                result.data[i].vehicleManufacturerName = $scope.vehicleManufacturers[k].text;
                                break;
                            }else{
                                result.data[i].vehicleManufacturerName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.vehicleModels.length;l++){
                            if(result.data[i].vehicleModelId===$scope.vehicleModels[l].value){
                                result.data[i].vehicleModelName = $scope.vehicleModels[l].text;
                                break;
                            }else{
                                result.data[i].vehicleModelName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.vehicleAxelConfigs.length;l++){
                            if(result.data[i].vehicleAxelConfigurationId===$scope.vehicleAxelConfigs[l].value){
                                result.data[i].vehicleAxelConfigurationName = $scope.vehicleAxelConfigs[l].text;
                                break;
                            }else{
                                result.data[i].vehicleAxelConfigurationName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.vehiclegroups.length;l++){
                            if(result.data[i].vehiclegroupId===$scope.vehiclegroups[l].value){
                                result.data[i].vehiclegroupName = $scope.vehiclegroups[l].text;
                                break;
                            }else{
                                result.data[i].vehiclegroupName = "None";
                            }                             
                        }
                        $scope.vehicleDetails.push(result.data[i]);    
                    } 
                    myGrid()
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };

        function deleteVehicleDetails(VehicleId) {            
            vehicleService.DeleteVehicleMaster(VehicleId,function (result) {
                if (result.success === true) {
                    $scope.vehicleDetails=[];
                    getAllDropdowns();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        function updateVehicleDetails() {            
            vehicleService.UpdateVehicleMaster(vm.VehicleId, vm.vehicleGroupId, vm.vehicleRegistrationNumber, vm.vehicleAxelConfigurationId,
                vm.rotationThreshold,vm.loadCapacity,vm.vehicleManufacturerId,vm.vehicleModelNo,vm.odometerReading,vm.distanceTravelled,vm.subscriptionStartDate,
                vm.subscriptionEndDate,vm.isvehicleModelNew,function (result) {
                if (result.success === true) {
                    vm.VehicleId= "";
                    // vm.customerId='';
                    vm.vehicleGroupId= "";
                    vm.vehicleRegistrationNumber= "";
                    vm.vehicleAxelConfigurationId= "";
                    vm.rotationThreshold="";
                    vm.loadCapacity="";
                    vm.vehicleManufacturerId="";
                    vm.vehicleModelNo="";
                    vm.odometerReading="";
                    vm.distanceTravelled = "";
                    vm.subscriptionStartDate="";
                    vm.subscriptionEndDate="";
                    vm.isvehicleModelNew="";
                    $scope.vehicleDetails=[];
                    getAllDropdowns();
                    $scope.isValidate=true;
                    vm.ModalInstance.close();
                    $scope.clearData();
                    toastr.success(result.message);
                    //$("#jqxwindowVehicleDetails").jqxWindow('close'); 
                } else {
                    $scope.isValidate = false;
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            //validation
            console.log("rowform", rowform);
            $scope.isValidate= false;
            var vehicleAxelConfigurationId=$('#vehicleAxelConfigurationId').val();
            var rotationThreshold=$('#rotationThreshold').val();
            var loadCapacity=$('#loadCapacity').val();
            var odometerReading= $('#odometerReading').val();
            if(rowform=={} || rowform==undefined || rowform.vehicleGroupId==undefined|| rowform.vehicleRegistrationNumber==undefined || rowform.vehicleRegistrationNumber=='' || vehicleAxelConfigurationId=='' || rotationThreshold=='' || loadCapacity=='' || rowform.vehicleManufacturerId==undefined || rowform.vehicleModelId==undefined || odometerReading == ''){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                //$scope.isValidate= true;
            }
            
            if(rowform._id != undefined){
                vm.VehicleId = rowform._id; 
                //vm.customerId = rowform.customerId;
                vm.vehicleGroupId= rowform.vehicleGroupId;
                vm.vehicleRegistrationNumber= rowform.vehicleRegistrationNumber;
                //vm.vehicleAxelConfigurationId= rowform.vehicleAxelConfigurationId;
                vm.vehicleAxelConfigurationId = $("#vehicleAxelConfigurationId").val();
                vm.rotationThreshold = rowform.rotationThreshold;
                vm.loadCapacity = rowform.loadCapacity;
                vm.odometerReading = rowform.odometerReading;
                vm.distanceTravelled = rowform.distanceTravelled;
                vm.vehicleManufacturerId = rowform.vehicleManufacturerId;
                vm.vehicleModelNo= $('#vehicleModelId :selected').text();
                vm.subscriptionStartDate= rowform.subscriptionStartDate;//$('#subscriptionStartDate').val();
                vm.subscriptionEndDate= rowform.subscriptionEndDate; //$('#subscriptionEndDate').val();
                vm.isvehicleModelNew = false
                updateVehicleDetails()
                $scope.editFlag=false;
            }else{
                //vm.customerId = rowform.customerId;
                vm.vehicleGroupId= rowform.vehicleGroupId;
                vm.vehicleRegistrationNumber= rowform.vehicleRegistrationNumber;
                //vm.vehicleAxelConfigurationId= rowform.vehicleAxelConfigurationId;
                vm.vehicleAxelConfigurationId = $("#vehicleAxelConfigurationId").val();
                vm.rotationThreshold= rowform.rotationThreshold;
                vm.loadCapacity= rowform.loadCapacity;
                vm.odometerReading = rowform.odometerReading;
                vm.distanceTravelled = rowform.distanceTravelled;
                vm.vehicleManufacturerId= rowform.vehicleManufacturerId;
                vm.vehicleModelNo= $('#vehicleModelId :selected').text();
                vm.subscriptionStartDate= rowform.subscriptionStartDate;//$('#subscriptionStartDate').val();
                vm.subscriptionEndDate= rowform.subscriptionEndDate; //$('#subscriptionEndDate').val();
                vm.isvehicleModelNew = false
                addVehicleDetails();
                $scope.editFlag=false;
            }               
        }
         //to get all Customer Details details.
         function getAllCustomerDetails() {            
            $scope.customers = [];
            vehicleService.GetAllCustomerDetails(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.customers.length;i++){
                        $scope.customers.push({value: result.customers[i]._id, text: result.customers[i].customerName});    
                    }   
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        //to get all tire manufacurer details.
        function getAllVehicleManufacturerDetails() {            
            $scope.vehicleManufacturers = [];
            vehicleService.GetAllVehicleManufacturer(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleManufacturers.push({value: result.data[i]._id, text: result.data[i].vehicleManufacturerName});    
                    } 
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

          //to get all Vehicle  Axel Config details.
        function getAllVehicleAxelConfigDetails() {            
            $scope.vehicleAxelConfigs =[];
            vehicleService.GetAllVehicleAxelConfiguration(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleAxelConfigs.push({value: result.data[i]._id, text: result.data[i].vehicleAxelConfiguration});    
                    } 
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

         //to get all Vehicle  Group Name details.
         function getAllVehicleGroupDetails() {            
            $scope.vehiclegroups =[];
            vehicleService.GetAllVehicleGroup(function (result) {
                if (result.success === true) {  
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehiclegroups.push({value: result.data[i]._id, text: result.data[i].vehicleGroupName});    
                    } 
                } else {
                    toastr.error(result.message); 
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        };

        //to get all Vehicle Model details.
        function getAllVehicleModelDetails() {        
            $scope.vehicleModels=[];    
            vehicleService.GetAllVehicleModel(function (result) {
                console.log(result);
                if (result.success === true) { 
                    vm.userVehicleModel=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleModels.push({value: result.data[i]._id, text: result.data[i].vehicleModelNo, vehicleManufacturerId: result.data[i].vehicleManufacturerId});    
                    } 
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    //console.log(result);
                    vm.error = result.message;                    
                }
            });
        }
        // For Open Popup for Add and Edit        
        // $("#popup").click(function () {
        //     $("#gridvehicle").jqxGrid('clearselection');
        //     getselectedrowindexes =null;
        //     selectedRowData =null;
        //     $('#_id').val('');
        //     vm._id =''
        //     //$("#jqxwindowVehicleDetails").jqxWindow('open');
        // });
        // //$("#jqxwindowVehicleDetails").bind('close', function (event){
        //     $scope.clearData();
        // });
        // For Loading the Grid Widgets
        function myGrid() { 
            var source =
            {
                localdata: $scope.vehicleDetails,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: "subscriptionStartDate", type: 'string' },
                    { name: "subscriptionEndDate", type: 'string' },
                    { name: "customerId", type: 'string' },
                    { name: "vehicleGroupId", type: 'string' },
                    { name: "vehicleRegistrationNumber", type: 'string' },
                    { name: "vehicleAxelConfigurationId", type: 'string' },
                    { name: "vehicleManufacturerId", type: 'string' },
                    { name: "vehicleModelId", type: 'string' },
                    { name: "odometerReading", type: 'string'},
                    { name: "distanceTravelled", type: 'string'},
                    { name: "customerName", type: 'string' },
                    { name: "vehicleGroupName", type: 'string' },
                    { name: "vehicleAxelConfigurationName", type: 'string' },
                    { name: "vehicleManufacturerName", type: 'string' },
                    { name: "vehicleModelName", type: 'string' },

                    { name: 'rotationThreshold', type: 'number' },
                    { name: 'loadCapacity', type: 'number' }
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#gridvehicle").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Subscription Start Date', datafield: 'subscriptionStartDate',hidden:true,align: 'center',cellsalign: 'center', width: '15%',cellsformat: 'dd-MMM-yyyy' },
                    { text: 'Subscription End Date', datafield: 'subscriptionEndDate',hidden:true,align: 'center',cellsalign: 'center', width: '15%',cellsformat: 'dd-MMM-yyyy' },
                    { text: 'Customer Id', datafield: 'customerId',hidden:true,align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Odometer Reading', datafield: 'odometerReading',hidden:true,align: 'center',cellsalign: 'center', width: '25%' },
                    { text: 'Distance Travelled', datafield: 'distanceTravelled',hidden:true,align: 'center',cellsalign: 'center', width: '25%' },
                    //distanceTravelled
                    { text: 'Vehicle Group Id', datafield: 'vehicleGroupId',hidden:true,align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Vehicle Number', datafield: 'vehicleRegistrationNumber',align: 'center',cellsalign: 'center', width: '19%' },
                    { text: 'Vehicle Axel Configuration Name', datafield: 'vehicleAxelConfigurationId',hidden:true,align: 'center',cellsalign: 'center', width: '20%' },

                    { text: 'Vehicle Manufacturer Id', datafield: 'vehicleManufacturerId',hidden:true,align: 'center',cellsalign: 'center', width: '19%' },
                    { text: 'Vehicle Model Id', datafield: 'vehicleModelId',align: 'center',hidden:true,cellsalign: 'center', width: '20%' },

                    { text: 'Vehicle Manufacturer Name', datafield: 'vehicleManufacturerName',align: 'center',cellsalign: 'center', width: '19%' },
                    { text: 'Vehicle Model Name', datafield: 'vehicleModelName',align: 'center',cellsalign: 'center', width: '20%' },

                    { text: 'Rotation Threshold', datafield: 'rotationThreshold',align: 'center',cellsalign: 'center', width: '12%' },
                    { text: 'Load Capacity', datafield: 'loadCapacity',align: 'center',cellsalign: 'center', width: '10%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '14%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                            // render custom column.
                            if(vm.curruserData.otherData.roleName=="System Super Admin" || vm.curruserData.otherData.roleName=="System Admin"){
                                return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button disabled data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                            }else{
                                return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                            }
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents() 
            $("#gridvehicle").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
                buttonsEvents();
            });
        }
        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
        // For Edit Details
        $scope.currentOdoMeterValue = 0;
        function editDetails() { 
            $scope.editFlag=true;
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#gridvehicle').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#gridvehicle').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.subscriptionStartDate= selectedRowData.subscriptionStartDate;
            $scope.subscriptionEndDate= selectedRowData.subscriptionEndDate;            
            $scope.currentDistanceTravelledValue = 0;
            $scope.currentOdoMeterValue = selectedRowData.odometerReading;
            if(selectedRowData.distanceTravelled==undefined){
                $scope.currentDistanceTravelledValue = 0;
            }else{
                $scope.currentDistanceTravelledValue = selectedRowData.distanceTravelled;
            }
            
            $scope.vccUser = {
                _id : selectedRowData._id,
                //customerId: selectedRowData.customerId,
                vehicleGroupId: selectedRowData.vehicleGroupId,
                vehicleRegistrationNumber: selectedRowData.vehicleRegistrationNumber,
                vehicleAxelConfigurationId: selectedRowData.vehicleAxelConfigurationId,
                rotationThreshold: selectedRowData.rotationThreshold,
                loadCapacity: selectedRowData.loadCapacity,
                odometerReading: selectedRowData.odometerReading,
                distanceTravelled: selectedRowData.distanceTravelled,
                vehicleManufacturerId: selectedRowData.vehicleManufacturerId,
                vehicleModelId: selectedRowData.vehicleModelId,
                subscriptionStartDate: selectedRowData.subscriptionStartDate,
                subscriptionEndDate: selectedRowData.subscriptionEndDate,
                isvehicleModelNew: selectedRowData.isvehicleModelNew
            }
            $timeout(function(){ $("#vehicleAxelConfigurationId").attr("disabled","disabled") },100);
            
            $scope.openModal();
            
            //$("#jqxwindowVehicleDetails").jqxWindow('open'); 
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#gridvehicle').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#gridvehicle').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteVehicleDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
        
        }    

        $scope.clearData = function(){ //window.location.reload();
            $("#gridvehicle").jqxGrid('clearselection');
            $scope.currentOdoMeterValue=0;
            $scope.subscriptionStartDate= new Date().toDateString();
            //$scope.subscriptionEndDate= new Date().toDateString();
            $scope.vccUser ={};
            $scope.getEndDate($scope.subscriptionStartDate);
        }

        $scope.openModal = function () {
            vm.ModalInstance =$uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowVehicleDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    console.log(data , "data here");
                    if($scope.editFlag==true && data.odometerReading < $scope.currentOdoMeterValue ){
                        alertify.alert("Odometer value should be higher than prevoius value, Please change its value");
                        return
                    }else{
                        //$scope.currentOdoMeterValue = selectedRowData.odometerReading;
                        if(data.odometerReading==0){
                            data.distanceTravelled= 0;
                        }else{
                            data.distanceTravelled= $scope.currentDistanceTravelledValue+(data.odometerReading - $scope.currentOdoMeterValue);
                        }
                        
                        $scope.currRowData(data);
                        if($scope.isValidate==true){
                            $uibModalInstance.close();
                            $scope.clearData();
                            $scope.currentOdoMeterValue=0;
                            $scope.editFlag=false;
                        }      
                    }              
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.editFlag=false;
                    $scope.clearData();
                    $scope.currentOdoMeterValue=0;
                };
              },
                scope: $scope,
                backdrop:'static'
            })
        };
    }
  
})();
  
  