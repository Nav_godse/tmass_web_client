(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleModelCtrl', VehicleModelCtrl);
  
    /** @ngInject */
    function VehicleModelCtrl($scope,vehicleService,$filter, editableOptions, editableThemes,toastr, $uibModal) {
        var vm = this;
        vm.getAllVehicleModelDetails = getAllVehicleModelDetails;
        vm.addVehicleModel = addVehicleModel;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            vehicleManufacturerId:"",
            vehicleModelNo:"",
            vehicleModelDescription:""
       }
        initController();
        $scope.vehicleModels = [];
        var data = [];
        $scope.vehicleManufacturers = [];
        $scope.currVehicleManufacturerId=0;

        function initController() {
            getAllVehicleManufacturerDetails();       
            //$("#jqxwindowVehicleModel").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });    
        }

        function addVehicleModel() {            
            vehicleService.AddVehicleModel(vm.vehicleManufacturerId, vm.vehicleModelNo, vm.vehicleModelDescription, function (result) {
                if (result.success === true) { 
                    vm.vehicleManufacturerId= "";
                    vm.vehicleModelNo= "";
                    vm.vehicleModelDescription= "";
                    $scope.vehicleModels= [];
                    data=[];
                    getAllVehicleModelDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowVehicleModel").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function getAllVehicleModelDetails() {            
            vehicleService.GetAllVehicleModel(function (result) {
                if (result.success === true) {
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.vehicleManufacturers.length;j++){                        
                            if(result.data[i].vehicleManufacturerId==$scope.vehicleManufacturers[j].value){
                                result.data[i].vehicleManufacturerName= $scope.vehicleManufacturers[j].text
                            }
                        }
                        $scope.vehicleModels.push(result.data[i]);    
                        data.push(result.data[i]);
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function deleteVehicleModelDetails(_id) {         
            // confirm dialog
            vehicleService.DeleteVehicleModel(_id,function (result) {
                if (result.success === true) {
                    $scope.vehicleModels=[];
                    data=[];
                    getAllVehicleModelDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            }); 
        }

        function updateVehicleModelDetails() {            
            vehicleService.UpdateVehicleModel(vm._id, vm.vehicleManufacturerId, vm.vehicleModelNo, vm.vehicleModelDescription,function (result) {
                if (result.success === true) { 
                    vm.vehicleManufacturerId= "";
                    vm.vehicleModelNo= "";
                    vm.vehicleModelDescription= "";
                    $scope.vehicleModels=[];
                    data=[];
                    getAllVehicleModelDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowVehicleModel").jqxWindow('close');
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';
                    vm.error = result.message;                    
                }
            });
        };
        //to get all tire manufacurer details.
        function getAllVehicleManufacturerDetails() {            
            vehicleService.GetAllVehicleManufacturer(function (result) {
                if (result.success === true) {
                    vm.userVehicleManufacturer=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleManufacturers.push({value: result.data[i]._id, text: result.data[i].vehicleManufacturerName});    
                    }      
                    getAllVehicleModelDetails();
                    //toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                    //vm.error = 'Username or password is incorrect';\
                    vm.error = result.message;                    
                }
            });
        };
        
        $scope.currRowData = function(rowform) { 
            //for validation if data is blank or not.
            $scope.isValidate= false;
            if(rowform==undefined || rowform.vehicleModelNo==''|| rowform.vehicleManufacturerId=='' || (rowform.vehicleManufacturerId==undefined)||(rowform.vehicleModelNo==undefined)){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }
            if(rowform._id == "" || rowform._id == undefined){
                vm.vehicleManufacturerId= rowform.vehicleManufacturerId;
                vm.vehicleModelNo= rowform.vehicleModelNo;
                vm.vehicleModelDescription= rowform.vehicleModelDescription;
                addVehicleModel();
                $scope.clearData();
                rowform ={};
            }else{
                vm._id = rowform._id; 
                vm.vehicleManufacturerId= rowform.vehicleManufacturerId;
                vm.vehicleModelNo= rowform.vehicleModelNo;
                vm.vehicleModelDescription= rowform.vehicleModelDescription;
                updateVehicleModelDetails();
                $scope.clearData();
                rowform ={};
            }               
        }

      /////////////////////////////////////////////
        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'vehicleManufacturerId', type: 'string' },
                    { name: 'vehicleManufacturerName', type: 'string' },
                    { name: 'vehicleModelNo', type: 'string' },
                    { name: 'vehicleModelDescription', type: 'string' },
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#grid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                            return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Manufacturer Id', datafield: 'vehicleManufacturerId',align: 'center',cellsalign: 'center', width: '20%', hidden:true },
                    { text: 'Manufacturer Name', datafield: 'vehicleManufacturerName',align: 'center',cellsalign: 'center', width: '20%' },
                    { text: 'Model No', datafield: 'vehicleModelNo',align: 'center',cellsalign: 'center', width: '18%' },
                    { text: 'Model Description', datafield: 'vehicleModelDescription',align: 'center',cellsalign: 'center', width: '28%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button disabled style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button><input disabled type="button" data-row="' + row + '" class="deleteButtons btn btn-danger" value="Delete" /> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
            $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) {
                buttonsEvents();
            })
        }
             
        
            function buttonsEvents() {
                // For Edit Details
                var editbuttons = document.getElementsByClassName('editButtons');
                for (var i = 0; i < editbuttons.length; i+=1) {
                    editbuttons[i].addEventListener('click', editDetails);
                }
                // For Delete Details
                var deletebuttons = document.getElementsByClassName('deleteButtons');
                for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
                }
             }


             function editDetails() { 
                var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                { 
                    var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                $scope.vccUser = {
                    _id : selectedRowData._id,
                    vehicleManufacturerId:selectedRowData.vehicleManufacturerId,
                    vehicleModelNo:selectedRowData.vehicleModelNo,
                    vehicleModelDescription: selectedRowData.vehicleModelDescription
                }
                //$scope.currVehicleManufacturerId = selectedRowData.vehicleManufacturerId;
                //$("#jqxwindowVehicleModel").jqxWindow('open'); 
                $scope.openModal();
            }  
    
            function deleteDetails(){ 
                getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0)
                {
                    // returns the selected row's data.
                    selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
                } 
                // confirm dialog
                alertify.confirm("Are you sure, you want to delete this record?", function () {
                // user clicked "ok"
                deleteVehicleModelDetails(selectedRowData._id);
                }, function() {
                // user clicked "cancel"
                })                  
            }
    
            

        $scope.clearData = function(){ //window.location.reload();

            $("#grid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                vehicleManufacturerId:"",
                vehicleModelNo:"",
                vehicleModelDescription:""
           }
        }

        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowVehicleModel.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              scope: $scope,
              backdrop:'static'
            })
        };
    }
  
})();
  
  