(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleSpecCtrl', VehicleSpecCtrl);
  
    /** @ngInject */
    function VehicleSpecCtrl($scope,vehicleService,toastr, $uibModal,$http,config,animation) {
        var vm = this;
        vm.getAllVehicleSpecDetails = getAllVehicleSpecDetails;
        vm.addVehicleSpec = addVehicleSpec;
        vm.VehicleSpeclId="";
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            vehicleManufacturerId: "",
            vehicleModelId: "",
            vehicleAxelConfigurationId: "",
            rotationThreshold:"",
            loadCapacity:""
       }
        initController();
        $scope.vehicleSpecications = [];
        vm.datatableData = [];
        $scope.vehicleManufacturers = [];
        $scope.vehicleModels = [];
        $scope.vehicleAxelConfigs = [];

        function initController() {
            getAllDropdowns()            
            // For Open Popup for Add and Edit
            //$("#jqxwindowVehicleSpec").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }
        $scope.openModal = function () {
            vm.ModalInstance0= $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowVehicleSpec.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.save = function (data) {
                        console.log(data);
                        $scope.currRowData(data);
                        if($scope.isValidate==true){
                            $uibModalInstance.dismiss('cancel');
                        }                    
                    };

                    $scope.uploadTagDetails = function() {
                        //console.log("ssssss"); 
                        var returnVal =false;
                        var currData = {};
                        var fileInput = document.getElementById('the-file');
                        var file = fileInput.files[0];     
                        var fd = new FormData();
                        fd.append('file', file);
                        fd.append('data', 'string');
                        $http.post(config.apiUrl+'/api/upload', fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        })
                        .success(function(response){
                            console.log(response);
                            var currTagType= '';
                            bulktags=[];
                            if(response.data){
                                if(response.data.length ==0){
                                    alertify.alert("Please check uploaded file for valid data");
                                    return
                                }
                                if(Object.keys(response.data[0]).length!=5){
                                    alertify.alert("Please check your file with sample file and upload again");
                                    return
                                }
                                
                                //alert(response.data.length);
                                if(response.data.length>0)
                                {
                                for(var i=0;i<response.data.length;i++){
                                //  console.log(response.data[i]);
                                    returnVal =false;
                                    //console.log(response.data[i]['depot name']);
                                    //console.log(response.data[i]['pincode']);
                                    if((response.data[i]['depot name']=="")&&(response.data[i]['depot name']=="")&&(response.data[i]['depot name']=="")&&(response.data[i]['pincode']=="")&&(response.data[i]['lattitude']=="")&&(response.data[i]['longitude']==""))
                                    {
                                        continue;
                                    }
                                    if((response.data[i]['depot name']!="")&&(response.data[i]['pincode']!="")&&(response.data[i]['latitude']!="")&&(response.data[i]['logitude']!="")&&(response.data[i]['city']!="")&&(response.data[i]['country']!="")&&(response.data[i]['state']!=""))
                                    {
                                        returnVal= validatefilerows(response.data[i]['depot name'],
                                        response.data[i]['pincode'],
                                        response.data[i]['lattitude'],
                                        response.data[i]['longitude'],
                                        response.data[i]['city'],
                                        response.data[i]['country'],
                                        response.data[i]['state']);
                                        
                                    }else{
                                        returnVal =false;
                                    }

                                    $.each(bulktags, function (index, value) {
                                    
                                        if(returnVal)
                                        {
                                        if(value.depotName.toLowerCase()==response.data[i]['depot name'].toLowerCase())
                                        {
                                            returnVal= false;
                                            //break;
                                        }else{
                                        //  console.log("Unique "+response.data[i]['depot name']);
                                            returnVal= true;
                                        }
                                    }
                                    
                                    });

                                if(returnVal)
                                {
                                        currData = {
                                        depotName:response.data[i]['depot name'],
                                        addressLine1: response.data[i]['address 1'],
                                        addressLine2:response.data[i]['address 2'],
                                        pincode: response.data[i]['pincode'],
                                        cityId: response.data[i]['city'],
                                        stateId :response.data[i]['state'],
                                        countryName :response.data[i]['country'],
                                        latitude :response.data[i]['lattitude'],
                                        logitude :response.data[i]['longitude']
                                    };
                                    bulktags.push(currData);
                                }
                                else{
                                    //console.log("invalid");
                                    alertify.alert("INVALID DATA : depotName :" +response.data[i]['depot name']+", addressLine1: "+ response.data[i]['address 1']+", addressLine2: "+ response.data[i]['address 2']
                                    +", pincode: "+ response.data[i]['pincode']
                                    +", cityId: "+ response.data[i]['cityId']
                                    +", stateId: "+ response.data[i]['state']
                                    +", countryName: "+ response.data[i]['country']
                                    +", latitude: "+ response.data[i]['lattitude']
                                    +", logitude: "+ response.data[i]['longitude']
                                );
                                    break;
                                }


                                }  
                                    if(returnVal)
                                    {
                                    $scope.fileData= bulktags;
                                    //if(response.data.length== bulktags.length)
                                    console.log(bulktags +" bulktags");
                                        AdddepotinBulk(bulktags);
                                        getAllDropdowns();
                                        //$('#vehiclespecbulk').waitMe('hide');
                                    }else{
                                        console.log("invalid");
                                    }
                                
                                    
                                }else{
                                    console.log("file is empty");
                                }
                            }
                        })
                        .error(function(response){
                            console.log(response);
                        });
                    }

                    function validatefilerows(depotname, pincode,latitude,logitude,city,country,state)
                    {
                    var DepotCode = /^[a-zA-Z_]*$/;
                    var EmptyCode = /^[A-Za-z\s]*$/;
                    var CheckZipCode = /(^\d{6}$)/;
                    var lat=/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
                    var lon=/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;
                    var ErrFlag=false;
                    if(DepotCode.test(depotname))
                    {
                    ErrFlag= true;
                    }
                    else
                    {
                    ErrFlag= false;
                    return ErrFlag;
                    }

                    if(EmptyCode.test(city))
                    {
                    console.log(EmptyCode.test(city) +" eeeeeeee "+city);
                    ErrFlag= true;
                    }
                    else
                    {
                    console.log(EmptyCode.test(city) +" eeeeeeee "+city);
                    ErrFlag= false;
                    return ErrFlag;
                    }

                    if(EmptyCode.test(country))
                    {
                    ErrFlag= true;
                    }
                    else
                    {
                    ErrFlag= false;
                    return ErrFlag;
                    }

                    if(EmptyCode.test(state))
                    {
                    ErrFlag= true;
                    }
                    else
                    {
                    ErrFlag= false;
                    return ErrFlag;
                    }

                    if(CheckZipCode.test(pincode))   
                    {
                    ErrFlag= true;
                    }
                    else
                    {
                    ErrFlag= false;
                    return ErrFlag;
                    }

                    if(!lat.test(latitude)) {   
                    ErrFlag= false;
                    return ErrFlag;
                    } else {
                    ErrFlag= true;
                    }

                    if(!lon.test(logitude)) 
                    {   
                    ErrFlag= false;
                    return ErrFlag;
                    } else {
                    ErrFlag= true;
                    }

                    return  ErrFlag;

                    }

                        var bulktags = [];

                        function addBulkTags(data) {
                            console.log(data);
                            depotService.AddBulkDepot(data,function (result) {
                                if (result.success === true) {
                                    toastr.success(result.message);   
                                    getAllCustomerDepotDetails();
                                    $scope.clearData();
                                }else{
                                    toastr.error(result);                
                                }
                            })
                        }

                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                            $scope.clearData();
                        };

                },
                    backdrop: 'static',
                    scope: $scope,
            })
        };
        function getAllDropdowns(){
            getAllVehicleManufacturerDetails();
            getAllVehicleModelDetails();
            getAllVehicleAxelConfigDetails();
            getAllVehicleSpecDetails();
        }
        function addVehicleSpec() {            
            vehicleService.AddVehicleSpec(vm.vehicleManufacturerId, vm.vehicleModelNo, vm.vehicleAxelConfigurationId,
                vm.rotationThreshold,vm.loadCapacity, function (result) {
                if (result.success === true) {
                    vm.vehicleManufacturerId= "";
                    vm.vehicleModelNo= "";
                    vm.vehicleAxelConfigurationId= "";
                    vm.rotationThreshold="";
                    vm.loadCapacity="";
                    $scope.vehicleSpecications= [];
                    getAllVehicleSpecDetails();
                    toastr.success(result.message);
                    $scope.isValidate=true;
                    vm.ModalInstance0.close();
                    //$("#jqxwindowVehicleSpec").jqxWindow('close');
                    $scope.clearData();
                } else {
                    toastr.error(result.message); 
                    $scope.isValidate=false;
                    //console.log(result);           
                }
            });
        };

        function getAllVehicleSpecDetails() {            
            vehicleService.GetAllVehicleSpec(function (result) {
                //console.log(result);
                $scope.vehicleSpecications=[]
                if (result.success === true) {                  
                    for(var i=0;i<result.data.length;i++){
                        for(var j=0;j<$scope.vehicleAxelConfigs.length;j++){
                            if(result.data[i].vehicleAxelConfigurationId===$scope.vehicleAxelConfigs[j].value){
                                result.data[i].vehicleAxelConfigurationName = $scope.vehicleAxelConfigs[j].text;
                                break;
                            }else{
                                result.data[i].vehicleAxelConfigurationName = "None";
                            }
                        }
                        for(var k=0;k<$scope.vehicleModels.length;k++){
                            if(result.data[i].vehicleModelId===$scope.vehicleModels[k].value){
                                result.data[i].vehicleModelName = $scope.vehicleModels[k].text;
                                break;
                            }else{
                                result.data[i].vehicleModelName = "None";
                            } 
                        }
                        for(var l=0;l<$scope.vehicleManufacturers.length;l++){
                            if(result.data[i].vehicleManufacturerId===$scope.vehicleManufacturers[l].value){
                                result.data[i].vehicleManufacturerName = $scope.vehicleManufacturers[l].text;
                                break;
                            }else{
                                result.data[i].vehicleManufacturerName = "None";
                            } 
                        }
                        $scope.vehicleSpecications.push(result.data[i]);    
                    }
                    myGrid();
                } else {
                    toastr.error(result.message);
                    vm.error = result.message;                    
                }
            });
        };

        function deleteVehicleSpecDetails(VehicleSpeclId) {            
            vehicleService.DeleteVehicleSpec(VehicleSpeclId,function (result) {
                if (result.success === true) {
                    $scope.vehicleSpecications=[];
                    getAllVehicleSpecDetails();
                    toastr.success(result.message);
                    $scope.clearData();
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };

        function updateVehicleSpecDetails() {            
            vehicleService.UpdateVehicleSpec(vm.VehicleSpeclId, vm.vehicleManufacturerId, vm.vehicleModelNo, vm.vehicleAxelConfigurationId,
                vm.rotationThreshold,vm.loadCapacity,function (result) {
                if (result.success === true) {
                    vm.VehicleSpeclId= "";
                    vm.vehicleManufacturerId= "";
                    vm.vehicleModelNo= "";
                    vm.vehicleAxelConfigurationId= "";
                    vm.rotationThreshold="";
                    vm.loadCapacity="";
                    $scope.vehicleSpecications=[];
                    getAllVehicleSpecDetails();
                    toastr.success(result.message);
                    $scope.isValidate=true;
                    vm.ModalInstance0.close();
                    $scope.clearData();
                } else {
                    toastr.error(result.message);
                    $scope.isValidate=false;
                    //console.log(result);   
                }
            });
        };

        $scope.currRowData = function(rowform) {
            //for validation if data is blank or not.

            $scope.isValidate= false;
            if(rowform==undefined||(rowform.vehicleManufacturerId==undefined)||(rowform.vehicleModelId==undefined)||(rowform.vehicleAxelConfigurationId==undefined)
            ||(rowform.rotationThreshold==undefined)||(rowform.loadCapacity==undefined) || rowform.loadCapacity=="" || rowform.rotationThreshold==""){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                //$scope.isValidate= true;
            }
            if(rowform._id != ""){
                vm.VehicleSpeclId = rowform._id; 
                vm.vehicleManufacturerId= rowform.vehicleManufacturerId;
                vm.vehicleModelNo= $('#vehicleModelId :selected').text();
                vm.vehicleAxelConfigurationId= rowform.vehicleAxelConfigurationId;
                vm.rotationThreshold= JSON.parse(rowform.rotationThreshold);
                vm.loadCapacity= JSON.parse(rowform.loadCapacity);                
                updateVehicleSpecDetails()
            }else{
                vm.vehicleManufacturerId= rowform.vehicleManufacturerId;
                vm.vehicleModelNo=$('#vehicleModelId :selected').text();
                vm.vehicleAxelConfigurationId= rowform.vehicleAxelConfigurationId;
                vm.rotationThreshold= JSON.parse(rowform.rotationThreshold);
                vm.loadCapacity= JSON.parse(rowform.loadCapacity);
                addVehicleSpec();
            }               
        }

        //to get all tire manufacurer details.
        function getAllVehicleManufacturerDetails() {            
            vehicleService.GetAllVehicleManufacturer(function (result) {
                if (result.success === true) {
                  //  //console.log(result.data.length);
                  $scope.vehicleManufacturers=[];
                    vm.userVehicleManufacturer=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleManufacturers.push({value: result.data[i]._id, text: result.data[i].vehicleManufacturerName});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);       
                }
            });
        };

        //to get all Vehicle  Axel Config details.
        function getAllVehicleAxelConfigDetails() {            
            vehicleService.GetAllVehicleAxelConfiguration(function (result) {
                if (result.success === true) {
                    $scope.vehicleAxelConfigs=[];
                    vm.userVehicleAxelConfig=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleAxelConfigs.push({value: result.data[i]._id, text: result.data[i].vehicleAxelConfiguration});    
                    }
                    //console.log($scope.vehicleAxelConfigs);      
                } else {
                    toastr.error(result.message);
                    //console.log(result);         
                }
            });
        };

        //to get all Vehicle Model details.
        function getAllVehicleModelDetails() {            
            vehicleService.GetAllVehicleModel(function (result) {
                if (result.success === true) {
                    $scope.vehicleModels=[];
                    vm.userVehicleModel=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicleModels.push({value: result.data[i]._id, text: result.data[i].vehicleModelNo, vehicleManufacturerId:result.data[i].vehicleManufacturerId});    
                    }
                } else {
                    toastr.error(result.message);
                    //console.log(result);     
                }
            });
        };


        // For Loading the Grid Widgets
        function myGrid(){ 
        var source =
        {
            localdata: $scope.vehicleSpecications,
            datatype: "json",
            datafields:
            [
                { name: '_id', type: 'string' },
                { name: 'vehicleManufacturerId', type: 'string' },
                { name: 'vehicleModelId', type: 'string' },
                { name: 'vehicleAxelConfigurationId', type: 'string' },
                { name: 'vehicleManufacturerName', type: 'string' },
                { name: 'vehicleModelName', type: 'string' },
                { name: 'vehicleAxelConfigurationName', type: 'string' },
                { name: 'rotationThreshold', type: 'number' },
                { name: 'loadCapacity', type: 'number' }
            ]
        };

        var dataAdapter = new $.jqx.dataAdapter(source);         
        $("#grid").jqxGrid(
        {
            width: '100%',
            theme: 'dark',
            source: dataAdapter,
            columnsresize: false,
            //selectionmode: 'checkbox',
            sortable: true,
            pageable: true,
            pagesize:5,
            autoheight: true,
            columnsheight: 60,
            rowsheight: 50,
            columns: [
                {
                    text: 'Sr No',sortable: false, filterable: false, editable: false,
                    groupable: false, draggable: false, resizable: false,
                    datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '5%' ,
                    cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                    }
                },
                { text: 'vehicleManufacturerId', datafield: 'vehicleManufacturerId',hidden:true,align: 'center',cellsalign: 'center', width: '33%' },
                { text: 'vehicleModelId', datafield: 'vehicleModelId',hidden:true,align: 'center', width: '33%' },
                { text: 'vehicleAxelConfigurationId', datafield: 'vehicleAxelConfigurationId',hidden:true,align: 'center',cellsalign: 'center', width: '33%' },

                { text: 'Vehicle Manufacturer', datafield: 'vehicleManufacturerName',align: 'center',cellsalign: 'center', width: '20%' },
                { text: 'Vehicle Model', datafield: 'vehicleModelName',align: 'center',cellsalign: 'center', width: '19%' },
                { text: 'Vehicle Axel Configuration', datafield: 'vehicleAxelConfigurationName',align: 'center',cellsalign: 'center', width: '20%' },
                { text: 'Rotation Threshold', datafield: 'rotationThreshold',align: 'center',cellsalign: 'center', width: '12%' },
                { text: 'Load Capacity', datafield: 'loadCapacity',align: 'center',cellsalign: 'center', width: '10%' },
                {
                    text: 'Action', cellsAlign: 'center', align: "center",width: '14%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                    // render custom column.
                    return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                    }
                }
            ]
        });
        buttonsEvents();
        // Initlize Button Event
        $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) { 
            buttonsEvents();
        });
        }
        function buttonsEvents() 
        {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
            // For Delete Details
            var deletebuttons = document.getElementsByClassName('deleteButtons');
            for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
            }
        }
        // For Edit Details
        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            console.log('selectedRowData');
            console.log(selectedRowData);
            $scope.vccUser = {
                _id : selectedRowData._id,
                vehicleManufacturerId: selectedRowData.vehicleManufacturerId,
                vehicleModelId: selectedRowData.vehicleModelId,
                vehicleAxelConfigurationId: selectedRowData.vehicleAxelConfigurationId,
                rotationThreshold:selectedRowData.rotationThreshold,
                loadCapacity:selectedRowData.loadCapacity
            }
            $scope.openModal();
        }  
        // For Delete Details
        function deleteDetails(){
            getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete?", function () {
                // user clicked "ok".
                deleteVehicleSpecDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            });
        
        }
    
        $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                vehicleManufacturerId: "",
                vehicleModelId: "",
                vehicleAxelConfigurationId: "",
                rotationThreshold:"",
                loadCapacity:""
            }
            $scope.isValidate=true;
        }

        $scope.openvehicleSpecModal = function () {
            vm.ModalInstance1 =$uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowVehicleSpecDetailBulkUpload.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.save = function (data) {
                        console.log(data);
                        $scope.currRowData(data);
                        if($scope.isValidate==true){
                            $uibModalInstance.close();
                        }                    
                    };

                    $scope.uploadTagDetails = function() {
                        //console.log("ssssss"); 
                    var returnVal =0;
                        var currData = {};
                        var fileInput = document.getElementById('the-file');
                        var file = fileInput.files[0];     
                        var fd = new FormData();
                        fd.append('file', file);
                        fd.append('data', 'string');
                        $http.post(config.apiUrl+'/api/upload', fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        })
                        .success(function(response){
                            console.log(response);
                            var currTagType= '';
                            bulktags=[];
                            if(response.data.length ==0){
                                alertify.alert("Please check uploaded file for valid data");
                                return
                            }
                            if(response.data){
                                if(Object.keys(response.data[0]).length!=5){
                                    alertify.alert("Please check your file with sample file and upload again");
                                    return
                                }
                                console.log(response.data);
                                if(response.data.length>0)
                                {
                                for(var i=0;i<response.data.length;i++){
                                  console.log(response.data[i]);
                                    returnVal = 0;
                                    //console.log(response.data[i]['depot name']);
                                    //console.log(response.data[i]['pincode']);
                                    if((response.data[i]['vehicle manufacturer']=="")&&(response.data[i]['vehicle model']=="")&&(response.data[i]['vehicleaxel configuration']=="")&&(response.data[i]['rotation threshold']=="")&&(response.data[i]['load capacity']==""))
                                    {
                                        continue;
                                    }
                                    // if((response.data[i]['vehicle manufacturer']!="")&&(response.data[i]['vehicle model']!="")&&(response.data[i]['vehicleaxel configuration']!="")&&(response.data[i]['rotation threshold']!="")&&(response.data[i]['load capacity']!=""))
                                    // {
                                    returnVal= validatefilerows(response.data[i]['vehicle manufacturer'],
                                    response.data[i]['vehicle model'],
                                    response.data[i]['vehicleaxel configuration'],
                                    response.data[i]['rotation threshold'],
                                    response.data[i]['load capacity']);
                                    console.log(returnVal +" returnVal returnVal returnVal");
                                    // }else{
                                    //     returnVal =1;
                                    // }
                                    if(returnVal==0)
                                    {
                                    $.each(bulktags, function (index, value) {
                                    
                                        if(returnVal)
                                        {
                                        if(
                                            (value.vehiclemanufacturer.toLowerCase()==response.data[i]['vehicle manufacturer'].toLowerCase())
                                            &&
                                            (value.vehiclemodel.toLowerCase()==response.data[i]['vehicle model'].toLowerCase())
                                        )
                                        {
                                            returnVal= 6;
                                            //break;
                                        }else{
                                        //  console.log("Unique "+response.data[i]['depot name']);
                                        returnVal= 0;
                                        }
                                    }
                                    
                                    });
                                }

                                if(returnVal==0)
                                {
                                        currData = {
                                            vehiclemanufacturer:response.data[i]['vehicle manufacturer'],
                                            vehiclemodel: response.data[i]['vehicle model'],
                                            vehicleaxelconfiguration:response.data[i]['vehicleaxel configuration'],
                                            rotationthreshold: response.data[i]['rotation threshold'],
                                            loadcapacity: response.data[i]['load capacity']
                                    };
                                    bulktags.push(currData);
                                }
                                else{
                                    //console.log("invalid");
                                    if(returnVal==1)
                                    {
                                        alertify.alert("VehicleAxel Configuration is invalid at line "+(i+1));
                                    }else if(returnVal==2)
                                    {
                                        alertify.alert("Vehicle Manufacturer is invalid at line "+(i+1));
                                    }else if(returnVal==3)
                                    {
                                        alertify.alert("Rotation Threshold is invalid at line "+(i+1));
                                    }else if(returnVal==4)
                                    {
                                        alertify.alert("Load Capacity is invalid at line "+(i+1));
                                    }else if(returnVal==5)
                                    {
                                        alertify.alert("Max Pressure Threshold is invalid at line "+(i+1));
                                    }else if(returnVal==6)
                                    {
                                        alertify.alert("Duplicate record is present at line "+(i+1));
                                    }else if(returnVal==7)
                                    {
                                        alertify.alert("Vehicle Model is invalid at line "+(i+1));
                                    }
                                    break;
                                }


                                }  
                                    if(returnVal==0)
                                    {
                                    $scope.fileData= bulktags;
                                    //if(response.data.length== bulktags.length)
                                      console.log(JSON.stringify(bulktags) +" bulktags12");
                                        AdddepotinBulk(bulktags);
                                        $uibModalInstance.close();
                                    }else{
                                        console.log("invalid");
                                    }
                                
                                    
                                }else{
                                    console.log("file is empty");
                                }
                            }
                        })
                        .error(function(response){
                            console.log(response);
                        });
                    }

        function validatefilerows(Vehmanuf, vehmod,axelconfig,threshold,loadcap)
            {
                var ErrorFlag=0;
                var NameCode= /^(?=.*[a-zA-Z0-9])$/;
                var req = /^(?=.*[a-zA-Z0-9]).{2,100}$/;
                //var req = /[a-zA-Z]/;
                   //var ErrFlag=false;
                //    if((Vehmanuf!="")||(vehmod!="")||(axelconfig!="")||(threshold!="")||(loadcap!="")){
                //     ErrorFlag=true;
                //    }else{
                //     return  false; 
                //    }

                   //var axelconfig=axelconfig.split();

                   if(!Vehmanuf.match(req)) {
                        console.log("Vehmanuf+",i," : ",Vehmanuf);
                        ErrorFlag=2;
                        console.log("Vehmanuf invalid number "+Vehmanuf);
                        return  ErrorFlag;
                        }else{
                        ErrorFlag =0;
                    }

                    if(!vehmod.match(req)) {
                        console.log("vehmod+",i," : ",vehmod);
                        ErrorFlag=7;
                        console.log("vehmod invalid number "+vehmod);
                        return  ErrorFlag;
                        }else{
                        ErrorFlag =0;
                    }


                    if( axelconfig!=undefined && axelconfig.length>0 ){
                        if(axelconfig.length>15){
                            ErrorFlag=1; 
                            return ErrorFlag;
                        }
                        for(var i=0;i<axelconfig.length;i++)
                        {
                            console.log(axelconfig[i]);
                            //start axel 
                            if((i==0)&&(axelconfig[i]=='0' || axelconfig[i]=='2' || axelconfig[i]=='1' || axelconfig[i]=='4'))
                            {
                                ErrorFlag=0; 
                            }

                            if((i+1==axelconfig.length))
                            {
                                if((axelconfig[i].toLowerCase()!='x'))
                                {
                                    ErrorFlag=0; 
                                }else
                                {
                                    ErrorFlag=1; 
                                    return ErrorFlag;
                                }
                            }

                            if(i%2==0)
                            {
                                console.log("inside axel config",axelconfig[i]);
                                if((axelconfig[i]=='0') || (axelconfig[i]=='2') || (axelconfig[i]=='4') || (axelconfig[i]=='1') || (axelconfig[i]=='0'))
                                {
                                    ErrorFlag=0; 
                                } else{
                                    ErrorFlag=1; 
                                    return ErrorFlag;
                                }
                            }else{
                                if((axelconfig[i].toLowerCase()=='x'))
                                {
                                    ErrorFlag=0; 
                                } else{
                                    ErrorFlag=1; 
                                    return ErrorFlag;
                                }
                            }
                        }
                    }else{
                        ErrorFlag=1; 
                        return ErrorFlag;
                    }
                   var numbers = /^\d+(?:\.\d\d?)?$/;
                   if (!threshold.match(numbers)) {
                        console.log("Please enter a valid number");
                        ErrorFlag=3;
                        return  ErrorFlag;
                    }else {
                        if(parseInt(threshold)>0)
                        {
                            console.log("Please enter a valid number2");
                           
                             ErrorFlag=0;
                             
                        }else{
                             ErrorFlag = 3;
                             return  ErrorFlag;
                        }
                      
                    }

                            if (!loadcap.match(numbers)) {
                                    console.log("Please enter a valid number");
                                    ErrorFlag=4;
                                    return  ErrorFlag;
                            }else {
                                    if(parseInt(loadcap)>0)
                                    {
                                        console.log("Please enter a valid number2");
                                    
                                        ErrorFlag=0;
                                        //return  ErrorFlag;
                                    }else{
                                        ErrorFlag=4;
                                        return  ErrorFlag;
                                    }
                                
                            }


                            return  ErrorFlag;

                        }

                        var bulktags = [];

                        function addBulkTags(data) {
                        console.log(data);
                        depotService.AddBulkDepot(data,function (result) {
                            animation.myFunc('vehiclespecbulk');
                        if (result.success === true) {
                            toastr.success(result.message);   
                            getAllCustomerDepotDetails();
                            getAllDropdowns();
                            $("#vehiclespecbulk").hide();
                            $scope.clearData();
                            // $scope.fileData= result.data;
                        }else{
                            $("#vehiclespecbulk").hide();
                            toastr.error(result);            
                            getAllDropdowns();    
                        }
                        })
                        }

                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                            $scope.clearData();
                        };

                },
                    backdrop: 'static',
                    scope: $scope,
                })
        };



        function AdddepotinBulk(data){
            //console.log(data +" AdddepotinBulk");
            //t_vehicleSpecificationMaster 
            animation.myFunc('vehiclespecbulk');
            vehicleService.AddVehicaleSpecBulk(data, function (result) {
               console.log(result.data +" :AdddepotinBulk "+ data);
                if (result.success === true) {
                    vm.custTagAssignDetails = {};
                    $scope.vccUser={};
                    $scope.customerDepotData= [];
                    getAllDropdowns();
                    $('#vehiclespecbulk').waitMe('hide');
                    toastr.success(result.message);
                    $scope.clearData();
                    vm.ModalInstance.close()
                } else {
                    $('#vehiclespecbulk').waitMe('hide');
                   // toastr.error(result.message); 
                    //console.log(result.data[0].errmsg);
                    if(result.data==null)
                    {
                   // var message=showMissingValue(result.data[0].message);
                        toastr.error(result.message);
                    }
                    else{
                        var message=showMissingValue(result.data.err[0].errmsg);  
                        getAllDropdowns();
                        toastr.error(message +" at line "+(result.data.line)+" blank records in file are ignored");
                    }
                    getAllDropdowns();
                    //vm.error = message;                    
                }
            })
        }

        var showMissingValue = function(errorMsg){
            //if(errorMsg!=undefined){
                var afterIndex = errorMsg.split('index:');
                var value = afterIndex[1];
                var field = value.split('_1');
                var afterDupErr = errorMsg.split('dup key: { :');
                var value2 = afterDupErr[1];
                var field2= value2.split(', :');
                //alert(field[0]);
               // alert(field2[0]);
                //duplicate key error
                var str1 = errorMsg;
                var str2 = "duplicate";
                if(str1.indexOf(str2) != -1){
                    console.log(str2 + " found");
                    if(field[0]==" vehicleModelId"){
                        return 'Vehicle Model is duplicate';
                    }                    
                    return field[0]+' is duplicate';
                }
                else{
                    return field[0]+field2[0];
                }
            //}
        }
    }
  
})();
  
  