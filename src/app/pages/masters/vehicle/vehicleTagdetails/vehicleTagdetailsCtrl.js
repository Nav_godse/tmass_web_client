(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('VehicleTagCtrl', VehicleTagCtrl);
  
    /** @ngInject */
    function VehicleTagCtrl($scope,vehicleService,$filter, editableOptions, editableThemes, toastr, $uibModal) {
        var vm = this; 
        vm.getAllVehicleGroupDetails = getAllVehicleGroupDetails;
        vm.addVehicleGroup = addVehicleGroup;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            vehicleGroupName:"",
            vehicleGroupDesc:""
       }
        initController();
        $scope.vehicles = [];
        var data = [];

        
        function initController() {
            //getAllVehicleGroupDetails();
            //$("#jqxwindowVehicleGroup").jqxWindow({height: 'auto', width: '70%',resizable: false,draggable: false, theme: 'dark', isModal: true, autoOpen: false });
        }

        function addVehicleGroup() {            
            vehicleService.AddVehicleGroup(vm.vehicleGroupName, vm.vehicleGroupDesc, function (result) {
                if (result.success === true) { 
                    vm.vehicleGroupName="";
                    vm.vehicleGroupDesc="";
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleGroupDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowVehicleGroup").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        } 

        function getAllVehicleGroupDetails() {            
            vehicleService.GetAllVehicleGroup(function (result) {
                if (result.success === true) { 
                    vm.userVehicleGroup=result.data;
                    for(var i=0;i<result.data.length;i++){
                        $scope.vehicles.push(result.data[i]); 
                        data.push(result.data[i]);   
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        }

        function deleteVehicleGroupDetails(_id) {            
            vehicleService.DeleteVehicleGroup(_id,function (result) {
                if (result.success === true) {  
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleGroupDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
        function updateVehicleGroupDetails() {            
            vehicleService.UpdateVehicleGroup(vm._id,vm.vehicleGroupName,vm.vehicleGroupDesc,function (result) {
                if (result.success === true) {
                    vm.vehicleGroupName="";
                    vm.vehicleGroupDesc=""; 
                    $scope.vehicles=[];
                    data=[];
                    getAllVehicleGroupDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowVehicleGroup").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };
           $scope.currRowData = function(rowform) {
              // Validation 
            $scope.isValidate= false;
            if(rowform==undefined || rowform.vehicleGroupName=='' || (rowform.vehicleGroupName==undefined)){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return;
            }else{
                $scope.isValidate= true;
            }
            
            if(rowform._id == "" || rowform._id == undefined){
               
                vm.vehicleGroupName = rowform.vehicleGroupName; 
                vm.vehicleGroupDesc = rowform.vehicleGroupDesc;
                addVehicleGroup();
                $scope.clearData();
                rowform ={};
                
            }else{
                vm._id = rowform._id; 
                vm.vehicleGroupName = rowform.vehicleGroupName; 
                vm.vehicleGroupDesc = rowform.vehicleGroupDesc;
                updateVehicleGroupDetails();
                $scope.clearData();
                rowform ={};
            }               
        }

        function myGrid(){ 
            var source =
            {
                localdata: data,
                datatype: "json",
                datafields:
                [
                    { name: '_id', type: 'string' },
                    { name: 'vehicleGroupName', type: 'string' },
                    { name: 'vehicleGroupDesc', type: 'string' }
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source);         
            $("#grid").jqxGrid(
            {
                width: '100%',
                theme: 'dark',
                source: dataAdapter,
                columnsresize: false,
                //selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagesize:5,
                autoheight: true,
                columnsheight: 60,
                rowsheight: 50,
                columns: [
                    {
                        text: 'Sr No',sortable: false, filterable: false, editable: false,
                        groupable: false, draggable: false, resizable: false,
                        datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                        cellsrenderer: function (row, column, value) {
                        return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                        }
                    },
                    { text: 'Name', datafield: 'vehicleGroupName',align: 'center',cellsalign: 'center', width: '33%' },
                    { text: 'Description', datafield: 'vehicleGroupDesc',align: 'center',cellsalign: 'center', width: '33%' },
                    {
                        text: 'Action', cellsAlign: 'center', align: "center",width: '22%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                        // render custom column.
                        return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  >Edit </button>  <button data-row="' + row + '" class="deleteButtons btn btn-danger" >Delete</button> </div>';
                        }
                    }
                ]
            });
            // Initlize Button Event
            buttonsEvents();
        }
      
        function buttonsEvents() {
            // For Edit Details
            var editbuttons = document.getElementsByClassName('editButtons');
            for (var i = 0; i < editbuttons.length; i+=1) {
                editbuttons[i].addEventListener('click', editDetails);
            }
              // For Delete Details
              var deletebuttons = document.getElementsByClassName('deleteButtons');
              for (var i = 0; i < deletebuttons.length; i+=1) {
                deletebuttons[i].addEventListener('click', deleteDetails);
              }
         }

        function editDetails() { 
            var id = this.getAttribute("data-row"); 
            var getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            { 
                var selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            $scope.vccUser = {
                _id : selectedRowData._id,
                vehicleGroupName:selectedRowData.vehicleGroupName,
                vehicleGroupDesc:selectedRowData.vehicleGroupDesc
            }
            $scope.openModal();
            //$("#jqxwindowVehicleGroup").jqxWindow('open'); 
        }  

        function deleteDetails(){
            getselectedrowindexes = $('#grid').jqxGrid('getselectedrowindexes');
            if (getselectedrowindexes.length > 0)
            {
                selectedRowData = $('#grid').jqxGrid('getrowdata', getselectedrowindexes[0]);
            } 
            // confirm dialog
            alertify.confirm("Are you sure, you want to delete this record?", function () {
                // user clicked "ok"
                deleteVehicleGroupDetails(selectedRowData._id);
            }, function() {
                // user clicked "cancel"
            })            
        }

        $("#grid").on("pagechanged filter sort scroll pagesizechanged", function (event) {
            buttonsEvents();
        });

        $scope.clearData = function(){ //window.location.reload();
            $("#grid").jqxGrid('clearselection');
            $scope.vccUser = {
                _id:"",
                vehicleGroupName:"",
                vehicleGroupDesc:""
            }
        }

        $scope.openModal = function () {
            $uibModal.open({
                //templateUrl: 'master.html',
                template: "<div ng-include src=\"'jqxwindowvehicleTagDetails.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.searchTerm = $scope.$parent.searchTerm2;
                $scope.done = function (data) {
                    //console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              scope: $scope,
              backdrop:'static'
            })
        };
    }
  
})();
  
  