(function () {
    'use strict';
  
    angular.module('TmassAdmin.pages.masters')
        .controller('RolesCtrl', RolesCtrl);
  
        
    /** @ngInject */
    function RolesCtrl($uibModal,$scope,roleService,$filter, editableOptions, editableThemes, toastr) {
        var vm = this;

        $scope.openRoleMasterModal = function () {
            $uibModal.open({
                template: "<div ng-include src=\"'roleMasterPopup.html'\"></div>",
                size:'lg',
                controller: function ($scope, $uibModalInstance) {
                $scope.save = function (data) {
                    console.log(data);
                    $scope.currRowData(data);
                    if($scope.isValidate==true){
                        $uibModalInstance.close();
                        $scope.clearData();
                    }                    
                };
              
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                    $scope.clearData();
                };
              },
              backdrop: 'static',
              scope: $scope,
            })
        };

        vm.getAllRoleDetails = getAllRoleDetails;
        vm.addRole = addRole;
        var getselectedrowindexes =null;
        var selectedRowData =null;
        $scope.vccUser = {
            _id:"",
            roleName:"",
            roleLevel:""
       }
        initController();
        $scope.roles = [];
       var data = [];

        function initController() {
            getAllRoleDetails();
        }

        function addRole() {            
            roleService.AddRole(vm.roleName, vm.roleLevel, function (result) {
                if (result.success === true) { 
                    vm.roleName="";
                    vm.roleLevel="";
                    $scope.roles=[];
                    data=[];
                    getAllRoleDetails();
                    toastr.success(result.message);
                    //$("#jqxwindowRoll").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function getAllRoleDetails() {            
            roleService.GetAllRoles(function (result) {
                if (result.success === true) { 
                    for(var i=0;i<result.roleMasters.length;i++){
                        $scope.roles.push(result.roleMasters[i]);
                        data.push(result.roleMasters[i]);    
                    }
                myGrid();
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function deleteRoleDetails(_id) {            
            roleService.DeleteRole(_id,function (result) {
                if (result.success === true) {  
                    $scope.roles=[];
                    data=[];
                    getAllRoleDetails();
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        function updateRoleDetails() {            
            roleService.UpdateRole(vm._id,vm.roleName,vm.roleLevel,function (result) {
                if (result.success === true) {
                    vm.roleName="";
                    vm.roleLevel=""; 
                    $scope.roles=[];
                    data=[];
                    getAllRoleDetails();
                    toastr.success(result.message);
                   // $("#jqxwindowRoll").jqxWindow('close');
                } else {
                    toastr.error(result.message); 
                    vm.error = result.message;                    
                }
            });
        };

        $scope.currRowData = function(rowform) {
            $scope.isValidate= false;
            if(rowform.roleName == "" || rowform.roleName == undefined || rowform.roleLevel == "" || rowform.roleLevel == undefined){
                toastr.error("Please Fill valid data in required fields");
                $scope.isValidate= false;
                return ;
            } else {
                $scope.isValidate= true;
                if(rowform._id == "" || rowform._id == undefined){
                    vm.roleName = rowform.roleName; 
                    vm.roleLevel = rowform.roleLevel;
                    addRole();
                    rowform ={};
                }else{
                    vm._id = rowform._id; 
                    vm.roleName = rowform.roleName; 
                    vm.roleLevel = rowform.roleLevel;
                    updateRoleDetails();
                    rowform ={};
                }  
            }
             



        }
        

       // For Loading the Grid Widgets
       function myGrid(){ 
       var source =
       {
           localdata: data,
           datatype: "json",
           datafields:
           [
               { name: '_id', type: 'string' },
               { name: 'roleName', type: 'string' },
               { name: 'roleLevel', type: 'number' }
           ]
       };

       var dataAdapter = new $.jqx.dataAdapter(source);         
       $("#rolegrid").jqxGrid(
       {
           width: '100%',
           theme: 'dark',
           source: dataAdapter,
           columnsresize: false,
           //selectionmode: 'checkbox',
           sortable: true,
           pageable: true,
           pagesize:5,
           autoheight: true,
           columnsheight: 60,
           rowsheight: 50,
           columns: [
               {
                   text: 'Sr No',sortable: false, filterable: false, editable: false,
                   groupable: false, draggable: false, resizable: false,
                   datafield: '', columntype: 'number',align: 'center',cellsalign: 'center', width: '12%' ,
                   cellsrenderer: function (row, column, value) {
                       return "<div  class='text-center' style='margin-top:15px'>" + (value + 1) + "</div>";
                   }
               },
               { text: 'Name', datafield: 'roleName',align: 'center',cellsalign: 'center', width: '30%' },
               { text: 'Level', datafield: 'roleLevel',align: 'center',cellsalign: 'center', width: '30%' },
               {
                   text: 'Action', cellsAlign: 'center', align: "center",width: '28%', editable: false, sortable: false, dataField: '_id', cellsRenderer: function (row, column, value) {
                   // render custom column.
                   return '<div class="text-center" style="padding:5px;"> <button style="margin-right:5px" data-row="' + row + '" class="editButtons btn btn-primary"  disabled>Edit </button>  <button style="margin-right:5px" data-row="' + row + '" class="deleteButtons btn btn-danger" disabled>Delete</button></div>';
                   }
               }
           ]
       });
       // Initlize Button Event
       buttonsEvents();
       }
       function buttonsEvents() 
       {
           // For Edit Details
           var editbuttons = document.getElementsByClassName('editButtons');
           for (var i = 0; i < editbuttons.length; i+=1) {
               editbuttons[i].addEventListener('click', editDetails);
           }
             // For Delete Details
             var deletebuttons = document.getElementsByClassName('deleteButtons');
             for (var i = 0; i < deletebuttons.length; i+=1) {
               deletebuttons[i].addEventListener('click', deleteDetails);
             }
               // For Disable Roll
               var deletebuttons = document.getElementsByClassName('disableButtons');
               for (var i = 0; i < deletebuttons.length; i+=1) {
                 deletebuttons[i].addEventListener('click', disableButton);
               }
       }
       // For Edit Details
       function editDetails() { 
           var id = this.getAttribute("data-row"); 
           var getselectedrowindexes = $('#rolegrid').jqxGrid('getselectedrowindexes');
           if (getselectedrowindexes.length > 0)
           { 
               var selectedRowData = $('#rolegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
           } 
           $scope.vccUser = {
               _id : selectedRowData._id,
               roleName:selectedRowData.roleName,
               roleLevel:selectedRowData.roleLevel
           }

           $scope.openRoleMasterModal();
       }  
       // For Delete Details
       function deleteDetails(){
           getselectedrowindexes = $('#rolegrid').jqxGrid('getselectedrowindexes');
           if (getselectedrowindexes.length > 0)
           {
               selectedRowData = $('#rolegrid').jqxGrid('getrowdata', getselectedrowindexes[0]);
           } 
           
            alertify.confirm("Are you sure, you want to delete?", function () {
                deleteRoleDetails(selectedRowData._id);
            }, function() {
            });
       }
         // For Disable Roll
         function disableButton(){
             alert("Are you sure want to disable roll ?");
        }
   
       $("#rolegrid").on("pagechanged", function (event) { 
           buttonsEvents();
       });
       $scope.clearData = function(){ 
           $scope.vccUser = {
               _id:"",
               roleName:"",
               roleLevel:""
           }
       }
    }
  
})();
  
  